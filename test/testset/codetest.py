#! /bin/usr/env python
# -*- coding: utf-8 -*-
import os
import Testset

nodes = ["r1i0n4"]

def write_jobscript(filename, inputfile, jobname, node ):
    jobscript = open(filename,"w")
    jobscript.write('#!/bin/bash\n\n')
    jobscript.write('#PBS -N '+jobname+"\n")
    jobscript.write('#PBS -l nodes='+node+':ppn=16\n')
    jobscript.write('#PBS -l walltime=500:00:00\n')

    jobscript.write('\n')
    jobscript.write('date\n')
    jobscript.write('\n')
    
    jobscript.write('export INPUT='+inputfile+'\n')
    jobscript.write('export OUTPUT='+jobname+'.log\n')
    jobscript.write('export EXEC=../../ace\n\n')
    
    jobscript.write('module purge\n')
    jobscript.write('module load intel\n')
    jobscript.write('module load intel_mkl\n')
    jobscript.write('module load openmpi-1.8.3-intel\n')
    jobscript.write('module load trilinos-11.12.1_intel_openmpi-1.8.3\n')
    jobscript.write('module load libxc-2.2.1_intel_impi\n')
    
    jobscript.write('\n')
    jobscript.write('cd $PBS_O_WORKDIR\n')
    jobscript.write('\n')

    jobscript.write('echo `cat $PBS_NODEFILE`\n')
    jobscript.write('cat $PBS_NODEFILE\n')
    jobscript.write('NPROCS=`wc -l < $PBS_NODEFILE`')
    jobscript.write('\n')

    jobscript.write('mpirun -machinefile $PBS_NODEFILE -np $NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT\n')
    jobscript.write('\n')
    jobscript.write('date')
    
    jobscript.close()

def run(molecules_list):
    count = 0
    moleculess = [molecules_list["Ni"], molecules_list["Fe"]]
    #moleculess = [molecules_list["C"], molecules_list["Al"], molecules_list["Si"]]
    moleculess = [molecules_list["Ni"]]
    for molecules in moleculess:
        #for pptyp in ["HGH"]:
        #for pptyp in ["PAW", "UPF"]:
        for pptyp in ["UPF"]:
            molecule = molecules[pptyp]
            label = molecule.label
            molecule.write_xyz("DATA/"+label+".xyz")
            molecule.write_input(label+"-"+pptyp+".inp", "DATA/"+label+".xyz")
            write_jobscript("job-"+label+"-"+pptyp+".x", label+"-"+pptyp+".inp", label+"-"+pptyp, nodes[count%len(nodes)])
            os.system("qsub job-"+label+"-"+pptyp+".x")
            count += 1

if __name__ == "__main__":
    run(Testset.molecules)
