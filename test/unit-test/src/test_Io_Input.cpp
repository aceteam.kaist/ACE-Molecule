#include <string>
#include "../../../source/Io/Io_Input.hpp"
#include "gtest/gtest.h"

#include <iostream>

using std::string;
using Teuchos::ParameterList;
using Teuchos::Array;

TEST(TEST_Io_Input, read_input){
    Array< Teuchos::RCP<ParameterList> > params;
    Io_Input io = Io_Input();
    io.read_input("test.inp", params);

    ParameterList basic("BasicInformation");
    ParameterList guess("Guess");
    ParameterList scf("Scf");

    basic.set<double>("Scaling", 0.35);
    basic.set<string>("Grid", "Atoms");
    basic.set< Array<string> >("Radius", Array<string>(1, "5.5"));
    basic.set<string>("InpName", "test.inp");

    guess.set<int>("InitialGuess", 3);
    Array<string> init_fnames(2);
    init_fnames[0] = "1.cube"; init_fnames[1] = "2.cube";
    guess.set< Array<string> >("InitialFilenames", init_fnames);

    scf.sublist("ExchangeCorrelation").set<double>("PDcutoff", -1.0);
    Array<string> gaus_pot(2);
    gaus_pot[0] = "0.27 0.075"; gaus_pot[1] = "0.18 0.006";
    scf.sublist("ExchangeCorrelation").set< Array<string> >("GaussianPotential", gaus_pot);
    scf.sublist("ExchangeCorrelation").set< Array<string> >("XCFunctional", Array<string>(1, "10478"));
    scf.sublist("Diagonalize").set<double>("Tolerance", 1.0E-5);

    EXPECT_EQ(*params[0], basic);
    EXPECT_EQ(*params[1], guess);
    EXPECT_EQ(*params[2], scf);
}

int main(int argc, char **argv){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
