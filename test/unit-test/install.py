import os
os.system('cmake \
-D TRILINOS_PATH:PATH=/appl/trilinos/libs/12.14.1_openmp_release_intel_openmpi-1.8.3 \
-D CMAKE_CXX_FLAGS:STRING="-std=c++11 -fopenmp -liomp5 -lpthread" \
-D MKL_PATH:PATH=/appl/intel/composerxe/mkl/ \
-D CMAKE_CXX_COMPILER:FILEPATH=/appl/mpi/gnu/openmpi-1.8.3/bin/mpicxx \
.')
