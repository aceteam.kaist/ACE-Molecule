#/bin/env python
import sys
sys.path.append('../../utility/')
import ACEInput
import os
import itertools
from collections import OrderedDict

################# ALL ##################
_SYSTEM_ = "messi"
exe = "../../ace"
mpi_and_omps = None
test_run = False
any_node = None # SARASWATI SHOULD USE THIS
########################################
################ MESSI #################
r1i0n_nodes = []
intel_nodes = [10]
amd_nodes = []
########################################

test_set = [
    {
        "BasicInformation.Polarize": 0, #rPBE + rTDDFT
        "Guess.InitialFilePath": "pbe.gs/cube/uPBE",
        "Guess.Info": "pbe.gs/ACE-rPBE.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XFunctional": 101,
        "TDDFT.OrbitalInfo.ExchangeCorrelation.CFunctional": 130
    },
    {
        "BasicInformation.Polarize": 1, #uPBE + uTDDFT
        "Guess.InitialFilePath": "pbe.gs/cube/uPBE",
        "Guess.Info": "pbe.gs/ACE-uPBE.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XFunctional": 101,
        "TDDFT.OrbitalInfo.ExchangeCorrelation.CFunctional": 130
    },
    {
        "BasicInformation.Polarize": 1, # charged molecule (double)
        "BasicInformation.SpinMultiplicity": 2.0,
        "BasicInformation.NumElectrons": 11,
        "Guess.InitialFilePath": "pbe.gs/cube/pPBE",
        "Guess.Info": "pbe.gs/ACE-pPBE.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XFunctional": 101,
        "TDDFT.OrbitalInfo.ExchangeCorrelation.CFunctional": 130
    },
    {
        "BasicInformation.Polarize": 0, #rPBE + rTDDFT
        "Guess.InitialFilePath": "pbe.gs/cube/uKLI",
        "Guess.Info": "pbe.gs/ACE-rKLI.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XFunctional": -12
    },
    {
        "BasicInformation.Polarize": 1, #uPBE + uTDDFT
        "Guess.InitialFilePath": "pbe.gs/cube/uKLI",
        "Guess.Info": "pbe.gs/ACE-uKLI.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XFunctional": -12
    },
    {
        "BasicInformation.Polarize": 1, # charged molecule (double)
        "BasicInformation.SpinMultiplicity": 2.0,
        "BasicInformation.NumElectrons": 11,
        "Guess.InitialFilePath": "pbe.gs/cube/pKLI",
        "Guess.Info": "pbe.gs/ACE-pKLI.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XFunctional": -12
    },
    {
        "BasicInformation.Polarize": 0, #rB3LYP + rTDDFT
        "Guess.InitialFilePath": "pbe.gs/cube/uB3LYP",
        "Guess.Info": "pbe.gs/ACE-rB3LYP.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XCFunctional": "HYB_GGA_XC_B3LYP"
    },
    {
        "BasicInformation.Polarize": 1, #uB3LYP + uTDDFT
        "Guess.InitialFilePath": "pbe.gs/cube/uB3LYP",
        "Guess.Info": "pbe.gs/ACE-uB3LYP.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XCFunctional": "HYB_GGA_XC_B3LYP"
    },
    {
        "BasicInformation.Polarize": 1, # charged molecule (double)
        "BasicInformation.SpinMultiplicity": 2.0,
        "BasicInformation.NumElectrons": 11,
        "Guess.InitialFilePath": "pbe.gs/cube/pB3LYP",
        "Guess.Info": "pbe.gs/ACE-pB3LYP.reflog",
        "TDDFT.OrbitalInfo.ExchangeCorrelation.XCFunctional": "HYB_GGA_XC_B3LYP"
    }
]

calc_args = [
    {
        "TDDFT.ExchangeCorrelation.XFunctional": 101,
        "TDDFT.ExchangeCorrelation.CFunctional": 130,
        "TDDFT._TDClass": "C",
    },
    {
        "TDDFT.ExchangeCorrelation.XFunctional": 101,
        "TDDFT.ExchangeCorrelation.CFunctional": 130,
        "TDDFT._TDClass": "ABBA",
    },
    {
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.ExchangeCorrelation.XCFunctional": 10478,
        "TDDFT.ExchangeCorrelation.GaussianPotential": ["\"0.27 0.075\"", "\"0.18 0.006\""],
        #"TDDFT.xc_kernel_without_EXX.Exchange_Correlation.XCFunctional": 10478,
        #"TDDFT.xc_kernel_without_EXX.Exchange_Correlation.AutoEXXPortion": 0,
        #"TDDFT.xc_kernel_without_EXX.Exchange_Correlation.Gaussian_potential": ["\"0.27 0.075\"", "\"0.18 0.006\""],
    },
    {
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.ExchangeCorrelation.XCFunctional": "HYB_GGA_XC_B3LYP"
        #"TDDFT.xc_kernel_without_EXX.Exchange_Correlation.XFunctional": "\"101 0.5\"",
        #"TDDFT.xc_kernel_without_EXX.Exchange_Correlation.CFunctional": 130,
    },
    {
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.DeltaCorrection": 2,
        "TDDFT.ExchangeCorrelation.XCFunctional": "HYB_GGA_XC_B3LYP"
        #"TDDFT.xc_kernel_without_EXX.Exchange_Correlation.XFunctional": "\"101 0.5\"",
        #"TDDFT.xc_kernel_without_EXX.Exchange_Correlation.CFunctional": 130,
    },
    {
        "TDDFT.TheoryLevel": "TammDancoff",
        "TDDFT.ExchangeCorrelation.XFunctional": 101,
        "TDDFT.ExchangeCorrelation.CFunctional": 130,
    },
    {
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.ExchangeCorrelation.XFunctional": -12,
    },
    {
        "TDDFT.ExchangeKernel": "PGG",
        "TDDFT.ExchangeCorrelation.XFunctional": -12,
    },
    {
        "TDDFT.TheoryLevel": "TammDancoff",
        "TDDFT.ExchangeKernel": "HF-EXX",
        "TDDFT.ExchangeCorrelation.XCFunctional": "HYB_GGA_XC_B3LYP"
    },
    #{
    #    "BasicInformation.Polarize": 1,
    #    "Guess.Info": "pbe.gs/GS.openshell.eigvals",
    #    "TDDFT.ExchangeCorrelation.XFunctional": 101,
    #    "TDDFT.ExchangeCorrelation.CFunctional": 130,
    #    "TDDFT._TDClass": "C",
    #},
]
test_nick = ["PBE_r","PBE_u","PBE_p","KLI_r","KLI_u","KLI_p","B3LYP_r","B3LYP_u","B3LYP_p"]
args_nick = [".C", ".ABBA", ".LC", ".HYB", ".HYB_diag", ".TDA", ".HFkernel", ".PGGkernel", ".TDAHYB"]

def initialize_nodes():
    nodes = []
    if any_node:
        nodes = ["1"]
    else:
        for node in r1i0n_nodes:
            nodes.append("r1i0n"+str(node))
        for node in amd_nodes:
            nodes.append("amd"+str(node))
        for node in intel_nodes:
            nodes.append("intel"+str(node))
    return nodes

def initialize_node_procs():
    r1i0n_nodes = range(12)
    intel_nodes = range(18)
    amd_nodes = [0]
    node_procs = {}
    for node in r1i0n_nodes:
        node_procs["r1i0n"+str(node)] = "24"
    for node in amd_nodes:
        node_procs["amd"+str(node)] = "16"
    for node in intel_nodes:
        if int(node) <= 3 or int(node) >= 11:
            node_procs["intel"+str(node)] = "16"
        else:
            node_procs["intel"+str(node)] = "20"
    if any_node:
        node_procs["1"] = str(int(any_node))
    return node_procs

nodes = initialize_nodes()
node_procs = initialize_node_procs()

env_args = {
    "messi": """module purge
module load intel
module load intel_mkl
module load openmpi-1.8.3-intel
module load trilinos-12.8.1_openmp_release_intel_openmpi-1.8.3_jaechang
module load gsl-2.4
module load libxc-4.3.4
module load pcmsolver-1.2.3""",
}

def main():
    if len(args_nick) == 0:
        for new_arg in calc_args:
            tmp_nick = []
            for key, val in new_arg.iteritems():
                #tmp_nick.append(key.split(".")[-1]+"."+str(val))
                tmp_nick.append(str(val))
            nick += ".".join(tmp_nick)
            args_nick.append(nick)
    new_args_nick = []
    new_calc_args = []
    
    for kinds in range(len(test_nick)):
        for calculations in range(len(args_nick)):
            new_args_nick.append(test_nick[kinds]+args_nick[calculations])
            new_item = dict(test_set[kinds])
            new_item.update(calc_args[calculations])
            new_calc_args.append(new_item)
    inputs = ACEInput.create_inputs("ACE.template", OrderedDict(zip(new_args_nick, new_calc_args)))

    i = 0
    for name in inputs.keys():
        node = nodes[i%len(nodes)]
        with open("jobscript_"+name+".x", 'w') as fpt:
            #ACEInput.write_jobscript(fpt, name+".inp", name, node)
            if mpi_and_omps is None:
                write_jobscript(fpt, inputs[name], name, node, node_procs[node], exe)
            else:
                write_jobscript(fpt, inputs[name], name, node, node_procs[node], exe, mpi_and_omps[i])
        if not test_run:
            print(i, name+":\t", end='')
            sys.stdout.flush()
            os.system("qsub jobscript_"+name+".x")
        i += 1


def write_jobscript(fpt, inp_name, job_name, node_name, node_procs, exe_name = "./ace", mpi_and_omp = None):
    if mpi_and_omp is not None:
        if int(node_procs) != int(mpi_and_omp[0]) * int(mpi_and_omp[1]):
            print("Assigned "+node_procs+" cores to node "+node_name+" but MPI NPROCS ("+str(mpi_and_omp[0])+") * OMP_NUM_THREADS ("+str(mpi_and_omp[1])+") are different!")
    fpt.write("#!/bin/bash\n\n")
    fpt.write("#PBS -N "+job_name+"\n")
    fpt.write('#PBS -l nodes='+node_name+':ppn='+node_procs+'\n')
    fpt.write('#PBS -l walltime=500:00:00\n')
    fpt.write('\n')
    fpt.write('date\n')
    fpt.write('\n')
    fpt.write('export INPUT='+inp_name+'\n')
    fpt.write('export OUTPUT='+job_name+'.log\n')
    fpt.write('export EXEC='+exe_name+'\n')
    if mpi_and_omp is not None:
        fpt.write('export MPI_NPROCS='+str(mpi_and_omp[0])+'\n')
        fpt.write('export OMP_NUM_THREADS='+str(mpi_and_omp[1])+'\n')
    fpt.write('\n')
    fpt.write(env_args["messi"])
    fpt.write('\n\n')
    fpt.write('cd $PBS_O_WORKDIR\n')
    fpt.write('\n')
    fpt.write('echo `cat $PBS_NODEFILE`\n')
    fpt.write('cat $PBS_NODEFILE\n')
    fpt.write('NPROCS=`wc -l < $PBS_NODEFILE`')
    fpt.write('\n')
    if mpi_and_omp is None:
        fpt.write('mpirun -machinefile $PBS_NODEFILE -np $NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT\n')
    else:
        fpt.write('mpirun -machinefile $PBS_NODEFILE -np $MPI_NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT\n')
    fpt.write('\n')
    fpt.write('date')

if __name__ == "__main__":
    main()
