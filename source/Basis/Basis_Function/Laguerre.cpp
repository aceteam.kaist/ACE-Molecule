#include "../Basis_Function/Laguerre.hpp"

#include <cmath>
#include "Epetra_BLAS.h"

using std::abs;

Laguerre::Laguerre(std::array<int,3>_point, std::array<double,3> _scaling){
    this->points[0] = _point[0];
    this->points[1] = _point[1];
    this->points[2] = _point[2];

    size = points[0]*points[1]*points[2];
    compute_grid_and_weight_factors();

    this->scaling[0] = _scaling[0];
    this->scaling[1] = _scaling[1];
    this->scaling[2] = _scaling[2];

    compute_scaled_grid();

}

Laguerre::~Laguerre(){
    for(int i = 0; i < 3; ++i){
        delete[] this -> grid[i];
        delete[] this -> scaled_grid[i];
        delete[] this -> weight_factor[i];
    }
    delete[] this -> grid;
    delete[] this -> scaled_grid;
    delete[] this -> weight_factor;
}

std::array<int,3> Laguerre::get_points(){
    return points;
}
double** Laguerre::get_grid(){
    return grid;
}
std::array<double,3> Laguerre::get_scaling(){
        return scaling;
}

void Laguerre::compute_grid_and_weight_factors(){

    double **grid = new double* [3];
    double **weight_factor = new double* [3];
    for(int i=0;i<3;i++){
        grid[i] = new double[points[i]];
        weight_factor[i] = new double[points[i]];
    }
    for(int i=0;i<3;i++){
        gaulag(grid[i],weight_factor[i],points[i]);
    }
    this->grid = grid;
    this->weight_factor = weight_factor;
    return;


}
void Laguerre::compute_scaled_grid(){
    double ** scaled_grid = new double* [3];
    Epetra_BLAS blas;

    for (int i=0;i<3;i++){
        scaled_grid[i] = new double[points[i]];
    }

    for (int i=0;i<3;i++){
        blas.COPY(points[i],grid[i],scaled_grid[i]);
        blas.SCAL(points[i],scaling[i],scaled_grid[i]);
    }
    this->scaled_grid =scaled_grid;

}

double Laguerre::get_weight_factor(int j, int axis){
    return weight_factor[axis][j];
}

double Laguerre::get_weight_function(double point, int axis){
    //gauss-laguerre quadrature : w(x) = x^(alpha)exp(-x), alpha = 0
    double x = point / scaling[axis];
    return exp(-x);
}

double** Laguerre::get_scaled_grid(){
    return scaled_grid;
}

void Laguerre::gaulag(double *x, double *w, int n){
/*
 *void gaulag(float x[], float w[], int n, float alf), alf = 0
 *Given alf, the parameter α of the Laguerre polynomials, this routine returns arrays x[1..n]
 *and w[1..n] containing the abscissas and weights of the n-point Gauss-Laguerre quadrature
 *formula. The smallest abscissa is returned in x[1], the largest in x[n].
 */
    // alf = 0 because it is not a general gauss-laguerre quadrature
    int i,its,j;
    double ai;
    double p1,p2,p3,pp,z,z1;
    for (i=0;i<n;i++) {
        if (i == 0) {
            z=3.0/(1.0+2.4*n);
        } else if (i == 1) {
            z += 15.0/(1.0+2.5*n);
        } else {
            ai = i-1;
            z += ((1.0+2.55*ai)/(1.9*ai))*(z-x[i-2]);
        }
        while(true){
        //for (its=1;its <= 10;its++) { //max iteration number = 10
            //cout <<"Laguerre:: \t" <<endl;
            p1=1.0;
            p2=0.0;
            for (j=1;j<=n;j++) {
                p3=p2;
                p2=p1;
                p1=((2*j-1-z)*p2-(j-1)*p3)/j;
            }
            /*p1 is now the desired Laguerre polynomial. We next compute pp, its derivative,
             * by a standard relation involving also p2, the polynomial of one lower order.
             */
            pp=(n*p1-n*p2)/z;
            z1=z;
            z=z1-p1/pp;//Newton’s formula.
            //if (fabs(z-z1) <= 3.0e-14) break;  //EPS = 3.0e-14
            if (fabs(z-z1) <= 1.0e-10) break;  //EPS = 3.0e-14
        }    

//        if (its > 10) cout << "too many iterations in gaulag" << endl;
        x[i]=z;
        w[i] = -exp(gammln(n)-gammln((double)n))/(pp*n*p2);
    }
}



double Laguerre::gammln(double xx){
/* Returns the value ln[Γ(xx)] for xx > 0.
 * Internal arithmetic will be done in double precision, a nicety that you can omit if five-figure accuracy is good enough.
 */
    double x,y,tmp,ser;
    static double cof[6]={76.18009172947146,-86.50532032941677,24.01409824083091,-1.231739572450155,0.1208650973866179e-2,-0.5395239384953e-5};
    int j;
    y=x=xx;
    tmp=x+5.5;
    tmp -= (x+0.5)*log(tmp);
    ser=1.000000000190015;
    for (j=0;j<=5;j++) ser += cof[j]/++y;
    return -tmp+log(2.5066282746310005*ser/x);
}

int Laguerre::set_scaling(std::array<double,3> scaling){
    this->scaling=scaling;
    compute_scaled_grid();
    return 0;
}


/*
int main(){
    int point[3] = {32,1,1};
    double scaling[3] ={1.0,1.0,1.0};
    Laguerre* test = new Laguerre(point,scaling);
    cout.precision(12);
    cout << "i\txi\twi" << endl;
    for(int i=0;i<32;i++){
        cout << i << '\t' << test->get_scaled_grid()[0][i] << '\t' << test->get_weight_factor(i,0) <<endl;
    }
    return 0;
}
*/
