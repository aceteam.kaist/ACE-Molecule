#include "../Basis_Function/Basis_Function.hpp"

#include <limits>
#include <cmath>
//#include "Epetra_BLAS.h"
#include <cassert>

#include "../../Utility/Verbose.hpp"

using std::string;
using std::ostream;

/*
class Basis_Function{
    public:
           int* get_points();
        int get_size();
           double* get_scaling();
        double** get_grid();
        double** get_scaled_grid();
        int set_scaling(double* scaling);        

        double compute_distance(int i_x,int i_y, int i_z);

        virtual double compute_kinetic()=0;
        virtual double compute_1d_basis()=0;
        virtual double compute_second_der()=0;

    protected:
        int* points;
        double* scaling;
        double** grid;
        double** scaled_grid;
        int size;

        void compute_scaled_grid();    
           virtual void compute_grid()=0;
};
*/

double Basis_Function::compute_distance(int i_x,int i_y, int i_z){
    if( i_x < 0 or i_x >= points[0] ) return std::numeric_limits<double>::infinity();
    if( i_y < 0 or i_y >= points[1] ) return std::numeric_limits<double>::infinity();
    if( i_z < 0 or i_z >= points[2] ) return std::numeric_limits<double>::infinity();
    return sqrt((scaled_grid[0][i_x])*(scaled_grid[0][i_x])+(scaled_grid[1][i_y])*(scaled_grid[1][i_y])+(scaled_grid[2][i_z])*(scaled_grid[2][i_z]));

}
double Basis_Function::compute_distance(int i_x,int i_y, int i_z, int j_x, int j_y, int j_z){
    assert(this -> get_periodicity() == 0);// Not implemented.
    if( i_x < 0 or i_x >= points[0] ) return std::numeric_limits<double>::signaling_NaN();
    if( i_y < 0 or i_y >= points[1] ) return std::numeric_limits<double>::signaling_NaN();
    if( i_z < 0 or i_z >= points[2] ) return std::numeric_limits<double>::signaling_NaN();
    if( j_x < 0 or j_x >= points[0] ) return std::numeric_limits<double>::signaling_NaN();
    if( j_y < 0 or j_y >= points[1] ) return std::numeric_limits<double>::signaling_NaN();
    if( j_z < 0 or j_z >= points[2] ) return std::numeric_limits<double>::signaling_NaN();
    return sqrt( (scaled_grid[0][i_x]-scaled_grid[0][j_x])*(scaled_grid[0][i_x]-scaled_grid[0][j_x])+(scaled_grid[1][i_y]-scaled_grid[1][j_y])*(scaled_grid[1][i_y]-scaled_grid[1][j_y])+(scaled_grid[2][i_z]-scaled_grid[2][j_z])*(scaled_grid[2][i_z]-scaled_grid[2][j_z]));
}
string Basis_Function::get_type(){
    return basis_type;
}
std::array<int,3> Basis_Function::get_points(){
        return points;
}
int Basis_Function::get_size(){
        return size;
}
std::array<double,3> Basis_Function::get_scaling(){
        return scaling;
}
double** Basis_Function::get_grid(){
    return grid;
}
const double** Basis_Function::get_scaled_grid(){
    return const_cast<const double**>( scaled_grid) ;
}
// (PBC)
int Basis_Function::get_periodicity() const{
    return periodicity;
}

void Basis_Function::compute_scaled_grid(){
    if (scaled_grid==NULL){
        scaled_grid = new double* [3];
    }
    else{
        #pragma unroll
        for (int i=0;i<3;i++) delete[] scaled_grid[i];
    }       

    for (int i=0;i<3;i++){
        scaled_grid[i] = new double[points[i]];
    }

    for(int i =0; i<3; i++){
        for(int j=0;j<points[i];j++) scaled_grid[i][j] = grid[i][j]*scaling[i];
    }
    return;
}

bool Basis_Function::operator==(Basis_Function* basis){

    if ( (this->get_type()).compare(basis->get_type() )!=0 ){
        //Verbose::single()<< "type is diff" <<std::endl;
        return false;
    }
    if(this->get_scaling()[0] != basis->get_scaling()[0]){
        return false;
    }
    if(this->get_scaling()[1] != basis->get_scaling()[1]){
        return false;
    }
    if(this->get_scaling()[2] != basis->get_scaling()[2]){
        return false;
    }


    if (this->get_points()[0]!=basis->get_points()[0]){
        return false;
    }
    if (this->get_points()[1]!=basis->get_points()[1]){
        return false;
    }
    if (this->get_points()[2]!=basis->get_points()[2]){
        return false;
    }

    return true; 
}


ostream &operator <<(ostream &c, const Basis_Function &basis){
    // ??!? I think this should be c << or Verbose::all() <<. (KSW)
    Verbose::single() <<"========== Basis ===========" <<std::endl;
    Verbose::single() << "This basis is " << basis.basis_type << " type" << std::endl;
    Verbose::single() << "Grid is " << basis.points[0]<<","<< basis.points[1]<<","<< basis.points[2]<< std::endl;
    Verbose::single() << "x scaling factor is " << basis.scaling[0] <<std::endl;
    Verbose::single() << "y scaling factor is " << basis.scaling[1] <<std::endl;
    Verbose::single()  << "z scaling factor is " << basis.scaling[2] <<std::endl; 
    Verbose::single()  << "============================" <<std::endl;
    return c;
}
ostream &operator <<(ostream &c, const Basis_Function* pbasis){
    Verbose::all() << *pbasis ;
    return c;
}



/*
Epetra_Comm* Basis_Function::get_comm(){
    return Comm;
}
*/
