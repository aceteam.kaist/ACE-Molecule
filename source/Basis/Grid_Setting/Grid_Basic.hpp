#pragma once 
#include "Grid_Setting.hpp"

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Basic: public Grid_Setting{
    public:
        /**
         * @brief Decompose 1-D form index into the indices of x-, y-, z-coordinates.
         * @param [in] index 1-D form index.
         * @param [in] points Number of points of each axis.
         * @param [out] i_x Pointer to x-axis index.
         * @param [out] i_y Pointer to y-axis index.
         * @param [out] i_z Pointer to z-axis index.
         */
        void decompose(int index, std::array<int,3> points, int& i_x, int& i_y, int& i_z);
        /**
         * @brief Combine x-, y-, z-direction indices into 1-D form index.
         * @param [in] i_x x-axis index.
         * @param [in] i_y y-axis index.
         * @param [in] i_z z-axis index.
         * @param [out] points 1-D form index.
         */
        int  combine(int i_x,int i_y,int i_z, std::array<int,3> points);
        /**
         * @brief Convert basic grid index to basic grid index.
         * @detail Return basic grid index itself without any processing.
         * @param [in] basic_grid_index Basic grid index.
         */
        int from_basic(int basic_grid_index);
        /**
         * @brief Convert basic grid index to basic grid index.
         * @detail Return basic grid index itself without any processing.
         * @param [in] int current_grid_index Basic grid index.
         */
        int to_basic(int current_grid_index);

        Grid_Basic(std::array<int,3> points);
        ~Grid_Basic();
};
