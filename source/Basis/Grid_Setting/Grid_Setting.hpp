#pragma once 
//#include <ostream>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#ifdef USE_CUDA
#include "cuda_runtime_api.h"
#include "device_launch_parameters.h"
#endif 

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Setting {
    friend std::ostream &operator <<(std::ostream &c, const Grid_Setting &grid_setting);
    friend std::ostream &operator <<(std::ostream &c, const Grid_Setting* pgrid_setting);

    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Grid_Setting(){};
        /**
         * @brief See Basis::decompose.
         * @details Implementation of Basis::decompose. 
         * @todo Probably change to protected / friend of Basis class?
         * @param [in] index 1-D form index.
         * @param [in] points Number of points of each axis.
         * @param [out] i_x Pointer to x-axis index.
         * @param [out] i_y Pointer to y-axis index.
         * @param [out] i_z Pointer to z-axis index.
         **/
        virtual void decompose(int index, std::array<int,3> points, int& i_x, int& i_y, int& i_z)=0;
        //virtual int* decompose(int index, int* points)=0;
        /**
         * @brief See Basis::combine.
         * @details Implementation of Basis::combine. 
         * @todo Probably change to protected / friend of Basis class?
         * @param [in] i_x x-axis index.
         * @param [in] i_y y-axis index.
         * @param [in] i_z z-axis index.
         * @param [out] points 1-D form index.
         **/
        virtual int  combine(int i_x,int i_y,int i_z, std::array<int,3> points)=0;
        bool operator== (Grid_Setting* grid_setting) const;

        /**
         * @brief Change index from Grid_Basic index to own index.
         * @param [in] basic_grid_index basic grid index.
         **/
        virtual int from_basic(int basic_grid_index)=0;
        /**
         * @brief Change index to Grid_Basic index from own index.
         * @param [in] current_grid_index own index.
         **/
        virtual int to_basic(int current_grid_index)=0;

        std::string get_type() const;
        int get_size() const;
        std::array<int,3> get_points() const;
        std::vector<int*> get_boundary();

    protected:
        std::string grid_type;
        int size;
        std::array<int,3> points;
        std::vector<int*> boundary;
};
