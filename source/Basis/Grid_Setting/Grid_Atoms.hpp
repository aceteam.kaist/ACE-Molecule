#pragma once 
#include "Teuchos_RCP.hpp"
#include "Grid_Setting.hpp"
#include "Grid_Basic.hpp"
#include "../Basis_Function/Basis_Function.hpp"
#include "../../Io/Atoms.hpp"
#include "../../Io/Periodic_table.hpp"
#include "Epetra_Comm.h"
#include "Epetra_Map.h"
#include <vector>
#include "../../Utility/Time_Measure.hpp"

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Atoms: public Grid_Setting{
    public:
        /**
         * @brief Decompose 1-D form index into the indices of x-, y-, z-coordinates.
         * @param [in] index 1-D form index.
         * @param [in] points Number of points of each axis.
         * @param [out] i_x Pointer to x-axis index.
         * @param [out] i_y Pointer to y-axis index.
         * @param [out] i_z Pointer to z-axis index.
         */
        void decompose(int index, std::array<int,3> points, int& i_x, int& i_y, int& i_z);
        /**
         * @brief Combine x-, y-, z-direction indices into 1-D form index.
         * @param [in] i_x x-axis index.
         * @param [in] i_y y-axis index.
         * @param [in] i_z z-axis index.
         * @param [out] points 1-D form index.
         */
        int  combine(int i_x,int i_y,int i_z, std::array<int,3> points);
        /**
         * @brief Convert basic grid index to atomic grid index.
         * @param [in] basic_grid_index Basic grid index.
         */
        int from_basic(int basic_grid_index);
        /**
         * @brief Convert atomic grid index to basic grid index.
         * @param [in] current_grid_index Atomic grid index.
         */
        int to_basic(int current_grid_index);

        std::vector<int> get_match_atoms_to_basic();
        std::vector<int> get_match_basic_to_atoms();
        ///This is constructor that specifies radius of total grid.
        //Grid_Atoms(int* points, Basis_Function* basis,Atoms* atoms, double radius);
        Grid_Atoms(std::array<int,3> points, Teuchos::RCP<Basis_Function> basis, Teuchos::RCP<Atoms> atoms, double factor, bool is_relative);
        ///This is constructor that specifies radius of total grid.
        Grid_Atoms(std::array<int,3> points, Teuchos::RCP<Basis_Function> basis, Teuchos::RCP<Atoms> atoms , std::vector<double> radius);
        //Grid_Atoms(const int* points, Teuchos::RCP<Basis_Function> basis,Atoms* atoms, std::vector<double> radius);
        ~Grid_Atoms();

    protected:
        Teuchos::RCP<Atoms> atoms;
        Teuchos::RCP<Grid_Setting> grid_basic;
        std::vector<int> match_atoms_to_basic;
        std::vector<int> match_basic_to_atoms;
        Teuchos::RCP<Time_Measure> timer;
};
