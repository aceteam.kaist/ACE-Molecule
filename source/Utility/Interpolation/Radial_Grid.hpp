#pragma once
#include <vector>
#include "Linear_Interpolation.hpp"
//#include <limits>
//std::numeric_limits<double>::max()
//template<typename T>

/**
  @breif This class contains the values on radial grid
  
*/
class RadialGrid {
/*
    friend bool operator==(const RadialGrid& grid1, const RadialGrid& grid2 ){
        
        if ( !grid1.compatible(grid2) ){
            return false;
        }

        const int num_points = grid1.mesh.size();

        for (int i =0; i< num_points; i++ ){
            if( fabs( grid1.mesh[i] -  grid2.mesh[i]) > tolerance_mesh){
                return false;
            }
        }

        for (int i =0; i< num_points; i++ ){
            if( fabs( grid1.value[i] -  grid2.value[i]) > tolerance_val){
                return false;
            }
        }
        return true;

    };
    friend RadialGrid operator+(const RadialGrid& grid1, const RadialGrid& grid2){
        if ( !grid1.compatible(grid2) ){
            exit(-1);
        }
        int size = grid1.mesh.size();
        std::vector<double> result_value( grid2.value.begin(), grid2.value.end() );

        for (int i=0; i< size; i++){
            result_value[i]+=grid1.value[i];
        }

        return RadialGrid(result_value, grid1.mesh);        
    };
*/
    public:
        /**
          This is constructor for RadialGrid class
          @param [in] value values radial mesh  
          @param [in] mesh It store grids points 
        */
        RadialGrid(std::vector< std::vector<double> > values, std::vector<double> mesh):values(values),mesh(mesh){
            values_size = values.size();
            if (values_size==0){
                std::cout <<  "size of values should be more than 0"  <<std::endl; 
                exit(-1);
            }

            num_points = values[0].size();

            if (num_points != mesh.size()){
                std::cout << " RadialGrid Error  " << std::endl;
                exit(-1);
            }

            for (int i =0; i< values.size(); i++){
                if(num_points != values[i].size()){
                    std::cout << " RadialGrid Error  " << std::endl;
                    exit(-1);
                }
            }
        };
        
        const std::vector<double>& operator[](int index) const { return values[index];};
        const std::vector<double> get_mesh() const {return mesh;};

        /**
          this function returns value at the given radius 

        */
        inline double interpolated( int index, double r ) const{
            const std::vector<double>& value = values[index];
            if(r<mesh[0]){ 
                return (value[1] - value[0]) / ( mesh[1] - mesh[0]) * (r - mesh[0]) + mesh[0];
            }
            return Interpolation::Linear::linear_interpolate(r, value, mesh);
        };

        /**
          This function check that the given grid contains the same format to mine 
          @param [in] grid to be tested
        */
        bool compatible(const RadialGrid& radial_grid, double tolerance = -1.0) const {

            // check size of grid points
            if ( mesh.size() != radial_grid.mesh.size() ){
                return false;
            }
            // if the given tolerance is negative, tolerance is replaced to pre-designated number (tolerance_mesh)
            if(tolerance<0){
                tolerance = tolerance_mesh;
            }
            // check all grid points are the same 
            for (int i =0; i<num_points; i++){
                if( fabs( mesh[i] - radial_grid.mesh[i]) >tolerance){
                    return false;
                }
            }
            return true;
        };

        void set(int index, double val){
            if(index>=values_size){
                exit(-1);
            }

            std::vector<double>& value = values[index];
            
            for(int i=0; i<num_points; i++){
                value[i]=val;
            }
        }

        void scale (int index, double factor){
            if(index>=values_size){
                exit(-1);
            }

            std::vector<double>& value = values[index];
            
            for(int i=0; i<num_points; i++){
                value[i]*=factor;
            }
            return;
        };

        void add(int index, std::vector<double> val){
            if(index>=values_size){
                exit(-1);
            }
            if(val.size()!=num_points){
                exit(-1);
            }

            std::vector<double>& value = values[index];

            for(int i=0; i<num_points; i++){
                value[i]+=val[i];
            }
            return;
        };
    protected:

        /// number of vectors stored on this grids
        int values_size;
        /// number of grid points 
        int num_points;

        /// stored values
        std::vector< std::vector<double> > values;
        /// radial grid information 
        std::vector<double> mesh;
        /// tolerance to determine whether two grid points are same
        constexpr static double tolerance_mesh = 1e-6;
        /// tolerance to determine whether two values are same
        constexpr static double tolerance_val = 1e-6;
};
