#pragma once
#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
#include "Teuchos_RCP.hpp"

#include "../../Basis/Basis.hpp"

namespace Interpolation{
/**
 * @brief Performs 3D interpolation using tricubic interpolation.
 * @details Computed interpolated values by the tricubic interpolation method correspond to given basis.
 *          Refer to http://www.paulinternet.nl/?page=bicubic
 * @author Sungwoo Kang
 * @date 2015 
 * @version 0.0.2
 * This function computes interpolated values by the interpolation method corresponded to given basis. 
 **/
namespace Tricubic{
    /**
     * @brief Basis to Basis interpolation (Epetra_MultiVector version)
     * @param before_basis Source basis.
     * @param before_values Value to interpolate.
     * @param after_basis Destination basis.
     * @param after_values Output. Should be initialized.
     * @param after_center Change center to this value. If NULL, (0, 0, 0).
     * @see Tricubic::calculate(): Actual computation routine.
     **/
    void interpolate(
            Teuchos::RCP<const Basis> before_basis, 
            Teuchos::RCP<Epetra_MultiVector> before_values, 
            Teuchos::RCP<const Basis> after_basis, 
            Teuchos::RCP<Epetra_MultiVector> &after_values, 
            double * after_center = NULL 
    );

    /**
     * @brief Basis to Basis interpolation (Epetra_MultiVector version)
     * @param before_basis Source basis.
     * @param before_values Value to interpolate.
     * @param after_basis Destination basis.
     * @param after_values Output. Should be initialized.
     * @param after_center Change center to this value. If NULL, (0, 0, 0).
     * @note Since Epetra_Vector is child of Epetra_MultiVector, is there a way to remove this overload?
     * @see Tricubic::calculate(): Actual computation routine.
     **/
    void interpolate(
            Teuchos::RCP<const Basis> before_basis, 
            Teuchos::RCP<Epetra_Vector> before_values, 
            Teuchos::RCP<const Basis> after_basis, 
            Teuchos::RCP<Epetra_Vector> &after_values, 
            double * after_center = NULL 
    );

    /*
    double interpolate(
            Teuchos::RCP<const Basis> before_basis, 
            Teuchos::RCP<Epetra_MultiVector> before_values, 
            int row, double x, double y, double z
    );

    double interpolate(
            Teuchos::RCP<const Basis> before_basis, 
            Teuchos::RCP<Epetra_Vector> before_values, 
            double x, double y, double z
    );
    */

    /**
     * @brief Basis to points interpolation (Epetra_MultiVector version)
     * @param before_basis Source basis.
     * @param before_values Value to interpolate.
     * @param row Index of MultiVector to be interpolated.
     * @param positions positions to calculate ( (x1, y1, z1), ...).
     * @return Interpolated values correspond to the given positions.
     * @see Tricubic::calculate(): Actual computation routine.
     **/
    std::vector<double> interpolate(
            Teuchos::RCP<const Basis> before_basis, 
            Teuchos::RCP<Epetra_MultiVector> before_values, 
            int row, std::vector< std::array<double,3> > positions
    );

    /**
     * @brief Basis to points interpolation (Epetra_Vector version)
     * @param before_basis Source basis.
     * @param before_values Value to interpolate.
     * @param positions positions to calculate ( (x1, y1, z1), ...).
     * @return Interpolated values correspond to the given positions.
     * @note Since Epetra_Vector is child of Epetra_MultiVector, is there a way to remove this overload?
     * @see Tricubic::calculate(): Actual computation routine.
     **/
    std::vector<double> interpolate(
            Teuchos::RCP<const Basis> before_basis, 
            Teuchos::RCP<Epetra_Vector> before_values, 
            std::vector< std::array<double,3> > positions
    );

    /**
     * @brief Actual calculation routine for Tricubic Interpolation.
     * @param before_basis Source basis.
     * @param srcval Pointer to the concatenated source values.
     * @param x destination x position.
     * @param y destination y position.
     * @param z destination z position.
     * @return Interpolated value.
     * @see Tricubic::find_neighbors(): Returns neighboring points.
     **/
    double calculate(
        Teuchos::RCP<const Basis> before_basis, 
        double * srcval, 
        double x, double y, double z
    );

    /**
     * @brief Find neighboring points necessary for Tricubic Interpolation.
     * @details Get 4 neighboring points of given point along x,y,z axis (total 64). 
     *          Given point lies between second and third point, or second point.
     * @param basis Source basis.
     * @param x given x position.
     * @param y given y position.
     * @param z given z position.
     * @param decomposed_index 
     *        Index of neighboring points along axis, in the order of [0-4][axis].
     *        Used for scaled_grid.
     * @param combined_index 
     *        Index of neighboring points for, in the order of [x-dir][y-dir][z-dir].
     *        Can be directly used to Epetra_Vector, etc.
     **/
    void find_neighbors( 
            Teuchos::RCP<const Basis> basis, 
            double x, double y, double z, 
            std::vector< std::vector<int> > &decomposed_index, 
            std::vector< std::vector< std::vector<int> > > &combined_index 
    );
};
};

