#pragma once
#include <vector>
#include <string>
#include "Epetra_Vector.h"
#include "Teuchos_RCP.hpp"

#include "../../Basis/Basis.hpp"

/**
 * @brief Utility for atom-centered radial grid.
 * @details Mainly used for manipulating data on atom-centered radial grid for PAW dataset.
 * @author Sungwoo Kang
 * @date 2015 
 * @version 0.0.2
 * @note Is linar interpolation better or spline interpolation better?
 **/
namespace Radial_Grid{
namespace Upf{
    /**
     * @brief Read radial mesh information.
     * @param upf_filename UPF filename
     * @return Resulting mesh, inserted r=0 and cut strange values.
     * @todo Move to Read_Upf and remove postprocessing after its call.
     **/
    std::vector<double> read_radial_mesh( std::string upf_filename );

    /**
     * @brief Read radial mesh information.
     * @param upf_filename UPF filename.
     * @param radial_mesh Radial mesh value.
     * @return Resulting mesh, inserted r=0 and cut strange values, and divided by \f$ 4 \pi r^2 \f$.
     * @todo Move to Read_Upf and remove postprocessing after its call.
     **/
    std::vector<double> read_rhoatom( std::string upf_filename, std::vector<double> radial_mesh );

    /**
     * @brief Read radial mesh information.
     * @param upf_filename UPF filename.
     * @param radial_mesh Radial mesh value.
     * @param atom_number Atom number.
     * @return Resulting mesh, inserted r=0 and cut strange values, and divided by \f$ 4 \pi r^2 \f$.
     * @todo Move to Read_Upf and remove postprocessing after its call.
     **/
    std::vector< std::vector<double> > read_pao( std::string upf_filename, std::vector<double> radial_mesh, int atom_number );

    /**
     * @brief Interpolate properties on radial grid onto Basis and change it to mesh coefficient. 
     * @details Interpolate [src]*Ylm to Basis and change it to mesh coefficient. Put all values larger than RGD2GD_CUTOFF.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param src Value to interpolate, after multiplying spherical harmonics.
     * @param src_grid Source radial mesh.
     * @param src_center Center of src_grid will be positioned here on dest_basis.
     * @param dest_basis Destination mesh.
     * @param retval Resulting coefficient of interpolated value. Input and output. Need to be initialized.
     * @param rc Radial mesh cutoff. Upper bound of src_grid if less then zero.
     * @note Repeating this code is faster (one less loop), but bad for maintanence.
     *       What will be better?
     **/
    void get_basis_coeff_from_radial_grid( 
        int l, int m, std::vector<double> src, std::vector<double> src_grid, double * src_center, 
        Teuchos::RCP<const Basis> dest_basis, Teuchos::RCP<Epetra_Vector> retval
    );
}
}

