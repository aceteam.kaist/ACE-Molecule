#include "Linear_Interpolation.hpp"
#include <stdexcept>
#include <cmath>
#include <cstdlib>
#include "../Verbose.hpp"

using std::vector;
using std::abs;

double Interpolation::Linear::linear_interpolate(double x, vector<double> basis, vector<double> value){
    double y;

    if(x < basis[0] || x > basis[basis.size()-1]){
        throw std::invalid_argument("x value is outside of provided basis");
    }
    else if(std::fabs(x-basis[0]) < 1.0E-10){
        y = value[0];
    }
    else{
        for(int i=0;i<basis.size();i++){
            if(x < basis[i]){
                y = (value[i] - value[i-1]) / (basis[i] - basis[i-1]) * (x - basis[i-1]) + value[i-1];
                break;
            }
        }
    }

    return y;
}

double Interpolation::Linear::linear_interpolate_bisection(double x, vector<double> basis, vector<double> value){
    //double y;
    int a,b,c;

    a=0;
    b=value.size()-1;

    while(true){
        c=(a+b)/2;
        if(basis[c] > x) b=c;
        else a=c;

        if(b-a==1) break;
    }

    double y = (value[b]-value[a]) / (basis[b]-basis[a]) * (x-basis[a]) + value[a];

    return y;
}
