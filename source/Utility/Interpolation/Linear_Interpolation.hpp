#pragma once
#include <vector>

namespace Interpolation{
namespace Linear {
    ///< @brief Interpolate the value at x on basis using linear interpolation.
    double linear_interpolate(double x, std::vector<double> basis, std::vector<double> value);
    ///< @brief Interpolate the value at x on basis using bisection.
    double linear_interpolate_bisection(double x, std::vector<double> basis, std::vector<double> value);
};
};

