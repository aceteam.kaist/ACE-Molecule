#pragma once
#include "../Basis/Basis.hpp"
#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"

namespace Value_Coef{
    /**
     * @brief Change the representation between the value on the grid to grid coefficient.
     * @param basis Basis.
     * @param input Input value.
     * @param is_val_input True if input representation is value, false if input representation is basis coefficient.
     * @param is_val_input True if desired representation is value, false if desired representation is basis coefficient.
     * @param output Return value.
     **/
    int Value_Coef(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<const Epetra_MultiVector> input, 
        bool is_val_input, bool is_val_output, 
        Teuchos::RCP<Epetra_MultiVector>& output
    );
    /**
     * @brief Change the representation between the value on the grid to grid coefficient.
     * @param basis Basis.
     * @param input Input value.
     * @param is_val_input True if input representation is value, false if input representation is basis coefficient.
     * @param is_val_input True if desired representation is value, false if desired representation is basis coefficient.
     * @param output Return value.
     **/
    int Value_Coef(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<const Epetra_Vector> input, 
        bool is_val_input, bool is_val_output, 
        Teuchos::RCP<Epetra_Vector>& output
    );
    /**
     * @brief Change the representation between the value on the grid to grid coefficient.
     * @param basis Basis.
     * @param input Input value.
     * @param is_val_input True if input representation is value, false if input representation is basis coefficient.
     * @param is_val_input True if desired representation is value, false if desired representation is basis coefficient.
     * @param output Return value.
     * @param size Input (and output) array size.
     **/
    int Value_Coef(
        Teuchos::RCP<const Basis> basis, 
        double* input, 
        bool is_val_input, 
        bool is_val_output, 
        double* output, 
        int size
    );
}
