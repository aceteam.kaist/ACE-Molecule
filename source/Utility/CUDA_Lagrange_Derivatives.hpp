#pragma once
#ifdef USE_CUDA
#pragma once 
#include <vector>
//#include "Time_Measure.hpp"

/**
@breif This class performs kernel integration. 
@author Sunghwan Choi

@details 
Here, kernel function is linear combination of Gaussian functions to represent 1/r.
The method, performed here was proposed by Sundholm (JCP 132, 024102, 2010).
Within two-divided regions ([t_i,t_l], [t_l,t_f]), num_points1, num_points2 quadrature points are made, respectively.
Poisson_Solver inherits this class
*/

namespace CUDA_Lagrange_Derivatives{
    int divergence_gpu(const int total_size,
                    const int* points, const double* scaling,
                    int start_index_z, int end_index_z,
                    int start_index_y, int end_index_y,
                    double* input1, double* input2, double* input3,
                    double*& divergence);
    extern bool gpu_memory_allocated;
    /// device pointer for density
    extern double* d_result1;
    extern double* d_result2;
    extern double* d_result3;

    extern double* d_input1;
    extern double* d_input2;
    extern double* d_input3;
    /// temporary memory space for convolution
    extern double** dT_gamma;
    extern double** dT_gamma2;
    /// temporary memory space for convolution
    extern double** dT_beta;
    /// temporary memory space for convolution
    extern double** d_input_fold1;
    extern double** d_input_fold2;
    extern double** d_input_fold3;
    // pointer of pointer in Device
    extern double* dFx; extern double* dFy; extern double* dFz;
    extern double** dFx_dev; extern double** dFy_dev; extern double** dFz_dev;
    // pointer of pointer in Device
    extern double** dT_gamma_dev; 
    extern double** dT_gamma2_dev;
    extern double** dT_beta_dev; 
    extern double** d_input_fold1_dev;
    extern double** d_input_fold2_dev;
    extern double** d_input_fold3_dev;
    extern int* di_xs; extern int* di_ys; extern int* di_zs;  
    extern int* d_from_basic;        

    __global__ void unfold1(  const int start_index_z, const int end_index_z,
                                const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                double const** __restrict__ original_matrices, double* values );

    __global__ void unfold2(  const int start_index_y, const int end_index_y,
                                const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                double const** __restrict__ original_matrices, double* values );

    __global__ void fold1 (const int start_index_z, const int end_index_z,
                                const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                const double* d_density, double** d_rho_dev);

    __global__ void fold2 (const int start_index_y, const int end_index_y,
                                const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                const double* d_density, double** d_rho_dev);
};
#endif 
