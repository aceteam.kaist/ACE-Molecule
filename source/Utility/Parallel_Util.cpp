#include "ACE_Config.hpp"
#include "Parallel_Util.hpp"
#include "Parallel_Manager.hpp"
#include "Verbose.hpp"
#include "Epetra_CrsMatrix.h"
#include "String_Util.hpp" 
#include "Epetra_Import.h"



#include "EpetraExt_BlockMapOut.h"
#include "EpetraExt_RowMatrixOut.h"

void Parallel_Util::rank_sum(double* send_data, double* recv_data, int count){
    Parallel_Manager::info().rank_allreduce(send_data, recv_data, count, Parallel_Manager::Sum);
}
void Parallel_Util::group_sum(double* send_data, double* recv_data, int count){
    Parallel_Manager::info().group_allreduce(send_data, recv_data, count, Parallel_Manager::Sum);
}
void Parallel_Util::all_sum(double* send_data, double* recv_data, int count){
    Parallel_Manager::info().all_allreduce(send_data, recv_data, count, Parallel_Manager::Sum);
}

void Parallel_Util::rank_sum(int* send_data, int* recv_data, int count){
    Parallel_Manager::info().rank_allreduce(send_data, recv_data, count, Parallel_Manager::Sum);
}
void Parallel_Util::group_sum(int* send_data, int* recv_data, int count){
    Parallel_Manager::info().group_allreduce(send_data, recv_data, count, Parallel_Manager::Sum);
}
void Parallel_Util::all_sum(int* send_data, int* recv_data, int count){
    Parallel_Manager::info().all_allreduce(send_data, recv_data, count, Parallel_Manager::Sum);
}

void Parallel_Util::group_allgather_multivector(Teuchos::RCP<const Epetra_MultiVector> send_data, double** recv_data){
#ifdef ACE_HAVE_MPI
    int NumMyElements = send_data->Map().NumMyElements();
    int mpi_size = Parallel_Manager::info().get_mpi_size();

    double** pValue = new double*[send_data -> NumVectors()];
    for(int i =0; i< send_data -> NumVectors(); ++i) pValue[i] = new double[NumMyElements];

    if (send_data->ExtractCopy(pValue)!=0){
        Verbose::all() << "Parallel_Util::group_allgather_multivector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
    int* sizeList = new int[mpi_size];
    Parallel_Manager::info().group_allgather(&NumMyElements, 1, sizeList, 1);
    int* displs = new int[mpi_size]();
    for(int i=0; i<mpi_size;i++){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }

    for(int i=0; i<send_data -> NumVectors(); ++i){
        Parallel_Manager::info().group_allgatherv(pValue[i], NumMyElements, recv_data[i], sizeList, displs);
    }
    delete[] displs;
    delete[] sizeList;
    for(int i = 0; i < send_data -> NumVectors(); ++i){ delete[] pValue[i]; }
    delete[] pValue;
#else
    if (send_data->ExtractCopy(recv_data)!=0){
        Verbose::all() << "Parallel_Util::group_allgather_multivector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
#endif
}

void Parallel_Util::group_allgather_vector(Teuchos::RCP<const Epetra_Vector> send_data, double* recv_data){
    Parallel_Util::group_allgather_vector(const_cast<Epetra_Vector*>(send_data.get()), recv_data);
}

void Parallel_Util::group_allgather_vector(Epetra_Vector* send_data, double* recv_data){
#ifdef ACE_HAVE_MPI
    int NumMyElements = send_data->Map().NumMyElements();
    int mpi_size = Parallel_Manager::info().get_mpi_size();

    double* pValue = new double[NumMyElements];
    if (send_data->ExtractCopy(pValue)!=0){
        Verbose::all() << "Parallel_Util::group_allgather_vector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
    int* sizeList = new int[mpi_size];
    Parallel_Manager::info().group_allgather(&NumMyElements, 1, sizeList, 1);
    int* displs = new int[mpi_size]();
    for(int i=0; i<mpi_size;i++){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }

    Parallel_Manager::info().group_allgatherv(pValue, NumMyElements, recv_data, sizeList, displs);
    delete[] displs;
    delete[] sizeList;
    delete[] pValue;
#else
    if (send_data->ExtractCopy(recv_data)!=0){
        Verbose::all() << "Parallel_Util::group_allgather_vector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
#endif
}

void Parallel_Util::group_gather_multivector(Teuchos::RCP<Epetra_MultiVector> send_data, double** recv_data, int root){
#ifdef ACE_HAVE_MPI
    int NumMyElements = send_data->Map().NumMyElements();
    int mpi_size = Parallel_Manager::info().get_mpi_size();

    double** pValue = new double*[send_data -> NumVectors()];
    for(int i =0; i< send_data -> NumVectors(); ++i) pValue[i] = new double[NumMyElements];

    if (send_data->ExtractCopy(pValue)!=0){
        Verbose::all() << "Parallel_Util::group_gather_multivector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
    int* sizeList = new int[mpi_size];
    Parallel_Manager::info().group_allgather(&NumMyElements, 1, sizeList, 1);
    int* displs = new int[mpi_size]();
    for(int i=0; i<mpi_size;i++){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }

    for(int i=0; i<send_data -> NumVectors(); ++i){
        Parallel_Manager::info().group_gatherv(pValue[i], NumMyElements, recv_data[i], sizeList, displs, root);
    }
    delete[] displs;
    delete[] sizeList;
    for(int i = 0; i < send_data -> NumVectors(); ++i){ delete[] pValue[i]; }
    delete[] pValue;
#else
    if (send_data->ExtractCopy(recv_data)!=0){
        Verbose::all() << "Parallel_Util::group_gather_multivector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
#endif
}

void Parallel_Util::group_gather_vector(Teuchos::RCP<Epetra_Vector> send_data, double* recv_data, int root){
    Parallel_Util::group_gather_vector(send_data.get(), recv_data, root);
}

void Parallel_Util::group_gather_vector(Epetra_Vector * send_data, double* recv_data, int root){
#ifdef ACE_HAVE_MPI
    int NumMyElements = send_data->Map().NumMyElements();
    int mpi_size = Parallel_Manager::info().get_mpi_size();

    double* pValue = new double[NumMyElements];
    if (send_data->ExtractCopy(pValue)!=0){
        Verbose::all() << "Parallel_Util::group_gather_vector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
    int* sizeList = new int[mpi_size];
    Parallel_Manager::info().group_allgather(&NumMyElements, 1, sizeList, 1);
    int* displs = new int[mpi_size]();
    for(int i=0; i<mpi_size;i++){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }

    Parallel_Manager::info().group_gatherv(pValue, NumMyElements, recv_data, sizeList, displs, root);
    delete[] displs;
    delete[] sizeList;
    delete[] pValue;
#else
    if (send_data->ExtractCopy(recv_data)!=0){
        Verbose::all() << "Parallel_Util::group_gather_vector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
#endif
}

void Parallel_Util::group_extract_nonzero_from_multivector(
        Teuchos::RCP<Epetra_MultiVector> entity,
        std::vector< std::vector<double> >& vals, std::vector< std::vector<int> >& inds, double cutoff/* = 1.0E-30*/){
    vals.clear();
    inds.clear();
    int vec_dim = entity -> NumVectors();
    int dim = entity -> GlobalLength();
    inds.resize(vec_dim);
    vals.resize(vec_dim);

    double** val = new double*[vec_dim];
    for(int i=0; i<vec_dim; ++i){
        val[i] = new double[dim]();
    }
    Parallel_Util::group_allgather_multivector(entity, val);
    for(int i=0; i<vec_dim; ++i){
        for( int j = 0; j < dim; ++j){
            if( std::abs(val[i][j]) > cutoff ){
                inds[i].push_back(j);
                vals[i].push_back(val[i][j]);
            }
        }
    }
    for(int i=0; i<vec_dim; ++i){
        delete[] val[i];
    }
    delete[] val;
}

void Parallel_Util::group_extract_nonzero_from_vector(
        Teuchos::RCP<Epetra_Vector> entity,
        std::vector<double>& vals, std::vector<int>& inds, double cutoff/* = 1.0E-30*/){
    std::vector< std::vector<double> > new_vals;
    std::vector< std::vector<int> > new_inds;
    Parallel_Util::group_extract_nonzero_from_multivector(entity, new_vals, new_inds, cutoff);
    vals = new_vals[0];
    inds = new_inds[0];
}

void Parallel_Util::extract_nonzero_from_multivector_locally(
        Teuchos::RCP<Epetra_MultiVector> entity,
        std::vector< std::vector<double> >& vals, std::vector< std::vector<int> >& inds, double cutoff/* = 1.0E-30*/){
    vals.clear();
    inds.clear();
    int vec_dim = entity -> NumVectors();
    int dim = entity -> Map().NumMyElements();
    inds.resize(vec_dim);
    vals.resize(vec_dim);

    for(int i=0; i<vec_dim; ++i){
        for( int j = 0; j < dim; ++j){
            if( std::abs(entity -> operator[](i)[j]) > cutoff ){
                inds[i].push_back(entity -> Map().GID(j));
                vals[i].push_back(entity -> operator[](i)[j]);
            }
        }
    }
}

void Parallel_Util::extract_nonzero_from_vector_locally(
        Teuchos::RCP<Epetra_Vector> entity,
        std::vector<double>& vals, std::vector<int>& inds, double cutoff/* = 1.0E-30*/){
    std::vector< std::vector<double> > new_vals;
    std::vector< std::vector<int> > new_inds;
    Parallel_Util::extract_nonzero_from_multivector_locally(entity, new_vals, new_inds, cutoff);
    vals = new_vals[0];
    inds = new_inds[0];
}

/*
double* Parallel_Util::rank_gather_std_vector(std::vector<double> send_data, int root){

    int NumMyElements = send_data.size();
    int mpi_size = Parallel_Manager::info().get_group_size();
    double* pValue = new double[NumMyElements];
#ifdef ACE_HAVE_MPI
    if (send_data->ExtractCopy(pValue)!=0){
        Verbose::all() << "Parallel_Util::allgather_multivector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
    int* sizeList = new int[mpi_size];
    Parallel_Manager::info().rank_allgather(&NumMyElements, 1, sizeList, 1);
    int* displs = new int[mpi_size]();
    for(int i=0; i<mpi_size;i++){
        if (i!=0) {displs[i] = displs[i-1]+sizeList[i-1];}
        else{ displs[i]=0; }
    }

    Parallel_Manager::info().rank_gatherv(pValue, NumMyElements, recv_data, sizeList, displs, root);
    delete[] displs;
    delete[] sizeList;
    delete[] pValue;
#else
    if (send_data->ExtractCopy(recv_data)!=0){
        Verbose::all() << "Parallel_Util::allgather_multivector: Failed to Extract Epetra_MultiVector!" << std::endl;
        exit(-1);
    }
#endif








    double* return_val = new double[];
    return return_val;
}*/




Teuchos::RCP<Epetra_CrsMatrix> Parallel_Util::matrix_redisitribution(Teuchos::RCP<Epetra_CrsMatrix> input, bool is_copy){
//Teuchos::RCP<Epetra_CrsMatrix> Parallel_Util::matrix_redisitribution(Teuchos::RCP<Epetra_CrsMatrix> input, int*& tar_row_indices, int*& tar_col_indices, double*& tar_values){
//    if(input->Filled()==false){
//        input-> FillComplete() ;
//    }
//    //EpetraExt::RowMatrixToMatrixMarketFile("matrix.dat", *input ); //shchoi
//    //EpetraExt::BlockMapToMatrixMarketFile("source_map.dat",input->RowMap() );
//
//    Epetra_Map target_map(input->NumGlobalRows(),0, *Parallel_Manager::info().get_group_comm() );
//    //EpetraExt::BlockMapToMatrixMarketFile("target_map.dat",target_map );
//    Epetra_Import importer (target_map,input->RowMap() );
//    Teuchos::RCP<Epetra_CrsMatrix> output = Teuchos::rcp(new Epetra_CrsMatrix(Copy,target_map,0 )  ) ;
//    output->Import(*input,importer, Insert );
//    output->FillComplete();
//    //Teuchos::RCP<Epetra_CrsMatrix> output = Teuchos::rcp(new Epetra_CrsMatrix(*input, importer )  ) ;

    
    if(input->Filled()==false){
        input-> FillComplete() ;
    }
    if(input->StorageOptimized ()==false){
        input-> OptimizeStorage ();
    }

    if(input->Comm().NumProc()==1){
        return input;
    }

    int* src_row_indices;
    int* src_col_indices = new int[input->NumMyNonzeros()];
    //int  src_col_indices[input->NumMyNonzeros()];
    int* tmp_col_indices; 
    int* tar_row_indices; //shchoi
    int* tar_col_indices; //shchoi
    double* tar_values;
    double* src_values; //double* tar_values; //shchoi
    //double* imaginary_values = new double[input->NumMyNonzeros()]();
    input->ExtractCrsDataPointers(src_row_indices,tmp_col_indices,src_values); 
/*
    for(int i =0; i<input->NumMyRows(); i++){
        src_row_indices2[i] = src_row_indices[i];
    }
*/
    for(int i=0; i<input->NumMyNonzeros();i++){
        src_col_indices[i] = input->ColMap().GID(tmp_col_indices[i]);
    }
    Teuchos::RCP<Epetra_CrsMatrix> output;
    auto source_map = input->Map();
    const int total_mpi_rank = Parallel_Manager::info().get_total_mpi_rank();
    const int total_mpi_size = Parallel_Manager::info().get_total_mpi_size();
    const int mpi_rank = Parallel_Manager::info().get_mpi_rank();
    const int mpi_size = Parallel_Manager::info().get_mpi_size();
    const int group_rank = Parallel_Manager::info().get_group_rank();
    const int group_size = Parallel_Manager::info().get_group_size();

    int nDataCount=0;
    int nMyRows=0;

    if(group_size==1){
        output = Teuchos::rcp( new Epetra_CrsMatrix(* input));
        return output;
    }
#ifdef ACE_HAVE_MPI

//    MPI_Status* status;

    int pidSend[group_size];
    int pidRecv[group_size];

    for(int i=0; i<group_size; i++){
        pidRecv[i] = group_size*mpi_rank+i;
        pidSend[i] = total_mpi_rank/group_size+mpi_size*i;
    }
//    int* firstElements = source_map.FirstPointInElementList();
/*
    for(int p=0; p< Parallel_Manager::info().get_total_mpi_size();p++){
        Parallel_Manager::info().all_barrier();
        if(p==Parallel_Manager::info().get_total_mpi_rank()){
            std::cout << "================= recv row size "<< p<< " ===============" <<std::endl;
            for(int i=0; i<group_size; i++){
                std::cout << i << "\t" <<pidRecv[i] <<"\t" << pidSend[i]<<std::endl;
            }
        }
    }
*/
    int recv_nnz_size[group_size];
    int send_nnz_size[group_size];

    int recv_row_size[group_size];
    int send_row_size[group_size];

    const int max_tag = total_mpi_size*total_mpi_size*2;

    /////////////////////   set Recv&Send size    //////////////
    {
        MPI_Request send_request[group_size];
        MPI_Request recv_request[group_size];

        MPI_Request send_request2[group_size];
        MPI_Request recv_request2[group_size];

        MPI_Status status1 [group_size];
        MPI_Status status2 [group_size];
        MPI_Status status3 [group_size];
        MPI_Status status4 [group_size];

        for(int i=0; i<group_size;i++){
            int send_tag = total_mpi_size*total_mpi_rank+pidSend[i];
            int recv_tag = total_mpi_size*pidRecv[i]+total_mpi_rank;
            send_nnz_size[i] = input->NumMyNonzeros();
            MPI_Isend(&send_nnz_size[i],  1, MPI_INT, pidSend[i], max_tag-send_tag, MPI_COMM_WORLD ,&send_request[i] );
            MPI_Irecv(&recv_nnz_size[i],  1, MPI_INT, pidRecv[i], max_tag-recv_tag, MPI_COMM_WORLD ,&recv_request[i] );

            send_row_size[i] = input->NumMyRows();
            MPI_Isend(&send_row_size[i],  1, MPI_INT, pidSend[i], send_tag, MPI_COMM_WORLD ,&send_request2[i] );
            MPI_Irecv(&recv_row_size[i],  1, MPI_INT, pidRecv[i], recv_tag, MPI_COMM_WORLD ,&recv_request2[i] );
        }
        int err;
        err=MPI_Waitall(group_size,send_request,status1);
        if(err!=0){
            exit(-3);
        }
        err=MPI_Waitall(group_size,recv_request,status2);
        if(err!=0){
            exit(-4);
        }

        err=MPI_Waitall(group_size,send_request2,status3);
        if(err!=0){
            exit(-3);
        }
        err=MPI_Waitall(group_size,recv_request2,status4);
        if(err!=0){
            exit(-4);
        }
    }
    /////////////////  Allocate Memory      ////////////////
    for(int i=0; i<group_size;i++){
        nMyRows+=recv_row_size[i];
        nDataCount+=recv_nnz_size[i];
    }
    tar_row_indices = new int[nMyRows];
    tar_col_indices = new int[nDataCount];
    tar_values      = new double[nDataCount];

    /////////////////  Allocate Memory      ////////////////
    ////////////////// Send&Recv CRS Matrix  //////////////////
    {
        MPI_Status status1 [group_size];
        MPI_Status status2 [group_size];
        MPI_Status status3 [group_size];
        MPI_Status status4 [group_size];
        MPI_Status status5 [group_size];
        MPI_Status status6 [group_size];

        MPI_Request send_request[group_size];
        MPI_Request recv_request[group_size];
        MPI_Request send_request2[group_size];
        MPI_Request recv_request2[group_size];
        MPI_Request send_request3[group_size];
        MPI_Request recv_request3[group_size];
        ///////////////////////////////////////// row index communication //////////////////////////////////////////////
        int index=0;
        for(int i=0; i<group_size;i++){
            int send_tag = total_mpi_size*total_mpi_rank+pidSend[i];
            int recv_tag = total_mpi_size*pidRecv[i]+total_mpi_rank;
            MPI_Isend(src_row_indices, input->NumMyRows(), MPI_INT, pidSend[i], send_tag, MPI_COMM_WORLD ,&send_request[i] );
            //MPI_Isend(src_row_indices2, input->NumMyRows(), MPI_INT, pidSend[i], send_tag, MPI_COMM_WORLD ,&send_request[i] );
            MPI_Irecv(&tar_row_indices[index],recv_row_size[i],MPI_INT, pidRecv[i], recv_tag, MPI_COMM_WORLD ,&recv_request[i] );
            index+=recv_row_size[i];
        }
        int err=MPI_Waitall(group_size,send_request,status1);
        if(err!=0){
            exit(-3);
        }
        err=MPI_Waitall(group_size,recv_request,status2);
        if(err!=0){
            exit(-4);
        }
        ///////////////////////////////////////// row index communication //////////////////////////////////////////////
        ////////////////////////////////// col index & nnz val communication ///////////////////////////////////////////
        index=0;
/*
        for(int i=0; i <total_mpi_size; i++){
            Parallel_Manager::info().all_barrier();
            if(i==total_mpi_rank){
                std::cout << i <<"\t"<< nDataCount  <<"\t";
                for(int j =0; j< group_size; j++){
                    std::cout << recv_nnz_size[j] <<"\t";
                }
                std::cout << std::endl;
            }
        }
*/
        
        Parallel_Manager::info().all_barrier();
        for(int i=0; i<group_size;i++){
            int send_tag = total_mpi_size*total_mpi_rank+pidSend[i];
            int recv_tag = total_mpi_size*pidRecv[i]+total_mpi_rank;
            MPI_Isend(src_col_indices, send_nnz_size[i], MPI_INT, pidSend[i], max_tag-send_tag, MPI_COMM_WORLD, &send_request2[i] );
            MPI_Irecv(&tar_col_indices[index], recv_nnz_size[i], MPI_INT, pidRecv[i], max_tag-recv_tag, MPI_COMM_WORLD, &recv_request2[i] );
            //shchoi
            MPI_Isend(src_values, send_nnz_size[i], MPI_DOUBLE, pidSend[i], send_tag, MPI_COMM_WORLD, &send_request3[i]);
            MPI_Irecv(&tar_values[index], recv_nnz_size[i], MPI_DOUBLE, pidRecv[i], recv_tag, MPI_COMM_WORLD, &recv_request3[i] );

            index +=recv_nnz_size[i];
        }
        ////////////////////////////////// col index & nnz val communication ///////////////////////////////////////////
       
        ////////////////////////////////////////// row index correction ////////////////////////////////////////////////
        Parallel_Manager::info().all_barrier();
        int count=0;
        for(int i=0; i<group_size; i++){
            count+=recv_row_size[i];
            for(int j=count;j<nMyRows;j++){
                tar_row_indices[j] += recv_nnz_size[i];
            }
        }
        ////////////////////////////////////////// row index correction ////////////////////////////////////////////////

        err=MPI_Waitall(group_size,send_request3,status3);
        if(err!=0){
            exit(-7);
        }
        err=MPI_Waitall(group_size,recv_request3,status4);
        if(err!=0){
            exit(-8);
        }
        
        err=MPI_Waitall(group_size,send_request2,status5);
        if(err!=0){
            exit(-5);
        }
        err=MPI_Waitall(group_size,recv_request2,status6);
        if(err!=0){
            exit(-6);
        }
    }
    delete[] src_col_indices;
    int size; int start;
    //std::cout << "done"<< std::endl;

    //tar_col_indices = new int[nDataCount];
    //output->SetRowCount(nMyRows);
    //output->SetColumnCount(  input->NumGlobalCols() );
    //output->BuildDataBuffer();

//////////////////////////////////////////////////////////////////////////////////////////make graph and insert values
    Epetra_Map target_map(source_map.NumGlobalElements() , nMyRows,0,*Parallel_Manager::info().get_group_comm());
    int err=0;
    int end;
/*
    Teuchos::RCP<Epetra_CrsGraph> graph = Teuchos::rcp(new Epetra_CrsGraph(View, target_map,0 ) );
    for(int i =0; i< nMyRows; i++){
        end = ( (i==nMyRows-1)? nDataCount: tar_row_indices[i+1]);
        err = graph->InsertGlobalIndices(target_map.GID(i), end-tar_row_indices[i], &tar_col_indices[tar_row_indices[i]]);
    }    
    err = graph->FillComplete();
    if (err!=0){
        std::cout << "FillComplete err!!" << err<<std::endl;
    }
*/
    if (is_copy){
        output = Teuchos::rcp(new Epetra_CrsMatrix(Copy, target_map, 0)); 
    }
    else{
        output = Teuchos::rcp(new Epetra_CrsMatrix(View, target_map, 0)); 
    }
    for(int i =0; i< nMyRows; i++){
        end = ( (i==nMyRows-1)? nDataCount: tar_row_indices[i+1]);
        err = output->InsertGlobalValues(target_map.GID(i), end-tar_row_indices[i], &tar_values[tar_row_indices[i]],&tar_col_indices[tar_row_indices[i]]);
//        if (err!=0){
//            std::cout << "Insert err!!" << err<<std::endl;
//        }
    }
    err = output->FillComplete();
    if (err!=0){
        std::cout << "FillComplete err!!" << err<<std::endl;
    }


    if (is_copy){
        delete[] tar_row_indices;
        delete[] tar_col_indices;
        delete[] tar_values;
    }
#endif
//    std::ofstream outFile1("output."+String_Util::to_string((long long )Parallel_Manager::info().get_group_rank())+"."+String_Util::to_string((long long )Parallel_Manager::info().get_mpi_rank())+".txt");
//    std::ofstream outFile0("output.origianl."+String_Util::to_string((long long )Parallel_Manager::info().get_total_mpi_rank())+".txt");

//    output->Print(outFile1);
//    input->Print(outFile0);

    //exit(-1);
    return output;
}

void Parallel_Util::matrix_serialize(Teuchos::RCP<Epetra_CrsMatrix> input, Teuchos::SerialDenseMatrix<int,double> &matrix){
    const int num_proc = Parallel_Manager::info().get_mpi_size();
    const int matrix_dimension = input->NumGlobalRows();
    const int total_nnz = input->NumGlobalNonzeros();

    int* tmp_col_indices;
    int* src_row_indices; int* tar_row_indices;
    int* src_col_indices; int* tar_col_indices;
    double* src_values; double* tar_values;

    int my_nnz_size = input->NumMyNonzeros ();
    int my_row_size = input->NumMyRows();

    int nnz_sizes[num_proc ];
    int row_sizes[num_proc ];

    if(num_proc!=1){
#ifdef ACE_HAVE_MPI

    if(input->Filled()==false){
        input-> FillComplete() ;
    }
    if(input->StorageOptimized ()==false){
        input-> OptimizeStorage ();
    }
    input->ExtractCrsDataPointers(src_row_indices,tmp_col_indices,src_values); 

    src_col_indices = new int[input->NumMyNonzeros()];
    for(int i=0; i<input->NumMyNonzeros();++i){
        src_col_indices[i] = input->ColMap().GID(tmp_col_indices[i]);
    }

    Parallel_Manager::info().group_allgather(&my_nnz_size, 1, nnz_sizes,1);
    Parallel_Manager::info().group_allgather(&my_row_size, 1, row_sizes,1);
    int nnz_displs[num_proc ];
    nnz_displs[0]=0;
    for(int i=1; i<num_proc; ++i){
        nnz_displs[i] = nnz_displs[i-1]+nnz_sizes[i-1];
    }
    
    int row_displs[num_proc];
    row_displs[0]=0;
    for(int i=1; i<num_proc; ++i){
        row_displs[i] = row_displs[i-1]+row_sizes[i-1];
    }

    tar_row_indices = new int[matrix_dimension];
//row_displs[num_proc-1]+row_sizes[num_proc-1]];
    tar_col_indices = new int[total_nnz];
    tar_values = new double[total_nnz];

    //Parallel_Manager::info().group_gatherv(src_row_indices, row_sizes[Parallel_Manager::info().get_group_rank()], tar_row_indices, row_sizes, row_displs, 0);
    Parallel_Manager::info().group_gatherv(src_row_indices, my_row_size, tar_row_indices, row_sizes, row_displs, 0);
    Parallel_Manager::info().group_gatherv(src_values, my_nnz_size, tar_values, nnz_sizes, nnz_displs, 0);
    Parallel_Manager::info().group_gatherv(src_col_indices, my_nnz_size, tar_col_indices, nnz_sizes, nnz_displs, 0 );

    
#else
    Verbose::all()<< "something wrong "<<std::endl;
    exit(-1);
#endif
    }
    else{
        input->ExtractCrsDataPointers(tar_row_indices,tar_col_indices,tar_values);
    }

    if(Parallel_Manager::info().get_total_mpi_rank()==0){
        int start_ =0;
        for(int i=1; i<num_proc; ++i){
            start_+=row_sizes[i-1];
            for(int j=start_; j<matrix_dimension; ++j){
                tar_row_indices[j]+=nnz_sizes[i-1];
            }
        }

        for(int  i=0; i<matrix_dimension-1; ++i){
            for(int j=tar_row_indices[i];j<tar_row_indices[i+1];++j ){
                matrix[i][tar_col_indices[j]] = tar_values[j];
            }
        }    
        for(int j=tar_row_indices[matrix_dimension-1]; j<total_nnz; ++j){
                matrix[matrix_dimension-1][tar_col_indices[j] ] = tar_values[j];
        }

    }
    if(num_proc!=1){
#ifdef ACE_HAVE_MPI
        delete[] src_col_indices;
        delete[] tar_row_indices;
        delete[] tar_col_indices;
        delete[] tar_values;
#endif
    }
    
}

void Parallel_Util::group_gather_vector(std::vector<double>& vector){ //this routine is possible only for nonlocal_pp of single grid
#ifdef ACE_HAVE_MPI 
    const int numProc =  Parallel_Manager::info().get_group_comm()->NumProc();
    int* size_list = new int[numProc];
    int tmp_size = vector.size();
    //int MyPID = basis->get_map()->Comm().MyPID();
    //Verbose::all() << map->Comm().MyPID() << "  " << tmp_size << std::endl << std::endl;

    //MPI_Allgather(&tmp_size, 1, MPI_INT, size_list,1, MPI_INT, MPI_COMM_WORLD);
    Parallel_Manager::info().group_allgather(&tmp_size,1,size_list,1);
    int* displs = new int[numProc];
    for(int i=0; i<numProc; i++){
        if(i!=0){
            displs[i] = displs[i-1] + size_list[i-1];
        }
        else{
            displs[i] = 0;
        }
    }

    int new_size = 0;
    for(int i=0; i<numProc; i++){
        new_size += size_list[i];
    }

    double *combine_vector = new double[new_size];
    auto tmp_vector = new double[vector.size()];
    for(int i=0; i<vector.size(); i++)
        tmp_vector[i] = vector[i];
    
    //MPI_Allgatherv(&match_sphere_to_basic, tmp_size, MPI_INT, &new_match_sphere_to_basic, size_list, displs, MPI_INT, MPI_COMM_WORLD);
//       Verbose::all() <<"new size : " << new_size << std::endl;
//       Verbose::all() <<"tmp size : " << tmp_size << std::endl;
//       Verbose::all() <<"num_proc : " << basis->get_map()->Comm().NumProc() << std::endl;
//       for(int i=0; i<basis->get_map()->Comm().NumProc(); i++){
//       Verbose::all() <<"size_list : " << size_list[i] << std::endl;
//       }
    //MPI_Allgatherv(tmp_vector, tmp_size, MPI_DOUBLE, combine_vector, size_list, displs, MPI_DOUBLE, MPI_COMM_WORLD);
//    Parallel_Manager::info().group_allgatherv(vector.data(), tmp_size, combine_vector, size_list, displs);
    Parallel_Manager::info().group_allgatherv(tmp_vector, tmp_size, combine_vector, size_list, displs);
    vector.clear();
    vector.resize(new_size);
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for 
    #endif
    for(int i=0; i<new_size; i++)
        vector[i] =combine_vector[i];
    //MPI_combine::MPI_combine(map, match_sphere_to_basic);
    delete [] tmp_vector;
    delete [] combine_vector;
    delete [] size_list;
    delete [] displs;
#else

#endif

}

void Parallel_Util::group_gather_vector(std::vector<int>& vector){ //this routine is possible only for nonlocal_pp of single grid
#ifdef ACE_HAVE_MPI 
    const int numProc = Parallel_Manager::info().get_group_comm()->NumProc();
    int* size_list = new int[numProc];
    int tmp_size = vector.size();
    //int MyPID = basis->get_map()->Comm().MyPID();
    //Verbose::all() << map->Comm().MyPID() << "  " << tmp_size << std::endl << std::endl;
    Parallel_Manager::info().group_barrier();
    
    Parallel_Manager::info().group_allgather(&tmp_size, 1, size_list, 1);
    //MPI_Allgather(&tmp_size, 1, MPI_INT, size_list,1, MPI_INT, MPI_COMM_WORLD);

    int* displs = new int[numProc];
    for(int i=0; i<numProc; i++){
        if(i!=0){
            displs[i] = displs[i-1] + size_list[i-1];
        }
        else{
            displs[i] = 0;
        }
    }

    int new_size = 0;
    for(int i=0; i<numProc; i++){
        new_size += size_list[i];
    }
    int *tmp_vector = new int[vector.size()];
    for(int i=0; i<vector.size(); i++)
        tmp_vector[i] = vector[i];
    int *combine_vector = new int[new_size];
    //MPI_Allgatherv(&match_sphere_to_basic, tmp_size, MPI_INT, &new_match_sphere_to_basic, size_list, displs, MPI_INT, MPI_COMM_WORLD);
    //    Verbose::all() <<"new size : " << new_size << std::endl;
    //    Verbose::all() <<"tmp size : " << tmp_size << std::endl;
    //    Verbose::all() <<"num_proc : " << basis->get_map()->Comm().NumProc() << std::endl;
    //   for(int i=0; i<basis->get_map()->Comm().NumProc(); i++){
    //   Verbose::all() <<"size_list : " << size_list[i] << std::endl;
    //   }
    Parallel_Manager::info().group_allgatherv(tmp_vector, tmp_size, combine_vector, size_list, displs);
    //MPI_Allgatherv(tmp_vector, tmp_size, MPI_INT, combine_vector, size_list, displs, MPI_INT, MPI_COMM_WORLD);
    vector.clear();
    for(int i=0; i<new_size; i++)
        vector.push_back(combine_vector[i]);
    //MPI_combine::MPI_combine(map, match_sphere_to_basic);

    delete [] tmp_vector;
    delete [] combine_vector;
    delete [] size_list;
    delete [] displs;
#else

#endif

}
