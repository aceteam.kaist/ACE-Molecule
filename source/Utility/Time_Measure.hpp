#pragma once

#include"ACE_Config.hpp"
#ifdef ACE_HAVE_MPI
#include"mpi.h"
#elif defined( ACE_HAVE_OMP)
#include "omp.h"
#else
#include <ctime>
#endif

#include <string>
#include <map>
#include <vector>
#include "Parallel_Manager.hpp"

/**
    @brief This class measures and prints time.
    @detail Time_Measure class measure elapsed time from the moment the start function called to the time the end function is called. Each processor measure the time with thier own timer therefore the elapsed time may slightly different. 
    
*/
class Time_Measure{

    friend std::ostream &operator << (std::ostream &c, const Time_Measure  &occupation);
    friend std::ostream &operator << (std::ostream &c, const Time_Measure* &occupation);

    public:
        /**
            @param[in] use_comm if true, print elapsed times of all processors. Otherwise, elapsed time of master processor will be printed.
        */
        Time_Measure(bool use_comm=true):use_comm_(use_comm){};
        /**
            @brief start measurement of certain tag.
            @param[in] tag name of tag.
            @warning if you call start twice without calling end, last start time is recorded.
        */
        void start(std::string tag);
        /**
            @brief end measurement of certain tag.
            @param[in] tag name of tag.
            @warning if you give undetermined tag, program is intimately dead.
        */
        void end(std::string tag);
        /**
            @brief Print elapsed times for given tag. If tag is not given, elapsed times for all tag are printed.
            @param[in] out pointer of ostream.
            @param[in] tag name of tag.
            @warning if you give undetermined tag, program is intimately dead.
        */
        void print(std::ostream& out, std::string tag=std::string()) const;
        /**
         * @brief Print elapsed times for given tag. If tag is not given, elapsed times for all tag are printed.
         * @param[in] out pointer of ostream.
         * @param[in] order prints the elapsed times for only given order.
         * @param[in] tag name of tag.
         * @warning if you give undetermined tag, program is intimately dead.
         **/
        void print(std::ostream& out, int order, std::string tag=std::string());
        /**
            @brief Get elapsed time.
        */
        double get_elapsed_time(std::string tag, int order);
        double get_elapsed_time(std::string tag);

        /**
         * @brief Get list of measured tags.
         **/
        std::vector<std::string> get_tags();
    private:
        double get_time();
        double get_mean(double time) const;
        /// Decides whether print elapsed time of all processors or it of just master processor.
        const bool use_comm_;
        /// Maps connect name of tag to index of tag.
        std::map<std::string, int> map;
        /// Contains name of all tags.
        std::vector<std::string> tags;
        /// Contains elapsed time of all tags.
        std::vector<std::vector<double> > elapsed_times;
        std::vector<double> start_times;
        std::vector<double> end_times;
        std::vector<int> counts;
};
