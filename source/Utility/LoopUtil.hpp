#pragma once 
#include"Parallel_Manager.hpp"
#include<functional>
#include<array>
#include<vector>

namespace LoopUtil{ 

inline void filter(const int N, std::vector<std::function<bool(int)> > vec_filter_f, 
                         std::vector<std::vector<int> >& indices){
    if (vec_filter_f.size()!= indices.size()){
        std::cout << "error LoopUtil"<<std::endl;
        exit(-1);
    }
    const int M = vec_filter_f.size();
    const int num_threads = Parallel_Manager::info().get_openmp_num_threads();
    std::vector<std::vector<std::vector<int> > > tmp_indices( num_threads, std::vector<std::vector<int> > (M, std::vector<int>() ) );

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel 
    #endif
    {
    const int thread_num = Parallel_Manager::info().get_openmp_thread_num();
    const int start = thread_num*(N/num_threads);
    const int end = thread_num==num_threads-1? N: (thread_num+1)* (N/num_threads);
    for(int i=start; i<end; i++){
        for(int j=0; j<M;j++){
            if (vec_filter_f[j](i)){
                tmp_indices[thread_num][j].push_back(i);
            }
        }
    }
    }
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for  
    #endif 
    for(int j=0; j<M; j++){
        indices[j].clear();
        for(int i =0; i<num_threads; i++){
            indices[j].insert(indices[j].end(), tmp_indices[i][j].begin(), tmp_indices[i][j].end());
        }
    }
    return;
}
template<typename T, int M>
inline void map(const int N, std::array<std::function<T(int)>,M> arr_f, std::array<std::vector<T>,M>& outputs){

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel 
    #endif 
    for(int j=0; j<M;j++){
        outputs[j].clear();
    }
    const int num_threads = Parallel_Manager::info().get_openmp_num_threads();
    std::vector<std::array<std::vector<T>,M> > tmp_outputs( num_threads );
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel 
    {
    const int thread_num = Parallel_Manager::info().get_openmp_thread_num();
    const int start = thread_num*(N/num_threads);
    const int end = thread_num==num_threads-1? N: (thread_num+1)* (N/num_threads);

    for(int i=start; i<end; i++){
        for(int j=0; j<M;j++){
            tmp_outputs[omp_get_thread_num()][j].push_back(arr_f[j](i));
        }
    }
    }
    #endif 

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for  
    #endif 
    for(int j=0; j<M; j++){
        for(int i =0; i<num_threads; i++){
            outputs[j].insert(outputs[j].end(), tmp_outputs[i][j].begin(), tmp_outputs[i][j].end());
        }
    }
    return;
}
}
