#ifdef USE_CUDA
#pragma once 
#include <vector>
//#include "Time_Measure.hpp"

/**
@breif This class performs kernel integration. 
@author Sunghwan Choi

@details 
Here, kernel function is linear combination of Gaussian functions to represent 1/r.
The method, performed here was proposed by Sundholm (JCP 132, 024102, 2010).
Within two-divided regions ([t_i,t_l], [t_l,t_f]), num_points1, num_points2 quadrature points are made, respectively.
Poisson_Solver inherits this class
*/

namespace CUDA_Kernel_Integral{
    int compute_gpu(const int total_size, 
                    const int* points, const double* scaling,
                    double* density, double*& hartree_potential,
                    std::vector<double> w_t,
                    std::vector<double*> F_xs,
                    std::vector<double*> F_ys,
                    std::vector<double*> F_zs);
    
    extern bool gpu_memory_allocated;
    /// device pointer for potential
    extern double* d_potential;
    /// device pointer for density
    extern double* d_density;
    /// temporary memory space for convolution
    extern double** dT_gamma;
    /// temporary memory space for convolution
    extern double** dT_gamma2;
    /// temporary memory space for convolution
    extern double** dT_beta;
    /// temporary memory space for convolution
    extern double** dT_beta2;
    /// temporary memory space for convolution
    extern double** drho;
    // pointer of pointer in Device
    extern double** dFx; extern double** dFy; extern double** dFz;
    // pointer of pointer in Device
    extern double** dFx_dev; extern double** dFy_dev; extern double** dFz_dev;
    // pointer of pointer in Device
    extern double** dT_gamma_dev; extern double** dT_gamma2_dev;
    extern double** dT_beta_dev; extern double** dT_beta2_dev;
    extern double** drho_dev;
    extern int* di_xs; extern int* di_ys; extern int* di_zs;  
    extern int* d_from_basic;        
    __global__ void transpose(const int points_x,const int points_y,const int points_z,
                                double** original_matrices, double** matrices );

    __global__ void unfold( const double weight, const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                double const** __restrict__ original_matrices, double* values );

    __global__ void fold (const int points_x,const int points_y,const int points_z,
                                            const int* di_xs,const int* di_ys,const int* di_zs,
                                            const int* d_from_basic,
                                            const double* d_density, double** d_rho_dev);

};
#endif 
