#pragma once
#include <vector>
#include "Teuchos_RCP.hpp"
#include "Epetra_MultiVector.h"
#include "../../Basis/Basis.hpp"

namespace Spherical_Harmonics{
/**
 * @brief Expand using spherical harmonics.
 * @details Returns \f$ F_L(r) of F(r) = \sum_L Y_L(r) \times F_L(r) \f$.
 * @author Sungwoo Kang
 * @date 2015
 **/
namespace Expansion{
    /**
     * @brief Get spherical harmonics expansion coefficient for given input.
     * @param l Angular momentum of spherical harmonics.
     * @param m Magnetic moment of spherical harmonics.
     * @param basis Basis describing given input.
     * @param values Function to expand.
     * @param index Multivector index of values.
     * @param dest_grid Destination radial grid.
     * @param expand_center Center of expansion. Default: (0, 0, 0).
     * @return Expansion coefficient, \f$ F_L(r) \f$.
     **/
    std::vector<double> expand(
        int l, int m, 
        Teuchos::RCP<const Basis> basis, 
        Teuchos::RCP<Epetra_MultiVector> values, 
        int index, 
        std::vector<double> dest_grid,
        double * expand_center = NULL
    );
};
};

