#include "Bessel.hpp"
#include <stdexcept>
#include <limits>
#include <cmath>

#define __CUTOFF__ 1.0E-20

double Bessel::jn(double x, int l){
    double val = 0.0;
    switch(l){
        case 0:{
            if(x < __CUTOFF__){
                val = 1.0;
            } else {
                val = sin(x)/x;
            }
            break;
        }
        case 1:{
            if(x < __CUTOFF__){
                val = x/3.;
            } else {
                val = sin(x)/x/x - cos(x)/x;
            }
            break;
        }
        case 2:{
            if(x < __CUTOFF__){
                val = x*x/15.;
            } else {
                val = (3./x/x - 1)*sin(x)/x - 3*cos(x)/x/x;
            }
            break;
        }
        case 3:{
            if(x < __CUTOFF__){
                val = x*x*x/105;
            } else {
                double x2 = x*x;
                val = (15./x2 - 6.)*sin(x)/x2 - (15./x2 - 1)*cos(x)/x;
            }
            break;
        }
        case -1:{
            val = cos(x)/x;
            break;
        }
        case -2:{
            val = -cos(x)/x/x-sin(x)/x;
            break;
        }
        case -3:{
            val = (-3./x/x+1)*cos(x)/x-3*sin(x)/x/x;
            break;
        }
        case -4:{
            double x2 = x*x;
            val = (-15./x2 + 6.)*cos(x)/x2 - (15./x2 - 1.)*sin(x)/x;
            break;
        }
        default:{
            throw std::runtime_error("Bessel::jn : Only -5 < l < 4 are implemented!");
            break;
        }
    }
    if(std::isnan(val)){
        val = std::numeric_limits<double>::signaling_NaN();
    }
    return val;
}


double Bessel::yn(double x, int l){
    if(l % 2 == 0){
        return -Bessel::jn(x, -(l+1));
    } else {
        return Bessel::jn(x, l+1);
    }
}
