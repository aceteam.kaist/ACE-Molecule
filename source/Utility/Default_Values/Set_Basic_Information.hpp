#pragma once
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Set_Default_Values.hpp"

namespace Default_Values{
    class Set_Basic_Information: public Set_Default_Values{
        public:
            Set_Basic_Information();
        private:
            int set_parameters(Teuchos::RCP<Teuchos::ParameterList>& total_parameters);
            //int check_parameters(RCP<ParameterList>& parameters);
    };
/*
class BasicInformation_Factory: public Default_Values_Factory{
    public:
        Set_Basic_Information* create();
};
*/
}
