#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"

namespace Default_Values{
    class Set_Default_Values{
        public:
            /**
             * @brief Virtual destructor for abstract class.
             **/
            virtual ~Set_Default_Values(){};
            virtual int set_parameters(Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> >& parameters);
            //virtual int check_parameters(RCP<ParameterList>& parameters)=0;

        protected:
            virtual int set_parameters(Teuchos::RCP<Teuchos::ParameterList>& parameters)=0;
            int call_components(Teuchos::RCP<Teuchos::ParameterList>& parameters);
            static bool use_default;
            Teuchos::Array<Teuchos::RCP<Set_Default_Values> > set_defaults;
            //int construct_set_defaults();
            //std::vector<std::string> node_names;

    };

//template<typename T> Set_Default_Values* createInstance();
//typedef std::map<std::string,Set_Default_Values*(*)()> map_type;
//map_type mapping;

}
