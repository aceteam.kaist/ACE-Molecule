#include "Set_External_Field.hpp"

using Teuchos::RCP;
using Teuchos::ParameterList;

Default_Values::Set_External_Field::Set_External_Field(){
}
int Default_Values::Set_External_Field::set_parameters(RCP<ParameterList>& parameters){
    //////////////// call ////////////////////////////////////
    if(set_default_values!=null){
        set_default_values->set_values(parameters);
    }
    if(parameters->sublist("Sp").isParameter("ExternalField")){
        field=parameters->sublist("Sp").get<string>("ExternalField");
        if(field != "Electric"){
            Verbose::all()<< "Wrong external field - only electric field is supported" << std::endl;
            exit(EXIT_FAILURE);
        }

        type=parameters->sublist("Sp").get<string>("ExternalFieldType");
        if(type != "Static"){
            Verbose::all()<< "Wrong external field type - only static field is supported" << std::endl;
            exit(EXIT_FAILURE);
        }

        field_direction=parameters->sublist("Sp").get<string>("ExternalFieldDirection");
        if(field_direction != "x" && field_direction != "y" && field_direction != "z"){
            Verbose::all()<< "Wrong external field direction - choose x, y, or z" << std::endl;
            exit(EXIT_FAILURE);
        }

        field_strength = parameters->sublist("Sp").get<double>("ExternalFieldStrength");

        true_false = true;
    }
    else{
        true_false = false;
    }
    return 1+call_components(parameters);
}
