#include"Set_Default_Values.hpp"

using Teuchos::RCP;
using Teuchos::ParameterList;
using Teuchos::Array;

int Default_Values::Set_Default_Values::call_components(RCP<ParameterList>& parameters){
    //////////////// call ////////////////////////////////////
    int return_val=0;
    for (int i =0; i<set_defaults.size();i++){
        return_val+=set_defaults[i]->set_parameters(parameters);
    }
    return return_val;
}
/*
int Default_Values::construct_set_defaults(){
    for(int i =0;i<node_names.size();i++){
        set_defaults.append();
    }
    return node_names.size();
}*/

int Default_Values::Set_Default_Values::set_parameters(Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> >& parameters){
    int return_val = 0;
    for(int i = 0; i < parameters.size(); ++i){
        return_val += this -> set_parameters(parameters[i]);
    }
    return return_val;
}

bool Default_Values::Set_Default_Values::use_default;
