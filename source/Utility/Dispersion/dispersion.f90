module dftd3


implicit none

!public approximate_pi_fortran
public dispersion_interaction

private

contains


real (c_double) function dispersion_interaction(rxyz2, atom_num, atom_num_list, inp_functional, f_len ,type_return, i_atom, i_axis) bind (c)
    
    use dftd3_api
    use, intrinsic :: iso_c_binding, only: c_double, c_int
    
    integer, parameter :: wp = kind(1.0d0)
    integer(c_int), value :: atom_num
    integer(c_int), value :: f_len
    integer(c_int), value :: type_return
    integer(c_int), value :: i_atom
    character(len=1), dimension(f_len) :: inp_functional
    integer(c_int), value :: i_axis
    real(c_double), dimension(atom_num*3) ::rxyz2
    !real(wp, dimension(3,atom_num) :: rxyz
    integer(c_int), dimension(atom_num) :: atom_num_list
    integer :: nAtoms
    !integer :: nAtoms = atom_num
    integer :: atnum(atom_num)
    real(wp) :: coords(3, atom_num)
    real(wp) :: edisp
    real(wp) :: grads(3, atom_num), stress(3, 3)
    type(dftd3_input) :: input
    type(dftd3_calc) :: dftd3
    integer :: x, y
    character(LEN=f_len) :: functional

    Do x= 1, f_len
        functional(x:x) = inp_functional(x)
    enddo
    !functional = inp_functional(1:f_len)
    !print *, functional
    nAtoms = atom_num

    Do x =0, 2
        Do y = 0, atom_num-1
            coords(x+1,y+1) = rxyz2(atom_num*x+y+1) !+ ( hgrids(x +1) *(ndims(x+1)-1)/2 )
        enddo
    enddo
    Do x =0, atom_num-1
        atnum(x+1) = atom_num_list(x+1)
    enddo
    ! Initialize dftd3
    call dftd3_init(dftd3, input)

    ! Choose functional. Alternatively you could set the parameters manually
    ! by the dftd3_set_params() function.
    !if(functional .EQ. 1) then
    !call dftd3_set_functional(dftd3, func='dftb3', version=4, tz=.false.)
    !call dftd3_set_functional(dftd3, func=functional, version=4, tz=.false.)
    call dftd3_set_functional(dftd3, func=functional, version=3, tz=.false.)
    !endif

    ! Convert species name to atomic number for each atom
    ! atnum(:) = get_atomic_number(speciesNames(species))

    ! Calculate dispersion and gradients for non-periodic case
    call dftd3_dispersion(dftd3, coords, atnum, edisp, grads)
    !write(*, "(A)") "*** Dispersion for non-periodic case"
    !write(*, "(A,ES20.12)") "Energy [au]:", edisp
    !write(*, "(A)") "Gradients [au]:"
    !write(*, "(3ES20.12)") grads
    if(type_return .EQ. 1) then
        dispersion_interaction = edisp
    else
        dispersion_interaction = grads(i_axis,i_atom)
    endif

    end function 


end module

