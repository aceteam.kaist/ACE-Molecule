#include "Teuchos_RCP.hpp"
#include "../../State/State.hpp"
#include "../../Basis/Basis.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "../../State/State.hpp"
#include "../../Basis/Basis.hpp"
#include "Teuchos_Array.hpp"

#include <cstdlib>
using std::string;
using std::vector;
class Dispersion_Calculation{
    public:
        double dispersion_interaction_c(Teuchos::RCP<State> state, string functional, int type_return, int i_atom, int i_axis);
        vector<double> dispersion_forces(Teuchos::RCP<State> state,string functional, int atom_num);            

};


