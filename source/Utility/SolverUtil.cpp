#ifdef USE_CUDA
#include "cuda_runtime_api.h"
#include "cublas_v2.h"
#include "nvToolsExt.h"
#endif
#ifdef USE_MAGMA
#include "magma.h"
#include "magma_v2.h"      // also includes cublas_v2.h
#include "magma_lapack.h"  // if you need BLAS & LAPACK
#endif
#include "SolverUtil.hpp"
#ifdef USE_MAGMA
#include "magma.h"

extern double* magma_pKK;
extern double *magma_pMM;
#endif
#include "Parallel_Manager.hpp"
#include "Verbose.hpp"

#ifdef USE_SCALAPACK
extern "C" {
    /* Cblacs declarations */
    void Cblacs_pinfo(int*, int*);
    void Cblacs_get(int, int, int*);
    void Cblacs_gridinit(int*, const char*, int, int);
    void Cblacs_gridexit(int);
    void Cblacs_gridinfo(int, int*, int*, int*, int*);
    void Cblacs_exit(int);
    void Cblacs_barrier(int, const char*);
    int numroc_(int*, int*, int*, int*, int*);
    void descinit_(int*, int*, int*, int*, int*, int*, int*,int*, int*, int*);
    void pdsyevd_( char *, char *, int *,
        double *, int *, int *, int *, double *, double *, int *, int *, int *,
        double *, int *, int *, int *, int * );
    void pdsygst_(int*, char*, int*, double*, int*, int*, int*, double*, int*, int*, int*, double*, int*);
    void Cdgsum2d(int, char*, char*, int, int, double*, int, int, int);
    void Cblacs_pcoord(int, int, int*, int*);
    void pdpotrf_(char*, int*, double*, int*, int*, int*, int*);
    void pdtrtri(char*, char*, int*, double*, int*, int*, int*, int*);
    void pdgemm_( char*, char*, int * ,int * ,int * , double * , double* ,int * ,int *, int *,
            double * , int * , int * , int * ,
            double * , double * , int * , int * , int * );
}


#endif

int MAGMA::directSolver(int size, const Teuchos::SerialDenseMatrix<int,double> &KK,
        Teuchos::RCP<const Teuchos::SerialDenseMatrix<int,double> > MM,
        Teuchos::SerialDenseMatrix<int,double> &EV,
        std::vector< typename Teuchos::ScalarTraits<double>::magnitudeType > &theta,
        int &nev, int esType){
//    Verbose::single() <<"00000000000000000000000000000000000000000000"<<std::endl;
//    Verbose::single() <<"size & nev: " << size <<std::endl;
//    Verbose::single() <<"size & nev: " <<   nev<<std::endl;
//    Verbose::single() <<"00000000000000000000000000000000000000000000"<<std::endl;
#ifdef USE_MAGMA
    nvtxRangePushA("direct solver");
    int ret = magma_init();
    if(ret!=MAGMA_SUCCESS)
        std::cout << "error for init" << std::endl;
//    Verbose::single() <<"00000000000000000000000000000000000000000000"<<std::endl;
//    Verbose::single() <<"size & nev: " << size <<std::endl;
//    Verbose::single() <<"size & nev: " <<   nev<<std::endl;
//    Verbose::single() <<"00000000000000000000000000000000000000000000"<<std::endl;
    // Routine for computing the first NEV generalized eigenpairs of the symmetric pencil (KK, MM)
    //
    // Parameter variables:
    //
    // size : Dimension of the eigenproblem (KK, MM)
    //
    // KK : Hermitian "stiffness" matrix
    //
    // MM : Hermitian positive-definite "mass" matrix
    //
    // EV : Matrix to store the nev eigenvectors
    //
    // theta : Array to store the eigenvalues (Size = nev )
    //
    // nev : Number of the smallest eigenvalues requested (input)
    //       Number of the smallest computed eigenvalues (output)
    //       Routine may compute and return more or less eigenvalues than requested.
    //
    // esType : Flag to select the algorithm
    //
    // esType =  0 (default) Uses LAPACK routine (Cholesky factorization of MM)
    //                       with deflation of MM to get orthonormality of
    //                       eigenvectors (S^T MM S = I)
    //
    // esType =  1           Uses LAPACK routine (Cholesky factorization of MM)
    //                       (no check of orthonormality)
    //
    // esType = 10           Uses LAPACK routine for simple eigenproblem on KK
    //                       (MM is not referenced in this case)
    //
    // Note: The code accesses only the upper triangular part of KK and MM.
    //
    // Return the integer info on the status of the computation
    //
    // info = 0 >> Success
    //
    // info < 0 >> error in the info-th argument
    // info = - 20 >> Failure in LAPACK routine

    // Define local arrays

    // Create blas/lapack objects.
    Teuchos::LAPACK<int,double> lapack;
    Teuchos::BLAS<int,double> blas;
    int rank = 0;
    int info = 0;

    if (size < nev || size < 0) {
        return -1;
    }
    if (KK.numCols() < size || KK.numRows() < size) {
        return -2;
    }
    if ((esType == 0 || esType == 1)) {
        if (MM == Teuchos::null) {
            return -3;
        }
        else if (MM->numCols() < size || MM->numRows() < size) {
            return -3;
        }
    }
    if (EV.numCols() < size || EV.numRows() < size) {
        return -4;
    }
    if (theta.size() < (unsigned int) size) {
        return -5;
    }
    if (nev <= 0) {
        return -6;
    }

    // Query LAPACK for the "optimal" block size for HEGV
//    int lwork = 2;  // For HEEV, lwork should be NB+2, instead of NB+1
    std::vector<MagnitudeType> rwork(3*size-2);
    int magma_NB = magma_get_dsytrd_nb(size);
    int lwork = std::max(2*size+size*magma_NB, 1+6*size+2*size*size);
    std::vector<double> work(lwork);
    //work.resize(lwork);


    int liwork = (3 + 5*size)*2;
    std::vector<int> iwork(liwork);
    // tt contains the eigenvalues from HEGV, which are necessarily real, and
    // HEGV expects this vector to be real as well
    std::vector<MagnitudeType> tt( size );
    std::vector<MagnitudeType> tt2( size );
    //typedef typename std::vector<MagnitudeType>::iterator MTIter; // unused

    MagnitudeType tol = SCT::magnitude(SCT::squareroot(SCT::eps()));
    // MagnitudeType tol = 1e-12;
    double zero = Teuchos::ScalarTraits<double>::zero();
    double one = Teuchos::ScalarTraits<double>::one();
    nvtxRangePushA("allocate pinned memory");

    nvtxRangePop();
    Teuchos::RCP<Teuchos::SerialDenseMatrix<int,double> > KKcopy, MMcopy;
    Teuchos::RCP<Teuchos::SerialDenseMatrix<int,double> > U;
    switch (esType) {
        default:
        case 0:
            //
            // Use LAPACK to compute the generalized eigenvectors
            //
            for (rank = size; rank > 0; --rank) {

                U = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>(rank,rank) );
                //
                // Copy KK & MM
                //
                KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, rank, rank ) );
                MMcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, *MM, rank, rank ) );
                //
                // Solve the generalized eigenproblem with LAPACK
                //
                info = 0;
                memcpy(magma_pKK, KKcopy->values(), rank*rank*sizeof(double));
                memcpy(magma_pMM, MMcopy->values(), rank*rank*sizeof(double));
                magma_dsygvd(1, MagmaVec, MagmaUpper, rank, magma_pKK, KKcopy->stride(),
                        magma_pMM, MMcopy->stride(),  &tt[0], &work[0], lwork,
                        &iwork[0], liwork, &info);
                /*
                   magma_dsygvd_m(Parallel_Manager::info().get_ngpu(), 1, MagmaVec, MagmaUpper, rank, magma_pKK, KKcopy->stride(),
                   magma_pMM, MMcopy->stride(),  &tt[0], &work[0], lwork,
                   &iwork[0], liwork, &info);
                 */
                memcpy(KKcopy->values(),magma_pKK,  rank*rank*sizeof(double));
                memcpy(MMcopy->values(),magma_pMM,  rank*rank*sizeof(double));
                /*
                   magma_dsygvd(1, MagmaVec, MagmaUpper, rank, KKcopy->values(), KKcopy->stride(),
                   MMcopy->values(), MMcopy->stride(),  &tt[0], &work[0], lwork,
                   &iwork[0], liwork, &info);
                 */
                /*
                   KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, rank, rank ) );
                   MMcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, *MM, rank, rank ) );
                   lapack.HEGV(1, 'V', 'U', rank, KKcopy->values(), KKcopy->stride(),
                   MMcopy->values(), MMcopy->stride(), &tt[0], &work[0], lwork,
                   &rwork[0], &info);

                   for(int i=0; i<tt.size(); i++)
                   std::cout << tt[i] << std::endl;
                 */

                //
                // Treat error messages
                //
                if (info < 0) {
                    std::cerr << std::endl;
                    std::cerr << "Anasazi::SolverUtils::directSolver(): In HEGV, argument " << -info << "has an illegal value.\n";
                    std::cerr << std::endl;
                    return -20;
                }
                if (info > 0) {
                    if (info > rank)
                        rank = info - rank;
                    continue;
                }
                //
                // Check the quality of eigenvectors ( using mass-orthonormality )
                //
                MMcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, *MM, rank, rank ) );
                for (int i = 0; i < rank; ++i) {
                    for (int j = 0; j < i; ++j) {
                        (*MMcopy)(i,j) = SCT::conjugate((*MM)(j,i));
                    }
                }
                // U = 0*U + 1*MMcopy*KKcopy = MMcopy * KKcopy
                TEUCHOS_TEST_FOR_EXCEPTION(
                        U->multiply(Teuchos::NO_TRANS,Teuchos::NO_TRANS,one,*MMcopy,*KKcopy,zero) != 0,
                        std::logic_error, "Anasazi::SolverUtils::directSolver() call to Teuchos::SerialDenseMatrix::multiply() returned an error.");
                // MMcopy = 0*MMcopy + 1*KKcopy^H*U = KKcopy^H * MMcopy * KKcopy
                TEUCHOS_TEST_FOR_EXCEPTION(
                        MMcopy->multiply(Teuchos::CONJ_TRANS,Teuchos::NO_TRANS,one,*KKcopy,*U,zero) != 0,
                        std::logic_error, "Anasazi::SolverUtils::directSolver() call to Teuchos::SerialDenseMatrix::multiply() returned an error.");
                MagnitudeType maxNorm = SCT::magnitude(zero);
                MagnitudeType maxOrth = SCT::magnitude(zero);
                for (int i = 0; i < rank; ++i) {
                    for (int j = i; j < rank; ++j) {
                        if (j == i)
                            maxNorm = SCT::magnitude((*MMcopy)(i,j) - one) > maxNorm
                                ? SCT::magnitude((*MMcopy)(i,j) - one) : maxNorm;
                        else
                            maxOrth = SCT::magnitude((*MMcopy)(i,j)) > maxOrth
                                ? SCT::magnitude((*MMcopy)(i,j)) : maxOrth;
                    }
                }
                /*        if (verbose > 4) {
                          std::cout << " >> Local eigensolve >> Size: " << rank;
                          std::cout.precision(2);
                          /std::cout.setf(std::ios::scientific, std::ios::floatfield);
                          std::cout << " Normalization error: " << maxNorm;
                          std::cout << " Orthogonality error: " << maxOrth;
                          std::cout << endl;
                          }*/
                if ((maxNorm <= tol) && (maxOrth <= tol)) {
                    break;
                }
            } // for (rank = size; rank > 0; --rank)
            //
            // Copy the computed eigenvectors and eigenvalues
            // ( they may be less than the number requested because of deflation )
            //
            // std::cout << "directSolve    rank: " << rank << "\tsize: " << size << endl;
            nev = (rank < nev) ? rank : nev;
            EV.putScalar( zero );
            std::copy(tt.begin(),tt.begin()+nev,theta.begin());
            for (int i = 0; i < nev; ++i) {
                blas.COPY( size, &magma_pKK[i*size], 1, EV[i], 1 );
            }
            /*
               for (int i = 0; i < nev; ++i) {
               blas.COPY( rank, (*KKcopy)[i], 1, EV[i], 1 );
               }
             */
            break;

        case 1:
            //
            // Use the Cholesky factorization of MM to compute the generalized eigenvectors
            //
            // Copy KK & MM
            //
            KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, size, size ) );
            MMcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, *MM, size, size ) );
            //
            // Solve the generalized eigenproblem with LAPACK
            //exit(-1);
            //
            info = 0;
            memcpy(magma_pKK, KK.values(), KK.numCols()*KK.numRows()*sizeof(double));
            memcpy(magma_pMM, MM->values(), MM->numCols()*MM->numRows()*sizeof(double));
            magma_dsygvd(1, MagmaVec, MagmaUpper, size, magma_pKK, KKcopy->stride(),
                    magma_pMM, MMcopy->stride(),  &tt[0], &work[0], lwork,
                    &iwork[0], liwork, &info);
            /*
               magma_dsygvd_m(Parallel_Manager::info().get_ngpu(),1, MagmaVec, MagmaUpper, size, magma_pKK, KKcopy->stride(),
               magma_pMM, MMcopy->stride(),  &tt[0], &work[0], lwork,
               &iwork[0], liwork, &info);
             */
            /*
               magma_dsygvd(1, MagmaVec, MagmaUpper, size, KKcopy->values(), KKcopy->stride(),
               MMcopy->values(), MMcopy->stride(),  &tt[0], &work[0], lwork,
               &iwork[0], liwork, &info);
             */

            // Treat error messages
            //
            //
            if (info < 0) {
                std::cerr << std::endl;
                std::cerr << "Anasazi::SolverUtils::directSolver(): In HEGV, argument " << -info << "has an illegal value.\n";
                std::cerr << std::endl;
                return -20;
            }
            if (info > 0) {
                if (info > size)
                    nev = 0;
                else {
                    std::cerr << std::endl;
                    std::cerr << "Anasazi::SolverUtils::directSolver(): In HEGV, DPOTRF or DHEEV returned an error code (" << info << ").\n";
                    std::cerr << std::endl;
                    return -20;
                }
            }
            //
            // Copy the eigenvectors and eigenvalues
            //
            std::copy(tt.begin(),tt.begin()+nev,theta.begin());
            for (int i = 0; i < nev; ++i) {
                blas.COPY( size, &magma_pKK[i*size], 1, EV[i], 1 );
            }

            /*
               std::copy(tt.begin(),tt.begin()+nev,theta.begin());
               for (int i = 0; i < nev; ++i) {
               blas.COPY( size, (*KKcopy)[i], 1, EV[i], 1 );
               }
             */
            break;

        case 10:
            //
            // Simple eigenproblem
            //
            // Copy KK
            //
            KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, size, size ) );
            //
            // Solve the generalized eigenproblem with LAPACK
            //
            magma_dsygvd(1, MagmaVec, MagmaUpper, size, KKcopy->values(), KKcopy->stride(),
                    MMcopy->values(), MMcopy->stride(),  &tt[0], &work[0], lwork,
                    &iwork[0], liwork, &info);
            //
            // Treat error messages
            if (info != 0) {
                std::cerr << std::endl;
                if (info < 0) {
                    std::cerr << "Anasazi::SolverUtils::directSolver(): In DHEEV, argument " << -info << " has an illegal value\n";
                }
                else {
                    std::cerr << "Anasazi::SolverUtils::directSolver(): In DHEEV, the algorithm failed to converge (" << info << ").\n";
                }
                std::cerr << std::endl;
                info = -20;
                break;
            }
            //
            // Copy the eigenvectors
            //
            std::copy(tt.begin(),tt.begin()+nev,theta.begin());
            for (int i = 0; i < nev; ++i) {
                blas.COPY( size, (*KKcopy)[i], 1, EV[i], 1 );
            }
            break;
    }
    nvtxRangePushA("allocate pinned memory");
    nvtxRangePop();
    magma_finalize();
    nvtxRangePop();
    return info;
#else
    std::cout << "To use this function, you must compile the code with magma enable" << std::endl;
    return 0;
#endif
}

int SCALAPACK::directSolver(int size, const Teuchos::SerialDenseMatrix<int,double> &KK,
        Teuchos::RCP<const Teuchos::SerialDenseMatrix<int,double> > MM,
        Teuchos::SerialDenseMatrix<int,double> &EV,
        std::vector< typename Teuchos::ScalarTraits<double>::magnitudeType > &theta,
        int &nev, int esType){
#ifdef USE_SCALAPACK
//    Verbose::single() << "size : " << size << std::endl;
    double time1, time2;
    time1 = MPI_Wtime();
    // Routine for computing the first NEV generalized eigenpairs of the symmetric pencil (KK, MM)
    //
    // Parameter variables:
    //
    // size : Dimension of the eigenproblem (KK, MM)
    //
    // KK : Hermitian "stiffness" matrix
    //
    // MM : Hermitian positive-definite "mass" matrix
    //
    // EV : Matrix to store the nev eigenvectors
    //
    // theta : Array to store the eigenvalues (Size = nev )
    //
    // nev : Number of the smallest eigenvalues requested (input)
    //       Number of the smallest computed eigenvalues (output)
    //       Routine may compute and return more or less eigenvalues than requested.
    //
    // esType : Flag to select the algorithm
    //
    // esType =  0 (default) Uses LAPACK routine (Cholesky factorization of MM)
    //                       with deflation of MM to get orthonormality of
    //                       eigenvectors (S^T MM S = I)
    //
    // esType =  1           Uses LAPACK routine (Cholesky factorization of MM)
    //                       (no check of orthonormality)
    //
    // esType = 10           Uses LAPACK routine for simple eigenproblem on KK
    //                       (MM is not referenced in this case)
    //
    // Note: The code accesses only the upper triangular part of KK and MM.
    //
    // Return the integer info on the status of the computation
    //
    // info = 0 >> Success
    //
    // info < 0 >> error in the info-th argument
    // info = - 20 >> Failure in LAPACK routine

    // Define local arrays

    // Create blas/lapack objects.
    Teuchos::LAPACK<int,double> lapack;
    Teuchos::BLAS<int,double> blas;

    int rank = 0;
    int info = 0;

    if (size < nev || size < 0) {
        return -1;
    }
    if (KK.numCols() < size || KK.numRows() < size) {
        return -2;
    }
    if ((esType == 0 || esType == 1)) {
        if (MM == Teuchos::null) {
            return -3;
        }
        else if (MM->numCols() < size || MM->numRows() < size) {
            return -3;
        }
    }
    if (EV.numCols() < size || EV.numRows() < size) {
        return -4;
    }
    if (theta.size() < (unsigned int) size) {
        return -5;
    }
    if (nev <= 0) {
        return -6;
    }

    // Query LAPACK for the "optimal" block size for HEGV
    std::string lapack_name = "hetrd";
    std::string lapack_opts = "u";
    int NB = lapack.ILAENV(1, lapack_name, lapack_opts, size, -1, -1, -1);
    int lwork = size*(NB+2);  // For HEEV, lwork should be NB+2, instead of NB+1
    int liwork = size*(NB+2);  // For HEEV, lwork should be NB+2, instead of NB+1
    std::vector<double> work(lwork);
    std::vector<int> iwork(liwork);
    std::vector<MagnitudeType> rwork(3*size-2);
    std::vector<double> W;
    // tt contains the eigenvalues from HEGV, which are necessarily real, and
    // HEGV expects this vector to be real as well
    std::vector<MagnitudeType> tt( size );
    //typedef typename std::vector<MagnitudeType>::iterator MTIter; // unused

    MagnitudeType tol = SCT::magnitude(SCT::squareroot(SCT::eps()));
    // MagnitudeType tol = 1e-12;
    double zero = Teuchos::ScalarTraits<double>::zero();
    double one = Teuchos::ScalarTraits<double>::one();

    Teuchos::RCP<Teuchos::SerialDenseMatrix<int,double> > KKcopy, MMcopy;
    Teuchos::RCP<Teuchos::SerialDenseMatrix<int,double> > KKcopy2, MMcopy2;
    Teuchos::RCP<Teuchos::SerialDenseMatrix<int,double> > U;
    int iam, nprocs;
    int myrank_mpi, nprocs_mpi;
    int ictxt, nprow, npcol, myrow, mycol;
    int np, nq, nb, n;
    int mpA, nqA;
    int itemp, seed, min_mn;
    int descA[9], descZ[9], descB[9];
    double *Z ;
    int izero=0,ione=1;
    double mone=(-1.0e0),pone=(1.0e0),dzero=(0.0e0);
    int check = 0;
    char uplo='U';
    char jobz = 'V';
    char diag='N';
    char trans='N';
    nb=64;
    nprocs_mpi = Parallel_Manager::info().get_total_mpi_size();
    nprow = 1;
    npcol = nprocs_mpi;
    int tmp1, tmp2;
    for(int i=2; i<nprocs_mpi; i++){
        if(nprocs_mpi%i==0){
            tmp1=i;
            tmp2=nprocs_mpi/i;
            if(abs(tmp2-tmp1) < abs(npcol-nprow)){
                nprow = tmp1;
                npcol = tmp2;
            }
        }
    }

    Cblacs_pinfo( &iam, &nprocs ) ;
    Cblacs_get( -1, 0, &ictxt );
    Cblacs_gridinit( &ictxt, "Row", nprow, npcol );
    Cblacs_gridinfo( ictxt, &nprow, &npcol, &myrow, &mycol );
    if ((myrow >= nprow) or (mycol >= npcol)){
        std::cout << "matrix size is two small" << std::endl;
        exit(-1);
    }
    /*
       Parallel_Manager::info().all_barrier();
     */
    //end fo initialization for scalapack routine
    switch (esType) {
        default:
        case 0:
            //
            // Use LAPACK to compute the generalized eigenvectors
            //
            for (rank = size; rank > 0; --rank) {

                //
                // Copy KK & MM
                //
                KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, rank, rank ) );
                MMcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, *MM, rank, rank ) );

                n =rank;
                if(nb>n)
                    nb=n/3;

                mpA    = numroc_( &n     , &nb, &myrow, &izero, &nprow );
                nqA    = numroc_( &n     , &nb, &mycol, &izero, &npcol );

                double* local_KK=new double[mpA*nqA];
                double* local_MM=new double[mpA*nqA];
                double* Z=new double[mpA*nqA];
                int x, y;
                itemp = std::max( 1, mpA );
                descinit_( descA,  &n, &n, &nb, &nb, &izero, &izero, &ictxt, &itemp, &info );
                descinit_( descB,  &n, &n, &nb, &nb, &izero, &izero, &ictxt, &itemp, &info );
                descinit_( descZ,  &n, &n, &nb, &nb, &izero, &izero, &ictxt, &itemp, &info );

                scatter(local_KK, KKcopy->values(), n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA  );
                scatter(local_MM, MMcopy->values(), n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA  );
                KKcopy = Teuchos::null;
                MMcopy = Teuchos::null;

                pdpotrf_(&uplo, &n, local_MM, &ione, &ione, descB, &info);
                 if(info!=0)
                    Verbose::single(Verbose::Detail) << "pdpotrf_" <<rank << std::endl;
                double scale;

                pdsygst_(&ione, &uplo, &n, local_KK, &ione, &ione, descA, local_MM, &ione, &ione, descB, &scale, &info );
                if(info!=0)
                    Verbose::single(Verbose::Detail) << "pdsygst_" <<rank << std::endl;
                int trilwmin = 3*n+std::max(nb*(mpA+1), 3*nb);
                lwork = 2*std::max(1+6*n+2*nqA*mpA, trilwmin)+2*n;
                liwork = 7*n+8*npcol+2;
                work.clear();
                iwork.clear();
                work.resize(lwork) ;
                iwork.resize(liwork);
                W.resize(n);

                pdsyevd_( &jobz, &uplo, &n, local_KK, &ione, &ione, descA, &W[0], Z, &ione,                                                                                    &ione, descZ, &work[0], &lwork, &iwork[0], &liwork, &info );
                Verbose::single(Verbose::Detail) << "end of pdsyevd" << std::endl;

                work.clear();
                iwork.clear();
                if(info!=0)
                    Verbose::single(Verbose::Detail) << "pdsyevd_" <<rank << " " << info << std::endl;
                pdtrtri(&uplo, &diag, &n, local_MM, &ione, &ione, descB, &info);
                if(info!=0)
                    Verbose::single(Verbose::Detail) << "pdtrtri" <<rank << " " << info << std::endl;
                make_upper(local_MM, n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA  );
                pdgemm_( &trans, &trans, &n, &n, &n, &pone,  local_MM, &ione, &ione, descB,
                        Z, &ione, &ione, descZ, &dzero, local_KK, &ione, &ione, descA);
                KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, rank, rank ) );
                gather(local_KK, KKcopy->values(), n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA, ictxt  );

                if(info!=0){
                    Verbose::all() << "Error of directsolver, EsType : " << esType << ", info : " << info << std::endl;
                    exit(-1);
                }
                delete [] local_KK;
                delete [] local_MM;
                delete [] Z;
                //jaechang
                /*
                MMcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, *MM, rank, rank ) );
                for (int i = 0; i < rank; ++i) {
                    for (int j = 0; j < i; ++j) {
                        (*MMcopy)(i,j) = SCT::conjugate((*MM)(j,i));
                    }
                }
                // U = 0*U + 1*MMcopy*KKcopy = MMcopy * KKcopy
                U = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>(rank,rank) );
                TEUCHOS_TEST_FOR_EXCEPTION(
                        U->multiply(Teuchos::NO_TRANS,Teuchos::NO_TRANS,one,*MMcopy,*KKcopy,zero) != 0,
                        std::logic_error, "Anasazi::SolverUtils::directSolver() call to Teuchos::SerialDenseMatrix::multiply() returned an error.");
                // MMcopy = 0*MMcopy + 1*KKcopy^H*U = KKcopy^H * MMcopy * KKcopy
                TEUCHOS_TEST_FOR_EXCEPTION(
                        MMcopy->multiply(Teuchos::CONJ_TRANS,Teuchos::NO_TRANS,one,*KKcopy,*U,zero) != 0,
                        std::logic_error, "Anasazi::SolverUtils::directSolver() call to Teuchos::SerialDenseMatrix::multiply() returned an error.");
                MagnitudeType maxNorm = SCT::magnitude(zero);
                MagnitudeType maxOrth = SCT::magnitude(zero);
                for (int i = 0; i < rank; ++i) {
                    for (int j = i; j < rank; ++j) {
                        if (j == i){
                            maxNorm = SCT::magnitude((*MMcopy)(i,j) - one) > maxNorm
                                ? SCT::magnitude((*MMcopy)(i,j) - one) : maxNorm;

                        }
                        else{
                            maxOrth = SCT::magnitude((*MMcopy)(i,j)) > maxOrth
                                ? SCT::magnitude((*MMcopy)(i,j)) : maxOrth;
                        }
                    }
                }

                if ((maxNorm <= tol) && (maxOrth <= tol)) {
                    break;
                }

                */
                break;
            } // for (rank = size; rank > 0; --rank)
            //
            // Copy the computed eigenvectors and eigenvalues
            // ( they may be less than the number requested because of deflation )
            //
            // std::cout << "directSolve    rank: " << rank << "\tsize: " << size << endl;
            nev = (rank < nev) ? rank : nev;
            EV.putScalar( zero );
            std::copy(W.begin(),W.begin()+nev,theta.begin());
            for (int i = 0; i < nev; ++i) {
                blas.COPY( rank, (*KKcopy)[i], 1, EV[i], 1 );
            }
            break;

        case 1:
            KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, size, size ) );
            MMcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, *MM, size, size ) );
            n =size;
            if(nb>n)
                nb=n/3;
            mpA    = numroc_( &n     , &nb, &myrow, &izero, &nprow );
            nqA    = numroc_( &n     , &nb, &mycol, &izero, &npcol );

            double* local_KK=new double[mpA*nqA];
            double* local_MM=new double[mpA*nqA];
            double* Z=new double[mpA*nqA];
            int x, y;
            itemp = std::max( 1, mpA );
            descinit_( descA,  &n, &n, &nb, &nb, &izero, &izero, &ictxt, &itemp, &info );
            descinit_( descB,  &n, &n, &nb, &nb, &izero, &izero, &ictxt, &itemp, &info );
            descinit_( descZ,  &n, &n, &nb, &nb, &izero, &izero, &ictxt, &itemp, &info );

            scatter(local_KK, KKcopy->values(), n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA  );
            scatter(local_MM, MMcopy->values(), n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA  );
            KKcopy = Teuchos::null;
            MMcopy = Teuchos::null;

            pdpotrf_(&uplo, &n, local_MM, &ione, &ione, descB, &info);
            double scale;
            pdsygst_(&ione, &uplo, &n, local_KK, &ione, &ione, descA, local_MM, &ione, &ione, descB, &scale, &info );
            int trilwmin = 3*n+std::max(nb*(mpA+1), 3*nb);
            lwork = 2*std::max(1+6*n+2*nqA*mpA, trilwmin)+2*n;
            liwork = 7*n+8*npcol+2;
            iwork.clear();
            work.clear();
            work.resize(lwork) ;
            iwork.resize(liwork);
            W.resize(n);
            pdsyevd_( &jobz, &uplo, &n, local_KK, &ione, &ione, descA, &W[0], Z, &ione,                                                                                    &ione, descZ, &work[0], &lwork, &iwork[0], &liwork, &info );
            work.clear();
            iwork.clear();
            pdtrtri(&uplo, &diag, &n, local_MM, &ione, &ione, descB, &info);
            make_upper(local_MM, n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA  );
            pdgemm_( &trans, &trans, &n, &n, &n, &pone,  local_MM, &ione, &ione, descB,
                    Z, &ione, &ione, descZ, &dzero, local_KK, &ione, &ione, descA);
            KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, size, size ) );
            gather(Z, KKcopy->values(), n, nb, nprow, npcol, myrow, mycol, nprocs, mpA, nqA, ictxt  );

            if(info!=0){
                Verbose::all() << "Error of directsolver, EsType : " << esType << ", info : " << info << std::endl;
                exit(-1);
            }
            EV.putScalar( zero );
            std::copy(W.begin(),W.begin()+nev,theta.begin());
            for (int i = 0; i < nev; ++i) {
                blas.COPY( size, (*KKcopy)[i], 1, EV[i], 1 );
            }
            delete [] local_KK;
            delete [] local_MM;
            delete [] Z;
            break;
        case 10:
            //
            // Simple eigenproblem
            //
            // Copy KK
            //
            KKcopy = Teuchos::rcp( new Teuchos::SerialDenseMatrix<int,double>( Teuchos::Copy, KK, size, size ) );
            //
            // Solve the generalized eigenproblem with LAPACK
            //
            lapack.HEEV('V', 'U', size, KKcopy->values(), KKcopy->stride(), &tt[0], &work[0], lwork, &rwork[0], &info);
            //
            // Treat error messages
            if (info != 0) {
                std::cerr << std::endl;
                if (info < 0) {
                    std::cerr << "Anasazi::SolverUtils::directSolver(): In DHEEV, argument " << -info << " has an illegal value\n";
                }
                else {
                    std::cerr << "Anasazi::SolverUtils::directSolver(): In DHEEV, the algorithm failed to converge (" << info << ").\n";
                }
                std::cerr << std::endl;
                info = -20;
                break;
            }
            //
            // Copy the eigenvectors
            //
            std::copy(tt.begin(),tt.begin()+nev,theta.begin());
            for (int i = 0; i < nev; ++i) {
                blas.COPY( size, (*KKcopy)[i], 1, EV[i], 1 );
            }
            break;
    }
    Cblacs_gridexit( 0 );
    time2 = MPI_Wtime();
    Verbose::single(Verbose::Normal) << "SCALAPACK::DirectSolver Direct diagoanlize : " << time2-time1 << std::endl;
    return info;

#else
    std::cout << "To use this function, you must compile the code with scalarpack enable" << std::endl;
    return 0;
#endif
}

void SCALAPACK::gather(double* local, double* global, int n, int nb, int nprow, int npcol, int myrow, int mycol, int nprocs, int mpA, int nqA, int ictxt){
#ifdef USE_SCALAPACK
    memset(global, 0.0, sizeof(double)*n*n);
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    for (int r = 0; r < n; r += nb, sendr=(sendr+1)%nprow) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int nr = nb;
        if (n-r < nb)
            nr = n-r;

        for (int c = 0; c < n; c += nb, sendc=(sendc+1)%npcol) {
            // Number of cols to be sent
            // Is this the last col block?
            int nc = nb;
            if (n-c < nb)
                nc = n-c;

            if (myrow == sendr && mycol == sendc) {
                for(int i=0; i<nr; i++){
                    for(int j=0; j<nc; j++){
                        global[n*(c+j)+r+i]=local[mpA*(recvc+j)+recvr+i] ;
                    }
                }
                recvc = (recvc+nc)%nqA;
            }
        }

        if (myrow == sendr)
            recvr = (recvr+nr)%mpA;
    }

    Cdgsum2d(ictxt, "all", " ", n, n, global, n, -1,  -1);
    return;
#else
    std::cout << "To use this function, you must compile the code with scalarpack enable" << std::endl;
    return ;
#endif
}

void SCALAPACK::scatter(double* local, double* global, int n, int nb, int nprow, int npcol, int myrow, int mycol, int nprocs, int mpA, int nqA){
#ifdef USE_SCALAPACK
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    for (int r = 0; r < n; r += nb, sendr=(sendr+1)%nprow) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int nr = nb;
        if (n-r < nb)
            nr = n-r;

        for (int c = 0; c < n; c += nb, sendc=(sendc+1)%npcol) {
            // Number of cols to be sent
            // Is this the last col block?
            int nc = nb;
            if (n-c < nb)
                nc = n-c;
            if (myrow == sendr && mycol == sendc) {
                for(int i=0; i<nr; i++){
                    for(int j=0; j<nc; j++){
                        local[mpA*(recvc+j)+recvr+i] = global[n*(c+j)+r+i];
                    }
                }
                recvc = (recvc+nc)%nqA;
            }

        }

        if (myrow == sendr)
            recvr = (recvr+nr)%mpA;
    }
#else
    std::cout << "To use this function, you must compile the code with scalarpack enable" << std::endl;
    return ;
#endif
}
void SCALAPACK::make_upper(double* local, int n, int nb, int nprow, int npcol, int myrow, int mycol, int nprocs, int mpA, int nqA){
#ifdef USE_SCALAPACK
    int sendr = 0, sendc = 0, recvr = 0, recvc = 0;
    for (int r = 0; r < n; r += nb, sendr=(sendr+1)%nprow) {
        sendc = 0;
        // Number of rows to be sent
        // Is this the last row block?
        int nr = nb;
        if (n-r < nb)
            nr = n-r;

        for (int c = 0; c < n; c += nb, sendc=(sendc+1)%npcol) {
            // Number of cols to be sent
            // Is this the last col block?
            int nc = nb;
            if (n-c < nb)
                nc = n-c;
            if (myrow == sendr && mycol == sendc) {
                for(int i=0; i<nr; i++){
                    for(int j=0; j<nc; j++){
                        if(c+j<r+i)
                            local[mpA*(recvc+j)+recvr+i] = 0.0;
                    }
                }
                recvc = (recvc+nc)%nqA;
            }

        }

        if (myrow == sendr)
            recvr = (recvr+nr)%mpA;
    }
#else
    std::cout << "To use this function, you must compile the code with scalarpack enable" << std::endl;
    return ;
#endif
}
/*
   void SCALAPACK::divisor(int* nprow, int* npcol){
   int tmp1, tmp2;
 *nprow = 1;
 *npcol = nprocs;
 for(int i=0; i<nprocs; i++){
 if(nprocs%i==0){
 tmp1=i;
 tmp2=nprocs/i;
 }
 if(tmp2-tmp1 < *npcol-*nprow){
 *nprow = tmp1;
 *npcol = tmp2;
 }
 else
 break;
 }
 }
 */
/*
   void SCALAPACK::initializer(int n, int& iam, int& nprocs, int& myrank_mpi, int& nprocs_mpi, int& ictxt, int& nprow, int& npcol, int& myrow, int& mycol, int& np, int& nq, int& nb, int& mpA, int& nqA, int* descA, int* descZ, int* descB) const{

   Verbose::single() << "nprocs_mpi\t" << nprocs_mpi << std::endl;
   Verbose::single() << "nb\t" << nb << std::endl;
   Verbose::single() << "n\t" << n << std::endl;
   Verbose::single() << "nprow\t" << nprow << std::endl;
   Verbose::single() << "npcol\t" << npcol << std::endl;
   Verbose::single() << "mpA\t" << mpA << std::endl;
   Verbose::single() << "nqA\t" << nqA << std::endl;
   Parallel_Manager::info().all_barrier();
//end fo initialization for scalapack routine
}
 */
