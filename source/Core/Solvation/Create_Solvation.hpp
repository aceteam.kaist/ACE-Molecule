#pragma once
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Interface_PCMSolver.hpp"

namespace Create_Solvation{
        Teuchos::RCP<Solvation> Create_Solvation(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Teuchos::ParameterList> parameters);
};
