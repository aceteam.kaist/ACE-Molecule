#ifdef ACE_PCMSOLVER
#include "Interface_PCMSolver.hpp"
#include <vector>
#include <array>
#include <string>
#include <cmath>
#include <algorithm>
#include <cmath>
#include "Epetra_Map.h"

#include "../../Io/Atoms.hpp"
#include "../../Utility/Interpolation/Tricubic_Interpolation.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Manager.hpp"

using std::vector;
using std::array;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

void host_writer(const char * message){Verbose::single(Verbose::Simple) << message << std::endl;}

Interface_PCMSolver::Interface_PCMSolver(Teuchos::RCP<const Basis> basis, 
                                         string solver_type, 
                                         PCMInput pcm_input, 
                                         double charge_width/* = 1.0*/, 
                                         bool allow_nonequilibrium/* = false*/)
{

    this -> basis = basis;
    this -> host_input = pcm_input;
    this -> input_filename = "";
    this -> allow_nonequilibrium = allow_nonequilibrium;
    this -> alpha = charge_width;
    this -> timer = rcp(new Time_Measure());
}

Interface_PCMSolver::Interface_PCMSolver(Teuchos::RCP<const Basis> basis, 
                                         string input_filename, 
                                         double charge_width/* = 1.0*/, 
                                         bool allow_nonequilibrium/* = false*/)
{
    this -> basis = basis;
    this -> input_filename = input_filename;
    this -> allow_nonequilibrium = allow_nonequilibrium;
    this -> alpha = charge_width;
    this -> timer = rcp(new Time_Measure());
}

Interface_PCMSolver::~Interface_PCMSolver(){
    delete[] this -> grid;
    delete[] this -> areas;

    Parallel_Manager::info().all_barrier();
    if(Parallel_Manager::info().get_mpi_rank()==0 or true){
        pcmsolver_delete(this -> pcm_context);
    }
    this -> timer -> print(Verbose::single(Verbose::Normal));
}
void Interface_PCMSolver::initialize(RCP<const Atoms> atoms,  std::vector<double> mol_charges){
    this -> pseudo_charges = vector<double>(atoms -> get_size());
    // If compensation charge is used, hartree potential contains nuclear charge.
    // Therefore, pseudo charges are left zero.
    for(int i = 0; i < atoms -> get_size(); i++){
        pseudo_charges[i] = mol_charges.at(i);
    }

    vector< array<double,3> > mol_coordinates = atoms -> get_positions();

    const int nuc_size = atoms -> get_size();
    double * atom_numbers = new double[nuc_size];
    double * coordinates = new double[nuc_size*3];

    for(int i = 0; i < nuc_size; ++i){
        atom_numbers[i] = atoms -> get_atomic_numbers()[i];
        for(int j = 0; j < 3; ++j){
            coordinates[3*i+j] = mol_coordinates[i][j];
        }
    }

    // C1 is fine.
    int symmetry_info[4] = {0,0,0,0};

    this -> timer -> start("Interface_PCMSolver::init::grid initialization");
    if(Parallel_Manager::info().get_mpi_rank()==0){
        if (!pcmsolver_is_compatible_library()) {
            Verbose::all() << "PCMSolver library not compatible" << std::endl;
            exit(EXIT_FAILURE);
        }
        if(this -> input_filename.size() == 0){
            this -> pcm_context = pcmsolver_new(PCMSOLVER_READER_HOST, nuc_size, atom_numbers, coordinates, symmetry_info, &this -> host_input, host_writer);
        } else {
            this -> pcm_context = pcmsolver_new_v1112(PCMSOLVER_READER_OWN, nuc_size, atom_numbers, coordinates, symmetry_info, this -> input_filename.c_str(), NULL, host_writer);
        }
        this -> grid_size = pcmsolver_get_cavity_size(pcm_context);
        pcmsolver_print(this -> pcm_context);
    }

    delete[] coordinates;
    delete[] atom_numbers;

    Parallel_Manager::info().group_barrier();
    Parallel_Manager::info().group_bcast(&this -> grid_size, 1, 0);

    this -> grid = new double[3 * this -> grid_size]();
    this -> areas = new double[this -> grid_size]();

    if(Parallel_Manager::info().get_mpi_rank() == 0){
        pcmsolver_get_centers(this -> pcm_context, this -> grid);
        pcmsolver_get_areas(this -> pcm_context, this -> areas);
    }

    Parallel_Manager::info().all_barrier();
    Parallel_Manager::info().group_bcast(this -> areas, this -> grid_size, 0);
    Parallel_Manager::info().group_bcast(this -> grid, 3*this -> grid_size, 0);

    this -> timer -> end("Interface_PCMSolver::init::grid initialization");
}

RCP<const Epetra_Vector> Interface_PCMSolver::get_PCM_local_potential(){
        return PCM_local_potential;
}

std::vector<double> Interface_PCMSolver::interpolate_mep(RCP<Epetra_Vector> ia_potential){
    this -> timer -> start("PCMSolver::interpolate_mep");
    double * ia_mep = electron_mep(basis, ia_potential, this -> grid_size, this -> grid);
    for(int i = 0; i < this -> grid_size; i++){
        ia_mep[i] = -ia_mep[i];
    }

    this -> timer -> end("PCMSolver::interpolate_mep");
    vector<double> mep(ia_mep, ia_mep + this -> grid_size);
    delete[] ia_mep;
    //this -> timer -> print(Verbose::single(Verbose::Simple), -1);
    return mep;
}

std::vector<double> Interface_PCMSolver::interpolate_mep(double* ia_potential){
    this -> timer -> start("PCMSolver::interpolate_mep");

    double * ia_mep = electron_mep(basis, ia_potential, this -> grid_size, this -> grid);
    for(int i = 0; i < this -> grid_size; i++){
        ia_mep[i] = -ia_mep[i];
    }

    this -> timer -> end("PCMSolver::interpolate_mep");
    vector<double> mep(ia_mep, ia_mep + this -> grid_size);
    delete[] ia_mep;
    //this -> timer -> print(Verbose::single(Verbose::Simple), -1);
    return mep;
}
double Interface_PCMSolver::compute_polarization_from_mep_allproc(std::vector<double> ia_mep, std::vector<double> jb_mep){
    if(this -> pcm_context == NULL){
        Verbose::all() << "Interface_PCMSolver::initialize is not called!" << std::endl;
        throw std::logic_error("Interface_PCMSolver::initialize is not called! (wrong code)");
    }
    double * jb_asc = new double[grid_size]();
    double energy = 0.0;
    if(this -> pcm_context != NULL){
        const char * jb_mep_lbl = "jbMEP";
        // Set MEP into the PCMSolver.
        pcmsolver_set_surface_function(this -> pcm_context, this -> grid_size, jb_mep.data(), jb_mep_lbl);

        const char * ia_mep_lbl = "iaMEP";
        pcmsolver_set_surface_function(this -> pcm_context, this -> grid_size, ia_mep.data(), ia_mep_lbl);

        const char * jb_asc_lbl = "jbASC";
        int irrep = 0;
        if(this->allow_nonequilibrium){
            pcmsolver_compute_response_asc(this -> pcm_context, jb_mep_lbl, jb_asc_lbl, irrep);
        } else {
            pcmsolver_compute_asc(this -> pcm_context, jb_mep_lbl, jb_asc_lbl, irrep);
        }
        //pcmsolver_get_surface_function(this -> pcm_context, this -> grid_size, jb_asc, jb_asc_lbl);
        energy = 2 * pcmsolver_compute_polarization_energy(this -> pcm_context, ia_mep_lbl, jb_asc_lbl);
    }

    delete[] jb_asc;
    return energy;
}

std::vector< std::array<double,3> > Interface_PCMSolver::get_grid(){
    vector< array<double,3> > rv;
    for(int i = 0; i < this -> grid_size; ++i){
        array<double,3> tmp = {grid[3*i], grid[3*i+1], grid[3*i+2]};
        rv.push_back(tmp);
    }
    return rv;
}

std::vector<double> Interface_PCMSolver::get_areas(){
    vector<double> rv(this -> areas, this -> areas + grid_size);
    return rv;
}

double Interface_PCMSolver::compute( RCP<State> state, RCP<Epetra_Vector> hartree_potential){
    
    double energy = 0.0;
    
    if(Parallel_Manager::info().get_mpi_rank()==0 and this -> pcm_context == NULL){
        Verbose::all() << "Interface_PCMSolver::initialize is not called!" << std::endl;
        throw std::logic_error("Interface_PCMSolver::initialize is not called! (wrong code)");
    }

    this -> timer -> start("Interface_PCMSolver::compute");

    double * asc_Ag = new double[grid_size]();
    this -> timer -> start("Interface_PCMSolver::compute::MEP");
    double * e_mep = electron_mep(basis, hartree_potential, this -> grid_size, this -> grid);
    double * mep = nuclear_mep( this->pseudo_charges, state -> get_atoms() -> get_positions(), this -> grid_size, this -> grid);

    for(int i = 0; i < this -> grid_size; i++){
        mep[i] -= e_mep[i];
    }

    this -> timer -> end("Interface_PCMSolver::compute::MEP");
    this -> timer -> start("InterPCMSolver::compute::ASC");

    if(Parallel_Manager::info().get_mpi_rank() == 0){
        const char * mep_lbl = {"NucMEP"};
        // Set MEP into the Interface_PCMSolver.
        pcmsolver_set_surface_function(this -> pcm_context, this -> grid_size, mep, mep_lbl);

        const char * asc_lbl = "NucASC";
        int irrep = 0;
        if(this->allow_nonequilibrium){
            pcmsolver_compute_response_asc(this -> pcm_context, mep_lbl, asc_lbl, irrep);
        } else {
            pcmsolver_compute_asc(this -> pcm_context, mep_lbl, asc_lbl, irrep);
        }
        pcmsolver_get_surface_function(this -> pcm_context, this -> grid_size, asc_Ag, asc_lbl);
        energy = pcmsolver_compute_polarization_energy(this -> pcm_context, mep_lbl, asc_lbl);
    }

    delete[] mep;
    delete[] e_mep;
    this -> timer -> end("Interface_PCMSolver::compute::ASC");

    Parallel_Manager::info().group_bcast(asc_Ag, this -> grid_size, 0);
    Parallel_Manager::info().group_bcast(&energy, 1, 0);

    this -> timer -> start("Interface_PCMSolver::compute::polarization potential");

    //double * potential_array = calculate_polarization_potential(basis, this -> alpha, this -> grid_size, this -> areas, asc_Ag, this -> grid);
    this -> PCM_local_potential = calculate_polarization_potential(basis, this -> alpha, this -> grid_size, this -> areas, asc_Ag, this -> grid);
    delete[] asc_Ag;
    this -> timer -> end("Interface_PCMSolver::compute::polarization potential");


    this -> timer -> end("Interface_PCMSolver::compute");
    this -> timer -> print(Verbose::single(Verbose::Simple), -1);
    return energy;
}

double * Interface_PCMSolver::nuclear_mep(std::vector<double> charges, std::vector< std::array<double,3> > coordinates,
        int grid_size, double * grid){
    double * mep = new double[grid_size]();
    int nr_nuclei = charges.size();
    for (int i = 0; i < nr_nuclei; i++) {
        for (int j = 0; j < grid_size; j++) {
            // Column-major ordering. Offsets: col_idx * nr_rows + row_idx
            double dist = pow((coordinates[i][0] - grid[j * 3]), 2) +
                pow((coordinates[i][1] - grid[j * 3 + 1]), 2) +
                pow((coordinates[i][2] - grid[j * 3 + 2]), 2);
            mep[j] += charges[i] / sqrt(dist);
        }
    }

    return mep;
}

double * Interface_PCMSolver::electron_mep(RCP<const Basis> basis, RCP<Epetra_Vector> hartree_potential, int grid_size, double* grid){
    vector< std::array<double,3> > PCM_grid_positions;
    std::vector<double> PCM_potential;
    double* e_mep = new double[grid_size]();
    for (int i=0; i<grid_size; i++){
        std::array<double,3> PCM_grid;
        std::copy(&grid[3*i], &grid[3*i]+3, PCM_grid.begin());
        PCM_grid_positions.push_back(PCM_grid);
    }

    PCM_potential = Interpolation::Tricubic::interpolate(basis,hartree_potential,PCM_grid_positions);
    //PCM_potential = Interpolation::Trilinear::interpolate(basis, hartree_potential, PCM_grid_positions);

    for(int i = 0; i < grid_size; i++){
        e_mep[i]=PCM_potential.at(i);
    }

    return e_mep;
}

double * Interface_PCMSolver::electron_mep(RCP<const Basis> basis, double* hartree_potential, int grid_size, double* grid){
    std::vector<double> PCM_potential;
    double* e_mep = new double[grid_size]();
    for (int i=0; i<grid_size; i++){
        //e_mep[i] = Interpolation::Trilinear::calculate(basis, hartree_potential, grid[3*i], grid[3*i+1], grid[3*i+2]);
        e_mep[i] = Interpolation::Tricubic::calculate(basis, hartree_potential, grid[3*i], grid[3*i+1], grid[3*i+2]);
    }

    return e_mep;
}

Teuchos::RCP<Epetra_Vector> Interface_PCMSolver::calculate_polarization_potential(RCP<const Basis> basis, double alpha, int grid_size, double* areas, double* grid_charge, double* grid){
    double p1 = 0.119763;
    double p2 = 0.205117;
    double q1 = 0.137546;
    double q2 = 0.434344;

    int NumMyElements = basis -> get_map() -> NumMyElements();

    //double* potential_array= new double[basis->get_original_size()]();
    double* potential_array= new double[NumMyElements]();
    int* MyGlobalElements = basis->get_map()->MyGlobalElements();
#pragma omp parallel for
    for(int jl = 0; jl < NumMyElements; jl++){
        int j = MyGlobalElements[jl];
        double x, y, z;
        basis -> get_position(j, x, y, z);
        double dot_potential = 0;
        double constant_value = 2./pow(M_PI * alpha, 0.5);
        for(int i = 0; i < grid_size; i++){
            double r2 = pow(grid[3*i]-x,2) + pow(grid[3*i+1]-y,2) + pow(grid[3*i+2]-z,2);
            double x2 = r2/(alpha*areas[i]);
            double x = sqrt(x2);
            dot_potential -= (grid_charge[i]*constant_value/sqrt(areas[i])*(1+p1*x+p2*x2))/(1+q1*x+q2*x2+p2*x2*x);
        }
        //potential_array[j] = dot_potential;
        potential_array[jl] = dot_potential;
    }

    int * indices = new int[NumMyElements];
    for(int i = 0; i < NumMyElements;i++){
        indices[i] = i;
    }
    RCP<Epetra_Vector> retval = rcp(new Epetra_Vector(*basis -> get_map(), false));
    //retval -> ReplaceMyValues(NumMyElements, &potential_array[basis -> get_map() -> MinMyGID()], indices);
    retval -> ReplaceMyValues(NumMyElements, potential_array, indices);
    delete[] indices;
    delete[] potential_array;
    return retval;
}

#endif

