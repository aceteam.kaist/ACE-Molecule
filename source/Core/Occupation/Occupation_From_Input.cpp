#include "Occupation_From_Input.hpp"

using std::vector;

Occupation_From_Input::Occupation_From_Input(){
}

Occupation_From_Input::Occupation_From_Input(vector<double> occ, vector<double> eigval/* = vector<double>()*/){
    type = "From_Input";
    this -> size = occ.size();
    this -> occupation = occ;
    this -> eigenvalues = eigval;
}

void Occupation_From_Input::set_eigenvalues(std::vector<double> eigenvalues){
    this->eigenvalues = eigenvalues;
    this -> size = eigenvalues.size();
}
Occupation* Occupation_From_Input::clone() const {
    return new Occupation_From_Input(this -> occupation, this -> eigenvalues);
}
