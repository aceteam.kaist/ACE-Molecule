#pragma once 
#include "Occupation.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"
namespace Create_Occupation{
    
//    Teuchos::Array<Teuchos::RCP<Occupation> > Create_Occupation(int occupation_size, int num_electrons,int spin_multiplicity,bool is_polar,std::string occupation_type="Zero_Temp"  );
    Teuchos::Array<Teuchos::RCP<Occupation> > Create_Occupation(Teuchos::RCP<Teuchos::ParameterList> parameters );

};
















