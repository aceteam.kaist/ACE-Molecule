#pragma once
//#include <string>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Linear_Mixing.hpp"
#include "../../State/Scf_State.hpp"

/**
 * @brief Performs linear mixing for orbital, same as density.
 * @note See http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf, pg 3.
 **/
class Linear_Mixing_Orbital: public Linear_Mixing {
    public:
//        int update(RCP<const Basis> basis, Array<RCP<State> > states, int i_spin, RCP<State>& state);
        /**
         * @brief Inherited from Mixing. See Mixing.
         * @return Always 0.
         **/
        int update(
            Teuchos::RCP<const Basis> basis, 
            Teuchos::Array< Teuchos::RCP<Scf_State> > states, 
            int i_spin, 
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null 
        );
    //void update(Teuchos::RCP<Epetra_MultiVector> eigenvectors,Teuchos::Array<Occupation* > occupations, Teuchos::RCP<Epetra_MultiVector> new_density, int sp_polarize = 0);
        /**
         * @brief Constructor.
         * @param alpha Mixing coefficient.
         **/
        Linear_Mixing_Orbital(double alpha);
};

