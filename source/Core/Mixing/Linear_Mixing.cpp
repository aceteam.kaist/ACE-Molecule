#include "Linear_Mixing.hpp"
#include "../../Utility/String_Util.hpp"

using std::string;

Linear_Mixing::Linear_Mixing(double alpha, bool update_paw/* = false*/){
    this->alpha = alpha;
    this->used_step_number = 1;
    this -> update_paw = update_paw;
}

std::string Linear_Mixing::get_mixing_info(int width/* = 50*/){
    std::string info;
    info += "= Mixing Parameter Informations ";
    info += string(width-info.size(), '=') + "\n";
    info += " Linear mixing with field " + this -> mixing_type + ".\n";
    info += " Mixing coefficient: " + String_Util::to_string(this -> alpha) + "\n";
    info += string(width, '=');
    return info;
}
