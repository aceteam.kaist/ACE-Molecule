#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Mixing.hpp"

/**
 * @brief Factory pattern for Mixing class.
 **/
namespace Create_Mixing{
    Teuchos::RCP<Mixing> Create_Mixing(
        Teuchos::RCP<Teuchos::ParameterList> parameters
    );
}
