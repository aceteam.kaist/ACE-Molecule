#include "Broyden_Mixing.hpp"
#include <cmath>
#include "../../Utility/String_Util.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#include "Teuchos_LAPACK.hpp"

#include "../../Utility/Verbose.hpp"

using std::vector;
using std::string;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Broyden_Mixing::Broyden_Mixing(int start, int history, double alpha, double broyden_alpha, double w_0/* = 0.01*/, double * w_m/* = NULL*/){
    this -> start = start;
    this -> history = history;

    if( history+1 > start ){
        this->start = history+1;
    }
    this -> alpha = alpha;
    this -> broyden_alpha = broyden_alpha;

    this -> w_0 = w_0;
    this -> w_m.clear();
    for(int i = 0; i < history; ++i){
        if( w_m == NULL ){
            this -> w_m.push_back(1.0);
        } else {
            this -> w_m.push_back(w_m[i]);
        }
    }
    this -> used_step_number = history+1;
}

// Array length == dim+1
int Broyden_Mixing::compute(
    Array< RCP<Epetra_Vector> > residue,
    Array< RCP<Epetra_Vector> > density_ins,
    double alpha, int i_spin, double w_0, vector<double> w_m,
    RCP<Epetra_Vector>& output,
    RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/
){
    // See PRC 78 014318 (2008) Section II.B
    // residue = F, density_in = V^(in)
    // Also note ref. 10 of PRC paper: PRB 38 12807 (1988)
    int dim = residue.size()-1;
    int code = 0;

    // Prepare eq.8 (PRC)
    /*
     * NOTE
     * I think this can be stored in the class itself for better performance,
     * but for capsulation propose, it is better to be ``hidden'' here.
     */
    Array< RCP<Epetra_Vector> > Delta_F;
    // Delta_F^(m) = [F^(m+1)-F^(m)]/|F^(m+1)-F^(m)|
    Array< RCP<Epetra_Vector> > Delta_V;
    // Delta_V^(m) = [V^(m+1)-V^(m)]/|F^(m+1)-F^(m)|
    Array< RCP<Epetra_Vector> > u;
    // u^(m) = alpha * Delta_F^(m) + Delta_V^(m)
    for(int m = 0; m < dim; ++m){
        RCP<Epetra_Vector> dF_m = rcp( new Epetra_Vector( *residue[m] ) );
        dF_m -> Update( 1.0, *residue[m+1], 0.0 );
        dF_m -> Update( -1.0, *residue[m], 1.0 );

        RCP<Epetra_Vector> dV_m = rcp( new Epetra_Vector( *density_ins[m] ) );
        dV_m -> Update( 1.0, *density_ins[m+1], 0.0 );
        dV_m -> Update( -1.0, *density_ins[m], 1.0 );

        double dF_m_norm = 0.0;
        dF_m -> Norm2( &dF_m_norm );
        if( std::abs(dF_m_norm) > 1.0E-8 ){
            dF_m_norm = 1.0/dF_m_norm;
        } else {
            dF_m_norm = 1.0;
        }
        //cout << "norm = " << dF_m_norm << endl;
        dF_m -> Scale( dF_m_norm );
        dV_m -> Scale( dF_m_norm );

        Delta_F.append(dF_m);
        Delta_V.append(dV_m);

        RCP<Epetra_Vector> u_m = rcp( new Epetra_Vector( *dF_m ) );
        u_m -> Update( 1.0, *dV_m, alpha );
        u.append(u_m);
    }

    // Prepare eq.7 (PRC)
    // Calculate a_kn = w_k w_n (Delta_F^(n))^\dagger Delta_F^(k)
    // Note: Under PRB eq.13a, a_kn is small dim-by-dim matrix.
    double ** a = new double*[dim];
    for(int i = 0; i < dim; ++i){
        a[i] = new double[dim];
        for(int j = 0; j < dim; ++j){
            double result;
            Delta_F[i] -> Dot( *Delta_F[j], &result );
            a[i][j] = w_m[i] * w_m[j] * result;
        }
    }

    // Calculate beta_kn = (w_0^2 I + a)^(-1)_kn
    // Original code used GESV (solving AX=B), not GETRI. I don't know why.
    Teuchos::SerialDenseMatrix<int,double> beta(dim, dim);
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < dim; ++j){
            if(i==j){
                beta(i,j) = a[i][j] + w_0 * w_0;
            }else{
                beta(i,j) = a[i][j];
            }
            //cout << "beta " << i << ", " << j << " = " << beta(i,j) << endl;
        }
    }
    Teuchos::LAPACK<int, double> lapack;
    int info = 0;
    int* IPIV=new int[dim]();
    int LWORK = dim;
    double* WORK = new double[LWORK]();

    // GETRI(obtaining inverse matrix) requres GETRF(PLU decomposition) first
    lapack.GETRF(dim, dim, beta.values(), beta.stride(), IPIV, &info);
    if(info != 0){
        delete[] IPIV;
        delete[] WORK;
        Verbose::single(Verbose::Simple) << "Broyden_Mixing::compute: LAPACK GETRF error: " << info << std::endl;
        //Verbose::single(Verbose::Simple) << "Fallback to linear mixing." << std::endl;
        //output -> Update(1.0, *density_ins[density_ins.size()-1], 0.0);
        code = info*10000;
    }
    lapack.GETRI(dim, beta.values(), beta.stride(), IPIV, WORK, LWORK, &info);
    delete[] IPIV;
    delete[] WORK;
    if(info != 0){
        Verbose::single(Verbose::Simple) << "Broyden_Mixing::compute: LAPACK GETRF error: " << info << std::endl;
        //Verbose::single(Verbose::Simple) << "Fallback to linear mixing." << std::endl;
        //output -> Update(1.0, *density_ins[density_ins.size()-1], 0.0);
        code = info*100;
    }

    // Calculate gamma_mn = sum_{k=0}^{dim} c_k^m beta_kn
    // Calculate c^m_k = w_k (Delta_F^(k))^\dagger F^(m)
    double * c_m = new double[dim];
    for(int k = 0; k < dim; ++k){
        double result;
        Delta_F[k] -> Dot( *residue[residue.size()-1], &result );
        c_m[k] = w_m[k] * result;
    }

    std::vector<double> gamma;
    for(int n = 0; n < dim; ++n){
        double tmp = 0.0;
        for(int k = 0; k < dim; ++k){
            tmp += c_m[k] * beta(k,n);
        }
        gamma.push_back(tmp);
    }

    output -> Update( 1.0, *density_ins[density_ins.size()-1], 0.0);
    output -> Update( alpha, *residue[residue.size()-1], 1.0);
    for(int n = 0; n < dim; ++n){
        output -> Update( -w_m[n]*gamma[n], *u[n], 1.0);
    }

    // Need WORK
    if( nuclear_potential != Teuchos::null ){
        vector<double> coeffs(1, 1.0);
        nuclear_potential -> mix_nuclear_potential( coeffs, this -> broyden_alpha, i_spin );
    }

    for(int i = 0; i < dim; ++i){
        delete[] a[i];
    }
    delete[] a;
    delete[] c_m;
    return code;
}

int Broyden_Mixing::compute(
    vector< vector<double> > residue,
    vector< vector<double> > density_ins,
    double alpha, int i_spin, double w_0, vector<double> w_m,
    vector<double> &output
){
    // See PRC 78 014318 (2008) Section II.B
    // residue = F, density_in = V^(in)
    // Also note ref. 10 of PRC paper: PRB 38 12807 (1988)
    int dim = residue.size()-1;
    int vec_size = residue[0].size();
    int code = 0;

    // Prepare eq.8 (PRC)
    vector< vector<double> > Delta_F;
    // Delta_F^(m) = [F^(m+1)-F^(m)]/|F^(m+1)-F^(m)|
    vector< vector<double> > Delta_V;
    // Delta_V^(m) = [V^(m+1)-V^(m)]/|F^(m+1)-F^(m)|
    vector< vector<double> > u;
    // u^(m) = alpha * Delta_F^(m) + Delta_V^(m)
    for(int m = 0; m < dim; ++m){
        vector<double> dF_m(vec_size);
        vector<double> dV_m(vec_size);
//        #pragma omp parallel for
        for(int i = 0; i < vec_size; ++i){
            dF_m[i] = residue[m+1][i] - residue[m][i];
            dV_m[i] = density_ins[m+1][i] - density_ins[m][i];
        }

        double dF_m_norm = 0.0;
//        #pragma omp parallel for reduction(+:dF_m_norm)
        for(int i = 0; i < vec_size; ++i){
            dF_m_norm += dF_m[i]*dF_m[i];
        }
        if( std::abs(dF_m_norm) < 1.0E-8 ){
            dF_m_norm = 1.0;
        }
//        #pragma omp parallel for
        for(int i = 0; i < vec_size; ++i){
            dF_m[i] /= dF_m_norm;
            dV_m[i] /= dF_m_norm;
        }

        Delta_F.push_back(dF_m);
        Delta_V.push_back(dV_m);

        vector<double> u_m(dF_m.size());
//        #pragma omp parallel for
        for(int i = 0; i < vec_size; ++i){
            u_m[i] = dF_m[i]-dV_m[i];
//            u_m[i] = alpha*dF_m[i]+dV_m[i];  //shchoi

        }
        u.push_back(u_m);

    }

    // Prepare eq.7 (PRC)
    // Calculate a_kn = w_k w_n (Delta_F^(n))^\dagger Delta_F^(k)
    // Note: Under PRB eq.13a, a_kn is small dim-by-dim matrix.
    vector< vector<double> > a(dim);
    for(int i = 0; i < dim; ++i){
        a[i].resize(dim);
        for(int j = 0; j < dim; ++j){
            double result = 0.0;
//            #pragma omp parallel for reduction(+:result)
            for(int k = 0; k < vec_size; ++k){
                result += Delta_F[i][k] * Delta_F[j][k];
            }
            a[i][j] = w_m[i] * w_m[j] * result;
        }
    }

    // Calculate beta_kn = (w_0^2 I + a)^(-1)_kn
    // Original code used GESV (solving AX=B), not GETRI. I don't know why.
    Teuchos::SerialDenseMatrix<int,double> beta(dim, dim);
//    #pragma omp parallel for collapse(2)
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < dim; ++j){
            if(i==j){
                beta(i,j) = a[i][j] + w_0 * w_0;
            }else{
                beta(i,j) = a[i][j];
            }
        }
    }
    Teuchos::LAPACK<int, double> lapack;
    int info = 0;
    int* IPIV=new int[dim]();
    int LWORK = dim;
    double* WORK = new double[LWORK]();

    // GETRI(obtaining inverse matrix) requres GETRF(PLU decomposition) first
    lapack.GETRF(dim, dim, beta.values(), beta.stride(), IPIV, &info);
    if(info != 0){
        delete[] IPIV;
        delete[] WORK;
        Verbose::single(Verbose::Simple) << "Broyden_Mixing::compute: LAPACK GETRF error: " << info << std::endl;
        code = info*10000;
        if(info > 0){
            Verbose::single(Verbose::Simple) << "Fallback to linear mixing." << std::endl;
            output = density_ins[density_ins.size()-1];
            return code;
        }
    }
    lapack.GETRI(dim, beta.values(), beta.stride(), IPIV, WORK, LWORK, &info);
    delete[] IPIV;
    delete[] WORK;
    if(info != 0){
        Verbose::single(Verbose::Simple) << "Broyden_Mixing::compute: LAPACK GETRF error: " << info << std::endl;
        code = info*100;
        if(info > 0){
            Verbose::single(Verbose::Simple) << "Fallback to linear mixing." << std::endl;
            output = density_ins[density_ins.size()-1];
            return code;
        }
    }

    // Calculate gamma_mn = sum_{k=0}^{dim} c_k^m beta_kn
    // Calculate c^m_k = w_k (Delta_F^(k))^\dagger F^(m)
    vector<double> c_m(dim);
    for(int k = 0; k < dim; ++k){
        double result = 0.0;
//        #pragma omp parallel for reduction(+:result)
        for(int x = 0; x < vec_size; ++x){
            result += Delta_F[k][x] * residue[residue.size()-1][x];
        }
        c_m[k] = w_m[k] * result;
    }

    std::vector<double> gamma;
    for(int n = 0; n < dim; ++n){
        double tmp = 0.0;
        for(int k = 0; k < dim; ++k){
            tmp += c_m[k] * beta(k,n);
        }
        gamma.push_back(tmp);
    }

//    #pragma omp parallel for
    for(int i = 0; i < vec_size; ++i){
        output[i] = density_ins[density_ins.size()-1][i] + alpha * residue[residue.size()-1][i];
        for(int n = 0; n < dim; ++n){
            output[i] -= w_m[n]*gamma[n] * u[n][i];
        }
    }

    return code;
}

std::string Broyden_Mixing::get_mixing_info(int width/* = 50*/){
    std::string info;
    info += "= Mixing Parameter Informations ";
    info += string(width-info.size(), '=') + "\n";
    info += " Broyden mixing with field " + this -> mixing_type + ".\n";
    info += " Linear mixing until " + String_Util::to_string(this -> start-1) + " steps are done.\n";
    info += " Initial linear mixing coefficient: " + String_Util::to_string(this -> alpha) + "\n";
    info += " Pulay mixing coefficient: " + String_Util::to_string(this -> broyden_alpha) + "\n";
    info += " Previous states to mix: " + String_Util::to_string(this -> history) + "\n";
    info += " Identity matrix weight: " + String_Util::to_string(this -> w_0) + "\n";
    info += " Privious states weight: [" + String_Util::to_string(this -> w_m[0]);
    for(int i = 1; i < history; ++i){
        info += ", " + String_Util::to_string(this -> w_m[i]);
    }
    info += "]\n";
    info += string(width, '=');
    return info;
}
