#pragma once
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "../../State/Scf_State.hpp"
#include "Mixing.hpp"

/**
 * @brief Abstract class for Linear_Mixing classes.
 * @author Jaewook Kim
 * @date 2016
 */

class Linear_Mixing: public Mixing{
    public:
        /**
         * @brief Default constructor of Linear_Mixing
         * @param alpha Mixing coefficient for Linear_Mixing
         */
        Linear_Mixing(double alpha, bool update_paw = false);
        /**
         * @brief Inherited from Mixing, but still pure virtual.
         **/
        virtual int update(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states,
            int i_spin,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        ) = 0;

        /**
         * @brief Returns formatted string that describes mixing class parameters.
         * @param width Width of display box.
         * @return Informative and formatted string about mixing parameters.
         **/
        virtual std::string get_mixing_info(int width = 50);
    protected:
        bool update_paw = true;
        double alpha;
};
