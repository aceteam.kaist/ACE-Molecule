#pragma once
#include <string>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
//#include "Epetra_MultiVector.h"

#include "../../State/Scf_State.hpp"
#include "../../Basis/Basis.hpp"
//#include "../Occupation/Occupation.hpp"
#include "../Pseudo-potential/Nuclear_Potential.hpp"

/**
 * @brief Abstract class for Mixing.
 **/
class Mixing{
    public:
        /**
         * @brief Update last state to contain mixed data.
         * @param basis Basis data
         * @param states Array of SCF states. Last entry is read and updated. (For first step, both entry is read).
         * @param i_spin Spin index to update.
         * @param nuclear_potential Match PAW density matrix to density mixing.
         * @callergraph
         * @callgraph
         **/
        virtual int update(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states,
            int i_spin,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        )=0;
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Mixing(){};
        /**
         * @brief Return mixing type(field).
         * @return Field to mix. Density, Potential, or PAW_Density.
         **/
        std::string get_mixing_type();
        /**
         * @brief Return used step number for this mixing class
         * @return The number of steps for this mixing. i.e. For Linear mixing, return value = 1
         **/
        int get_used_step_number();

        /**
         * @brief Returns formatted string that describes mixing class parameters.
         * @param width Width of display box.
         * @return Informative and formatted string about mixing parameters.
         **/
        virtual std::string get_mixing_info(int width = 50) = 0;
    protected:
        std::string mixing_type; ///<Field to mix. Density, Potential, or PAW_Density.
        int used_step_number; ///< The number of steps for this mixing. i.e. For Linear mixing, return value = 1

};
