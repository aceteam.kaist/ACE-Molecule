#pragma once
//#include <string>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "Linear_Mixing.hpp"

/**
 * @brief Performs linear mixing for potential.
 * @note See http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf, pg 3.
 **/
class Linear_Mixing_Potential: public Linear_Mixing {
    public:
//        int update(RCP<const Basis> basis, Array<RCP<State> > states, int i_spin, RCP<State>& state);
        /**
         * @brief Inherited from Mixing. See Mixing.
         * @return Always 0.
         **/
        int update(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states,
            int i_spin,
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
         );
        /**
         * @brief Constructor.
         * @param alpha Mixing coefficient.
         **/
        Linear_Mixing_Potential(double alpha, bool update_paw = false);
};
