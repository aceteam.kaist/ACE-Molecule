#pragma once
#include <complex>
#include "../Io/Atoms.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

#include "../State/State.hpp"
#include "../Basis/Basis.hpp"
#include "Teuchos_Array.hpp"
#include "../Utility/Time_Measure.hpp"

//#define ACE_ (1)
#ifdef ACE_PCMSOLVER
extern "C"{
//#include "PCMInput.h"
#include "PCMSolver/pcmsolver.h"
}

/**
 * @author Hyeonsu Kim, Sungwoo Kang
 * @todo
 * @note Reference: https://doi.org/10.1063/1.4932593
 **/
class PCMSolver{
    public:
        /**
         * @brief Default constructor.
         * @param parameters Parameters (Scf.SolvationModel).
         * @param basis Lagrange basis.
         * @param pcm_input Input for PCMSolver library.
         * @param charge_width Gaussian width to be used for broadning ASC.
         * @param is_neq Turn on the nonequilibrium solvation model.
         **/
        PCMSolver(Teuchos::RCP<const Basis> basis, std::string solver_type, PCMInput pcm_input, double charge_width = 1.0, bool is_neq = false);

        PCMSolver(Teuchos::RCP<const Basis> basis, std::string input_filename, double charge_width = 1.0, bool is_neq = false);
        ~PCMSolver();

        /**
         * @brief Calculate PCM solvation energy and potential. Store potential internally.
         * @param state State to compute
         * @param hartree_potential Hartree potential (electrostatic potential of electron density)
         * @return Solvation polarization energy.
         **/
        double compute(Teuchos::RCP<State> state, Teuchos::RCP<Epetra_Vector> hartree_potential);
        /**
         * @brief Store psuedo charge internally.
         * @param atoms Atoms to get atoms order
         * @param charges to get psuedo charge
        **/
        void initialize(Teuchos::RCP<const Atoms> atoms, std::vector<double> charges);
        Teuchos::RCP<const Epetra_Vector> get_PCM_local_potential();
    protected:
        /**
         * @return Nuclear contribution to molecular electrostatic potential. Should be delete[]-ed after use.
         **/
        double * nuclear_mep(std::vector<double> charges, std::vector< std::array<double,3> > coordinates, int grid_size, double * grid);

        /**
         * @return Electron contribution to molecular electrostatic potential. Should be delete[]-ed after use.
         **/
        double * electron_mep(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Epetra_Vector> hartree_potential, int grid_size, double * grid);


        Teuchos::RCP<Epetra_Vector> calculate_polarization_potential(
            Teuchos::RCP<const Basis> basis, double alpha, int grid_size,
            double* areas, double* grid_charge, double* grid
        );
        //std::vector<double> atom_type_charges;
        std::vector<double> pseudo_charges;
        Teuchos::RCP<Epetra_Vector> PCM_local_potential;
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
//        Teuchos::RCP<const Atoms> atoms;
        bool is_neq = false;
        double alpha = 1.0;
        struct PCMInput host_input;
        std::string input_filename;
        pcmsolver_context_t *pcm_context = NULL;
        int grid_size = 0;
        double * grid = NULL;
        double * areas = NULL;
        Teuchos::RCP<Time_Measure> timer;
};
#else
//struct PCMInput{}; // Dummy routine.
// Dummy class.
class PCMSolver{
   public:
       double compute(Teuchos::RCP<State> state, Teuchos::RCP<Epetra_Vector> hartree_potential){return 0.0;};

       Teuchos::RCP<const Epetra_Vector> get_PCM_local_potential(){return Teuchos::null;};
       void initialize(Teuchos::RCP<const Atoms> atoms, std::vector<double> charges){};
};
#endif
