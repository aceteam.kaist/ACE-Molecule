#include "Convergence_HOMO.hpp"
#include <cmath>

using Teuchos::RCP;

Convergence_HOMO::Convergence_HOMO(double tolerance){
    this->tolerance = tolerance;
    this->convergence_type = "HOMO";
}

bool Convergence_HOMO::converge(RCP<const Basis> basis, Teuchos::Array< RCP<Scf_State> > states){
    if(states.size()<=1){
        return false;
    }
    Teuchos::Array< Teuchos::RCP<const Occupation> > current_occupation = states[states.size()-1]->get_occupations();
    Teuchos::Array< Teuchos::RCP<const Occupation> > before_occupation = states[states.size()-2]->get_occupations();
    Teuchos::Array< std::vector<double > > current_orbital_energy = states[states.size()-1]->get_orbital_energies();
    Teuchos::Array< std::vector<double > > before_orbital_energy = states[states.size()-2]->get_orbital_energies();
    double current_HOMO_sum = 0.0;
    double before_HOMO_sum = 0.0;
    int HOMO_index; 
    for(int i=0; i<current_occupation.size(); i++){
        for(HOMO_index=0; HOMO_index<current_occupation[i]->get_size(); HOMO_index++){
            if(current_occupation[i]->operator[](HOMO_index)<0.0001){
                break;
            }
        }
        HOMO_index--;
        current_HOMO_sum+=current_orbital_energy[i][HOMO_index];
    }
    for(int i=0; i<before_occupation.size(); i++){
        for(HOMO_index=0; HOMO_index<before_occupation[i]->get_size(); HOMO_index++){
            if(before_occupation[i]->operator[](HOMO_index)<0.0001){

                break;
            }
        }
        HOMO_index--;
        before_HOMO_sum+=before_orbital_energy[i][HOMO_index];
    }
    double max_difference = std::fabs(current_HOMO_sum - before_HOMO_sum);
    Verbose::single() << "Convergence:: convergence type: " << convergence_type << std::endl; 
    Verbose::single() << "Convergence:: before sum : " << before_HOMO_sum << std::endl;
    Verbose::single() << "Convergence:: current sum : " << current_HOMO_sum << std::endl;
    Verbose::single() << "Convergence:: current deviation (eigenvalue sum difference): " << max_difference << std::endl; 
    if ( max_difference>tolerance){
        Verbose::single() << "Convergence:: not converged yet"  << std::endl;
        return false;
    }
    Verbose::single() << "Convergence:: converged!"  << std::endl;
    return true;
}
