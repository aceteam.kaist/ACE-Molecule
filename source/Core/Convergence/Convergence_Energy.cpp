#include "Convergence_Energy.hpp"
#include <cmath>

using Teuchos::RCP;

Convergence_Energy::Convergence_Energy(double tolerance){
    this->tolerance = tolerance;
    this->convergence_type = "Energy";
}

bool Convergence_Energy::converge(RCP<const Basis> basis, Teuchos::Array< RCP<Scf_State> > states){
    if(states.size()<=1){
        return false;
    }
    double current_tot_energy = states[states.size()-1]->total_energy;
    double before_tot_energy = states[states.size()-2]->total_energy;
    double max_difference = std::abs(current_tot_energy - before_tot_energy);

    Verbose::single() << "Convergence:: convergence type: " << convergence_type << std::endl; 
    Verbose::single() << "Convergence:: current deviation (absolute total energy difference): " << max_difference << std::endl;
    if ( max_difference > tolerance){
        Verbose::single() << "Convergence:: not converged yet"  << std::endl;
        return false;
    }
    Verbose::single() << "Convergence:: converged!"  << std::endl;
    return true;
}
