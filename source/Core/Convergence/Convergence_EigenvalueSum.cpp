#include "Convergence_EigenvalueSum.hpp"
#include <cmath>

using Teuchos::RCP;

Convergence_EigenvalueSum::Convergence_EigenvalueSum(double tolerance){
    this->tolerance = tolerance;
    this->convergence_type = "EigenvalueSum";
}

bool Convergence_EigenvalueSum::converge(RCP<const Basis> basis, Teuchos::Array< RCP<Scf_State> > states){
    if(states.size()<=1){
        return false;
    }
    Teuchos::Array< Teuchos::RCP<const Occupation> > current_occupation = states[states.size()-1]->get_occupations();
    Teuchos::Array< Teuchos::RCP<const Occupation> > before_occupation = states[states.size()-2]->get_occupations();
    Teuchos::Array< std::vector<double > > current_orbital_energy = states[states.size()-1]->get_orbital_energies();
    Teuchos::Array< std::vector<double > > before_orbital_energy = states[states.size()-2]->get_orbital_energies();
    double current_sum = 0.0;
    double previous_sum = 0.0;
    for(int i=0; i<current_occupation.size(); i++){
        for(int j=0; j<current_occupation[i]->get_size(); j++){
            current_sum+=current_orbital_energy[i][j]*current_occupation[i]->operator[](j);
        }
    }
    for(int i=0; i<before_occupation.size(); i++){
        for(int j=0; j<before_occupation[i]->get_size(); j++){
            previous_sum+=before_orbital_energy[i][j]*before_occupation[i]->operator[](j);
        }
    }
    double max_difference = std::fabs(current_sum - previous_sum);
    Verbose::single() << "Convergence:: convergence type: " << convergence_type << std::endl; 
    Verbose::single() << "Convergence:: previous sum : " << previous_sum << std::endl;
    Verbose::single() << "Convergence:: current sum : " << current_sum << std::endl;
    Verbose::single() << "Convergence:: current deviation (eigenvalue sum difference): " << max_difference << std::endl; 
    if ( max_difference>tolerance){
        Verbose::single() << "Convergence:: not converged yet"  << std::endl;
        return false;
    }
    Verbose::single() << "Convergence:: converged!"  << std::endl;
    return true;
}
