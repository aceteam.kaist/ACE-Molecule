#include "Create_Convergence.hpp"
#include <stdexcept>
#include "../../Utility/Verbose.hpp"
#include "Convergence_Density.hpp"
#include "Convergence_Energy.hpp"
#include "Convergence_EigenvalueSum.hpp"
#include "Convergence_HOMO.hpp"

using Teuchos::rcp;

Teuchos::RCP<Convergence> Create_Convergence::Create_Convergence(Teuchos::RCP<Teuchos::ParameterList> parameters){
    std::string convergence_type=parameters->get<std::string>("ConvergenceType", "Density");// This is overwritten by Create_Compute_Interface.
    double tolerance = 0.0;
    if(convergence_type == "Density"){
        tolerance = parameters->get<double>("ConvergenceTolerance", 0.0001);
    } else {
        tolerance = parameters->get<double>("ConvergenceTolerance", 0.000001);
    }
    if(convergence_type=="Density"){
        return  rcp( new Convergence_Density(tolerance,1));
    }
    else if(convergence_type=="HOMO"){
        return rcp( new Convergence_HOMO(tolerance));
    }
    else if(convergence_type=="EigenvalueSum"){
        return rcp( new Convergence_EigenvalueSum(tolerance));
    }
    else if(convergence_type=="Energy"){
        return rcp( new Convergence_Energy(tolerance));
    }
    else {
        throw std::invalid_argument("Invalid ConvergenceType!");
    }
    return Teuchos::null;
}
