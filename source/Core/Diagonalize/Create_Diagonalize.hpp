#pragma once 
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Diagonalize.hpp"

namespace Create_Diagonalize{
    Teuchos::RCP<Diagonalize> Create_Diagonalize(Teuchos::RCP<Teuchos::ParameterList> parameters,
        Teuchos::Array< std::vector<double> > eigvals = Teuchos::Array< std::vector<double> >());
}
