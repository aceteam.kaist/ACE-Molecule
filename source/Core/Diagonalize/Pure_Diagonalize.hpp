#pragma once
#include <string>
#include <vector>

#include "AnasaziBasicEigenproblem.hpp"
//#include "Epetra_CrsMatrix.h"
//#include "Teuchos_RCP.hpp"
//#include "Teuchos_RCPDecl.hpp"

#include "Diagonalize.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Ifpack.h"

#include "../../Utility/Time_Measure.hpp"

#include "Isorropia_Epetra.hpp"
#include "Isorropia_EpetraRedistributor.hpp"
#include "Isorropia_EpetraPartitioner.hpp"

//typedef Epetra_MultiVector MV;
//typedef Epetra_Operator OP;
//typedef Anasazi::MultiVecTraits<double, Epetra_MultiVector> MVT;


class Pure_Diagonalize: public Diagonalize{
    public:
//        void diagonalize(RCP<Epetra_CrsMatrix> matrix,RCP<Epetra_MultiVector> eigenvector);
//        void diagonalize(RCP<Epetra_CrsMatrix> matrix,RCP<Epetra_MultiVector> eigenvector,RCP<Epetra_MultiVector> initial_eigenvector=Teuchos::null);
        bool diagonalize(
            Teuchos::RCP<Epetra_CrsMatrix> matrix, int numev, 
            Teuchos::RCP<Epetra_MultiVector> initial_eigenvector = Teuchos::null, 
            Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null
        );
        
        bool diagonalize(
            Teuchos::RCP<Core_Hamiltonian_Matrix> core_hamiltonian_matrix, int ispin, int numev, Teuchos::RCP<Epetra_Vector> local_potential, 
            Teuchos::RCP<Epetra_MultiVector> initial_eigenvector = Teuchos::null,
            Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null
        );

        void initializer(Teuchos::RCP<Epetra_MultiVector>& eigenvector,
                         Teuchos::RCP<Epetra_MultiVector> initial_eigenvector,
                         const Epetra_BlockMap& map, int numev, 
                         Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null);
        template< class MV, class OP>
        void set_prec(Teuchos::RCP < Anasazi::BasicEigenproblem<double,MV,OP> >& MyProblem, Teuchos::RCP<OP> matrix);

        template< class MV, class OP>
        void set_problem( 
                Teuchos::RCP < Anasazi::BasicEigenproblem<double,MV,OP> >& MyProblem,
                Teuchos::RCP<OP> matrix = Teuchos::null,
                Teuchos::RCP<OP> overlap_matrix = Teuchos::null
                );
        void set_ParameterList(Teuchos::ParameterList* MyPL);
//        void diagonalize(RCP<Epetra_CrsMatrix> matrix,Anasazi::Eigensolution<double, Epetra_MultiVector> solution,RCP<Epetra_MultiVector> initial_eigenvector=Teuchos::null);
        /*
        Pure_Diagonalize(
            int num_eigen, int block_size, int prec_overlap_level, 
            int max_iter, std::string verb, bool locking, double locking_tolerance, 
            int max_locked, bool full_ortho, double tolerance, 
            std::string precond_type, std::string eigen_solver  
        );
        */
        Pure_Diagonalize(
            int block_size, int prec_overlap_level, 
            int max_iter, std::string verb, double locking_tolerance, 
            int max_locked, bool full_ortho, double tolerance, 
            std::string precond_type, std::string eigen_solver,
            bool redistribution, double balancing_tol,
            std::string redistribute_method,
            bool gpu_enable, 
            int NGPU
        );
 
//        Pure_Diagonalize(RCP<ParameterList> parameters);
        Teuchos::RCP<Epetra_MultiVector> get_eigenvectors ();
        std::vector<double > get_eigenvalues();

    protected:
        Anasazi::Eigensolution<double, Epetra_MultiVector> solution;
        Teuchos::RCP<Epetra_MultiVector> RitzVectors;
        std::vector< double > RitzValues;
        //int num_eigen, block_size, prec_overlap_level, max_iter, max_locked;
        int block_size, prec_overlap_level, max_locked;
        std::string verb, precond_type, eigen_solver;
        //bool locking, full_ortho;
        bool full_ortho;
        double locking_tolerance;
        bool gpu_enable;
        int NGPU;

        Teuchos::RCP<Ifpack_Preconditioner> prec;
        bool is_preconditioner_constructed = false;
        bool redistribution;
        std::string redistribute_method;
        double balancing_tol;
        void redistribute(Teuchos::RCP<Epetra_CrsMatrix> hamiltonian, Teuchos::RCP<Epetra_CrsMatrix> overlap, Teuchos::RCP<Epetra_MultiVector> eigenvectors,
                    Teuchos::RCP<Epetra_CrsMatrix>& new_hamiltonian, Teuchos::RCP<Epetra_CrsMatrix>& new_overlap, Teuchos::RCP<Epetra_MultiVector>& new_eigenvectors  );

        Teuchos::RCP<Isorropia::Epetra::Partitioner> partitioner=Teuchos::null;
        Teuchos::RCP<Isorropia::Epetra::Redistributor> redistributor=Teuchos::null;
        //Teuchos::RCP<Time_Measure>  timer;
};


