#pragma once
#include <string>

#include "Teuchos_Array.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"

/**
 * @brief Tools for PAW, mostly dealing with matrix index and reading matrix.
 * @author Sungwoo Kang
 * @date 2015
 * @note This class is used not that widely, and matrix index manuvering could be separated from here.
 **/
namespace Paw_Util{
    /**
     * @brief Change matrix index (i,j) to linearlized index describing upper triangle. (Assumes symmetric matrix).
     * @param n Matrix dimension.
     * @param i Matrix row index.
     * @param j Matrix column index.
     * @return j + n*i-i*(i+1)/2 (for i <= j)
     **/
    int pack_index(int n, int i, int j);
    /**
     * @brief Change linearlized matrix index describing upper triangle to row-column index. (Assumes symmetric matrix).
     * @param n Matrix dimension.
     * @param ind Linearlized matrix index.
     * @return Matrix row, column index. Always upper triangle.
     **/
    std::pair<int, int> unpack_index(int n, int ind);
    /**
     * @brief Read numpy-printed-like matrix from GPAW internal data (but, should remove parenthesis).
     * @param filename File name to read.
     * @param spin_size Spin size.
     * @param double_off_diagonal True if reading atom-centered density matrix. GPAW stores it with off-diagonal value doubled.
     * @return Resulting matrix. Array index equals to spin index.
     * @note This function exists for debug propose.
     **/
    Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > read_matrix(
        std::string filename, int spin_size, bool double_off_diagonal 
    );
}

