//#include <chrono>
#include "Paw.hpp"
#include <iostream>
#include <cmath>

#include "../../Basis/Create_Basis.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/Double_Grid.hpp"
#include "../../Utility/Interpolation/Tricubic_Interpolation.hpp"
#include "../../Utility/String_Util.hpp"
#include "../../Utility/Interpolation/Trilinear_Interpolation.hpp"
#include "../../Utility/Value_Coef.hpp"
#include "../../Compute/Create_Compute.hpp"

using std::vector;
using std::string;
using std::abs;
using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::RCP;
using Teuchos::rcp;

Paw::Paw(RCP<const Basis> basis, RCP<const Atoms> atoms, RCP<ParameterList> parameters){
    this -> basis = basis;
    this -> parameters = parameters;
    this -> atoms=atoms;
    this -> initialize_parameters();
    this -> initialize_finegrid();
    this -> read_all();
}

RCP<Paw_Atom> Paw::get_paw_atom(int iatom){
    return this -> paw_atoms.at(iatom);
}

RCP<Paw_Species> Paw::get_paw_species(int itype){
    return this -> paw_species.at(itype);
}

void Paw::initialize_parameters(){
    if(!parameters->sublist("BasicInformation").sublist("Pseudopotential").isParameter("PSFilenames")){
        Verbose::all() << "There are no PSFilenames in input file." << std::endl;
        exit(EXIT_FAILURE);
    }else{
        if(parameters->sublist("BasicInformation").sublist("Pseudopotential").get<string>("Format") == "xml"){
            Verbose::single(Verbose::Normal) << "GPAW-setup xml type of paw dataset is used" << std::endl;
            type_xml=true;
        } else if(parameters->sublist("BasicInformation").sublist("Pseudopotential").get<string>("Format") == "gz") {
            Verbose::all() << "If you use gzip compressed dataset, please decompress it." << std::endl;
            Verbose::all() << "Decompression not implemented yet." << std::endl;
            type_xml=false;
            exit(EXIT_FAILURE);
        } else {
            Verbose::all() << "Wrong PAW dataset file format: Pseudopotential.Format is wrong" << std::endl;
            exit(EXIT_FAILURE);
        }
        paw_filenames=parameters->sublist("BasicInformation").sublist("Pseudopotential").get< Array<std::string> >("PSFilenames");
    }

    parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("OverlapCutoff", 1.0E-7);

    this -> spin_size = parameters -> sublist("BasicInformation").get<int>("Polarize")+1;
    this -> num_electrons = (int)parameters -> sublist("BasicInformation").get<double>("NumElectrons");
    this -> hartree_type = parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("HartreePotentialMethod", 0);
    if(this -> basis -> get_periodicity() > 0){
        assert(this -> hartree_type == 2);
    }
    //this -> new_hamiltonian_structure = Teuchos::null;
    this -> charge = parameters -> sublist("BasicInformation").get<double>("Charge", 0.0);
    return;
}

void Paw::initialize_finegrid(){
    auto cpoints = this -> basis -> get_points();
    auto cscaling = this -> basis -> get_scaling();
    int* points = new int[3];
    double * scaling = new double[3];
    int multipliers = parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("FineGrid", 2);

    for(int i = 0; i < 3; ++i){
        scaling[i] = cscaling[i]/multipliers;
        if(this -> basis -> get_periodicity() > 0){
            points[i] = multipliers*cpoints[i];
        } else {
            points[i] = multipliers*(cpoints[i]-1)+1;
        }
    }

    //*
    RCP<ParameterList> fine_param = rcp( new ParameterList( parameters -> sublist("BasicInformation") ) );
    //RCP<ParameterList> fine_param = rcp( new ParameterList() );
    fine_param -> sublist("BasicInformation") = parameters -> sublist("BasicInformation");
    fine_param -> set("ScalingX", scaling[0]);
    fine_param -> set("ScalingY", scaling[1]);
    fine_param -> set("ScalingZ", scaling[2]);
    fine_param -> set("PointX", points[0]);
    fine_param -> set("PointY", points[1]);
    fine_param -> set("PointZ", points[2]);
    fine_param -> set("AllowOddPoints", 1);
    // */

    //this -> fine_basis = rcp( new Basis(basis, basis, grid_setting, this->basis -> get_map()) );
    Verbose::set_numformat(Verbose::Pos);
    this -> fine_basis = Create_Basis::Create_Basis( *this -> basis -> get_map() -> Comm().Clone(), fine_param, rcp( new Atoms(*this -> atoms) ) );
    Verbose::single(Verbose::Normal) << "============== PAW fine basis ==============" << std::endl;
    Verbose::single(Verbose::Normal) << *this -> fine_basis;
    Verbose::single(Verbose::Normal) << "===========================================" << std::endl << std::endl;

    //RCP<Teuchos::ParameterList> hartree_param = rcp( new Teuchos::ParameterList() );
    //this -> fine_poisson = Create_Compute::Create_Poisson(this -> fine_basis, hartree_param);

    delete[] points;
    delete[] scaling;
}

void Paw::read_all(){
    this -> paw_species.resize(atoms->get_num_types());
    for(int itype=0;itype < this -> atoms->get_num_types();itype++){
        this -> paw_species.at(itype) = rcp( new Paw_Species( this -> paw_filenames[itype], this -> type_xml ) );
    }
    this -> set_paw_element_types();
    this -> atom_to_element_list = this -> map_atom_to_element();

    this -> check_sphere_overlap();

    int fine_proj_degree = 1;
    if(this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").isParameter("FineProj")){
        Verbose::single(Verbose::Simple) << "FinProj variable is depreciated. Use UsingDoubleGrid, FineDimension and NonlocalRmax instead." << std::endl;
        this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("UsingDoubleGrid", 1);
        this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("FineDimension", 
            this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("FineProj")
        );
        this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("NonlocalRmax", 
            this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("Rmax")
        );
    }
    if( this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("UsingDoubleGrid", 0) > 0 ){
        fine_proj_degree = this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("NonlocalRmax", 1.5);
        if( fine_proj_degree < 1 ) fine_proj_degree = 1;
    }
    /*
    int lmax = -1;
    if( this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").isParameter("Lmax") ){
        lmax = this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("Lmax");
    }
    */

    RCP<ParameterList> addi_params = rcp( new ParameterList( this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential") ) );

    this -> paw_atoms.resize(atoms->get_size());
    for(int itype=0;itype < this -> atoms->get_size();itype++){
        Verbose::single(Verbose::Detail) << "PAW_ATOM #" << itype << " initialization:" << std::endl;
        addi_params -> set<string>("ID", this -> parameters -> sublist("BasicInformation").get<string>("InpName") + "." + String_Util::to_string(itype));
        this -> paw_atoms.at(itype) = rcp(new Paw_Atom(
            this -> paw_species[this->atom_to_element_list[itype]],
            this -> atoms -> get_positions()[itype],
            this -> spin_size,
            this -> basis, this -> fine_basis, addi_params
        ) );
        Verbose::single(Verbose::Detail) << "PAW_ATOM #" << itype << " initialization END" << std::endl;
    }

    Verbose::single(Verbose::Detail) << "Paw_Atom construction complete" << std::endl;

    this -> PAW_D_matrixes.clear();
    this -> PAW_D_matrixes.resize( this -> atoms -> get_positions().size() );

    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer -> start("PAW::Initial fine hartree potential");
    this -> fine_hartree_vector = rcp( new Epetra_Vector( *this -> fine_basis -> get_map() ) );
    for(int i = 0; i < this -> atoms -> get_size(); ++i){
        this -> fine_hartree_vector -> Update( 1.0, *this -> paw_atoms[i] -> get_initial_hartree_potential(true), 1.0 );
    }
    timer -> end("PAW::Initial fine hartree potential");

    Verbose::single(Verbose::Detail) << "Calculating overlap operator" << std::endl;
    this -> calculate_overlap_matrix();
    Verbose::single(Verbose::Normal) << "PAW ready" << std::endl;
    this -> print_reference_energy();
    Verbose::set_numformat(Verbose::Time);
    timer -> print(Verbose::single(Verbose::Simple));
    return;
}

void Paw::set_paw_element_types(){
    this -> paw_element_types.clear();
    for(int i = 0; i < this -> paw_species.size(); ++i){
        this -> paw_element_types.push_back( this -> paw_species[i] -> get_atomic_number() );
    }
}

vector<int> Paw::map_atom_to_element(){
    vector<int> atom_list = this -> atoms -> get_atomic_numbers();
    vector<int> paw_list = this -> paw_element_types;
    vector<int> pawmap;
    vector<int>::iterator it;

    for(int i = 0; i < atom_list.size(); ++i ){
        it = find( paw_list.begin(), paw_list.end(), atom_list[i] );
        if( it == paw_list.end() ){
            Verbose::all() << atom_list[0] << ", " << paw_list[0] << std::endl;
            Verbose::all() << "PAW list and atom type list not match!" << std::endl;
            exit(EXIT_FAILURE);
        }
        pawmap.push_back(it-paw_list.begin());
    }
    return pawmap;
}

int Paw::get_overlap_matrix( RCP<Epetra_CrsMatrix> &overlap_matrix ){
    overlap_matrix = this -> overlap_matrix;
    return 0;
}

int Paw::get_energy_correction(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > orbitals,
    RCP<Epetra_Vector> &hartree_vector,
    double &hartree_energy,
    double &int_n_vxc,
    double &x_energy,
    double &c_energy,
    double &kinetic_energy,
    std::vector<double> &kinetic_energies,
    double &zero_energy,
    double &external_energy
){
    vector<int> atom_list = this -> atoms -> get_atomic_numbers();

    Array< RCP<Epetra_Vector> > density;
    for(int s = 0; s < orbitals.size(); ++s){
        density.append(rcp(new Epetra_Vector(orbitals[0] -> Map())));
        for(int i = 0; i < occupations[s] -> get_size(); ++i){
            density[s] -> Multiply(occupations[s] -> operator[](i), *orbitals[s] -> operator()(i), *orbitals[s] -> operator()(i), 1.0);
        }
    }
    hartree_energy = this -> recalculate_hartree_energy(density, hartree_vector);

    Verbose::single(Verbose::Simple) << "PAW smooth energy contribution" << std::endl;
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;
    Verbose::single(Verbose::Simple) << " PS Kinetic           = " << std::setprecision(10) << kinetic_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS Electrostatic     = " << std::setprecision(10) << hartree_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS XC                = " << std::setprecision(10) << x_energy+c_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS X                 = " << std::setprecision(10) << x_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS C                 = " << std::setprecision(10) << c_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS external          = " << std::setprecision(10) << external_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;

    for(int i = 0; i < atom_list.size(); ++i){
        double atom_hartree_energy = 0, atom_int_n_vxc = 0, atom_x_energy = 0;
        double atom_c_energy = 0, atom_zero_energy = 0, atom_external_energy = 0;
        std::vector<double> atom_kinetic_energies(occupations.size());
        this -> paw_atoms[i] -> get_energy_correction(
            this -> PAW_D_matrixes[i],
            atom_hartree_energy, atom_int_n_vxc, atom_x_energy,
            atom_c_energy, atom_kinetic_energies, atom_zero_energy,
            atom_external_energy
        );
        hartree_energy += atom_hartree_energy;
        int_n_vxc += atom_int_n_vxc;
        x_energy += atom_x_energy;
        c_energy += atom_c_energy;
        for(int s = 0; s < occupations.size(); ++s){
            kinetic_energy += atom_kinetic_energies[s];
            kinetic_energies[s] += atom_kinetic_energies[s];
        }
        zero_energy += atom_zero_energy;
        external_energy += atom_external_energy;
    }

    return 3;
}

int Paw::get_density_correction(Array< RCP<Epetra_Vector> > &density){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atom_density_corr = this -> paw_atoms[i] -> get_density_correction( this -> PAW_D_matrixes[i] );
        for(int s = 0; s < spin_size; ++s){
            density[s] -> Update( 1.0, *atom_density_corr[s], 1.0 );
        }
    }

    return 3;
}

int Paw::get_orbital_correction(Array< RCP<Epetra_MultiVector> > &orbitals){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_MultiVector> > atom_orbital_corr;
        this -> paw_atoms[i] -> get_orbital_correction( orbitals, atom_orbital_corr );
        for(int s = 0; s < spin_size; ++s){
            orbitals[s] -> Update( 1.0, *atom_orbital_corr[s], 1.0 );
        }
    }

    return 3;
}

void Paw::calculate_hamiltonian_correction_coeff(
    vector< vector< vector< vector<double> > > > &dH_coeff
){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    Verbose::single(Verbose::Normal) << "Updating Hamiltonian correction" << std::endl;
    if( this -> fine_hartree_vector == Teuchos::null ){
        Verbose::all() << "calculate_hamiltonian_correction: hartree vector is not updated" << std::endl;
        exit(EXIT_FAILURE);
        return;
    }

    dH_coeff.clear();
    dH_coeff.resize(this -> spin_size);
    for(int i = 0; i < atom_pos.size(); ++i){
        vector< vector< vector<double> > > dH_coeff_atom = this -> paw_atoms[i] -> get_Hamiltonian_correction_matrix( this -> fine_hartree_vector, this -> PAW_D_matrixes[i] );
        for(int s = 0; s < this -> spin_size; ++s){
            for(int j1 = 0; j1 < dH_coeff_atom[s].size(); ++j1){
                for(int j2 = 0; j2 < dH_coeff_atom[s][j1].size(); ++j2){
                    if(!std::isfinite(dH_coeff_atom[s][j1][j2])){
                        Verbose::all() << "dH_coeff of spin " << s << ", atom " << i << ", indicies ("<< j1 << "," << j2 << ") = " << dH_coeff_atom[s][j1][j2] << std::endl;
                        Verbose::all() << "PAW atomcenter density matrix of spin " << s << ", atom " << i << ", indicies ("<< j1 << "," << j2 << ") = " << this -> PAW_D_matrixes[i][s](j1,j2) << std::endl;
                        throw std::runtime_error("Hamotonian correction coefficient is strange!");
                    }
                }
            }
            dH_coeff[s].push_back( dH_coeff_atom[s] );
        }
    }
    return;
}

void Paw::update_PAW_D_matrix(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > wavefunctions
){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();
    this -> PAW_D_matrixes.clear();
    this -> PAW_D_matrixes.resize( positions.size() );

    Verbose::single(Verbose::Detail) << "Updating PAW atomcenter density matrix" << std::endl;
    for(int i = 0; i < positions.size(); ++i ){
        this -> PAW_D_matrixes[i] = this -> paw_atoms[i] -> get_one_center_density_matrix( occupations, wavefunctions );
        Verbose::single(Verbose::Simple) << "PAW Atom " << i << " orbital occupancy" << std::endl;
        this -> paw_atoms[i] -> print_orbital_occupancy(occupations, wavefunctions);
    }
}

void Paw::initialize_PAW_D_matrix( ){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();
    this -> PAW_D_matrixes.clear();
    this -> PAW_D_matrixes.resize( positions.size() );

    Verbose::single(Verbose::Detail) << "Initializing PAW atomcenter density matrix" << std::endl;
    vector<int> val_electrons;
    int num_e = this -> num_electrons;
    int current_tot_num_e = 0;
    for(int i = 0; i < positions.size(); ++i ){
        val_electrons.push_back( this -> paw_atoms[i] -> get_paw_species() -> get_read_paw() -> get_valence_electron_number() );
        current_tot_num_e += val_electrons.back();
    }
    /*
    vector<int> neutral_val_electrons( val_electrons );
    while( num_e < current_tot_num_e ){
        --std::max_element( val_electrons.begin(), val_electrons.end() );
        --current_tot_num_e;
    }
    while( num_e > current_tot_num_e ){
        ++std::max_element( val_electrons.begin(), val_electrons.end() );
        ++current_tot_num_e;
    }
    // */
    for(int i = 0; i < positions.size(); ++i ){
        double charge = static_cast<double>(current_tot_num_e - num_e) / positions.size();
        this -> PAW_D_matrixes[i] = this -> paw_atoms[i] -> get_one_center_initial_density_matrix(charge);
    }
    this -> PAW_D_matrixes_history_in.push_back( this -> PAW_D_matrixes );
}

RCP<Epetra_Vector> Paw::get_zero_potential(){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    RCP<Epetra_Vector> zero_potential = rcp( new Epetra_Vector( * this -> basis -> get_map(), true) );

    for(int i = 0; i < atom_pos.size(); ++i){
        zero_potential -> Update( 1.0, *this -> paw_atoms[i] -> get_zero_potential(), 1.0 );
    }
    return zero_potential;
}

Array< RCP<Epetra_Vector> > Paw::get_zero_potential_gradient(bool is_fine/* = false*/){
    RCP<const Basis> obasis;
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    if(is_fine){
        obasis = this -> fine_basis;
    } else {
        obasis = this -> basis;
    }

    Array< RCP<Epetra_Vector> > zero_potential_grad;
    for(int d = 0; d < 3; ++d){
        zero_potential_grad.append(rcp( new Epetra_Vector( *obasis -> get_map(), true) ));
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atomic_zero_pot_grad = this -> paw_atoms[i] -> get_zero_potential_gradient();
        for(int d = 0; d < 3; ++d){
            zero_potential_grad[d] -> Update( 1.0, *atomic_zero_pot_grad[d], 1.0 );
        }
    }
    return zero_potential_grad;
}

RCP<Epetra_Vector> Paw::get_compensation_charge(
        bool is_fine/* = false*/
){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    RCP<Epetra_Vector> comp_charge;
    if( is_fine ){
        comp_charge = rcp( new Epetra_Vector( * this -> fine_basis -> get_map(), true) );
    } else {
        comp_charge = rcp( new Epetra_Vector( * this -> basis -> get_map(), true) );
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        if( is_fine ){
            comp_charge -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge( this -> PAW_D_matrixes[i], true), 1.0 );
        } else {
            comp_charge -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge( this -> PAW_D_matrixes[i] ), 1.0 );
        }
    }
    // ~0.06s

    return comp_charge;
}

void Paw::calculate_overlap_matrix(){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    int size = this -> basis -> get_map() -> NumGlobalElements();
    double tolerance=this->parameters->sublist("BasicInformation").sublist("Pseudopotential").get<double>("OverlapCutoff") ;
    Verbose::set_numformat(Verbose::Scientific);
    Verbose::single(Verbose::Normal) << "Overlap tolerance = " << tolerance << std::endl;

    RCP<Epetra_CrsGraph> S_structure = rcp( new Epetra_CrsGraph( Copy, *this -> basis -> get_map(), 0 ) );

    for(int i = 0; i < size; ++i){
        int indices = i;
        S_structure -> InsertGlobalIndices(i, 1, &indices);
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Teuchos::SerialDenseMatrix<int,double> S_correction = this -> paw_atoms.at(i) -> get_overlap_matrix();
        vector< vector<double> > proj_coeffs;
        vector< vector<int> > proj_inds;
        this -> paw_atoms.at(i) -> get_projector_coeffs_and_inds(proj_coeffs, proj_inds);
        int stateno = proj_inds.size();

        vector<double> proj_coeff_max;
        for(int ind = 0; ind < stateno; ++ind){
            double max_val = *std::max_element(proj_coeffs[ind].begin(), proj_coeffs[ind].end());
            double min_val = *std::min_element(proj_coeffs[ind].begin(), proj_coeffs[ind].end());
            proj_coeff_max.push_back( std::max(abs(max_val), abs(min_val) ) );
        }

        for(int index1 = 0; index1 < stateno; ++index1){
            for(int index2 = 0; index2 < stateno; ++index2){
                if( abs(S_correction(index1,index2)) * proj_coeff_max[index1]*proj_coeff_max[index2] > tolerance ){
                    for(int ind1 = 0; ind1 < proj_inds[index1].size(); ++ind1 ){
                        int a_index = proj_inds[index1][ind1];
                        if( this -> basis -> get_map() -> MyGID(a_index) ){
                            vector<int> index;
                            index.reserve(proj_inds[index2].size());

                            for(int ind2 = 0; ind2 < proj_inds[index2].size(); ++ind2 ){
                                double calcval = S_correction(index1,index2) * proj_coeffs[index1][ind1] * proj_coeffs[index2][ind2];
                                if( abs(calcval) > tolerance ){
                                    index.push_back( proj_inds[index2][ind2] );
                                }
                            }
                            int ierr = S_structure -> InsertGlobalIndices(a_index, index.size(), &index[0]);
                            if( ierr != 0 ){
                                Verbose::all() << "ERROR: Paw CrsGraph construction " << ierr << std::endl;
                                exit(EXIT_FAILURE);
                            }
                        }// if( map -> MyGID(a_index) )
                    }
                }
            }// index2
        }// index1
//        */
    }

    S_structure -> FillComplete();

    overlap_matrix = rcp( new Epetra_CrsMatrix( Copy, *S_structure) );
    //int dim = overlap_matrix -> NumGlobalRows();

    for(int j = 0; j < this -> basis -> get_map() -> NumMyElements(); ++j){
        double val = 1.0;
        int ind = this -> basis -> get_map() -> MyGlobalElements()[j];
        int ierr = overlap_matrix -> SumIntoGlobalValues( ind, 1.0, &val, &ind );
        if( ierr != 0 ){
            Verbose::all() << "calc overlap matrix InsertGlobalValues error: " << ierr << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Teuchos::SerialDenseMatrix<int,double> S_correction = this -> paw_atoms.at(i) -> get_overlap_matrix();
        vector< vector<double> > proj_coeffs;
        vector< vector<int> > proj_inds;
        this -> paw_atoms.at(i) -> get_projector_coeffs_and_inds(proj_coeffs, proj_inds);
        int stateno = proj_inds.size();

        for(int index1 = 0; index1 < stateno; ++index1){
            for(int index2 = 0; index2 < stateno; ++index2){
                for(int ind1 = 0; ind1 < proj_inds[index1].size(); ++ind1 ){
                    int a_index = proj_inds[index1][ind1];
                    if( this -> basis -> get_map() -> MyGID(a_index) ){
                        vector<double> value;
                        vector<int> index;
                        value.reserve(proj_inds[index2].size());
                        index.reserve(proj_inds[index2].size());

                        for(int ind2 = 0; ind2 < proj_inds[index2].size(); ++ind2 ){
                            double calcval = S_correction(index1,index2) * proj_coeffs[index1][ind1] * proj_coeffs[index2][ind2];
                            if( abs(calcval) > tolerance ){
                                value.push_back( calcval );
                                index.push_back( proj_inds[index2][ind2] );
                            }
                        }
                        int ierr = overlap_matrix -> SumIntoGlobalValues(a_index, value.size(), &value[0], &index[0]);
                        //if( ierr < 0 ){
                        if( ierr != 0 ){
                            Verbose::all() << "ERROR: Paw_Atom S matrix: " << ierr << std::endl;
                            exit(EXIT_FAILURE);
                        }
                    }// if( map -> MyGID(a_index) )
                }
            }// index2
        }// index1
    }

    overlap_matrix -> FillComplete();
    overlap_matrix -> OptimizeStorage();

    Verbose::set_numformat(Verbose::Scientific);
    Verbose::single(Verbose::Normal) << "PAW overlap matrix sparsity: "
             << static_cast<double>(overlap_matrix->NumGlobalNonzeros())/static_cast<double>(this->basis->get_original_size())/this->basis->get_original_size()
             << ", identity matrix sparsity: "
             << 1.0/static_cast<double>(this->basis->get_original_size())
             << " (" << static_cast<double>(overlap_matrix->NumGlobalNonzeros())/static_cast<double>(this->basis->get_original_size()) << " times)"
             << std::endl;
}

void Paw::hartree_vector_update(
    Array< RCP<Epetra_Vector> > ps_density,
    RCP<Epetra_Vector> &hartree_vector
){
    hartree_vector = this -> recalculate_hartree_potential(ps_density, hartree_vector);
}

RCP<Epetra_Vector> Paw::get_Hartree_potential_from_comp_charge(
        bool is_fine/* = false*/
){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    RCP<Epetra_Vector> hartree_potential;
    if( is_fine ){
        hartree_potential = rcp( new Epetra_Vector( * this -> fine_basis -> get_map(), true) );
    } else {
        hartree_potential = rcp( new Epetra_Vector( * this -> basis -> get_map(), true) );
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        if( is_fine ){
            hartree_potential -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge_Hartree_potential( this -> PAW_D_matrixes[i], true ), 1.0 );
        } else {
            hartree_potential -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge_Hartree_potential( this -> PAW_D_matrixes[i], false ), 1.0 );
        }
    }

    return hartree_potential;
}

std::vector<double> Paw::get_total_compensation_charges(){
    size_t atom_size = this -> atoms -> get_size();
    vector<double> charge(atom_size);
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_size; ++i){
        charge[i]= this -> paw_atoms[i] -> get_total_compensation_charge(this -> PAW_D_matrixes[i]);
    }

    return charge;
}

Array< RCP<Epetra_Vector> > Paw::get_atomcenter_core_density(
    bool is_ae,
    bool is_fine/* = false*/
){
    RCP<const Basis> obasis = is_fine? this -> fine_basis: this -> basis;
    Array< RCP<Epetra_Vector> > core_density;
    for(int s = 0; s < spin_size; ++s){
        core_density.push_back( rcp( new Epetra_Vector( *obasis -> get_map(), true ) ) );
    }
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atom_core_density = this -> paw_atoms[i] -> get_core_density_vector(is_ae, obasis);
        for(int s = 0; s < spin_size; ++s){
            core_density[s] -> Update( 1.0, *atom_core_density[s], 1.0 );
        }
    }

    return core_density;
}

Array< RCP<Epetra_Vector> > Paw::get_atomcenter_density(bool is_ae ){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    Array< RCP<Epetra_Vector> > density;
    for(int s = 0; s < spin_size; ++s){
        density.push_back(rcp( new Epetra_Vector( * this -> basis -> get_map() ) ) );
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atom_density = this -> paw_atoms[i] -> get_atomcenter_density_vector( is_ae, this -> PAW_D_matrixes[i] );
        for(int s = 0; s < spin_size; ++s){
            density[s] -> Update( 1.0, *atom_density[s], 1.0 );
        }
    }

    return density;
}

void Paw::check_sphere_overlap(){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    for(int itype = 0; itype < this -> atoms->get_size(); itype++){
        for(int jtype = itype+1; jtype < this -> atoms->get_size(); jtype++){
            double distance = this -> atoms -> distance(itype, jtype);
            //double rc1 = this -> paw_species[this->atom_to_element_list[itype]] -> get_projector_cutoff();
            //double rc2 = this -> paw_species[this->atom_to_element_list[jtype]] -> get_projector_cutoff();
            //double rc1 = this -> paw_species[this->atom_to_element_list[itype]] -> get_cutoff_radius();
            //double rc2 = this -> paw_species[this->atom_to_element_list[jtype]] -> get_cutoff_radius();
            double rc1 = this -> paw_species[this->atom_to_element_list[itype]] -> get_partial_wave_cutoff();
            double rc2 = this -> paw_species[this->atom_to_element_list[jtype]] -> get_partial_wave_cutoff();
            if( distance < rc1 + rc2 ){
                Verbose::single(Verbose::Simple) << "!!!!!!!!!!!! PAW spheres are overlapping !!!!!!!!!!!!" << std::endl;
                Verbose::single(Verbose::Simple) << "distance = " << distance << std::endl;
                Verbose::single(Verbose::Simple) << "atom " << itype << " rc = " << rc1 << std::endl;
                Verbose::single(Verbose::Simple) << "atom " << jtype << " rc = " << rc2 << std::endl;
            }
        }
    }
}

void Paw::density_matrix_mixing(
    std::vector<double> mixing_coeffs,
    double mixing_alpha,
    int ispin
){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();

    //Verbose::single(Verbose::Detail) << "Matrix history before = " << this -> PAW_D_matrixes_history_in.size() << " and " << this -> PAW_D_matrixes_history_out.size() << " (spin " << ispin << ")" << std::endl;
    if( ispin == 0 ){
        this -> PAW_D_matrixes_history_in.push_back( this -> PAW_D_matrixes );
        this -> PAW_D_matrixes_history_out.push_back( this -> PAW_D_matrixes );
    }

    if( this -> PAW_D_matrixes_history_in.size() > 0 ){
        for(int a = 0; a < positions.size(); ++a ){
            this -> PAW_D_matrixes[a][ispin].putScalar(0.0);
        }
        for(int a = 0; a < positions.size(); ++a ){
            for(int i = 0; i < mixing_coeffs.size(); ++i){
                int index = this -> PAW_D_matrixes_history_in.size() - mixing_coeffs.size()+i-1;
                //Verbose::single(Verbose::Detail) << "index = " << index << std::endl;
                //int index = i;
                Teuchos::SerialDenseMatrix<int,double> temp = this -> PAW_D_matrixes_history_in[index][a][ispin];
                temp.scale( mixing_coeffs[i]*mixing_alpha);
                this -> PAW_D_matrixes[a][ispin] += temp;
                Teuchos::SerialDenseMatrix<int,double> temp2 = this -> PAW_D_matrixes_history_out[index][a][ispin];
                temp2.scale( mixing_coeffs[i]*(1-mixing_alpha));
                this -> PAW_D_matrixes[a][ispin] += temp2;
            }
            this -> PAW_D_matrixes_history_in.back()[a][ispin] = this -> PAW_D_matrixes[a][ispin];
        }
    }
    //Verbose::single(Verbose::Detail) << "Matrix history = " << this -> PAW_D_matrixes_history_in.size() << " and " << this -> PAW_D_matrixes_history_out.size() << " (spin " << ispin << ")" << std::endl;

}

void Paw::print_PAW_D_matrix(){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();
    if( Parallel_Manager::info().get_mpi_rank() == 0 ){
        std::ios oldstate(nullptr);
        oldstate.copyfmt(Verbose::all());
        Verbose::all() << "D MATRIX" << std::endl;
        for(int a = 0; a < positions.size(); ++a){
            Verbose::all() << "atom " << a << std::endl;
            for(int s = 0; s < PAW_D_matrixes[a].size(); ++s){
                Verbose::all() << "spin " << s << std::endl;
                for(int i = 0; i < PAW_D_matrixes[a][s].numRows(); ++i){
                    for(int j = 0; j < PAW_D_matrixes[a][s].numCols(); ++j){
                        Verbose::all() << std::fixed << std::setw(8) << std::setprecision(5) << this -> PAW_D_matrixes[a][s](i,j) << "\t";
                    }
                    Verbose::all() << std::endl;
                }
                Verbose::all() << std::endl;
            }
            Verbose::all() << std::endl;
        }
        Verbose::all() << std::endl;
        Verbose::all().copyfmt(oldstate);
    }
}

vector< Array< Teuchos::SerialDenseMatrix<int,double> > > Paw::get_density_matrixes(){
    return this -> PAW_D_matrixes;
}

void Paw::set_density_matrixes( vector< Array< Teuchos::SerialDenseMatrix<int, double> > > D_matrixes ){
    this -> PAW_D_matrixes = D_matrixes;
}

void Paw::print_reference_energy(){
    double ref_kinetic = 0.0;
    double ref_potential = 0.0;
    //double ref_external = 0.0; // Does not support, yet.
    double ref_xc = 0.0;
    //double ref_local = 0.0;
    for(int itype=0;itype < this -> atoms->get_size();itype++){
        ref_kinetic += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_kinetic_energy();
        ref_potential += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_electrostatic_energy();
        //ref_external += this -> paw_species[this->atom_to_element_list[itype]] -> get_external_correction_scalar();
        ref_xc += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_xc_energy();
        //ref_local += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_kinetic_energy();
    }
    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference Kinetic       = " << std::setprecision(10) << ref_kinetic << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference Electrostatic = " << std::setprecision(10) << ref_potential << " Ha" << std::endl;
    //Verbose::single(Verbose::Simple) << " Reference External = " << ref_external << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference XC            = " << std::setprecision(10) << ref_xc << " Ha" << std::endl;
    //Verbose::single(Verbose::Simple) << " Reference Local = " << ref_local << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference Total         = " << std::setprecision(10) << ref_kinetic+ref_potential+ref_xc << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;
}

double Paw::recalculate_hartree_energy(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > scaled_density, Teuchos::RCP<Epetra_Vector> hartree_potential){
    double scaling = this -> basis -> get_scaling()[0] * this -> basis -> get_scaling()[1] * this -> basis -> get_scaling()[2];
    double fine_scaling = this -> fine_basis -> get_scaling()[0] * this -> fine_basis -> get_scaling()[1] * this -> fine_basis -> get_scaling()[2];

    if(this -> hartree_type > 0  and this -> fine_poisson == Teuchos::null){
        RCP<ParameterList> poisson_param = Teuchos::rcp(new Teuchos::ParameterList());
        this -> fine_poisson = Create_Compute::Create_Poisson(this -> fine_basis, poisson_param);
    }
    double hartree_energy = 0.0;
    if(this -> hartree_type == 0 or this -> hartree_type == 1){
        for(int s = 0; s < scaled_density.size(); ++s){
            double local_hartree_energy;
            hartree_potential -> Dot(*scaled_density[s], &local_hartree_energy);
            hartree_energy += 0.5 * local_hartree_energy;
        }

        Array< RCP<Epetra_Vector> > core_density = this -> get_atomcenter_core_density(false);
        for(int s = 0; s < scaled_density.size(); ++s){
            double local_hartree_energy = 0.0;
            core_density[s] -> Dot( *hartree_potential, &local_hartree_energy );
            hartree_energy += 0.5 * scaling * local_hartree_energy;
        }

        double energy = 0.0;

        RCP<Epetra_Vector> comp_charge = this -> get_compensation_charge(true);
        this -> fine_hartree_vector -> Dot( *comp_charge, &energy );

        hartree_energy += 0.5 * fine_scaling * energy;
    } else if(this -> hartree_type == 2){
        Array< RCP<Epetra_Vector> > fine_tot_density;
        Array< RCP<Epetra_Vector> > ps_density;
        for(int s = 0; s < scaled_density.size(); ++s){
            ps_density.append(rcp(new Epetra_Vector(*scaled_density[s])));
            ps_density[s] -> Scale(1./scaling);
        }
        RCP<Epetra_Vector> comp_charge = this -> get_compensation_charge(true);
        RCP<Epetra_Vector> unitvect = rcp(new Epetra_Vector(*this -> fine_basis -> get_map(), false));
        unitvect -> PutScalar(1.0);
        double comp_charge_norm;
        comp_charge -> Dot(*unitvect, &comp_charge_norm);
        comp_charge_norm *= fine_scaling;
        double spin_charge = 0.0;
        for(int s = 0; s < ps_density.size(); ++s){
            fine_tot_density.push_back( rcp( new Epetra_Vector( *this -> fine_basis -> get_map() ) ) );
            Interpolation::Tricubic::interpolate(basis, ps_density[s], fine_basis, fine_tot_density[s]);
            fine_tot_density[s] -> Update(1.0, *this -> get_atomcenter_core_density(false, true)[s], 1.0);
            double spin_charge_s;
            fine_tot_density[s] -> Dot(*unitvect, &spin_charge_s);
            spin_charge += spin_charge_s * fine_scaling;
        }
        // for debug
        Verbose::single(Verbose::Normal) << "recalculate_hartree_energy" << std::endl <<"PAW charge (tot), (pseudo), (comp), (input) = " << spin_charge + this -> charge + comp_charge_norm << ", " << spin_charge << ", " << comp_charge_norm << ", " << this -> charge << std::endl;

        for(int s = 0; s < ps_density.size(); ++s){
            fine_tot_density[s] -> Update(1.0/ps_density.size(), *comp_charge, (-comp_charge_norm + this -> charge)/spin_charge);

            // for debug
            double norm;
            fine_tot_density[s] -> Dot(*unitvect, &norm);
            Verbose::single(Verbose::Normal) << "PAW fine total density = " << norm*scaling << std::endl;
        }
        this -> fine_poisson -> compute(fine_tot_density, this -> fine_hartree_vector, hartree_energy);
    }
    //Verbose::set_numformat(Verbose::Energy);
    //Verbose::single(Verbose::Detail) << "PAW hartree*comp FINE = " << 0.5*fine_scaling*energy << std::endl;

    return hartree_energy;
}

Teuchos::RCP<Epetra_Vector> Paw::recalculate_hartree_potential(
    Teuchos::Array< Teuchos::RCP<Epetra_Vector> > ps_density,
    Teuchos::RCP<Epetra_Vector> &hartree_vector
){
    clock_t st, et;
    Parallel_Manager::info().all_barrier();
    st = clock();
    this -> fine_hartree_vector = rcp( new Epetra_Vector( *this -> fine_basis -> get_map() ) );

    int type = this -> hartree_type;
    RCP<ParameterList> poisson_param = Teuchos::rcp(new Teuchos::ParameterList());
    if(type == 0 or type == 1){
        if(type == 0){
            Verbose::single(Verbose::Detail) << "Hartree potential: Interpolate potential from PS density to fine grid and calculate others on radial grid." << std::endl;
            Interpolation::Tricubic::interpolate(basis, hartree_vector, fine_basis, this->fine_hartree_vector);

            for(int i = 0; i < this -> atoms -> get_positions().size(); ++i){
                Array< RCP<Epetra_Vector> > core_pot = this -> paw_atoms[i] -> get_smooth_core_potential( true );
                for(int s = 0; s < core_pot.size(); ++s){
                    this -> fine_hartree_vector -> Update( 1.0, *core_pot[s], 1.0 );
                }
                core_pot = this -> paw_atoms[i] -> get_smooth_core_potential( false );
                for(int s = 0; s < core_pot.size(); ++s){
                    hartree_vector -> Update( 1.0, *core_pot[s], 1.0 );
                }
            }
        } else if(type == 1){
            Verbose::single(Verbose::Detail) << "Hartree potential: Calculate potential from PS density on fine grid and others on radial grid." << std::endl;
            if(this -> fine_poisson == Teuchos::null){
                this -> fine_poisson = Create_Compute::Create_Poisson(this -> fine_basis, poisson_param);
            }
            Array< RCP<Epetra_Vector> > fine_ps_density;
            for(int s = 0; s < ps_density.size(); ++s){
                fine_ps_density.push_back( rcp( new Epetra_Vector( *this -> fine_basis -> get_map() ) ) );
                RCP<Epetra_Vector> tmp = rcp( new Epetra_Vector( *ps_density[s] ) );
                tmp -> Update(1.0, *this -> get_atomcenter_core_density(false, false)[s], 1.0);
                Interpolation::Tricubic::interpolate(basis, tmp, fine_basis, fine_ps_density[s]);
            }
            double hartree_energy;
            this -> fine_poisson -> compute(fine_ps_density, this -> fine_hartree_vector, hartree_energy);
            Interpolation::Trilinear::interpolate(fine_basis, this -> fine_hartree_vector, basis, hartree_vector);
        }

        this -> fine_hartree_vector -> Update( 1.0, *this->get_Hartree_potential_from_comp_charge(true), 1.0 );
        hartree_vector -> Update( 1.0, *this -> get_Hartree_potential_from_comp_charge(false), 1.0);

    } else {
        Verbose::single(Verbose::Detail) << "Hartree potential: Calculate everything on fine grid." << std::endl;
        if(this -> fine_poisson == Teuchos::null){
            this -> fine_poisson = Create_Compute::Create_Poisson(this -> fine_basis, poisson_param);
        }
        double scaling = this -> fine_basis -> get_scaling()[0] * this -> fine_basis -> get_scaling()[1] * this -> fine_basis -> get_scaling()[2];
        double coarse_scaling = this -> basis -> get_scaling()[0] * this -> basis -> get_scaling()[1] * this -> basis -> get_scaling()[2];
        Array< RCP<Epetra_Vector> > fine_tot_density;
        RCP<Epetra_Vector> comp_charge = this -> get_compensation_charge(true);
        RCP<Epetra_Vector> unitvect = rcp(new Epetra_Vector(*this -> fine_basis -> get_map()));
        unitvect -> PutScalar(1.0);
        RCP<Epetra_Vector> coarse_unitvect = rcp(new Epetra_Vector(*this -> basis -> get_map()));
        coarse_unitvect -> PutScalar(1.0);
        double comp_charge_norm;
        comp_charge -> Dot(*unitvect, &comp_charge_norm);
        comp_charge_norm *= scaling;
        double spin_charge = 0.0; double coarse_charge = 0.0;
        for(int s = 0; s < ps_density.size(); ++s){
            fine_tot_density.push_back( rcp( new Epetra_Vector( *this -> fine_basis -> get_map() ) ) );
            Interpolation::Tricubic::interpolate(basis, ps_density[s], fine_basis, fine_tot_density[s]);
            fine_tot_density[s] -> Update(1.0, *this -> get_atomcenter_core_density(false, true)[s], 1.0);
            double spin_charge_s;
            fine_tot_density[s] -> Dot(*unitvect, &spin_charge_s);
            spin_charge += spin_charge_s * scaling;
            ps_density[s] -> Dot(*coarse_unitvect, &spin_charge_s);
            coarse_charge += spin_charge_s * coarse_scaling;
        }

        // Debug (write cube file of fine_tot_density)
        //this -> fine_basis -> write_cube("fine_tot_density",this->atoms,fine_tot_density);

        // for debug (Check PAW compensation charge)
        vector<double> analytic_comps = this -> get_total_compensation_charges();
        double tot_analytic_comps = 0.0;
        for(int i = 0; i < analytic_comps.size(); ++i){
            tot_analytic_comps += analytic_comps[i];
        }
        Verbose::single(Verbose::Normal) << "recalculate_hartree_potential"<< std::endl << "PAW charge (tot), (pseudo), (comp), (input) = " << spin_charge + this -> charge + comp_charge_norm << ", " << spin_charge << ", " << comp_charge_norm << ", " << this -> charge << std::endl;
        Verbose::single(Verbose::Normal) << "PAW charge (val,coarse,ps) = " << coarse_charge << ", (comp,analytic) = " << tot_analytic_comps << std::endl;
        for(int s = 0; s < ps_density.size(); ++s){
            fine_tot_density[s] -> Update(1.0/ps_density.size(), *this -> get_compensation_charge(true), std::abs(comp_charge_norm + this -> charge)/spin_charge);
            double norm;
            fine_tot_density[s] -> Dot(*unitvect, &norm);
            ps_density[s] -> Scale((-comp_charge_norm + this -> charge)/spin_charge);
            Verbose::single(Verbose::Detail) << "PAW fine total density = " << norm*scaling << std::endl;
        }
        double hartree_energy;
        this -> fine_poisson -> compute(fine_tot_density, this -> fine_hartree_vector, hartree_energy);


        // Debug
        //Verbose::single(Verbose::Detail) << "recalculate_hartree_potential : Trilinear::interpolate" << std::endl;
        //Interpolation::Trilinear::interpolate(fine_mesh, this -> fine_hartree_vector, mesh, hartree_vector);
        Verbose::single(Verbose::Detail) << "recalculate_hartree_potential : Tricubic::interpolate" << std::endl;
        Interpolation::Tricubic::interpolate(fine_basis, this -> fine_hartree_vector, basis, hartree_vector);

        /*
        // Debug (write cube files)
        //this -> basis -> write_cube("ps_density", this->atoms,ps_density);
        this -> fine_basis -> write_cube("fine_tot_density_add_comp",this->atoms,fine_tot_density);
        this -> fine_basis -> write_cube("fine_hartree_vector",this->atoms,fine_hartree_vector);
        this -> basis -> write_cube("hartree_vector", this->atoms,hartree_vector);
        */
    }
    Parallel_Manager::info().all_barrier();
    et = clock();
    Verbose::single(Verbose::Detail) << "PAW: hartree_vector_update time = " << static_cast<double>(et-st)/CLOCKS_PER_SEC << "s" << std::endl;
    /*
    st = clock();
    hartree_vector = Double_Grid::sample(fine_hartree_vector, this -> fine_basis, this -> basis, "Sinc", 
        parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("FineGrid", 2), 1.0,
        vector<double>(3), 1.E30);
    et = clock();
    Verbose::single(Verbose::Detail) << "PAW: hartree_vector_update supersampling time = " << static_cast<double>(et-st)/CLOCKS_PER_SEC << "s" << std::endl;
    */

    return hartree_vector;
}
// */

Teuchos::RCP<Epetra_Vector> Paw::get_fine_hartree_potential() const{
    return rcp(new Epetra_Vector(*this -> fine_hartree_vector));
}

RCP<const Basis> Paw::get_fine_basis(){
    return this -> fine_basis;
}
