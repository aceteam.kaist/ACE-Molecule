#include "Upf2KB.hpp"
#include <cmath>
#include "Filter.hpp"
#include "EpetraExt_MatrixMatrix.h"
#include "Epetra_Comm.h"
#include "Epetra_Map.h"

#include "../../Basis/Create_Basis.hpp"
#include "../../Utility/String_Util.hpp"
#include "../../Utility/Math/Spherical_Harmonics.hpp"
#include "../../Utility/Math/Spherical_Harmonics_Derivative.hpp"
#include "../../Utility/Read/Read_Upf.hpp"
#include "../../Utility/Double_Grid.hpp"
#include "../../Utility/Interpolation/Linear_Interpolation.hpp"
#include "../../Utility/Interpolation/Spline_Interpolation.hpp"
#include "../../Utility/Calculus/Numerical_Derivatives.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/LoopUtil.hpp"

#ifdef ACE_HAVE_OMP
#include "omp.h"
#endif

using std::abs;
using std::string;
using std::vector;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
using Interpolation::Spline::splint;
using Numerical_Derivatives::numerical_derivatives;

Upf2KB::Upf2KB(RCP<const Basis> basis,RCP<const Atoms> atoms, RCP<Teuchos::ParameterList> parameters){
    this->basis = basis;
    this->parameters=Teuchos::sublist(parameters,"BasicInformation");
    this->atoms=atoms;

    initialize_parameters();
    read_pp();  // read information from pseudopotential file; fill out all values related to radial basis
    initialize_pp();
    make_vectors();
}

void Upf2KB::initialize_parameters(){
    upf_filenames=parameters->sublist("Pseudopotential").get< Array<string> >("PSFilenames");

    parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold",1E-6);
    parameters->sublist("Pseudopotential").get<double>("LocalThreshold",1E-7);
    parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid", 0);
    parameters->sublist("Pseudopotential").get<int>("UsingFiltering", 0);

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1){
        this -> fine_dimension = parameters->sublist("Pseudopotential").get<int>("FineDimension", 3);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - FineDimension = " << this -> fine_dimension << std::endl;

        parameters->sublist("Pseudopotential").get<string>("FilterType", "Sinc");
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - FilterType = " << parameters->sublist("Pseudopotential").get<string>("FilterType") << std::endl;

        if(parameters->sublist("Pseudopotential").get<string>("FilterType")=="Lagrange"){
            parameters->sublist("Pseudopotential").get<int>("InterpolationOrder", 8);
            Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - Using Lagrange Filter function of order-" << parameters->sublist("Pseudopotential").get<int>("InterpolationOrder") << std::endl;
        }

        parameters->sublist("Pseudopotential").get<double>("LocalRgauss", 1.0);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - LocalRgauss = " << parameters->sublist("Pseudopotential").get<double>("LocalRgauss") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("ShortDecayTol", 0.0001);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - ShortDecayTol = " << parameters->sublist("Pseudopotential").get<double>("ShortDecayTol") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange", 1.25);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - LocalIntegrationRange = " << parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("NonlocalRmax", 2.0);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - NonlocalRmax = " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << std::endl;
    }

    /*
    // Filtering should be initialized after Zvals initialized.
    if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
        //parameters->sublist("Pseudopotential").get<double>("NonlocalRmax", 2.0);
        //this->filter = rcp(new Filter(mesh, atoms, Zvals, gamma_local, gamma_nonlocal, alpha_local, alpha_nonlocal,eta)); 
    }
    */

    return;
}

void Upf2KB::read_pp(){

    for(int itype=0;itype<atoms->get_num_types();itype++){
        // Read header
        double Zval_tmp = 0.0;
        int number_vps = 0, number_pao = 0, mesh_size_tmp = 0;
        bool core_correction = false;

        //Read_Upf::read_header(upf_filenames[itype], &Zval_tmp, &number_vps, &number_pao, &mesh_size_tmp);
        Read::Upf::read_header(upf_filenames[itype], &Zval_tmp, &number_vps, &number_pao, &mesh_size_tmp, &core_correction);

        Zvals.push_back(Zval_tmp);
        number_vps_file.push_back(number_vps);
        number_pao_file.push_back(number_pao);
        mesh_size.push_back(mesh_size_tmp);
        // end

        // Read basis points
        vector<double> original_mesh_tmp;
        vector<double> original_d_mesh_tmp;

        //shchoi
        original_mesh_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_R");
        original_d_mesh_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_RAB");

        if(mesh_size[itype] != original_mesh_tmp.size()){
            Verbose::all()<< "Upf2KB::read_pp - The number of basis points from PP_HEADER & PP_R is different." << std::endl;
            Verbose::all()<< mesh_size[itype] << "\t!=\t" << original_mesh_tmp.size() << std::endl;
            Verbose::all()<< "Upf2KB::original basis " <<std::endl;
            for (int i=0;original_mesh_tmp.size();i++ ){
                Verbose::all()<< "r\t" <<original_mesh_tmp[i] <<std::endl;
            }
            exit(-1);
        }

        original_mesh.push_back(original_mesh_tmp);
        original_d_mesh.push_back(original_d_mesh_tmp);
        Verbose::single(Verbose::Detail) << "original_mesh_tmp size: " << original_mesh_tmp.size() <<std::endl;
        local_mesh.push_back(original_mesh_tmp);
        Verbose::single(Verbose::Detail) << "local_mesh size: " << local_mesh.size() <<std::endl;
        local_d_mesh.push_back(original_d_mesh_tmp);
        /*
           for (int k=0;k<original_mesh_tmp.size();k++){
           Verbose::single(Verbose::Detail)<< original_mesh_tmp[k] <<std::endl;
           }
           */
        // Read local pseudopotential
        vector<double> local_pp_radial_tmp;

        //int local_pp_radial_tmp_size = 0;

        local_pp_radial_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_LOCAL");
        // Unit conversion from Ryd to Hartree
        for(int t=0;t<local_pp_radial_tmp.size();t++){
            local_pp_radial_tmp[t] *= 0.5;
        }
        //end

        //local_pp_radial_tmp_size = local_pp_radial_tmp.size();
        local_pp_radial.push_back(local_pp_radial_tmp);

        if(local_pp_radial[itype].size()!=local_mesh[itype].size()){
            Verbose::all() <<"size of local term is not equal to the size of local basis" << std::endl;
            Verbose::all() << "size of original_mesh_tmp: " << original_mesh_tmp.size() << std::endl;
            Verbose::all() << "size of local_pp_radial: " << local_pp_radial[itype].size() << std::endl;
            Verbose::all() << "size of local_mesh: " <<local_mesh[itype].size() << std::endl;
            Verbose::all() << "itype " << itype  << std::endl;
            for (int k=0;k<local_mesh[itype].size();k++){
                Verbose::all() << local_mesh[itype][k] << std::endl;
            }
            exit(-1);
        }
        // end

        // Read nonlinear core correction
        vector<double> nonlinear_core_correction_mesh_tmp;
        vector<double> nonlinear_core_correction_radial_tmp;

        if(core_correction == true){
            Verbose::single(Verbose::Normal) << "Upf2KB: found nonlinear core correction for atom type " << itype << std::endl;
            nonlinear_core_correction_radial_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_NLCC");
            for(int i=0; i<nonlinear_core_correction_radial_tmp.size(); i++){
                nonlinear_core_correction_mesh_tmp.push_back(original_mesh_tmp[i]);
            }

            // end
            /*
               for(int i=0; i<nonlinear_core_correction_radial_tmp.size(); i++){
               cout << nonlinear_core_correction_mesh_tmp[i] << " " << nonlinear_core_correction_radial_tmp[i] << endl;
               }
               exit(-1);
               */
            nonlinear_core_correction.push_back(true);
        }
        else{
            Verbose::single(Verbose::Normal) << "Upf2KB: no nonlinear core correction for atom type " << itype << std::endl;
            nonlinear_core_correction.push_back(false);
        }

        nonlinear_core_correction_basis.push_back(nonlinear_core_correction_mesh_tmp);
        nonlinear_core_correction_radial.push_back(nonlinear_core_correction_radial_tmp);
        // end

        // Read EKB
        vector<double> EKB_tmp;

        //int EKB_tmp_size = 0;

        EKB_tmp = Read::Upf::read_ekb(upf_filenames[itype], number_vps);

        // Unit conversion from Ryd^-1 to Hartree^-1
        for(int i=0;i<EKB_tmp.size();i++){
            EKB_tmp[i] *= 2.0;
        }
        // end

        //EKB_tmp_size = EKB_tmp.size();

        EKB.push_back(EKB_tmp);
        // end

        // Read nonlocal pseudopotential
        vector < vector<double> > nonlocal_mesh_tmp;
        vector < vector<double> > nonlocal_d_mesh_tmp;
        if(number_vps > 0){
            vector< vector<double> > nonlocal_pp_radial_tmp;
            vector<double> nonlocal_cutoff_tmp;
            vector<int> oamom_tmp;

            nonlocal_pp_radial_tmp = Read::Upf::read_nonlocal(upf_filenames[itype], number_vps, oamom_tmp);
            oamom.push_back(oamom_tmp);

            for(int i=0;i<number_vps;i++){
                vector<double> nonlocal_mesh_tmp_tmp;
                vector<double> nonlocal_d_mesh_tmp_tmp;
                // 1. Change the value that I want to.
                for(int t=0;t<nonlocal_pp_radial_tmp[i].size();t++){
                    nonlocal_pp_radial_tmp[i][t] *= 0.5;// Unit conversion from Ryd*bohr^-0.5 to Hartree*bohr^-0.5
                    nonlocal_mesh_tmp_tmp.push_back(original_mesh[itype][t]);
                    nonlocal_d_mesh_tmp_tmp.push_back(original_d_mesh[itype][t]);
                }

                // 3. Cut the small or weird values at the end of the array.
                while(fabs(nonlocal_pp_radial_tmp[i][nonlocal_pp_radial_tmp[i].size()-1]) < 1.0E-30){
                    nonlocal_pp_radial_tmp[i].pop_back();
                    nonlocal_mesh_tmp_tmp.pop_back();
                    nonlocal_d_mesh_tmp_tmp.pop_back();
                }

                nonlocal_cutoff_tmp.push_back(nonlocal_mesh_tmp_tmp[nonlocal_mesh_tmp_tmp.size()-1]);
                nonlocal_mesh_tmp.push_back(nonlocal_mesh_tmp_tmp);
                nonlocal_d_mesh_tmp.push_back(nonlocal_d_mesh_tmp_tmp);
            }
            nonlocal_pp_radial.push_back(nonlocal_pp_radial_tmp);
            nonlocal_cutoff.push_back(nonlocal_cutoff_tmp);
        }
        else{
            nonlocal_pp_radial.push_back( vector< vector<double> >() );
            nonlocal_cutoff.push_back( vector<double>() );
            oamom.push_back(vector<int>());
        }
        // end
        nonlocal_mesh.push_back(nonlocal_mesh_tmp);
        nonlocal_d_mesh.push_back(nonlocal_d_mesh_tmp);

        ///////////// calcualte projector number ////////////
        projector_number.push_back(number_vps);
        ////////////          end        /////////////////////

        // KSW: for filtering.
        atomic_density.push_back(Read::Upf::read_upf(upf_filenames[itype], "PP_RHOATOM"));

        pp_information(itype);
    }

    for(int iatom = 0; iatom < atoms -> get_size(); ++iatom){
        int itype = atoms -> get_atom_type(iatom);
        int number_vps = projector_number[itype];
        vector< vector<double> > EKB_mat_tmp(number_vps);
        for(int i = 0; i < number_vps; ++i){
            EKB_mat_tmp[i].resize(number_vps);
            for(int j = 0; j < number_vps; ++j){
                EKB_mat_tmp[i][j] = EKB[itype].at(i*number_vps+j);
            }
        }
        this -> EKB_matrix.push_back(EKB_mat_tmp);
    }
    return;
}

void Upf2KB::compute_nonlinear_core_correction(){
    int * MyGlobalElements = basis->get_map()->MyGlobalElements();
    int NumMyElements =basis->get_map()->NumMyElements();

    core_density = rcp(new Epetra_Vector(*basis->get_map()));
    core_density_grad = rcp(new Epetra_MultiVector(*basis->get_map(),3) );

    vector<std::array<double,3> > positions = atoms->get_positions();

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        if(nonlinear_core_correction[itype] == true){
            double rcut = nonlinear_core_correction_basis[itype][nonlinear_core_correction_basis[itype].size() - 1];
            /*
               if(MyPID == 0){
               cout.precision(8);
               cout << scientific;
               for(int i=0; i<nonlinear_core_correction_radial[itype].size(); i++){
               cout << nonlinear_core_correction_basis[itype][i] << "  " << nonlinear_core_correction_radial[itype][i] << endl;
               }
               exit(-1);
               }
               */
            // Spline interpolation
            int core_correction_size = nonlinear_core_correction_radial[itype].size();
            vector<double> y2;
            for(int i=0; i<nonlinear_core_correction_radial[itype].size(); i++){
                double tmp = Numerical_Derivatives::numerical_derivatives(nonlinear_core_correction_basis[itype][i], nonlinear_core_correction_basis[itype], nonlinear_core_correction_radial[itype], 2, 5);
                y2.push_back(tmp);
            }

            for(int i=0; i<NumMyElements; i++){
                double x, y, z, r;
                /*
                double x_p, y_p, z_p;
                basis->get_position(MyGlobalElements[i], x_p, y_p, z_p);

                double x = x_p - positions[iatom][0];
                double y = y_p - positions[iatom][1];
                double z = z_p - positions[iatom][2];
                double r = sqrt(x*x + y*y + z*z);
                */
                basis -> find_nearest_displacement(MyGlobalElements[i], positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);

                if(r < rcut){
                    double tmp = splint(nonlinear_core_correction_basis[itype], nonlinear_core_correction_radial[itype], y2, core_correction_size, r);
                    double tmp_grad = numerical_derivatives(r, nonlinear_core_correction_basis[itype], nonlinear_core_correction_radial[itype], 1, 5);

                    core_density->SumIntoMyValue(i, 0, tmp);
                    if(r < 1.0E-30){
                        // if r = 0, x/r = y/r = z/r -> 1
                        for(int d = 0; d < 3; ++d){
                            core_density_grad -> SumIntoMyValue(i, d, tmp_grad);
                        }
                    } else {
                        core_density_grad->SumIntoMyValue(i, 0, tmp_grad * x / r); // x-axis
                        core_density_grad->SumIntoMyValue(i, 1, tmp_grad * y / r); // y-axis
                        core_density_grad->SumIntoMyValue(i, 2, tmp_grad * z / r); // z-axis
                    }
                }
            }
        }
    }

    double numerical_core_charge = 0.0;
    core_density->Norm1(&numerical_core_charge);
    numerical_core_charge *= basis->get_scaling()[0] * basis->get_scaling()[1] * basis->get_scaling()[2];

    Verbose::single().precision(12);
    Verbose::single() << std::fixed ;
    Verbose::single(Verbose::Normal) << std::endl;
    Verbose::single(Verbose::Normal) << "#------------------------------- Upf2KB::compute_nonlinear_core_correction" << std::endl;
    //cout << " Analytic core charge  = " << analytic_core_charge << endl;
    Verbose::single(Verbose::Normal) << " Numerical core charge = " << numerical_core_charge << std::endl;
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
    Verbose::single().precision(6);
    Verbose::single() << std::scientific;

    return;
}

void Upf2KB::get_comp_potential(vector<double> radial_mesh, int itype, vector<double>& comp_potential){
    vector<std::array<double,3> > positions =atoms->get_positions();
    comp_potential.clear();

    double r,scaled_r, r_gauss;
    r_gauss = parameters->sublist("Pseudopotential").get<double>("LocalRgauss");
    double comp_norm = 0.0;
    for (int i=0;i< radial_mesh.size();i++){
        r=radial_mesh[i];
        if(std::abs(r) > 1.0E-30){
            scaled_r = r/r_gauss;
            //comp_norm += Zvals[itype]/pow(sqrt(M_PI)*r_gauss,3)*exp(-scaled_r*scaled_r);
            comp_potential.push_back( Zvals[itype]*erf(scaled_r)/r );
        } else {
            comp_potential.push_back(Zvals[itype]*2/r_gauss/sqrt(M_PI));
        }
    }

    return;
}

void Upf2KB::calculate_nonlocal(int iatom,std::array<double,3> position,
                                    vector< vector< vector<double> > >& nonlocal_pp,
                                    vector< vector< vector<double> > >& nonlocal_dev_x,
                                    vector< vector< vector<double> > >& nonlocal_dev_y,
                                    vector< vector< vector<double> > >& nonlocal_dev_z,
                                    vector< vector< vector<int> > >& nonlocal_pp_index )
{

    int * MyGlobalElements = basis->get_map()->MyGlobalElements();
    int NumMyElements = basis->get_map()->NumMyElements();
    int size = basis->get_original_size();
    const double** scaled_grid = basis->get_scaled_grid();
    int itype = atoms->get_atom_type(iatom);
    const double threshold=parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold");

    auto check_xyzr = [this,&MyGlobalElements,&position]
                                (int i, double& x, double& y, double& z, double& r)->void {
                                /*
                                double x_p, y_p, z_p;
                                basis-> get_position(MyGlobalElements[i], x_p, y_p, z_p);
                                x = x_p - position[0];
                                y = y_p - position[1];
                                z = z_p - position[2];
                                r = std::sqrt( x*x + y*y +z*z);
                                */
                                basis -> find_nearest_displacement(MyGlobalElements[i], position[0], position[1], position[2], x, y, z, r);
                                return;
    };


    {//initialize vectors
        nonlocal_pp = vector< vector< vector<double> > >(number_vps_file[itype] );
        nonlocal_pp_index = vector< vector< vector<int> > >(number_vps_file[itype] );
        if(is_cal_nonlocal_dev){
            nonlocal_dev_x = vector< vector< vector<double> > >(number_vps_file[itype] );
            nonlocal_dev_y = vector< vector< vector<double> > >(number_vps_file[itype] );
            nonlocal_dev_z = vector< vector< vector<double> > >(number_vps_file[itype] );
        }
    }


    /*  initialize case
        case_ == 0 : UsingDoubleGrid=0  & UsingFiltering = 1
        case_ == 1 : UsingDoubleGrid=0  & UsingFiltering = 1
        case_ == 2 : UsingDoubleGrid=1
    */
    int case_=0;

    Verbose::single(Verbose::Simple) << std::endl;
    Verbose::single(Verbose::Simple) << "#---------------------------------------------- Upf2KB::calculate_nonlocal" << std::endl;
    if (parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0){
        if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
            case_=1;
            Verbose::single(Verbose::Simple) << " Using filtering to represent pseudopotential" << std::endl;
        }
        else{
            Verbose::single(Verbose::Simple) << " Using single-grid to represent pseudopotential" << std::endl;
        }
    }
    else if (parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1) {
        if (parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
            Verbose::all() << "Can not use doublegrid and filtering at the same time" << std::endl;
            exit(-1);
        }
        case_=2;
        Verbose::single(Verbose::Simple) << " Using double-grid to represent pseudopotential" << std::endl;
    }
    else{
        Verbose::all() << "Invalid UsingDoubleGrid value " << parameters -> sublist("Pseudopotential").get<int>("UsingDoubleGrid") << std::endl;
        exit(-1);
    }
    Verbose::single(Verbose::Simple) << " Nonlocal threshold = " << threshold << std::endl;
    Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" <<std::endl;
    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0){
        timer->start("Upf2KB::shchoi");

        vector< std::function<bool(int)> > filter_functions;
        for(int i =0;i<number_vps_file[itype];i++){ // projector index
            int l = oamom[itype][i]; //angular momentum
            nonlocal_pp[i].resize(2*l+1);
            nonlocal_pp_index[i].resize(2*l+1);
            if(is_cal_nonlocal_dev){
                nonlocal_dev_x[i].resize(2*l+1);
                nonlocal_dev_y[i].resize(2*l+1);
                nonlocal_dev_z[i].resize(2*l+1);
            }
            for(int m=0;m<2*l+1;m++){ //  magnetic momentum index
                double rcut = new_nonlocal_cutoff[itype][i];
                auto check_distance = [rcut,check_xyzr](int i)->bool {
                                            double x; double y; double z; double r;
                                            check_xyzr(i,x,y,z,r);
                                            return rcut>r;
                                            };
                filter_functions.push_back(check_distance);
            }
        }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        std::vector<std::vector<int> > indices(filter_functions.size());
        LoopUtil::filter(NumMyElements,filter_functions,indices);
        int loop_index=0;
        for(int i =0;i<number_vps_file[itype];i++){ // angular momentum index
            int l = oamom[itype][i]; //angular momentum
            for(int m=0;m<2*l+1;m++){ //  magnetic momentum index
                vector<double> input_radial,input_radial_mesh;
                switch(case_){
                    case (0):
                        // No filtering, no doublegrid.
                        input_radial = new_nonlocal_pp_radial[itype][i];
                        input_radial_mesh = new_nonlocal_mesh[itype][i];
                    break;
                    case(1):
                        /// @todo Check that we should not add origin values.
                        filter->filter_nonlocal(new_nonlocal_pp_radial[itype][i],
                                                new_nonlocal_mesh[itype][i],
                                                nonlocal_d_mesh[itype][i],
                                                l, new_nonlocal_cutoff[itype][i],
                                                input_radial, input_radial_mesh);

                    break;
                    case(2):
                        // Doublegrid. But since if(..."UsingDoubleGrid"...) here, it does not work.
                    default:
                        Verbose::all() << "Filtering/doublegrid option case_ = " << case_ << std::endl;
                        throw std::logic_error("Invalid filtering/doublegrid option!");
                    break;
                }

                // Spline interpolation
                const int mesh_size = input_radial_mesh.size();
                double yp1 = (input_radial[1]-input_radial[0])/(input_radial_mesh[1]-input_radial_mesh[0]);
                double ypn = (input_radial[mesh_size-1]-input_radial[mesh_size-2])/(input_radial_mesh[mesh_size-1]-input_radial_mesh[mesh_size-2]);
                vector<double> y2 = Interpolation::Spline::spline(input_radial_mesh, input_radial, mesh_size, yp1, ypn);
                //double tmp;

                const int loop_size = indices[loop_index].size();

                nonlocal_pp[i][m].resize(loop_size);
                nonlocal_pp_index[i][m].resize(loop_size);
                if(is_cal_nonlocal_dev){
                    nonlocal_dev_x[i][m].resize(loop_size);
                    nonlocal_dev_y[i][m].resize(loop_size);
                    nonlocal_dev_z[i][m].resize(loop_size);
                }

                Parallel_Manager::info().all_barrier();
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for
                #endif
                for(int j=0; j<loop_size; j++){
                    double x,y,z,r;
                    double dev_spherical_value_x; double dev_spherical_value_y; double dev_spherical_value_z;
                    double value;
                    double value_x; double value_y; double value_z;
                    int original_j = MyGlobalElements[indices[loop_index][j]];
                    double xp, yp, zp;
                    basis -> get_position(original_j, xp, yp, zp);
                    /*
                    basis -> get_position(original_j, xp, yp, zp);
                    x = xp - position[0]; y = yp - position[1]; z = zp - position[2];
                    r = sqrt(x*x+y*y+z*z);
                    */
                    basis -> find_nearest_displacement(original_j, position[0], position[1], position[2], x, y, z, r);
                    double spherical_value = Spherical_Harmonics::Ylm(l, m-l, x, y, z);
                    double denominator = basis->compute_basis(original_j, xp, yp, zp);
                    if(is_cal_nonlocal_dev){
                        dev_spherical_value_x = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,0);
                        dev_spherical_value_y = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,1);
                        dev_spherical_value_z = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,2);
                    }
                    if(r < 1.0E-10){
                        value = spherical_value * input_radial[0] / denominator;
                        if(is_cal_nonlocal_dev){
                            value_x = dev_spherical_value_x * input_radial[0] / denominator ;
                            value_y = dev_spherical_value_y * input_radial[0] / denominator ;
                            value_z = dev_spherical_value_z * input_radial[0] / denominator ;
                        }
                    }
                    else {
                        double radial_value = splint(input_radial_mesh, input_radial, y2, mesh_size, r);
                        value = spherical_value * radial_value / denominator;
                        if(is_cal_nonlocal_dev){
                            double der_value = numerical_derivatives(r,input_radial_mesh, input_radial, 1, 3);
                            value_x = (dev_spherical_value_x * radial_value + spherical_value * der_value*x/r) / denominator ;
                            value_y = (dev_spherical_value_y * radial_value + spherical_value * der_value*y/r) / denominator ;
                            value_z = (dev_spherical_value_z * radial_value + spherical_value * der_value*z/r) / denominator ;
                        }
                    }
                    nonlocal_pp[i][m][j] =value;
                    nonlocal_pp_index[i][m][j] = original_j;
                    if(is_cal_nonlocal_dev){
                        nonlocal_dev_x[i][m][j] =value_x;
                        nonlocal_dev_y[i][m][j] =value_y;
                        nonlocal_dev_z[i][m][j] =value_z;
                    }
                }
                Parallel_Util::group_gather_vector(nonlocal_pp_index[i][m]);
                Parallel_Util::group_gather_vector(nonlocal_pp[i][m]);
                if(is_cal_nonlocal_dev){
                    Parallel_Util::group_gather_vector(nonlocal_dev_x[i][m]);
                    Parallel_Util::group_gather_vector(nonlocal_dev_y[i][m]);
                    Parallel_Util::group_gather_vector(nonlocal_dev_z[i][m]);
                }
                loop_index++;
            }
        }
        timer->end("Upf2KB::shchoi");
        Verbose::single(Verbose::Normal) << "Upf2KB time(s): " << timer->get_elapsed_time("Upf2KB::shchoi",-1) <<std::endl;
    }
    else{
        timer->start("Upf2KB::calculate_nonlocal with double grid");

        Verbose::single(Verbose::Simple) << std::endl;
        Verbose::single(Verbose::Simple) <<"#---------------------------------------------- Upf2KB::calculate_nonlocal" << std::endl;
        Verbose::single(Verbose::Simple) << " Using double-grid to represent pseduopotential " << std::endl;
        Verbose::single(Verbose::Simple) << " Non-local pseudopotential integration Rmax : " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << std::endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << std::endl;

        vector<vector<vector<double> > > my_nonlocal_pp;
        vector<vector<vector<double> > > my_nonlocal_dev_x;
        vector<vector<vector<double> > > my_nonlocal_dev_y;
        vector<vector<vector<double> > > my_nonlocal_dev_z;

        auto scaling = basis->get_scaling();
        auto points =  basis -> get_points();
        std::array<double,3> fine_scaling ;
        std::array<int,3>    fine_points ;
        for(int d = 0; d < 3; ++d){
            fine_scaling[d] = scaling[d] / fine_dimension;
            fine_points[d] = fine_dimension*(points[d]-1)+1;
        }

        vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients( parameters -> sublist("Pseudopotential").get<string>("FilterType"), this -> fine_dimension );

        for(int i=0; i<number_vps_file[itype]; i++){
            int l = oamom[itype][i];

            my_nonlocal_pp.push_back( vector<vector<double> >() );
            my_nonlocal_dev_x.push_back( vector<vector<double> >() );
            my_nonlocal_dev_y.push_back( vector<vector<double> >() );
            my_nonlocal_dev_z.push_back( vector<vector<double> >() );

            for(int m=0; m<2*l+1; m++){
                my_nonlocal_pp[i].push_back(vector<double>() );
                my_nonlocal_dev_x[i].push_back(vector<double>() );
                my_nonlocal_dev_y[i].push_back(vector<double>() );
                my_nonlocal_dev_z[i].push_back(vector<double>() );

                nonlocal_pp[i].push_back(vector<double>() );
                nonlocal_dev_x[i].push_back(vector<double>() );
                nonlocal_dev_y[i].push_back(vector<double>() );
                nonlocal_dev_z[i].push_back(vector<double>() );

                nonlocal_pp_index[i].push_back(vector<int>() );

                vector<double> input_radial = new_nonlocal_pp_radial[itype][i];
                vector<double> input_radial_mesh = new_nonlocal_mesh[itype][i];

                int mesh_size = input_radial.size();
                vector<double> y2;

                for(int q=0; q<mesh_size; q++){
                    double tmp = Numerical_Derivatives::numerical_derivatives(input_radial_mesh[q], input_radial_mesh, input_radial, 2, 5);
                    y2.push_back(tmp);
                }

                double rcut = new_nonlocal_cutoff[itype][i];
                Verbose::single(Verbose::Detail) << "l : " << l << "\t m : " << m << "\trcut : " << rcut << std::endl;
                RCP<Basis> fine_basis = Create_Basis::Create_Auxiliary_Basis(fine_points, fine_scaling, basis -> get_map() -> Comm(), "Sinc", atoms->operator[](iatom), rcut);
                int NumFineElements = fine_basis -> get_map() -> NumMyElements();
                int* GlobalFineElements = fine_basis -> get_map() -> MyGlobalElements();

                for(int j=0; j<size; j++){
                    double rmax = rcut*parameters->sublist("Pseudopotential").get<double>("NonlocalRmax"); // This value should be decided.
                    double x, y, z, r;
                    /*
                    double x_p, y_p, z_p;
                    basis->get_position(j,  x_p, y_p, z_p);
                    double x = x_p - position[0];
                    double y = y_p - position[1];
                    double z = z_p - position[2];
                    double r = sqrt( x*x + y*y + z*z );
                    */
                    basis -> find_nearest_displacement(j, position[0], position[1], position[2], x, y, z, r);

                    if(r < rmax){
                        double value_tmp=0.0, value_tmp_x=0.0, value_tmp_y=0.0, value_tmp_z = 0.0;
                        double spherical_value,dev_spherical_value_x,dev_spherical_value_y,dev_spherical_value_z;
                        #ifdef ACE_HAVE_OMP
                        #pragma omp parallel for private(spherical_value,dev_spherical_value_x,dev_spherical_value_y,dev_spherical_value_z) reduction(+:value_tmp),reduction(+:value_tmp_x),reduction(+:value_tmp_y),reduction(+:value_tmp_z)
                        #endif
                        for(int fine_j=0; fine_j<NumFineElements; fine_j++){
                            double xa, yb, zc, fine_r;
                            double x_fine, y_fine, z_fine;
                            fine_basis -> get_position(GlobalFineElements[fine_j], x_fine, y_fine, z_fine);
                            /*
                            double xa = x_fine - position[0];
                            double yb = y_fine - position[1];
                            double zc = z_fine - position[2];
                            double fine_r = sqrt(xa*xa + yb*yb + zc*zc);
                            */
                            fine_basis -> find_nearest_displacement(GlobalFineElements[fine_j], position[0], position[1], position[2], xa, yb, zc, fine_r);

                            int zx = round(abs(x - x_fine)/fine_scaling[0]);
                            int zy = round(abs(y - y_fine)/fine_scaling[1]);
                            int zz = round(abs(z - z_fine)/fine_scaling[2]);

                            double sinc_x = sampling_coefficients[zx];
                            double sinc_y = sampling_coefficients[zy];
                            double sinc_z = sampling_coefficients[zz];

                            double pre_factor = sinc_x*sinc_y*sinc_z;
                            spherical_value = Spherical_Harmonics::Ylm(l, m-l, xa, yb, zc);
                            dev_spherical_value_x = Spherical_Harmonics::Derivative::dYlm(l, m-l, xa, yb, zc,0);
                            dev_spherical_value_y = Spherical_Harmonics::Derivative::dYlm(l, m-l, xa, yb, zc,1);
                            dev_spherical_value_z = Spherical_Harmonics::Derivative::dYlm(l, m-l, xa, yb, zc,2);
                            if(fine_r > 1.0E-30){
                                value_tmp+= pre_factor * spherical_value * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r);
                                value_tmp_x += pre_factor * (spherical_value * Numerical_Derivatives::numerical_derivatives(fine_r, input_radial_mesh, input_radial, 1, 3)*xa/fine_r + dev_spherical_value_x * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r));
                                value_tmp_y += pre_factor * (spherical_value * Numerical_Derivatives::numerical_derivatives(fine_r, input_radial_mesh, input_radial, 1, 3)*yb/fine_r + dev_spherical_value_y * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r));
                                value_tmp_z += pre_factor * (spherical_value * Numerical_Derivatives::numerical_derivatives(fine_r, input_radial_mesh, input_radial, 1, 3)*zc/fine_r + dev_spherical_value_z * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r));
                            }
                            else{
                                value_tmp+= pre_factor * spherical_value * input_radial[0];
                            }
                        }

                        value_tmp   /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);
                        value_tmp_x /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);
                        value_tmp_y /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);
                        value_tmp_z /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);

                        nonlocal_pp[i][m].push_back(0.0);
                        nonlocal_dev_x[i][m].push_back(0.0);
                        nonlocal_dev_y[i][m].push_back(0.0);
                        nonlocal_dev_z[i][m].push_back(0.0);

                        my_nonlocal_dev_x[i][m].push_back(value_tmp_x );
                        my_nonlocal_dev_y[i][m].push_back(value_tmp_y );
                        my_nonlocal_dev_z[i][m].push_back(value_tmp_z );
                        nonlocal_pp_index[i][m].push_back(j);

                    }
                }
            }
        }


        for(int i=0; i<number_vps_file[itype]; i++){
            int l = oamom[itype][i];
            for(int m=0; m<2*l+1; m++){
                Parallel_Util::group_sum(&my_nonlocal_pp[i][m][0], &nonlocal_pp[i][m][0], nonlocal_pp_index[i][m].size() );
                Parallel_Util::group_sum(&my_nonlocal_dev_x[i][m][0], &nonlocal_dev_x[i][m][0], nonlocal_pp_index[i][m].size());
                Parallel_Util::group_sum(&my_nonlocal_dev_y[i][m][0], &nonlocal_dev_y[i][m][0], nonlocal_pp_index[i][m].size());
                Parallel_Util::group_sum(&my_nonlocal_dev_z[i][m][0], &nonlocal_dev_z[i][m][0], nonlocal_pp_index[i][m].size());
                Verbose::single(Verbose::Detail) << "Upf2KB::index size : " << nonlocal_pp_index[i][m].size() << std::endl;
                for (int q=0;q<nonlocal_pp_index[i][m].size();q++){
                    if (fabs(nonlocal_pp[i][m][q])<threshold){
                        nonlocal_pp_index[i][m].erase(nonlocal_pp_index[i][m].begin()+q);
                        nonlocal_pp[i][m].erase(nonlocal_pp[i][m].begin()+q);
                        nonlocal_dev_x[i][m].erase(nonlocal_dev_x[i][m].begin()+q);
                        nonlocal_dev_y[i][m].erase(nonlocal_dev_y[i][m].begin()+q);
                        nonlocal_dev_z[i][m].erase(nonlocal_dev_z[i][m].begin()+q);

                        q--;
                    }
                }

                Verbose::single(Verbose::Detail) << "index size : " << nonlocal_pp_index[i][m].size() << std::endl;
            }
        }
        timer->end("Upf2KB::calculate_nonlocal with double grid");
        Verbose::single(Verbose::Simple) << std::endl;
        Verbose::single(Verbose::Simple) <<"#---------------------------------------------- Upf2KB::calculate_nonlocal" << std::endl;
        Verbose::single(Verbose::Simple) << " Time to compute numerical integration: " << timer->get_elapsed_time("Upf2KB::calculate_nonlocal with double grid",-1) << " s" << std::endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << std::endl;
    }
    return;
}

void Upf2KB::pp_information(int itype){
    // Printing pseudopotential information
    Verbose::single(Verbose::Simple) <<std::endl;
    Verbose::single(Verbose::Simple) << "==========================================================" << std::endl;
    Verbose::single(Verbose::Simple) << "Pseudopotential file for this atom  : " << upf_filenames[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Valence charge                      : " << Zvals[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Number of projectors                : " << number_vps_file[itype] << std::endl;
    for(int i=0;i<number_vps_file[itype];i++){
        Verbose::single(Verbose::Simple) << "     Orbital angular momentum       : " << oamom[itype][i] << std::endl;
    }
    Verbose::single(Verbose::Simple) << "Number of atomic pseudo orbitals    : " << number_pao_file[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Number of points in basis            : " << mesh_size[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Nonlinear core correction           : " << nonlinear_core_correction[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "==========================================================" << std::endl;
    Verbose::single(Verbose::Simple) << std::endl;
    // end

    return;
}

void Upf2KB::make_local_pp(){
     int size = basis->get_original_size();
     local_pp.resize(atoms->get_size() );

     for(int iatom=0; iatom<atoms->get_size(); iatom++){
         local_pp[iatom].resize(size);
         #ifdef ACE_HAVE_OMP
         #pragma omp parallel for
         #endif
         for(int i=0; i<size; i++){
             local_pp[iatom][i] = 0.0;
         }
     }

     if(is_cal_local_dev)
     {
         local_pp_dev.resize(atoms->get_size());
         for(int iatom=0; iatom<atoms->get_size(); iatom++){
             local_pp_dev[iatom].resize(size);
             #ifdef ACE_HAVE_OMP
             #pragma omp parallel for
             #endif
             for(int i=0; i<size; i++){
                 local_pp_dev[iatom][i] = 0.0;
             }
         }
     }

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1){
        timer->start("Upf2KB::double-grid integration");
        Verbose::single(Verbose::Normal) <<std::endl;
        Verbose::single(Verbose::Normal) <<"#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
        Verbose::single(Verbose::Normal) << "Using double-grid to represent pseudopotential : make local_pp vector" << std::endl;
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;

        //int ierr;
        const int * MyGlobalElements = basis->get_map()->MyGlobalElements();
        const int NumMyElements = basis->get_map()->NumMyElements();

        vector< vector<double> > short_potential_radial;
        vector< vector <double> > comp_pp_radial;
        vector< vector <double> > y2_short;
        vector< vector <double> > y2_comp;
        vector< vector <double> > y2_local;
        vector<double> rcut;

        //////////////////////////////////////////////////////////////////////////////////////
        auto scaling = basis->get_scaling();
        auto points = basis -> get_points();
        std::array<double,3> fine_scaling;
        std::array<int,3>    fine_points;
        for(int d = 0; d < 3; ++d){
            fine_scaling[d] = scaling[d] / fine_dimension;
            fine_points[d] = fine_dimension*(points[d]-1)+1;
        }

        vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients(parameters->sublist("Pseudopotential").get<string>("FilterType"), fine_dimension);

        for(int itype=0; itype<atoms->get_atom_types().size(); itype++){
            // Local basis and Short & Comp potential //
            local_mesh.push_back( original_mesh[itype]);
            local_cutoff.push_back( original_mesh[itype][local_pp_radial[itype].size()-1]);

            vector<double> comp_potential;
            vector<double> short_potential_radial_tmp;
            this->get_comp_potential(original_mesh[itype],itype,comp_potential);
            comp_pp_radial.push_back(comp_potential);

            for (int i =0;i<original_mesh[itype].size(); i++) {
                double short_potential_radial_tmp_tmp;
                short_potential_radial_tmp_tmp = comp_pp_radial[itype][i] + local_pp_radial[itype][i];
                short_potential_radial_tmp.push_back(short_potential_radial_tmp_tmp);
                //          cout << " short_potential_radial_tmp_tmp " << comp_pp_radial[itype][i] << " " << local_pp_radial[itype][i] << endl;
            }
            /*
               cout << "local pp radial : " << local_pp_radial[0].size() << endl;
               for(int k=0; k<local_pp_radial[0].size(); k++){
               cout << local_pp_radial[0][k] << endl;
               }
               */
            short_potential_radial.push_back(short_potential_radial_tmp);

            //Gather cutoff radius information//
            double rcut_tmp = 0.0;
            for(int i=0; i<local_mesh[itype].size(); i++){
                if(abs(short_potential_radial[itype][i]) < parameters->sublist("Pseudopotential").get<double>("ShortDecayTol") ){
                    rcut_tmp = local_mesh[itype][i];
                    break;
                }
            }
            rcut.push_back(rcut_tmp);

            // Make second derivative terms for SplInt. //
            int mesh_size = local_mesh[itype].size();
            vector<double> y2_short_tmp;
            vector<double> y2_comp_tmp;
            vector<double> y2_local_tmp;
            for(int i=0; i<mesh_size; i++){
                double y2_short_tmp_tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], short_potential_radial[itype], 2, 5);
                double y2_local_tmp_tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], local_pp_radial[itype], 2, 5);
                double y2_comp_tmp_tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], comp_pp_radial[itype], 2, 5);
                y2_short_tmp.push_back(y2_short_tmp_tmp);
                y2_local_tmp.push_back(y2_local_tmp_tmp);
                y2_comp_tmp.push_back(y2_comp_tmp_tmp);
            }
            y2_short.push_back(y2_short_tmp);
            y2_comp.push_back(y2_comp_tmp);
            y2_local.push_back(y2_local_tmp);
            // Make second derivative terms for SplInt. //

        }
        ///////////////////////////////////////////////
        vector<std::array<double,3> > positions = atoms->get_positions();
        for (int iatom=0;iatom<atoms->get_size();iatom++){
            vector<double> local_pp_index_out;
            vector<double> local_pp_index_in;

            ////////////////////         Spline interpolation           ///////////////////////////
            int itype = atoms->get_atom_type(iatom);
            int mesh_size = local_mesh[itype].size();

            ////////////////////         Check in or out           /////////////////////////////
            for(int j=0; j<size; j++){
                double x1 = 0.0, y1=0.0, z1=0.0, r1=0.0;
                /*
                double x_p,y_p,z_p;
                basis->get_position(j, x_p, y_p, z_p);
                x1 = x_p - positions[iatom][0];
                y1 = y_p - positions[iatom][1];
                z1 = z_p - positions[iatom][2];
                r1 = sqrt( x1*x1 + y1*y1 + z1*z1 );
                */
                basis -> find_nearest_displacement(j, positions[iatom][0], positions[iatom][1], positions[iatom][2], x1, y1, z1, r1);

                if(r1 < rcut[itype]){
                    local_pp_index_in.push_back(j);
                }
                else{
                    local_pp_index_out.push_back(j);
                }
            }
            int in_size = local_pp_index_in.size();

            vector<double> radius;
            double rcut_in = rcut[itype]*parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange");
            radius.push_back(rcut_in);
            //jaechang
            std::array<int,3> tmp_fine_points;
            tmp_fine_points[0] = fine_points[0];
            tmp_fine_points[1] = fine_points[1];
            tmp_fine_points[2] = fine_points[2];
            
            RCP<Basis> fine_basis = Create_Basis::Create_Auxiliary_Basis(tmp_fine_points, fine_scaling, basis -> get_map() -> Comm(), "Sinc", atoms->operator[](iatom), rcut_in);

            int fine_NumMyElements = fine_basis -> get_map() -> NumMyElements();
            int* fine_GlobalMyElements = fine_basis -> get_map() -> MyGlobalElements();

            for (int local_k=0; local_k < in_size; local_k++){
                int i= local_pp_index_in[local_k];
                double xi = 0.0, yi = 0.0, zi = 0.0, ri = 0.0;
                double x_p,y_p,z_p;
                basis->get_position(i, x_p, y_p, z_p);
                /*
                xi = x_p - positions[iatom][0];
                yi = y_p - positions[iatom][1];
                zi = z_p - positions[iatom][2];
                ri = sqrt( xi*xi + yi*yi + zi*zi );
                */
                basis -> find_nearest_displacement(i, positions[iatom][0], positions[iatom][1], positions[iatom][2], xi, yi, zi, ri);

                ////////////////////////   Integration     //////////////////////////////////
                double total_value = 0.0, total_dev = 0.0, value_tmp = 0.0, dev_tmp = 0.0;
                for(int fine_q=0; fine_q<fine_NumMyElements; fine_q++){
                    double x_fine, y_fine, z_fine;
                    fine_basis -> get_position(fine_GlobalMyElements[fine_q], x_fine, y_fine, z_fine);

                    double qx = x_fine - positions[iatom][0];
                    double qy = y_fine - positions[iatom][1];
                    double qz = z_fine - positions[iatom][2];
                    double fine_r = sqrt((qx)*(qx) + (qy)*(qy) + (qz)*(qz));

                    int zx = round(abs(x_p - x_fine)/fine_scaling[0]);
                    int zy = round(abs(y_p - y_fine)/fine_scaling[1]);
                    int zz = round(abs(z_p - z_fine)/fine_scaling[2]);

                    double sinc_x = sampling_coefficients[zx];
                    double sinc_y = sampling_coefficients[zy];
                    double sinc_z = sampling_coefficients[zz];


                    value_tmp +=  sinc_x * sinc_y * sinc_z * Interpolation::Spline::splint(local_mesh[itype], short_potential_radial[itype], y2_short[itype], mesh_size, fine_r);
                    if(is_cal_local_dev){
                        dev_tmp +=  sinc_x * sinc_y * sinc_z * Numerical_Derivatives::numerical_derivatives(fine_r, local_mesh[itype], short_potential_radial[itype], 1, 3);
                    }
                }
                if(is_cal_local_dev){
                    dev_tmp /= pow(fine_dimension,3);
                }
                value_tmp /= pow(fine_dimension,3);


                Parallel_Util::group_sum(&value_tmp, &total_value, 1);
                if(is_cal_local_dev){
                    Parallel_Util::group_sum(&dev_tmp, &total_dev, 1);
                }
                local_pp[iatom][i] += total_value;
                //local_pp[i] += total_value;
                if(is_cal_local_dev){
                    local_pp_dev[iatom][i] += total_dev;
                }
            }

            ////////////////////////// calcualte rest of points (only for diagonal values )   //////////////
            for(int local_j=0; local_j<NumMyElements; local_j++){
                int j = MyGlobalElements[local_j];
                bool is_in = false;
                for (int p=0;p<local_pp_index_in.size();p++){
                    if(local_pp_index_in[p]==j ){
                        //local_pp_index_in.erase(local_pp_index_in.begin() + p);
                        is_in=true;
                        break;
                    }
                }

                double x=0.0, y=0.0, z=0.0, r=0.0, tmp=0.0, dev_tmp=0.0;
                /*
                double x_p,y_p,z_p;
                basis->get_position(j, x_p, y_p, z_p);
                x = x_p - positions[iatom][0];
                y = y_p - positions[iatom][1];
                z = z_p - positions[iatom][2];
                r = sqrt( x*x + y*y + z*z );
                */
                basis -> find_nearest_displacement(j, positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);
                if(is_in==true){
                    if(r <= local_cutoff[itype]){
                        tmp= -Interpolation::Spline::splint(local_mesh[itype], comp_pp_radial[itype], y2_comp[itype], mesh_size, r);
                        if(is_cal_local_dev){
                            dev_tmp = -Numerical_Derivatives::numerical_derivatives(r, local_mesh[itype], comp_pp_radial[itype], 1, 3);
                        }

                    }
                }
                else{
                    if(r <= local_cutoff[itype]){
                        tmp = Interpolation::Spline::splint(local_mesh[itype], local_pp_radial[itype], y2_local[itype], mesh_size, r);
                        if(is_cal_local_dev){
                            dev_tmp = Numerical_Derivatives::numerical_derivatives(r, local_mesh[itype], local_pp_radial[itype], 1, 3);
                        }
                    }
                }

                local_pp[iatom][j] += tmp;
                //local_pp[j] += tmp;
                if(is_cal_local_dev){
                    local_pp_dev[iatom][j] += dev_tmp;
                }
                /*
                   if(ierr < 0){ // If the allocated length of the row has to be expanded, a positive warning code will be returned.
                   Verbose::all() << iatom <<  "ERROR::core_hamiltonian->InsertGlobalValues(j, 1, &tmp, &j) - ierr = " << ierr << std::endl;
                   }
                   */

            }
            //delete new_atom;
        }// for(iatom)

        for(int k=0; k<local_pp_radial.size(); k++){
            local_pp_radial[k].erase(local_pp_radial[k].begin());
        }
        /*
           cout << "local pp radial2 : " << local_pp_radial[0].size() << endl;
           for(int k=0; k<local_pp_radial[0].size(); k++){
           cout << local_pp_radial[0][k] << endl;
           }
           */
        //
        timer->end("Upf2KB::double-grid integration");
        Verbose::single(Verbose::Normal) << std::endl;
        Verbose::single(Verbose::Normal) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
        Verbose::single(Verbose::Normal) << " Time to compute double-grid integration: " << timer->get_elapsed_time("Upf2KB::double-grid integration",-1) << " s" << std::endl;
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
        /*
           for(int i=0; i<local_pp.size(); i++){
           cout << "check local pp" << endl;
           for(int j=0; j<local_pp[i].size(); j++){
           cout <<j << " " << local_pp[i][j] << endl;
           }
           }
           for(int i=0; i<local_pp_index_in.size(); i++){
           cout << "check local pp_index_in" << endl;
           for(int j=0; j<local_pp_index_in[i].size(); j++){
           cout <<j << " " << local_pp_index_in[i][j] << endl;
           }
           }
           */
    }
    else if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0){
        timer->start("single-grid integration");
        int * MyGlobalElements = basis->get_map()->MyGlobalElements();
        int NumMyElements = basis->get_map()->NumMyElements();
        if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 0){
            Verbose::single(Verbose::Normal) << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
            Verbose::single(Verbose::Normal) << "Using single-grid to represent pseudopotential without filtering" << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
            vector< vector<double> > local_potential_radial;
            for (int itype=0; itype<atoms->get_atom_types().size(); itype++){
                local_potential_radial.push_back(local_pp_radial[itype]);
                local_mesh.push_back( original_mesh[itype]);
                local_cutoff.push_back( original_mesh[itype][local_potential_radial[itype].size()-1]);
            }

            vector<std::array<double,3> > positions = atoms->get_positions();

            /*for(int i=0;i<NumMyElements;i++){
              local_pp[MyGlobalElements[i]] = 0.0;
              }*/

            // Why this???
            for (int iatom=0;iatom<atoms->get_size();iatom++){
                int itype = atoms->get_atom_type(iatom);
                // Spline interpolation
                int mesh_size = local_mesh[itype].size();
                //double tmp;
                double yp1 = (local_potential_radial[itype][1]-local_potential_radial[itype][0])/(local_mesh[itype][1]-local_mesh[itype][0]);
                double ypn = (local_potential_radial[itype][mesh_size-1]-local_potential_radial[itype][mesh_size-2])/(local_mesh[itype][mesh_size-1]-local_mesh[itype][mesh_size-2]);
                vector<double> y2 = Interpolation::Spline::spline(local_mesh[itype], local_potential_radial[itype], mesh_size, yp1, ypn);
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif
                for(int i=0;i<NumMyElements;i++){
                    double x,y,z,r;
                    /*
                    basis->get_position(MyGlobalElements[i],x_p,y_p,z_p);
                    x = x_p - positions[iatom][0];
                    y = y_p - positions[iatom][1];
                    z = z_p - positions[iatom][2];
                    r = sqrt( x*x + y*y + z*z );
                    */
                    basis -> find_nearest_displacement(MyGlobalElements[i], positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);
                    if(r <= local_cutoff[itype]){
                        double tmp = Interpolation::Spline::splint(local_mesh[itype], local_potential_radial[itype], y2, mesh_size, r);
                        //double tmp = Interpolation::Linear::linear_interpolate(r, local_mesh[itype], short_potential_radial[itype]);
                        double dev_tmp = Numerical_Derivatives::numerical_derivatives(r,local_mesh[itype], local_potential_radial[itype], 1,3);
                        //double dev_tmp = Radial_Grid::Paw::linear_derivative(r,local_mesh[itype], short_potential_radial[itype], 1,3);
                        //local_pp[MyGlobalElements[i]] += tmp;
                        local_pp[iatom][MyGlobalElements[i]] += tmp;
                        local_pp_dev[iatom][MyGlobalElements[i]] = dev_tmp;
                        //local_potential->operator[](i) += tmp;
                        //short_potential->operator[](iatom)[i] = tmp;
                        //cout << "tmp : " << tmp << ", r :  " << r << endl;
                    }
                }
            }
        }
        //by jaechang
        else if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
            Verbose::single(Verbose::Normal) << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
            Verbose::single(Verbose::Normal) << "Using single-grid to represent pseudopotential with filtering" << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
            /*
               for(int i=0;i<NumMyElements;i++){
               local_pp[MyGlobalElements[i]] = 0.0;
               }
               */

            vector<std::array<double,3> > positions = atoms->get_positions();

            int itype;
            timer->start("filtering timer1");
            filter->optimize_comp_charge_exps(atomic_density, original_mesh, original_d_mesh);
            vector< vector<double> > filtered_local_pp_radial;
            vector< vector<double> > filtered_local_mesh;
            for (int itype=0; itype<atoms->get_atom_types().size(); itype++){
                local_cutoff.push_back( original_mesh[itype][original_mesh[itype].size()-1]);
                vector<double> new_local_pp_radial_tmp; vector<double> new_mesh_tmp;
                filter -> filter_local(local_pp_radial[itype], original_mesh[itype], original_d_mesh[itype], itype, new_local_pp_radial_tmp, new_mesh_tmp);
                filtered_local_pp_radial.push_back(new_local_pp_radial_tmp);
                filtered_local_mesh.push_back(new_mesh_tmp);
            }
//            #ifdef ACE_HAVE_OMP
//            #pragma omp parallel for private(x,y,z,r,i_x,i_y,i_z,itype) collapse(2)
//            #endif
            for (int iatom=0;iatom<atoms->get_size();iatom++){
                double local_size = original_mesh[itype].size();
                vector<double> y2l = Interpolation::Spline::spline(original_mesh[itype], filtered_local_pp_radial[itype], local_size);

                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for
                #endif
                for(int i=0;i<NumMyElements;i++){
                    double x,y,z,r;
                    /*
                    double x_p,y_p,z_p;
                    basis->get_position(MyGlobalElements[i],x_p,y_p,z_p);
                    x = x_p - positions[iatom][0];
                    y = y_p - positions[iatom][1];
                    z = z_p - positions[iatom][2];
                    r = sqrt( x*x + y*y + z*z );
                    */
                    basis -> find_nearest_displacement(MyGlobalElements[i], positions[iatom][0], positions[iatom][1], positions[iatom][2], x, y, z, r);
                    double tmp = 0.0;
                    double dev_tmp = 0.0;
                    if(r < 1.0E-30){
                        tmp = filtered_local_pp_radial[itype][0];
                        dev_tmp = 0.0;// RLY?
                    } else if(r <= local_cutoff[itype]){
                        tmp = Interpolation::Spline::splint(original_mesh[itype], filtered_local_pp_radial[itype], y2l, original_mesh[itype].size(), r);
                        dev_tmp = Numerical_Derivatives::numerical_derivatives(r, original_mesh[itype], filtered_local_pp_radial[itype], 1, 3);
                    }
                    local_pp[iatom][MyGlobalElements[i]] += tmp;
                    local_pp_dev[iatom][MyGlobalElements[i]] += dev_tmp;
                }
            }
            timer->end("filtering timer1");
            Verbose::single(Verbose::Detail) << " filtering timer1: " << timer->get_elapsed_time("filtering timer1",-1) << " s" << std::endl;
        }

        //short_potential_radial.clear();
        //local_mesh.clear();
        //local_cutoff.clear();
        timer->end("single-grid integration");
        Verbose::single(Verbose::Simple) << std::endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
        Verbose::single(Verbose::Simple) << " Time to compute single-grid integration: " << timer->get_elapsed_time("single-grid integration",-1) << " s" << std::endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << std::endl;


    }
    //arrange_local_pp();
    return;
}


    /*
       void Upf2KB::arrange_local_pp(){
       int * MyGlobalElements = basis->get_map()->MyGlobalElements();
       int NumMyElements =basis->get_map()->NumMyElements();
       double ** tmp1 = new double* [local_pp.size()];
       for(int i=0; i<local_pp.size(); i++){
       tmp1[i] = new double [local_pp[i].size()];
       double* tmp2 = new double[local_pp[i].size()];
       for(int k=0; k<local_pp[i].size();k++)
       tmp2[k] = local_pp[i][k];
//Parallel_Util::group_sum(&local_pp[i][0] , tmp1[i], local_pp[i].size());
Parallel_Util::group_sum(tmp2 , tmp1[i], local_pp[i].size());
delete tmp2;
}
for(int i=0; i<local_pp[0].size(); i++)
cout << i << " " << local_pp[0][i] << " " << local_pp[1][i] << endl;
local_pp.clear();
local_pp.resize(atoms->get_original_size());
int size = basis->get_original_size();
for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
local_pp[iatom].resize(size);
for(int i=0; i<size; i++){
local_pp[iatom][i] = 0.0;
}
}
for(int i=0; i<local_pp.size(); i++){
for(int j=0; j<NumMyElements; j++){
local_pp[i][MyGlobalElements[j]] = tmp1[i][MyGlobalElements[j]];
}
}
for(int i=0; i< local_pp.size(); i++){
delete [] tmp1[i];
}
delete tmp1;
cout << "check" << endl;

return ;
}
*/
void Upf2KB::initialize_pp(){
    // Filtering non-local pseudopotentials
    for(int itype=0; itype<atoms->get_atom_types().size(); itype++){

        vector< vector<double> > new_nonlocal_mesh_tmp;
        vector< vector<double> > new_nonlocal_pp_radial_tmp;
        vector<double> new_nonlocal_cutoff_tmp;

        for(int i=0; i<number_vps_file[itype]; i++){
            int l = oamom[itype][i];

            vector<double> new_nonlocal_mesh_tmp_tmp;
            vector<double> new_nonlocal_pp_radial_tmp_tmp;

            for(int t=0; t<nonlocal_mesh[itype][i].size(); t++){
                new_nonlocal_mesh_tmp_tmp.push_back(nonlocal_mesh[itype][i][t]);
            }
            for(int t=0; t<nonlocal_pp_radial[itype][i].size(); t++){
                new_nonlocal_pp_radial_tmp_tmp.push_back(nonlocal_pp_radial[itype][i][t]);
            }
            new_nonlocal_mesh_tmp.push_back(new_nonlocal_mesh_tmp_tmp);
            new_nonlocal_pp_radial_tmp.push_back(new_nonlocal_pp_radial_tmp_tmp);
            new_nonlocal_cutoff_tmp.push_back(nonlocal_cutoff[itype][i]);
        }
        new_nonlocal_mesh.push_back(new_nonlocal_mesh_tmp);
        new_nonlocal_pp_radial.push_back(new_nonlocal_pp_radial_tmp);
        new_nonlocal_cutoff.push_back(new_nonlocal_cutoff_tmp);
    }
    // end
    if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
        //parameters->sublist("Pseudopotential").get<double>("NonlocalRmax", 2.0);
        double gamma_local = parameters->sublist("Pseudopotential").get<double>("GammaLocal", 2.0);
        double gamma_nonlocal = parameters->sublist("Pseudopotential").get<double>("GammaNonlocal", 2.0);
        double alpha_local = parameters->sublist("Pseudopotential").get<double>("AlphaLocal", 1.1);
        double alpha_nonlocal = parameters->sublist("Pseudopotential").get<double>("AlphaNonlocal", 1.1);
        double eta = parameters->sublist("Pseudopotential").get<double>("Eta",0.0);
        this->filter = rcp(new Filter(basis, atoms, Zvals, gamma_local, gamma_nonlocal, alpha_local, alpha_nonlocal,eta)); 
    }
}



