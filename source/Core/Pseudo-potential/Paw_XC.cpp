#include "Paw_XC.hpp"

#include <cmath>
#include <iostream>
#include <xc.h>

#include "../../Utility/Interpolation/Radial_Grid_Paw.hpp"
#include "../../Utility/Calculus/Lebedev_Quadrature.hpp"
#include "../../Utility/Math/Spherical_Harmonics_Derivative.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"

#define COEF_CUTOFF_ (1.0E-10)// If coefficient for phi_i phi_j is smaller than this, the product is zero.
#define POS_ZERO (1.0E-15)// If position value is smaller than this, it is origin.

using std::abs;
using std::make_pair;
using std::vector;
using Teuchos::Array;
using Teuchos::SerialDenseMatrix;
using Spherical_Harmonics::Derivative::dYlm;

Paw_XC::Paw_XC( Teuchos::RCP<Paw_Species> paw_species, int lmax/* = -1*/ ){
    this -> timer = Teuchos::rcp(new Time_Measure());
    this -> timer -> start("Initialize PAW XC");
    this -> xc_functionals = paw_species -> get_xc_functionals();
    this -> grid = paw_species -> get_grid();
    this -> grid_deriv = paw_species -> get_grid_derivative();

    this -> rc = -1.0;
    for(int i = 0; i < paw_species -> get_partial_wave_types().size(); ++i){
        double tmprc = paw_species -> get_partial_wave_state(i).cutoff_radius;
        /*
        vector<double> ae_pw = paw_species -> get_all_electron_partial_wave_r(i);
        vector<double> ps_pw = paw_species -> get_smooth_partial_wave_r(i);
        double tmprc = -1;
        for(int r = 0; r < this -> grid.size(); ++r){
            if( std::abs(ae_pw[r] - ps_pw[r]) < 1.0E-10 ){
                tmprc = this -> grid[r];
                break;
            }
        }
        */
        if( tmprc > rc ){
            this -> rc = tmprc;
        }
    }
    //this -> rc *= 2.0;
    //this -> rc = this -> grid.back();
    int r = 0;
    for(r = 0; r < this -> grid.size() and this -> grid[r] <= rc; ++r){
    }
    this -> grid.resize(r);
    this -> grid_deriv.resize(r);
    this -> grid_size = this -> grid.size();

    this -> iter = 0;

    this -> ae_core_density = paw_species -> get_all_electron_core_density_r();
    this -> ps_core_density = paw_species -> get_smooth_core_density_r();

    this -> ae_core_density.resize(r);
    this -> ps_core_density.resize(r);

    for(int i = 0; i < paw_species -> get_partial_wave_types().size(); ++i){
        this -> ae_pw.push_back( paw_species -> get_all_electron_partial_wave_r(i) );
        this -> ps_pw.push_back( paw_species -> get_smooth_partial_wave_r(i) );
        this -> pw_l_list.push_back( paw_species -> get_partial_wave_state(i).l );
    }

    this -> lmax = (lmax >= 0)? lmax: 2*(*std::max_element(pw_l_list.begin(), pw_l_list.end()) );
    Verbose::single(Verbose::Detail) << "PAW_XC LMAX " << this -> lmax << ", grid_size = " << this -> grid_size << std::endl;
    this -> total_size = paw_species -> get_int_to_l_map().size();

    /*
    this -> ae_pw_prod_L.resize(total_size);
    this -> ps_pw_prod_L.resize(total_size);
    for(int i1 = 0; i1 < ae_pw.size(); ++i1){
        int l1 = this -> pw_l_list[i1];
        for(int m1 = -l1; m1 <= l1; ++m1){
            int ind1 = this -> get_integrated_index( i1, m1);
            this -> ae_pw_prod_L[ind1].resize(total_size);
            this -> ps_pw_prod_L[ind1].resize(total_size);
            for(int i2 = 0; i2 < ae_pw.size(); ++i2){
                int l2 = this -> pw_l_list[i2];
                for(int m2 = -l2; m2 <= l2; ++m2){
                    int ind2 = this -> get_integrated_index(i2, m2);
                    this -> ae_pw_prod_L[ind1][ind2] = this -> expand_pwprod_to_L(i1, i2, m1, m2, true);
                    this -> ps_pw_prod_L[ind1][ind2] = this -> expand_pwprod_to_L(i1, i2, m1, m2, false);
                }
            }
        }
    }
//    */
//    /*
    int packed_tot_size = this -> pack_index(total_size-1, total_size-1)+1;
    this -> ae_pw_prod_L.resize(packed_tot_size);
    this -> ps_pw_prod_L.resize(packed_tot_size);
    for(int i1 = 0; i1 < ae_pw.size(); ++i1){
        int l1 = this -> pw_l_list[i1];
        for(int m1 = -l1; m1 <= l1; ++m1){
            int ind1 = this -> get_integrated_index( i1, m1);
            for(int i2 = 0; i2 < ae_pw.size(); ++i2){
                int l2 = this -> pw_l_list[i2];
                for(int m2 = -l2; m2 <= l2; ++m2){
                    int ind2 = this -> get_integrated_index(i2, m2);
                    int packed_ind = this -> pack_index( ind1, ind2 );
                    if( ind1 > ind2 ){
                        continue;
                    }
                    this -> ae_pw_prod_L[packed_ind] = this -> expand_pwprod_to_L(i1, i2, m1, m2, true);
                    this -> ps_pw_prod_L[packed_ind] = this -> expand_pwprod_to_L(i1, i2, m1, m2, false);
                }
            }
        }
    }
//    */
    this -> timer -> end("Initialize PAW XC");
    Verbose::single(Verbose::Detail) << "Initialize PAW XC: " << this -> timer -> get_elapsed_time("Initialize PAW XC") << " s" << std::endl;
}

int Paw_XC::get_integrated_index( int pw_state_no, int m ){
    int retval = 0;
    if( pw_state_no > this -> pw_l_list.size() ){
        Verbose::all() << "Paw_XC::get_integrated_index out of range" << std::endl;
        exit(EXIT_FAILURE);
    }
    for(int i = 0; i < pw_state_no; ++i){
        retval += this -> pw_l_list[i]*2+1;
    }
    retval += this -> pw_l_list[pw_state_no]+m;
    return retval;
}

vector< vector< vector<double> > > Paw_XC::expand_density_to_L(
    int spin_size,
    SerialDenseMatrix<int,double> D_matrix,
    bool is_ae
){
    if(is_ae){
        this -> timer -> start("Expand AE density");
    } else {
        this -> timer -> start("Expand PS density");
    }
    vector< vector< vector<double> > > retval(this->lmax+1);
    for(int l = 0; l <= this->lmax; ++l){
        retval[l].resize(2*l+1);
        for(int m = 0; m < 2*l+1; ++m){
            retval[l][m].resize(this->grid_size);
        }
    }

    for(int r = 0; r < this->grid_size; ++r){
        if( is_ae ){
            retval[0][0][r] = this -> ae_core_density[r]/spin_size;
        } else {
            retval[0][0][r] = this -> ps_core_density[r]/spin_size;
        }
    }

    vector< vector<double> > pw;
    if( is_ae ){
        pw = this -> ae_pw;
    } else {
        pw = this -> ps_pw;
    }

    for(int i = 0; i < pw.size(); ++i){
        for( int j = 0; j < pw.size(); ++j){
            int l1 = this -> pw_l_list[i];
            int l2 = this -> pw_l_list[j];
            for(int l = 0; l <= lmax; ++l){
                for(int m = -l; m <=l; ++m){
                    for(int m1 = -l1; m1 <= l1; ++m1){
                        for(int m2 = -l2; m2 <= l2; ++m2){
                            double coeff = Spherical_Harmonics::real_YYY_integrate(l1, l2, l, m1, m2, m);
                            int ind1 = this -> get_integrated_index(i,m1);
                            int ind2 = this -> get_integrated_index(j,m2);
                            for(int r = 0; r < this->grid_size; ++r){
                                //retval[l][l+m][r] += D_matrix[ind1][ind2]*pw[i][r]*pw[j][r]*coeff;
                                retval[l][l+m][r] += D_matrix(ind1,ind2)*pw[i][r]*pw[j][r]*coeff;
                            }
                        }
                    }
                }
            }
        }
    }
    if(is_ae){
        this -> timer -> end("Expand AE density");
    } else {
        this -> timer -> end("Expand PS density");
    }
    return retval;
}

vector< vector< vector<double> > > Paw_XC::expand_pwprod_to_L( int i, int j, int m1, int m2, bool is_ae ){
    if(is_ae){
        this -> timer -> start("Expand AE PW pair");
    } else {
        this -> timer -> start("Expand PS PW pair");
    }
    vector< vector< vector<double> > > retval(this->lmax+1);
    for(int l = 0; l <= this->lmax; ++l){
        retval[l].resize(2*l+1);
        for(int m = 0; m < 2*l+1; ++m){
            retval[l][m].resize(this->grid.size());
        }
    }

    vector<double> pw1, pw2;
    if( is_ae ){
        pw1 = this -> ae_pw[i];
        pw2 = this -> ae_pw[j];
    } else {
        pw1 = this -> ps_pw[i];
        pw2 = this -> ps_pw[j];
    }

    int l1 = this -> pw_l_list[i];
    int l2 = this -> pw_l_list[j];
    for(int l = 0; l <= this->lmax; ++l){
        for(int m = -l; m <=l; ++m){
            double coeff = Spherical_Harmonics::real_YYY_integrate(l1, l2, l, m1, m2, m);
            if(std::abs(coeff) > COEF_CUTOFF_){
                for(int r = 0; r < this->grid.size(); ++r){
                    retval[l][l+m][r] = pw1[r]*pw2[r]*coeff;
                }
            } else {
                retval[l][l+m].clear();
            }
        }
    }

    if(is_ae){
        this -> timer -> end("Expand AE PW pair");
    } else {
        this -> timer -> end("Expand PS PW pair");
    }
    return retval;
}

vector<double> Paw_XC::calculate_radial_exc(
        int xc_func_no,
        vector< vector<double> > radial_val,
        vector< vector< vector<double> > > gradient
){
    this -> timer -> start("calculate_radial_exc");

    int spin_size = radial_val.size();
    int grid_size = this->grid.size();
    vector<double> retval(grid_size);
    vector< vector<double> > cont_grad;

    xc_func_type func;

    if( spin_size == 1 ){
        xc_func_init(&func, xc_func_no, XC_UNPOLARIZED);
    } else {
        xc_func_init(&func, xc_func_no, XC_POLARIZED);
    }

    if(func.info -> family != XC_FAMILY_LDA){
        cont_grad = this -> contract_gradient( gradient );
    }

    double * p_n, * p_grad_n;
    double tmp;
    switch(func.info -> family){
        case XC_FAMILY_LDA:
            for(int i = 0; i < grid_size; ++i){
                tmp = 0.0;
                if( spin_size == 1 ){
                    p_n = new double[1];
                    p_n[0] = radial_val[0][i];
                } else {
                    p_n = new double[2];
                    p_n[0] = radial_val[0][i];
                    p_n[1] = radial_val[1][i];
                }
                xc_lda_exc(&func, 1, p_n, &tmp);
                retval[i] = tmp;
                delete[] p_n;
            }
            break;
        case XC_FAMILY_HYB_GGA:
        case XC_FAMILY_GGA:
            for(int i = 0; i < grid_size; ++i){
                tmp = 0.0;
                if( spin_size == 1 ){
                    p_n = new double[1];
                    p_n[0] = radial_val[0][i];
                    p_grad_n = new double[1];
                    p_grad_n[0] = cont_grad[0][i];
                } else {
                    p_n = new double[2];
                    p_n[0] = radial_val[0][i];
                    p_n[1] = radial_val[1][i];
                    p_grad_n = new double[3];
                    p_grad_n[0] = cont_grad[0][i];
                    p_grad_n[1] = cont_grad[1][i];
                    p_grad_n[2] = cont_grad[2][i];
                }
                xc_gga_exc(&func, 1, p_n, p_grad_n, &tmp );
                retval[i] = tmp;
                delete[] p_n;
                delete[] p_grad_n;
            }
            break;
//        case XC_FAMILY_MGGA:
//            xc_mgga_exc(&func, grid_size, p_n, p_grad_n, laplacian, KE_density, e_xc );
//            break;
        default:
            Verbose::all() << "Not supported xc type " << func.info -> family << std::endl;
            xc_func_end(&func);
            exit(EXIT_FAILURE);
    }

    xc_func_end( &func );
    this -> timer -> end("calculate_radial_exc");
    return retval;
}

void Paw_XC::calculate_radial_vxc(
        int xc_func_no,
        vector< vector<double> > radial_val,
        vector< vector< vector<double> > > gradient,
        vector< vector<double> > &vxc,
        vector< vector<double> > &vsigma
){
    this -> timer -> start("calculate_radial_vxc");
    int spin_size = radial_val.size();
    vector< vector<double> > retval(spin_size);
    vector< vector<double> > cont_grad;
    vector< vector<double> > v_sigma;

    for(int s = 0; s < spin_size; ++s){
        retval[s].resize(grid_size);
    }
    xc_func_type func;

    if( spin_size == 1 ){
        xc_func_init(&func, xc_func_no, XC_UNPOLARIZED);
    } else {
        xc_func_init(&func, xc_func_no, XC_POLARIZED);
    }

    if(func.info -> family != XC_FAMILY_LDA){
        cont_grad = this -> contract_gradient( gradient );
        v_sigma.resize(2*spin_size-1);
        for(int s = 0; s < v_sigma.size(); ++s){
            v_sigma[s].resize(grid_size);
        }
    }

    double * p_n, * p_grad_n;
    double * tmp, *vsigma_tmp;
    switch(func.info -> family){
        case XC_FAMILY_LDA:
            for(int i = 0; i < grid_size; ++i){
                tmp = new double[spin_size];
                p_n = new double[spin_size];
                if( spin_size == 1 ){
                    p_n[0] = radial_val[0][i];
                } else {
                    p_n[0] = radial_val[0][i];
                    p_n[1] = radial_val[1][i];
                }
                xc_lda_vxc(&func, 1, p_n, tmp);
                for(int s = 0; s < spin_size; ++s){
                    retval[s][i] = tmp[s];
                }
                delete[] p_n;
                delete[] tmp;
            }
            break;
        case XC_FAMILY_HYB_GGA:
        case XC_FAMILY_GGA:
            for(int i = 0; i < grid_size; ++i){
                tmp = new double[spin_size];
                p_n = new double[spin_size];
                vsigma_tmp = new double[2*spin_size-1];// 1 for 1, 3 for 2
                p_grad_n = new double[2*spin_size-1];
                if( spin_size == 1 ){
                    p_n[0] = radial_val[0][i];
                    p_grad_n[0] = cont_grad[0][i];
                } else {
                    p_n[0] = radial_val[0][i];
                    p_n[1] = radial_val[1][i];
                    p_grad_n[0] = cont_grad[0][i];
                    p_grad_n[1] = cont_grad[1][i];
                    p_grad_n[2] = cont_grad[2][i];
                }
                xc_gga_vxc(&func, 1, p_n, p_grad_n, tmp, vsigma_tmp );
                for(int s = 0; s < spin_size; ++s){
                    retval[s][i] = tmp[s];
                }
                for(int s = 0; s < 2*spin_size-1; ++s){
                    v_sigma[s][i] = vsigma_tmp[s];
                }
                delete[] p_n;
                delete[] p_grad_n;
                delete[] tmp;
                delete[] vsigma_tmp;
            }
            break;
//        case XC_FAMILY_MGGA:
//            xc_mgga_exc(&func, grid_size, p_n, p_grad_n, laplacian, KE_density, e_xc );
//            break;
        default:
            Verbose::all() << "Not supported xc type " << func.info -> family << std::endl;
            xc_func_end(&func);
            exit(EXIT_FAILURE);
    }

    // GGA VXC correction part
    /*
     *  VXC = libxc_vxc - 2 * div( libxc_vsigma * grad(n) ) for UNPOLARIZED
     *  VXC_alpha = libxc_vxc_alpha - 2 * div( libxc_vsigma_alphaalpha * grad(n_alpha) )  - div( libxc_vsigma_alphabeta * grad(n_beta) ) for POLARIZED alpha
     *  VXC_beta  = libxc_vxc_beta - 2 * div( libxc_vsigma_betabeta * grad(n_beta) ) - div( libxc_vsigma_alphabeta * grad(n_alpha) ) for POLARIZED beta
     *
     * div( libxc_vsigma * grad(n) ) contribution will be:
     * \int div( libxc_vsigma * grad(n) ) phi_1 phi_2 d vect(r)
     *          d   (                     dn  )
     *  = \int ---- ( r^2 libxc_vsigma * ---- ) phi_1(r) phi_2(r) dr \int Y_L1 Y_L2 d\Omega
     *          dr  (                     dr  )
     *               [    d     (                              dn    )        1          d    (                dn   ) ]
     *   + \int \int [ -------- ( sin(theta) libxc_vsigma * -------- ) + ------------  ------ ( libxc_vsigma ------ ) ] Y_L1 Y_L2 d\theta d\phi \int r^2 phi_1(r) phi_2(r) dr
     *               [  dtheta  (                            dtheta  )    sin(theta)    dphi  (               dphi  ) ]
     *
     * Integration by parts gives:
     *
     *          d   (                     dn  )
     *  = \int ---- ( r^2 libxc_vsigma * ---- ) phi_1(r) phi_2(r) dr \int Y_L1 Y_L2 d\Omega
     *          dr  (                     dr  )
     *                                         [                (    dn       d Y_L1 Y_L2      dn      d Y_L1 Y_L2  ) ]
     *   -  \int r^2 phi_1(r) phi_2(r) dr \int [ libxc_vsigma * ( -------- * ------------- + ------ * ------------- ) ] d\Omega
     *                                         [                (  dtheta        dtheta       dphi        dphi      ) ]
     *
     *          d   (                     dn  )
     *  = \int ---- ( r^2 libxc_vsigma * ---- ) phi_1(r) phi_2(r) dr \int Y_L1 Y_L2 d\Omega
     *          dr  (                     dr  )
     *   -  \int r^2 phi_1(r) phi_2(r) dr \int [ libxc_vsigma * ( r grad(n) )_theta,phi dot ( r grad( Y_L1 Y_L2 ) )_theta,phi ] d\Omega
     *
     * Since phi_1(r) Y_L1 phi_2(r) Y_L2 is expanded to L, grad( Y_L1 Y_L2 ) will be grad( Y_L )
     */
    if(func.info -> family == XC_FAMILY_GGA){
        vector< vector<double> > vv_sg(spin_size);
        vector< vector<double> > radial_deriv(spin_size);
        for(int s = 0; s < spin_size; ++s){
            radial_deriv[s] = Radial_Grid::Paw::linear_derivative( radial_val[s], this -> grid, this -> grid_deriv );
        }
        for(int s = 0; s < spin_size; ++s){
            vector<double> tmp(this->grid.size());
            for(int r = 0; r < this -> grid.size(); ++r){
                tmp[r] = -2.0 * this -> grid[r] * this -> grid[r] * v_sigma[2*s][r] * radial_deriv[s][r];
            }
            vv_sg[s] = Radial_Grid::Paw::linear_derivative(tmp, this -> grid, this -> grid_deriv );
        }
        if( spin_size == 2 ){
            vector<double> tmp(this->grid.size());
            vector<double> v_g(this->grid.size());
            for(int r = 0; r < this -> grid.size(); ++r){
                tmp[r] = -1.0 * this -> grid[r] * this -> grid[r] * v_sigma[1][r] * radial_deriv[1][r];
            }
            v_g = Radial_Grid::Paw::linear_derivative(tmp, this -> grid, this -> grid_deriv );
            for(int r = 0; r < this -> grid.size(); ++r){
                vv_sg[0][r] += v_g[r];
            }
            for(int r = 0; r < this -> grid.size(); ++r){
                tmp[r] = -1.0 * this -> grid[r] * this -> grid[r] * v_sigma[1][r] * radial_deriv[0][r];
            }
            v_g = Radial_Grid::Paw::linear_derivative(tmp, this -> grid, this -> grid_deriv );
            for(int r = 0; r < this -> grid.size(); ++r){
                vv_sg[1][r] += v_g[r];
            }
        }

        for(int s = 0; s < spin_size; ++s){
            for(int r = 1; r < this -> grid.size(); ++r){
                vv_sg[s][r] /= this -> grid[r] * this -> grid[r];
            }
            vv_sg[s][0] = vv_sg[s][1];
        }

        for(int s = 0; s < spin_size; ++s){
            retval[s] = Radial_Grid::Paw::sum( retval[s], vv_sg[s] );
        }
        vsigma = v_sigma;
    }

    xc_func_end( &func );
    vxc = retval;
    this -> timer -> end("calculate_radial_vxc");
}

double Paw_XC::calculate_xc_energy(
    int xc_func_no,
    vector< vector< vector< vector<double> > > > &density_in_L
){
    vector<double> weight = Lebedev_Quadrature::get_weight();
    vector< vector<double> > pts = Lebedev_Quadrature::get_points_on_unit_sphere();

    int spin_size = density_in_L.size();
    double energy = 0.0;

    for(int n = 0; n < weight.size(); ++n){
        vector< vector<double> > radial_val(spin_size);
        vector< vector< vector<double> > > gradient;
        vector<double> radial_exc;
        for(int s = 0; s < spin_size; ++s){
            radial_val[s].resize(this->grid.size());
        }

        for(int l = 0; l <= this -> lmax; ++l){
            for(int m = -l; m <= l; ++m){
                double Y = Spherical_Harmonics::Ylm(l, m, pts[n][0], pts[n][1], pts[n][2]);
                for(int s = 0; s < spin_size; ++s){
                    for(int r = 0; r < this->grid.size(); ++r){
                        radial_val[s][r] += density_in_L[s][l][l+m][r]*Y;
                    }
                }
            }
        }

        if( xc_func_no >= 100 ){
            gradient = Paw_XC::calculate_gradient( density_in_L, pts[n] );
        }
        radial_exc = this -> calculate_radial_exc( xc_func_no, radial_val, gradient );
        for(int s = 0; s < spin_size; ++s){
            energy += Radial_Grid::Paw::radial_integrate( radial_exc, radial_val[s], this->grid, this->grid_deriv ) * weight[n] * 4 * M_PI;
        }
    }
    Verbose::set_numformat(Verbose::Time);
    this -> timer -> print(Verbose::single(Verbose::Detail));
    return energy;
}

vector< vector< vector< double > > > Paw_XC::calculate_vxc_correction(
    int xc_func_no,
    vector< vector< vector< vector<double> > > > density_in_L,
    //vector< vector< vector< vector< vector<double> > > > > &pw_prod_in_L
    vector< vector< vector< vector<double> > > > pw_prod_in_L
){
    vector<double> weight = Lebedev_Quadrature::get_weight();
    vector<double> vgrid = this -> grid;// NOT AMAZING
    vector<double> dgrid = this -> grid_deriv;// NOT AMAZING
    vector< vector<double> > pts = Lebedev_Quadrature::get_points_on_unit_sphere();

    int spin_size = density_in_L.size();
    int packed_size = this -> pack_index( this->total_size-1, this -> total_size-1 )+1;
    vector< vector< vector<double> > > int_vxc;
    int_vxc.resize(this->total_size);
    for(int i = 0; i < int_vxc.size(); ++i){
        int_vxc[i].resize(this->total_size);
        for(int j = 0; j < int_vxc.size(); ++j){
            int_vxc[i][j].resize(spin_size, 0.0);
        }
    }
    vector< vector< double> > int_vxc2;
    int_vxc2.resize(packed_size);
    for(int i = 0; i < int_vxc2.size(); ++i){
        int_vxc2[i].resize(spin_size, 0.0);
    }

    for(int n = 0; n < weight.size(); ++n){
        vector< vector<double> > radial_val(spin_size);
        vector< vector<double> > radial_vxc;
        vector< vector<double> > v_sigma;
        vector< vector< vector<double> > > gradient;
        for(int s = 0; s < spin_size; ++s){
            radial_val[s].resize(this->grid_size);
        }
        for(int l = 0; l <= this -> lmax; ++l){
            for(int m = -l; m <= l; ++m){
                double Y = Spherical_Harmonics::Ylm(l, m, pts[n][0], pts[n][1], pts[n][2]);
                for(int s = 0; s < spin_size; ++s){
                    for(int r = 0; r < this->grid_size; ++r){
                        radial_val[s][r] += density_in_L[s][l][l+m][r]*Y;
                    }
                }
            }
        }

        vector< vector<double> > rgradn_theta(spin_size);
        vector< vector<double> > rgradn_phi(spin_size);
        if( xc_func_no >= 100 ){
            gradient = Paw_XC::calculate_gradient( density_in_L, pts[n] );

            for(int s = 0; s < spin_size; ++s){
                rgradn_theta[s].resize(this->grid.size());
                rgradn_phi[s].resize(this->grid.size());
                for(int r = 0; r < this->grid.size(); ++r){
                    if( abs(pts[n][0]) <= POS_ZERO && abs(pts[n][1]) <= POS_ZERO ){
                        rgradn_theta[s][r] = (gradient[s][0][r] + gradient[s][1][r])*pts[n][2]
                                             - gradient[s][2][r]*sqrt(pts[n][0]*pts[n][0]+pts[n][1]*pts[n][1]);
                    } else {
                        rgradn_theta[s][r] = (gradient[s][0][r]*pts[n][0] + gradient[s][1][r]*pts[n][1])*pts[n][2]/sqrt(pts[n][0]*pts[n][0]+pts[n][1]*pts[n][1])
                                             - gradient[s][2][r]*sqrt(pts[n][0]*pts[n][0]+pts[n][1]*pts[n][1]);
                    }
                    rgradn_phi[s][r] = -pts[n][1]*gradient[s][0][r] + pts[n][0]*gradient[s][1][r];
                }
            }
        }

        this -> calculate_radial_vxc( xc_func_no, radial_val, gradient, radial_vxc, v_sigma );

        for(int ind = 0; ind < packed_size; ++ind){
            if( Parallel_Manager::info().get_total_mpi_rank() != ind % Parallel_Manager::info().get_total_mpi_size() ){
                continue;
            }
            double inv_sin_theta_2 = 0.0;
            double xy_distance = 0.0;
            if (v_sigma.size() != 0 and std::abs(pts[n][0]) > POS_ZERO and std::abs(pts[n][1]) > POS_ZERO) {
                xy_distance = sqrt(pts[n][0]*pts[n][0]+pts[n][1]*pts[n][1]);
                // In other cases ( theta = 0 or PI so sin(theta) = 0 ), Y=0 and dY/dphi = 0 or LDA
                inv_sin_theta_2 = (xy_distance*xy_distance+pts[n][2]*pts[n][2]) / (xy_distance*xy_distance);
            }

            for(int l = 0; l <= this -> lmax; ++l){
                for(int m = -l; m <= l; ++m){
                    //int i = this -> unpack_index(ind).first;
                    //int j = this -> unpack_index(ind).second;
                    vector<double> pw_prod_L = pw_prod_in_L[ind][l][l+m];
                    if(pw_prod_L.size() == 0){
                        continue;
                    }

                    double Y = Spherical_Harmonics::Ylm(l, m, pts[n][0], pts[n][1], pts[n][2]);
                    for(int s = 0; s < spin_size; ++s){
                        //double tmp = Radial_Grid::Paw::radial_integrate( radial_vxc[s], pw_prod_in_L[i][j][l][l+m], this -> grid, this -> grid_deriv );// -6s on PID 0 and 15 when using 16 cores.
                        double tmp = Radial_Grid::Paw::radial_integrate( radial_vxc[s], pw_prod_L, vgrid, dgrid );// -6s on PID 0 and 15 when using 16 cores.
                        int_vxc2[ind][s] += tmp * weight[n] * Y * 4*M_PI;
                    }

                    // GGA VXC correction part
                    /*
                     *  VXC = libxc_vxc - 2 * div( libxc_vsigma * grad(n) ) for UNPOLARIZED
                     *  VXC_alpha = libxc_vxc_alpha - 2 * div( libxc_vsigma_alphaalpha * grad(n_alpha) )  - div( libxc_vsigma_alphabeta * grad(n_beta) ) for POLARIZED alpha
                     *  VXC_beta  = libxc_vxc_beta - 2 * div( libxc_vsigma_betabeta * grad(n_beta) ) - div( libxc_vsigma_alphabeta * grad(n_alpha) ) for POLARIZED beta
                     *
                     * From following correction term,
                     * \int div( libxc_vsigma * grad(n) ) phi_1 phi_2 d vect(r)
                     * this should be considered
                     *  - \int r^2 phi_1(r) phi_2(r) dr \int [ libxc_vsigma * ( r grad(n) )_theta,phi dot ( r grad( Y_L1 Y_L2 ) )_theta,phi ] d\Omega
                     *
                     * Since phi_1(r) Y_L1 phi_2(r) Y_L2 is expanded to L, grad( Y_L1 Y_L2 ) will be grad( Y_L )
                     */
                    if( v_sigma.size() != 0 and l != 0){// 0.02s - 0.08s for LDA, 1s - 10s for PBE
                        vector<double> dY(3);
                        for(int d = 0; d < 3; ++d){
                            dY[d] = dYlm(l, m, pts[n][0], pts[n][1], pts[n][2], d);
                        }

                        vector<double> rnablaY(2);
                        if( std::abs(pts[n][0]) > POS_ZERO and std::abs(pts[n][1]) > POS_ZERO){
                            rnablaY[0] = ( dY[0] * pts[n][0] + dY[1] * pts[n][1] )*pts[n][2] / xy_distance - dY[2]*xy_distance;
                        } else {
                            rnablaY[0] = ( dY[0] + dY[1] ) * pts[n][2] - dY[2]*xy_distance;
                        }
                        rnablaY[1] = -pts[n][1]*dY[0] + pts[n][0]*dY[1];
                        for(int s = 0; s < spin_size; ++s){
                            vector<double> tmp(this->grid_size);
                            for(int r = 0; r < this->grid_size; ++r){
                                tmp[r] = v_sigma[2*s][r] * ( rgradn_theta[s][r]*rnablaY[0] + rgradn_phi[s][r]*rnablaY[1]*inv_sin_theta_2 );
                            }
                            //int_vxc[i][j][s] += 2.0 * Radial_Grid::Paw::linear_integrate( tmp, pw_prod_in_L[i][j][l][l+m], this->grid, this -> grid_deriv ) * weight[n] *4*M_PI;
                            int_vxc2[ind][s] += 2.0 * Radial_Grid::Paw::linear_integrate( tmp, pw_prod_L, vgrid, dgrid ) * weight[n] *4*M_PI;
                        }

                        // For spin polarized case
                        if (v_sigma.size() == 3){
                            vector<double> tmp(this->grid_size);
                            for(int r = 0; r < this->grid_size; ++r){
                                tmp[r] = v_sigma[1][r] * ( rgradn_theta[1][r]*rnablaY[0] + rgradn_phi[1][r]*rnablaY[1]*inv_sin_theta_2 );
                            }
                            int_vxc2[ind][0] += Radial_Grid::Paw::linear_integrate( tmp, pw_prod_L, vgrid, dgrid ) * weight[n] * 4*M_PI;
                            for(int r = 0; r < this->grid_size; ++r){
                                tmp[r] = v_sigma[1][r] * ( rgradn_theta[0][r]*rnablaY[0] + rgradn_phi[0][r]*rnablaY[1]*inv_sin_theta_2 );
                            }
                            int_vxc2[ind][1] += Radial_Grid::Paw::linear_integrate( tmp, pw_prod_L, vgrid, dgrid ) * weight[n] * 4*M_PI;
                        }
                    }// GGA correction
                }// for m
            }// for l
        //    }// for j
        //}// for i
        }// for ind
    }// for n

    //cout << "PID = " << MyPID << " NEW XC Hamiltonian LDA integration WALL time: \t" << tl << " s" << endl;
    //cout << "PID = " << MyPID << " NEW XC Hamiltonian GGA integration WALL time: \t" << tg << " s" << endl;

    vector< vector<double> > tmp( packed_size );
    for(int ind = 0; ind < int_vxc2.size(); ++ind){
        tmp[ind].resize(2, 0.0);
        Parallel_Util::group_sum(&int_vxc2[ind][0], &tmp[ind][0], spin_size);
    }
    int_vxc2 = tmp;

    for(int ind = 0; ind < int_vxc2.size(); ++ind){
        int i = this -> unpack_index(ind).first;
        int j = this -> unpack_index(ind).second;
        for(int s = 0; s < spin_size; ++s){
            int_vxc[i][j][s] = int_vxc2[ind][s];
            int_vxc[j][i][s] = int_vxc2[ind][s];
        }
    }
    //et = clock();
    //cout << "PID = " << MyPID << " NEW XC Hamiltonian TOT time: \t" << ( (double)(et-st) )/CLOCKS_PER_SEC << " s" << endl;

    Verbose::set_numformat(Verbose::Time);
    this -> timer -> print(Verbose::single(Verbose::Detail));
    return int_vxc;
}

// retval[dir][r]
//vector< vector<double> > Paw_XC::calculate_contracted_gradient(
vector< vector< vector<double> > > Paw_XC::calculate_gradient(
    vector< vector< vector< vector<double> > > > radial_density_in_L,
    vector<double> point
){
    int spin_size = radial_density_in_L.size();
    vector< vector< vector<double> > > gradient(spin_size);
    double unitrad = sqrt( point[0]*point[0] + point[1]*point[1] + point[2]*point[2] );

    for(int s = 0; s < spin_size; ++s){
        gradient[s].resize( 3 );
        for(int i = 0; i < 3; ++i){
            gradient[s][i].resize(this->grid_size);
        }
    }

    for(int l = 0; l <= this->lmax; ++l){
        for(int m = -l; m <= l; ++m){
            for(int s = 0; s < spin_size; ++s){
                vector<double> radial_val = radial_density_in_L[s][l][l+m];
                vector<double> radial_grad = Radial_Grid::Paw::linear_derivative( radial_val, this->grid, this -> grid_deriv );

                for(int r = 0; r < this->grid_size; ++r){
                    vector<double> pt(3);
                    for(int i = 0; i < 3; ++i){
                        pt[i] = point[i]/unitrad*this->grid[r];
                    }
                    double Y = Spherical_Harmonics::Ylm(l, m, pt[0], pt[1], pt[2]);
                    vector<double> dY(3);
                    for(int d = 0; d < 3; ++d){
                        dY[d] = dYlm(l, m, pt[0], pt[1], pt[2], d);
                    }

                    if( this ->grid[r] < 1.0E-15 ){
                        vector<double> new_pt(3);
                        for(int i = 0; i < 3; ++i){
                            new_pt[i] = point[i]/unitrad*this->grid[r+1];
                        }

                        for(int i = 0; i < 3; ++i){
                            gradient[s][i][r] += Y*radial_grad[r] + radial_val[r]*dY[i];
                        }
                    } else {
                        for(int i = 0; i < 3; ++i){
                            gradient[s][i][r] += Y*radial_grad[r]*pt[i]/this->grid[r] + radial_val[r]*dY[i];
                        }
                    }
                }
            }
        }
    }
    return gradient;
}


vector< vector<double> > Paw_XC::contract_gradient(
    vector< vector< vector<double> > > gradient
){
    vector< vector<double> > cont_grad;
    int spin_size = gradient.size();

    if( spin_size == 1){
        cont_grad.resize(1);
    } else {
        cont_grad.resize(3);
    }
    for(int i = 0; i < cont_grad.size(); ++i){
        cont_grad[i].resize(this->grid_size);
    }

    for(int r = 0; r < this -> grid_size; ++r){
        double cont_val = 0.0;
        for(int i = 0; i < 3; ++i){
            cont_val += gradient[0][i][r]*gradient[0][i][r];
        }
        cont_grad[0].at(r) = cont_val;
        if( spin_size != 1 ){
            for(int j = 0; j < 2; ++j){
                cont_val = 0.0;
                for(int i = 0; i < 3; ++i){
                    cont_val += gradient[1][i][r]*gradient[j][i][r];
                }
                cont_grad[j+1].at(r) = cont_val;
            }
        }
    }

    return cont_grad;
}

vector<double> Paw_XC::get_xc_energy_correction(
    Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    this -> timer -> start("get_xc_energy_correction");
    int spin_size = sD_matrix.size();
    vector<double> retval(this -> xc_functionals.size());

    this -> timer -> start("get_xc_energy_correction: expand density");
    vector< vector< vector< vector<double> > > > ae_density_L;
    vector< vector< vector< vector<double> > > > ps_density_L;
    for(int s = 0; s < spin_size; ++s){
        ae_density_L.push_back( this -> expand_density_to_L(spin_size, sD_matrix[s], true) );
        ps_density_L.push_back( this -> expand_density_to_L(spin_size, sD_matrix[s], false) );
    }
    this -> timer -> end("get_xc_energy_correction: expand density");

    this -> timer -> start("get_xc_energy_correction: calculate_xc_energy");
    for(int ixc = 0; ixc < this -> xc_functionals.size(); ++ixc){
        retval[ixc] += this -> calculate_xc_energy( this -> xc_functionals[ixc], ae_density_L );
        retval[ixc] -= this -> calculate_xc_energy( this -> xc_functionals[ixc], ps_density_L );

        /*
        double aeval = this -> calculate_xc_energy( this -> xc_functionals[ixc], ae_density_L );
        double psval = this -> calculate_xc_energy( this -> xc_functionals[ixc], ps_density_L );
        retval[ixc] = aeval;
        retval[ixc] -= psval;
        std::cerr << "PAWXC ecorr " << ixc << ", " << aeval << ", " << psval << std::endl;
        // */
    }
    this -> timer -> end("get_xc_energy_correction: calculate_xc_energy");
    this -> timer -> end("get_xc_energy_correction");
    Verbose::set_numformat(Verbose::Time);
    this -> timer -> print(Verbose::single(Verbose::Detail));
    return retval;
}

vector< vector< vector<double> > > Paw_XC::get_xc_hamiltonian_correction(
    Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    this -> timer -> start("get_xc_hamiltonian_correction");
    int spin_size = sD_matrix.size();
    vector< vector< vector<double> > > retval;
    retval.resize(this->total_size);
    for(int i = 0; i < this->total_size; ++i){
        retval[i].resize(this->total_size);
        for(int j = 0; j < this->total_size; ++j){
            retval[i][j].resize(spin_size, 0.0);
        }
    }

    this -> timer -> start("get_xc_hamiltonian_correction: expand");
    vector< vector< vector< vector<double> > > > ae_density_L;
    vector< vector< vector< vector<double> > > > ps_density_L;
    for(int s = 0; s < spin_size; ++s){
        ae_density_L.push_back( this -> expand_density_to_L(spin_size, sD_matrix[s], true) );
        ps_density_L.push_back( this -> expand_density_to_L(spin_size, sD_matrix[s], false) );
    }
    this -> timer -> end("get_xc_hamiltonian_correction: expand");

    this -> timer -> start("get_xc_hamiltonian_correction: calculate_vxc_correction");
    for(int ixc = 0; ixc < this -> xc_functionals.size(); ++ixc){
        vector< vector< vector< double > > > vxc_ae = this -> calculate_vxc_correction( this -> xc_functionals[ixc], ae_density_L, this -> ae_pw_prod_L );
        vector< vector< vector< double > > > vxc_ps = this -> calculate_vxc_correction( this -> xc_functionals[ixc], ps_density_L, this -> ps_pw_prod_L );
        for(int i = 0; i < this->total_size; ++i){
            for(int j = 0; j < this->total_size; ++j){
                for(int s = 0; s < spin_size; ++s){
                    retval[i][j][s] += vxc_ae[i][j][s];
                    retval[i][j][s] -= vxc_ps[i][j][s];
                }
            }
        }
    }
    this -> timer -> end("get_xc_hamiltonian_correction: calculate_vxc_correction");
    this -> timer -> end("get_xc_hamiltonian_correction");
    Verbose::set_numformat(Verbose::Time);
    this -> timer -> print(Verbose::single(Verbose::Detail));

    this -> iter++;
    return retval;
}

double Paw_XC::get_int_n_vxc_correction(
    Array< SerialDenseMatrix<int,double> > &sD_matrix
){
    int spin_size = sD_matrix.size();
    double retval = 0.0;

    vector< vector< vector< vector<double> > > > ae_density_L;
    vector< vector< vector< vector<double> > > > ps_density_L;
    for(int s = 0; s < spin_size; ++s){
        ae_density_L.push_back( this -> expand_density_to_L(spin_size, sD_matrix[s], true) );
        ps_density_L.push_back( this -> expand_density_to_L(spin_size, sD_matrix[s], false) );
    }

    for(int ixc = 0; ixc < this -> xc_functionals.size(); ++ixc){
        vector< vector< vector< double > > > vxc_ae = this -> calculate_vxc_correction( this -> xc_functionals[ixc], ae_density_L, this -> ae_pw_prod_L );
        vector< vector< vector< double > > > vxc_ps = this -> calculate_vxc_correction( this -> xc_functionals[ixc], ps_density_L, this -> ps_pw_prod_L );
        for(int i = 0; i < this->total_size; ++i){
            for(int j = 0; j < this->total_size; ++j){
                for(int s = 0; s < spin_size; ++s){
                    retval += vxc_ae[i][j][s] * sD_matrix[s](i,j);
                    retval -= vxc_ps[i][j][s] * sD_matrix[s](i,j);
                }
            }
        }
    }
    return retval;
}

/*
void Paw_XC::get_ae_density(Basis * basis, Grid_Setting * grid_setting, double * atom_center,
        vector< vector< vector<double> > > &sD_matrix,
        Teuchos::RCP<Epetra_MultiVector> &density
){
    density -> PutScalar(0.0);
    for(int s = 0; s < sD_matrix.size(); ++s){
        vector< vector< vector<double> > > density_L = this -> expand_density_to_L(sD_matrix.size(), sD_matrix[s], true);
        for(int l = 0; l <= this->lmax; ++l){
            for(int m=-l; m <=l; ++m){
                Teuchos::RCP<Epetra_Vector> tmp = Teuchos::rcp( new Epetra_Vector(density->Map()));
                Radial_Grid::Paw::Interpolate_from_radial_grid(l,m,density_L[l][l+m], this->grid, atom_center, basis, grid_setting, tmp );
                density -> operator()(s) -> Update(1.0, *tmp, 1.0);
            }
        }
    }
}

void Paw_XC::get_ps_density(Basis * basis, Grid_Setting * grid_setting, double * atom_center,
        vector< vector< vector<double> > > &sD_matrix,
        Teuchos::RCP<Epetra_MultiVector> &density
){
    density -> PutScalar(0.0);
    for(int s = 0; s < sD_matrix.size(); ++s){
        vector< vector< vector<double> > > density_L = this -> expand_density_to_L(sD_matrix.size(), sD_matrix[s], false);
        for(int l = 0; l <= this->lmax; ++l){
            for(int m=-l; m <=l; ++m){
                Teuchos::RCP<Epetra_Vector> tmp = Teuchos::rcp( new Epetra_Vector(density->Map()));
                Radial_Grid::Paw::Interpolate_from_radial_grid(l,m,density_L[l][l+m], this->grid, atom_center, basis, grid_setting, tmp );
                density -> operator()(s) -> Update(1.0, *tmp, 1.0);
            }
        }
    }
}
*/

int Paw_XC::pack_index(int i, int j){
    if( j >= i ){
        return j + this->total_size * i - i*(i+1)/2;
    } else {
        return i + this->total_size * j - j*(j+1)/2;
    }
}

std::pair<int, int> Paw_XC::unpack_index(int ind){
    int n = this -> total_size;
    int i = floor( ( 2*n+1 - sqrt( (2*n+1)*(2*n+1) - 8*ind ) ) / 2 );
    int j = ind - n*i + i*(i+1)/2;

    return make_pair(i, j);
}
