#pragma once
#include <vector>

#include "Teuchos_RCP.hpp"
#include "Epetra_MultiVector.h"

#include "../../Io/Atoms.hpp"
#include "../../Basis/Basis.hpp"

/**
 * @author Sunghwan choi? Seongok Ryu?
 * @brief Implementation of DOI: 10.1063/1.2193514
 **/
class Filter {
    public:
        /**
        @brief Constructor of filter class

        */
        Filter(Teuchos::RCP<const Basis> basis, Teuchos::RCP<const Atoms> atoms, std::vector<double> Zvals, double gamma_local, double gamma_nonlocal, double alpha_local, double alpha_nonlocal, double eta);
        /**
           @brief set parameters and mask function
           @param [in] original_f  radial grid for original values
           @param [in] r_g         radial grid for output values
           @param [in] dr_g        distances of output grid
           @param [in] rcut        r^{cut}  in reference 
           @param [in]  
        */
        int filter_local(std::vector<double>original_f,std::vector<double>r_g,std::vector<double>dr_g, int itype, std::vector<double>& new_f,std::vector<double>& new_mesh);
        int filter_short(std::vector<double>original_f,std::vector<double>r_g,std::vector<double>dr_g, double rcut, std::vector<double>& new_f,std::vector<double>& new_mesh);

        /**
            filter function for non-local potentials, which 
            @brief Nonlocal filtering.
            @param original_f Function to filter.
            @param r_g Radial grid of original_f.
            @param dr_g Derivative of radial grid.
            @param l Angular momentum.
            @param nonlocal_cutoff Nonlocal cutoff value
            @param new_f Filtered function.
            @param new_mesh Radial grid of new_f.
        */
        int filter_nonlocal(std::vector<double>original_f,std::vector<double>r_g,std::vector<double>dr_g,int l,double nonlocal_cutoff,std::vector<double>& new_f,std::vector<double>& new_mesh);

        /**
         * @brief Optimize r_i^{gauss} in eq.10 by minimizing electrostatic self-interaction.
         * @param local_pp_radial Local pseudopotential
         * @param atomic_density Atomic density, from PP.
         * @param original_mesh PP mesh.
         **/
        void optimize_comp_charge_exps(
                std::vector< std::vector<double> > atomic_density,
                std::vector<std::vector<double> > original_mesh,
                std::vector< std::vector<double> > original_d_mesh
        );

        /**
         * @brief Returns V^{comp}, eq.11.
         **/
        int get_comp_potential(std::vector<double> mesh, std::vector<double>& return_val,int itype);
        /**
         * @brief Returns r_gaus, eq.10 and eq.11.
         **/
        void get_comp_charge_exps(std::vector<double>& comp_charge_exps2);

        /**
         * @brief Returns F_fs.
         **/
        double Fourier_filtering_function(double r,double r_cutoff, double beta);
        /**
         * @brief Retuns exp(-r/r_cutoff*beta).
         **/
        double get_mask_function_value(std::vector<double> mask_function_grid, std::vector<double> mask_function, double point, double cutoff);
        /**
         * @brief Returns m(r/cutoff) for given r.
         **/
//        std::vector<double> mask_function(double eta);
        /**
         * @brief Returns m(r/cutoff).
         **/
        double get_mask_function_value(double point, double cutoff);
    protected:
        std::vector<double> Zvals;
        /**
         * @brief Cutoff radius scaling factor for short potential. Cutoff radius for V_short is r_short * gamma_local.
         **/
        double gamma_local;
        /**
         * @brief Cutoff radius scaling factor for short potential. Cutoff radius for V_nl is r_nl * gamma_nonlocal.
         **/
        double gamma_nonlocal;
        /**
         * @brief Cutoff frequency scaling factor for local potential. Cutoff frequency for V_short is q_max / alpha_local.
         **/
        double alpha_local;
        /**
         * @brief Cutoff frequency scaling factor for nonlocal potential. Cutoff frequency for V_nl is q_max / alpha_nonlocal.
         **/
        double alpha_nonlocal;
        double eta;

        /**
         * @brief get values of mask function at point by interpolation mask function eq. 7-9.
         **/
        double optimize_comp_charge_exp(
                double Zval, double initial_comp_charge_exp,
                std::vector<double> atomic_density,
                std::vector<double> original_mesh,
                std::vector<double> original_d_mesh
        );
        /**
         *  @brief Filtering Pseudo potential by using Bessel transformation and mask function eq(7)~(9) return new_r_g, new_dr_g, new_f
         *  @param target_scaling scaling
         *  @param original_f original projection function
         *  @param r_g radial mesh from pseudopotential
         *  @param dr_g interval value of radial mesh
         *  @param l angular momentum of orbital
         *  @param new_r_g radial mesh for projection function after filtering(Bessel transformation)
         *  @oaran new_dr_g interval value of radial mesh after filtering
         *  @param q_i mesh for reciprocal space
         *  @param dq_i interval value of points in q_i
         *  @param q_cut cutoff of reciprocal space
         *  @oaram rc2 cutoff radius for filtering
         *  @oaram filtered projection function
         *  @param beta_fs constant for Fourier filtering function which makes F(q_max)= 10^(-5)
         **/
        void compute( double target_scaling, 
                      std::vector<double> original_f, 
                      std::vector<double> r_g, 
                      std::vector<double> dr_g, 
                      int l, 
                      std::vector<double> new_r_g, 
                      std::vector<double> new_dr_g,
                      std::vector<double> q_i,
                      double dq_i,
                      double q_cut, 
                      double rc2, 
                      std::vector<double>& new_f, 
                      double beta_fs
                    );
//        Teuchos::RCP<Epetra_MultiVector> comp_charge;
//        Teuchos::RCP<Epetra_MultiVector> comp_potential;
        Teuchos::RCP<Epetra_MultiVector> local_potential;
//        int compute_comp_charge_and_potential();
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<const Atoms> atoms;
        std::vector<double> comp_charge_exps;
        void initialize_mask_function(double eta);
        /**
         *  @brief generate mask function for given eta, eta = q_cut * cutoff_r
         **/
        std::vector<double> mask_function;
        std::vector<double> mask_grid;
};

