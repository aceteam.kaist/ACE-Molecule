#include "KBprojector.hpp"
#include <iostream>

#include "Epetra_Map.h"

#include "../../Utility/Math/Spherical_Harmonics.hpp"
#include "../../Utility/Verbose.hpp"

using std::string;
using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

// NLCC
int KBprojector::get_core_electron_info(Array< RCP<Epetra_Vector> >& density, Array< RCP<Epetra_MultiVector> >& density_grad){
    if(density.size() == 1){
        density[0]->Update(1.0, *this->core_density, 0.0);
        density_grad[0]->Update(1.0, *this->core_density_grad, 0.0);
    }
    else if(density.size() == 2){
        for(int i_spin=0; i_spin<density.size(); i_spin++){
            density[i_spin]->Update(0.5, *this->core_density, 0.0);
            density_grad[i_spin]->Update(0.5, *this->core_density_grad, 0.0);
        }
    }
    else{
        Verbose::all() << "KBprojector::get_core_electron_info - ERROR.\n";
        Verbose::all() << density.size() << '\n';
        exit(EXIT_FAILURE);
    }

    return 0;
}

vector< vector< vector< vector<double> > > > KBprojector::get_V_nl(){
    if(V_vector.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_vector;
}

vector< vector< vector< vector<double> > > > KBprojector::get_V_nl_dev_x(){
    if(V_dev_x.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_x;
}

vector< vector< vector< vector<double> > > > KBprojector::get_V_nl_dev_y(){
    if(V_dev_y.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_y;
}

vector< vector< vector< vector<double> > > > KBprojector::get_V_nl_dev_z(){
    if(V_dev_z.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_z;
}

vector< vector< vector < vector<int> > > > KBprojector::get_V_index(){
    if(V_index.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_index;
}

std::vector< std::vector< std::vector<double> > > KBprojector::get_EKB_matrix(){
    return this -> EKB_matrix;
}

void KBprojector::make_nonlocal_vectors(){
    V_vector.clear(); V_dev_x.clear(); V_dev_y.clear(); V_dev_z.clear(); V_index.clear();
    //////////////Nonlocal potential ///////////////////
    vector<std::array<double,3> > positions = atoms->get_positions();
    for(int iatom=0;iatom<atoms->get_size();iatom++){
        //int itype = atoms->get_atom_type(iatom);
        vector<vector<vector<double> > > nonlocal_pp;
        vector<vector<vector<double> > > nonlocal_dev_x;
        vector<vector<vector<double> > > nonlocal_dev_y;
        vector<vector<vector<double> > > nonlocal_dev_z;
        vector<vector<vector<int> > > nonlocal_pp_index;
        calculate_nonlocal(iatom,positions[iatom],nonlocal_pp,nonlocal_dev_x,nonlocal_dev_y,nonlocal_dev_z,nonlocal_pp_index);  //calculate nonlocal potential
        V_vector.push_back(nonlocal_pp);
        V_dev_x.push_back(nonlocal_dev_x);
        V_dev_y.push_back(nonlocal_dev_y);
        V_dev_z.push_back(nonlocal_dev_z);
        V_index.push_back(nonlocal_pp_index);
//       Verbose::single()<< "make_nonlocal_vectors test" << std::endl;
//        Verbose::single()<< V_dev_x[0][0][0][0] << std::endl;
//        Verbose::single()<< V_dev_y[0][0][0][0] << std::endl;
//        Verbose::single()<< V_dev_z[0][0][0][0] << std::endl;
//        Verbose::single()<< V_index[0][0][0][0] << std::endl;
    }
    return;
}

void KBprojector::make_vectors(){
    make_local_pp();
    make_nonlocal_vectors();
}

vector< vector<double> > KBprojector::get_V_local_dev(){
    if(local_pp_dev.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed local vectors" << std::endl;
        this -> make_local_pp();
    }
    return local_pp_dev;
}

vector< vector< vector<double> > > KBprojector::get_h_ij_total(){
    return this -> h_ij_total;
}

void KBprojector::clear_vector(){
    this -> local_pp.clear();
    this -> local_pp_dev.clear();
    this -> V_index.clear();
    this -> V_vector.clear();
    this -> V_dev_x.clear();
    this -> V_dev_y.clear();
    this -> V_dev_z.clear();
    Verbose::single(Verbose::Detail) << "Interpolated pseudopotentials are deleted." <<std::endl;
}

vector<double> KBprojector::get_Zvals(){
  return Zvals;
}
vector<int> KBprojector::get_projector_numbers(){
    return number_vps_file;
}
/*
void KBprojector::calculate_KB(RCP<Epetra_CrsMatrix>& core_hamiltonian){
    clock_t start_time,end_time;
    start_time=clock();

    int ierr;
    int size = basis->get_original_size();
    Epetra_Map Map = core_hamiltonian->RowMap();

    int number_atom = atoms->get_size();
    int NumMyElements = core_hamiltonian->Map().NumMyElements();
    int* MyGlobalElements = core_hamiltonian->Map().MyGlobalElements();
    //////////// local potential ///////////////////////
    if(parameters->sublist("Pseudopotential").get<string>("UsingDoubleGrid") == "No"){
        Teuchos::RCP<Epetra_Vector> local_potential = Teuchos::rcp(new Epetra_Vector(Map));
        calculate_local(local_potential);
        for (int i=0; i<NumMyElements; i++)
            core_hamiltonian->InsertGlobalValues(MyGlobalElements[i], 1, &local_potential->operator[](i), &MyGlobalElements[i]);{
        }
    }
    else if(parameters->sublist("Pseudopotential").get<string>("UsingDoubleGrid")=="Yes"){
        //calculate_local_DG(core_hamiltonian);
    }
    ////////////// end /////////////////////////////////

    //construct_matrix(core_hamiltonian);

    end_time =clock();
    Verbose::single()<< std::endl;
    Verbose::single()<< "#------------------------------------------------- KBprojector::calculate_KB" << std::endl;
    Verbose::single()<< " Time to perform outer product for pseudopotential_matrix: " << double(end_time-start_time)/CLOCKS_PER_SEC << " s" << std::endl;
    Verbose::single()<< "#---------------------------------------------------------------------------" << std::endl;

    return;
}
*/

vector< vector<int> > KBprojector::get_oamom(){
    return oamom;
}

vector<double> KBprojector::get_local_cutoff(){
    return local_cutoff;
}

vector< vector<double> > KBprojector::get_input_mesh(){
    return local_mesh;
}

vector< vector<double> > KBprojector::get_V_local(){
    if(local_pp.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed local vectors" << std::endl;
        this -> make_local_pp();
    }
    return local_pp;
}

vector< vector<double> > KBprojector::get_input_EKB(){
    return EKB;
}
