#pragma once
#include <vector>
#include <string>
#include <utility>

#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#include "../../Compute/Exchange_Correlation.hpp"
#include "../../Utility/Time_Measure.hpp"

#include "../Pseudo-potential/Paw_Species.hpp"

/**
 * @brief Calculate XC on the radial grid. Implemented for PAW.
 * @details
 * @parblock
 * This routine takes 50 Lebedev quadrature points.
 * First, expand density/partial wave product to corresponding Ylm. Then calculate exc/vxc on radial grid corrspond to a Lebedev quadrature point.
 * Reconstruct density or partial wave product at the radial grid on the Lebedev quadrature point and corresponding Ylm, then integrate.
 * @endparblock
 * @date 2015
 * @author Sungwoo Kang
 * @todo For better maintanence, this should use Exchange_Correlation class, while currently, does not.
 * @note Only LDA and GGA are implemented.
 * @note This routine is too complicated because I was obsessed with the idea of radial grid and spherical harmonics.
 * @todo Just gather all points (50 Lebedev quadrature points times dataset grid points, about 30000), push it into Exchange_Correlation.
 * This way we do not need to expand quantity over angular momentum, which makes calculation ~13 times more.
 **/
class Paw_XC{
    public:
        // Constructor
        /**
         * @brief Constructor.
         * @param[in] paw_species To retrieve grid, XC functional, AE/PS core density, AE/PS partial wave.
         * @param[in] lmax Maximum angular monentum to expand products.
         * @callergraph
         * @callgraph
         **/
        Paw_XC( Teuchos::RCP<Paw_Species> paw_species, int lmax = -1 );
        // Use these
        /**
         * @brief Returns PAW XC energy correction.
         * @param[in] sD_matrix Atom-centered density matrix.
         * @return PAW XC energy correction. Vector index follows that of the XC functional list.
         * @note See actual computation routine: calculate_radial_exc and its wrapper: calculate_xc_energy.
         * @callergraph
         * @callgraph
         **/
        std::vector<double> get_xc_energy_correction(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix
        );

        /**
         * @brief Returns PAW XC hamiltonian correction.
         * @param[in] sD_matrix Atom-centered density matrix.
         * @return PAW XC hamiltonian correction. Array index follows that of the XC functional list.
         * @note See actual computation routine: calculate_radial_vxc and its wrapper: calculate_vxc_correction.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector< std::vector<double> > > get_xc_hamiltonian_correction(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix
        );

        /**
         * @brief Returns PAW XC correction of \f$ \int dr [density]*v_{xc} \f$.
         * @param[in] sD_matrix Atom-centered density matrix.
         * @return PAW XC correction of \f$ \int dr [density]*v_{xc} \f$.
         * @note See actual computation routine: calculate_radial_vxc and its wrapper: calculate_vxc_correction.
         * @note This is too slow.
         * @callergraph
         * @callgraph
         **/
        double get_int_n_vxc_correction(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix
        );

        // Data
        /*
        void get_ae_density(Basis * basis, Grid_Setting * grid_setting, double * atom_center,
                std::vector< std::vector< std::vector<double> > > &sD_matrix,
                Teuchos::RCP<Epetra_MultiVector> &density
        );
        void get_ps_density(Basis * basis, Grid_Setting * grid_setting, double * atom_center,
                std::vector< std::vector< std::vector<double> > > &sD_matrix,
                Teuchos::RCP<Epetra_MultiVector> &density
        );
        */

    private:
        /**
         * @brief Expand density as a form of \f$ \sum_{l,m} n_L(r) * Y_{l,m} \f$.
         * @param[in] spin_size Number of spin index. Used for dealing with core density.
         * @param[in] D_matrix Atom-centered density matrix for a spin index.
         * @param[in] is_ae true if AE, false if PS.
         * @return vector<vector<vector<double>>> Decomposed density n_L as a index order of retval[l][m][r].
         * @todo Move spin-dealing part to constructor.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector< std::vector<double> > >
        expand_density_to_L( int spin_size, Teuchos::SerialDenseMatrix<int,double> D_matrix, bool is_ae );

        /**
         * @brief Expand product of two partial waves as a form of \f$ \sum_{l,m} n_L(r) * Y_{l,m} \f$.
         * @param[in] i First partial wave index, m-independent version.
         * @param[in] j Second partial wave index, m-independent version.
         * @param[in] m1 First partial wave m.
         * @param[in] m2 Second partial wave m.
         * @param[in] is_ae true if AE, false if PS.
         * @return Decomposed partial wave product n_L as a index order of retval[l][m][r].
         * @todo Move spin-dealing part to constructor.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector< std::vector<double> > >
        expand_pwprod_to_L( int i, int j, int m1, int m2, bool is_ae );

        /**
         * @brief Compute XC energy correction.
         * @param[in] xc_func_no Libxc type XC functional number.
         * @param[in] radial_val Density with spin index in the order of radial_val[spin][grid].
         * @param[in] gradient Density with spin index in the order of radial_val[spin][grid].
         * @return Resulting XC energy density.
         * @note This should be definitely removed and change to the Exchange_Correlation class.
         * @callergraph
         * @callgraph
         **/
        std::vector<double> calculate_radial_exc(
                int xc_func_no,
                std::vector< std::vector<double> > radial_val,
                std::vector< std::vector< std::vector<double> > > gradient
        );

        /**
         * @brief Compute XC energy correction.
         * @param[in] xc_func_no Libxc type XC functional number.
         * @param[in] radial_val Density with spin index in the order of radial_val[spin][grid].
         * @param[in] gradient Density with spin index in the order of radial_val[spin][grid].
         * @param[out] vxc Output XC potential. Spin index first, and grid index last.
         * @param[out] vsigma Output v_sigma. Spin index first, and grid index last.
         * @note This should be definitely removed and change to the Exchange_Correlation class.
         * @callergraph
         * @callgraph
         **/
        void calculate_radial_vxc(
                int xc_func_no,
                std::vector< std::vector<double> > radial_val,
                std::vector< std::vector< std::vector<double> > > gradient,
                std::vector< std::vector<double> > &vxc,
                std::vector< std::vector<double> > &vsigma
        );

        /**
         * @brief Wrapper of calculate_raidal_exc.
         * @details 1) Get density along a line passing Lebedev quadrature point. 2) Call calculate_radial_exc 50 times. 3) Add all.
         * @param[in] xc_func_no Libxc type XC functional number.
         * @param[out] vals_in_L Density to compute. Index order of [spin][l][l+m][grid]
         * @return Resulting XC energy.
         * @note Come to think of it, it is not necessary to call calculate_radial_exc 50 times.
         * @callergraph
         * @callgraph
         **/
        double calculate_xc_energy(
            int xc_func_no,
            std::vector< std::vector< std::vector< std::vector<double> > > > &vals_in_L
        );
        /**
         * @brief Wrapper of calculate_raidal_vxc.
         * @details 1) Get density along a line passing Lebedev quadrature point. 2) Call calculate_radial_vxc 50 times. 3) Process vxc from 2) and vsigma to get XC potential. 4) Add all.
         * @param[in] xc_func_no Libxc type XC functional number.
         * @param[in] vals_in_L Density to compute. Index order of [spin][l][l+m][grid]
         * @param[in] pw_prod_in_L Product of two partial waves to compute. Index order of [spin][l][l+m][grid]
         * @return Resulting hamiltonian correction from XC potential.
         * @note Come to think of it, it is not necessary to call calculate_radial_vxc 50 times.
         * @note This function has minor deviation from GPAW for GGA, mainly for different gradient control.
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector< std::vector<double> > > calculate_vxc_correction(
            int xc_func_no,
            std::vector< std::vector< std::vector< std::vector<double> > > > vals_in_L,
            //std::vector< std::vector< std::vector< std::vector< std::vector<double> > > > > &pw_prod_in_L
            std::vector< std::vector< std::vector< std::vector<double> > > > pw_prod_in_L
        );

        /**
         * @brief Calculate gradient from angular momentum expanded value.
         * @param[in] radial_vals_in_L Value to compute gradient. Index order is [spin][l][l+m][grid].
         * @param[in] point Point denoting direction to compute gradient.
         * @return Resulting gradient, index order of [spin][x:0,y:1,z:2][grid].
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector< std::vector<double> > > calculate_gradient(
            std::vector< std::vector< std::vector< std::vector<double> > > > radial_vals_in_L,
            std::vector<double> point
        );

        /**
         * @brief Calculate contracted gradient from gradient.
         * @param[in] gradient Gradient to contract. Index order of [spin][x:0,y:1,z:2][grid].
         * @return Contracted gradient, index order of [spin;uu:0,ud:1,dd:2][grid].
         * @callergraph
         * @callgraph
         **/
        std::vector< std::vector<double> > contract_gradient(
            std::vector< std::vector< std::vector<double> > > gradient
        );

        // Utility (Silimar to the same function in Paw_Species)
        /**
         * @brief Change m-independent pw state index to m-independent index, which is used for matrix.
         * @param[in] pw_state_no m-independent partial wave state index.
         * @param[in] m value.
         * @return m-dependent partial wave state index.
         **/
        int get_integrated_index( int pw_state_no, int m );
        /**
         * @brief Change matrix index (i,j) to linearlized index describing upper triangle. (Assumes symmetric matrix).
         * @param[in] i Matrix row index.
         * @param[in] j Matrix column index.
         * @return j + n*i-i*(i+1)/2 (for i <= j)
         * @note Why not Paw_Util?
         **/
        int pack_index(int i, int j);
        /**
         * @brief Change linearlized matrix index describing upper triangle to row-column index. (Assumes symmetric matrix).
         * @param[in] ind Linearlized matrix index.
         * @return Matrix row, column index. Always upper triangle.
         * @note Why not Paw_Util?
         **/
        std::pair<int, int> unpack_index(int ind);

        // Data Storage
        std::vector<int> xc_functionals;
        std::vector<double> grid;
        std::vector<double> grid_deriv;

        std::vector<double> ae_core_density;
        std::vector<double> ps_core_density;

        std::vector< std::vector<double> > ae_pw;
        std::vector< std::vector<double> > ps_pw;
        std::vector<double> pw_l_list;

        int lmax;
        int total_size;
        int grid_size;
        double rc;


        //std::vector< std::vector< std::vector< std::vector< std::vector<double> > > > > ae_pw_prod_L;
        //std::vector< std::vector< std::vector< std::vector< std::vector<double> > > > > ps_pw_prod_L;
        std::vector< std::vector< std::vector< std::vector<double> > > > ae_pw_prod_L;
        std::vector< std::vector< std::vector< std::vector<double> > > > ps_pw_prod_L;

        int iter;
        Teuchos::RCP<Time_Measure> timer;
};
