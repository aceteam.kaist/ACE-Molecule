#pragma once
#include <string>
#include <vector>

#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"
#include "Epetra_CrsMatrix.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

#include "../../Io/Atoms.hpp"
#include "../../Basis/Basis.hpp"
#include "Filter.hpp"
#include "../../Utility/Time_Measure.hpp"

class KBprojector{
    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~KBprojector(){};

        virtual void read_pp()=0;
/**
 * @brief Construct pseudopotential matrix and add it to the core Hamiltonian matrix.
 * @author Kwangwoo Hong,Sunghwan Choi
 * @date 2014-08-14
 * @param core_hamiltonian The core Hamiltonian which is updated
 * */
//        void calculate_KB(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);
        double get_ion_ion_correction();
        int get_core_electron_info(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& density, 
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> >& density_grad
        );
        virtual void compute_nonlinear_core_correction() = 0;
        std::vector<double> get_Zvals();
        std::vector<int> get_projector_numbers();

//        void get_comp_charge_exps(std::vector<double>& comp_charge_exps);
        
        virtual std::vector< std::vector<int> > get_oamom();
        virtual std::vector< std::vector<double> > get_input_EKB();

        std::vector< std::vector< std::vector<double> > > get_EKB_matrix();///< Index order: [atom index][projector 1 index][projector 2 index]
        /**
         * @brief Returns onlocal projector nonzero values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        virtual std::vector< std::vector <std::vector < std::vector<double> > > > get_V_nl();
        /**
         * @brief Returns onlocal projector x-derivative values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        virtual std::vector< std::vector <std::vector < std::vector<double> > > > get_V_nl_dev_x();
        /**
         * @brief Returns onlocal projector y-derivative values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        virtual std::vector< std::vector <std::vector < std::vector<double> > > > get_V_nl_dev_y();
        /**
         * @brief Returns onlocal projector z-derivative values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        virtual std::vector< std::vector <std::vector < std::vector<double> > > > get_V_nl_dev_z();
        /**
         * @brief Returns onlocal projector indicies with nonzero values.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        virtual std::vector< std::vector <std::vector < std::vector<int> > > > get_V_index();
        virtual std::vector< std::vector< std::vector<double> > > get_h_ij_total();
        
        virtual std::vector< std::vector<double> > get_V_local();
        virtual std::vector< std::vector<double> > get_V_local_dev();
        virtual std::vector<double> get_local_cutoff();
        virtual std::vector< std::vector<double> > get_input_mesh();
        //virtual std::vector< std::vector<double> > get_input_local();
        
        /**
         * @brief Removes stored PP informations
         * @callergraph
         * @callgraph
         * @author Sungwoo Kang
         **/
        virtual void clear_vector();

    protected:
        /**
         * @brief Calls calculate_nonlocal.
         * @details Initializes V_vector, V_dev_[xyz], V_index member variables and calls calculate_nonlocal.
         * @author Jaechang Lim
         * @callergraph
         * @callgraph
         **/
        void make_nonlocal_vectors();
        virtual void make_local_pp()=0;
        
        /**
         * @brief Calls make_local_pp() and make_nonlocal_vectors().
         * @author Jaechang Lim
         * @callergraph
         * @callgraph
         **/
        void make_vectors(); 

        /**
         * @brief Interpolates PP projectors to our basis and calculates gradients.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index].
         * @callergraph
         * @callgraph
         **/
        virtual void calculate_nonlocal(int iatom,std::array<double,3> position,
                                        std::vector<std::vector<std::vector<double> > >& nonlocal_pp,
                                        std::vector<std::vector<std::vector<double> > >& nonlocal_dev_x,
                                        std::vector<std::vector<std::vector<double> > >& nonlocal_dev_y,
                                        std::vector<std::vector<std::vector<double> > >& nonlocal_dev_z,
                                        std::vector<std::vector<std::vector<int> > >& nonlocal_pp_index ) = 0;
        //virtual void calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential) = 0;
        //virtual void calculate_local_DG(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian) = 0;

        //virtual void construct_matrix(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian) = 0;

        std::vector< std::vector<double> > local_pp_radial;  // itype, value
        std::vector< std::vector< std::vector<double> > > nonlocal_pp_radial;   ///< Index order: itype, projector, value

        std::vector< std::vector<double> > nonlocal_cutoff;  ///< Index order: itype, projector

        std::vector<std::vector<double> > local_mesh; // radial mesh; itype, value
        std::vector<std::vector <double> > local_d_mesh; //radial mesh; itype, value
        std::vector< std::vector< std::vector<double> > > nonlocal_mesh; //radial mesh;  itype, projector, value
        std::vector< std::vector< std::vector<double> > > nonlocal_d_mesh; //radial mesh; itype, projector, value

        std::vector< std::vector< std::vector<double> > > new_nonlocal_mesh; // radial mesh;  itype, projector, value    
        std::vector< std::vector< std::vector<double> > >  new_nonlocal_pp_radial;   //< Index order: itype, projector, value
        std::vector< std::vector<double> > new_nonlocal_cutoff;  // Index order: itype,  projector

        std::vector<int>  projector_number; ///< Index: itype

        std::vector<double> local_cutoff;
        std::vector< std::vector<int> > oamom;///< Index: atom type, projector
        std::vector< std::vector< std::vector<double> > > EKB_matrix; ///< Index: iatom, projector1, projector2
        std::vector< std::vector<double> > EKB;
        std::vector< std::vector< std::vector<double> > > h_ij_total;

        Teuchos::RCP<Filter> filter;
        Teuchos::RCP<const Basis> basis ;

        Teuchos::RCP<Teuchos::ParameterList> parameters;
        Teuchos::RCP<const Atoms> atoms;

        std::vector<int> number_vps_file, number_pao_file;
        std::vector<double> Zvals;
    
        std::vector< std::vector<double> > original_mesh;
        std::vector< std::vector<double> > original_d_mesh;
 
        Teuchos::RCP<Epetra_Vector> core_density;
        Teuchos::RCP<Epetra_MultiVector> core_density_grad;

        std::vector< std::vector<double> > local_pp;
        std::vector< std::vector<double> > local_pp_dev;

        std::vector< std::vector<double> > atomic_density;
        Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure() );

        /**
         * @name Interpolated values
         **/
        /** @{ **/        
        /**
         * @brief Nonlocal projector nonzero values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        std::vector<std::vector<std::vector<std::vector<double> > > > V_vector;
        /**
         * @brief Nonlocal projector x-derivative values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        std::vector<std::vector<std::vector<std::vector<double> > > > V_dev_x;
        /**
         * @brief Nonlocal projector y-derivative values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        std::vector<std::vector<std::vector<std::vector<double> > > > V_dev_y;
        /**
         * @brief Nonlocal projector z-derivative values. Paired with V_index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        std::vector<std::vector<std::vector<std::vector<double> > > > V_dev_z;
        /**
         * @brief Nonlocal projector index.
         * @details Index order: [atom index][projector index][magnetic moment index][grid index]
         **/
        std::vector<std::vector<std::vector<std::vector<int> > > > V_index;
        /**  @} **/
};


