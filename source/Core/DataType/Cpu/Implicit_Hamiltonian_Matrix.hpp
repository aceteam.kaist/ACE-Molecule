#pragma once

#include "Epetra_Operator.h"
#include "../../../Utility/Warning.hpp"
#include "AnasaziOperator.hpp"
#include "AnasaziEpetraAdapter.hpp"
#include "Teuchos_RCP.hpp"
#include "Epetra_MultiVector.h"
//class Implicit_Hamiltonian_Matrix: public Anasazi::Operator{
class Implicit_Hamiltonian_Matrix: public Epetra_Operator{
//class Implicit_Hamiltonian_Matrix{
    public:

        Implicit_Hamiltonian_Matrix(
            Teuchos::RCP<Core_Hamiltonian_Matrix> core_hamiltonian_matrix,
            Teuchos::RCP< Epetra_Vector > local_potential, bool gpu_enable=false ):
            core_hamiltonian_matrix(core_hamiltonian_matrix),
            local_potential(local_potential),gpu_enable(gpu_enable){ 
                comm =Teuchos::rcp (local_potential->Comm().Clone());
                map  =Teuchos::rcp (new Epetra_Map(local_potential->GlobalLength(), local_potential->MyLength(), 0, *comm ) );
            };

        int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const{
        //void Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const{

            Teuchos::RCP<Epetra_MultiVector> tmp_X = Teuchos::rcp(& const_cast< Epetra_MultiVector&> (X),false);
            Teuchos::RCP<Epetra_MultiVector> tmp_Y = Teuchos::rcp(&Y,false);
    
//            Teuchos::RCP<Epetra_MultiVector> tmp_X 
//                    = Teuchos::rcp(dynamic_cast<Epetra_MultiVector* >(&const_cast< Anasazi::MultiVec<double> &>(X)), false);
//            Teuchos::RCP<Epetra_MultiVector> tmp_Y = Teuchos::rcp(dynamic_cast<Epetra_MultiVector* >(&Y), false);
            this -> core_hamiltonian_matrix->multiply( ispin, tmp_X,tmp_Y );
            for(int i=0; i<tmp_X->NumVectors(); i++)
                tmp_Y->operator()(i)->Multiply(1.0, *local_potential, *tmp_X->operator()(i), 1.0);
            return 0;
        } ;

        void set_spin(int ispin){ this->ispin = ispin;    };
        //int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const ;
    private:        
        int SetUseTranspose(bool UseTranspose) {Warning::throw_error("Hamiltonian_Matrix::SetUseTranspose"); return 0;};
        int ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const{Warning::throw_error("Hamiltonian_Matrix::ApplyInverse"); return 0;} ;
        double NormInf() const{Warning::throw_error("Hamiltonian_Matrix::NormInf"); return 0;} ;
        const char * Label() const{Warning::throw_error("Hamiltonian_Matrix::Label"); return NULL; } ;
        bool UseTranspose() const{Warning::throw_error("Hamiltonian_Matrix::UseTranspose"); return false;} ;
        bool HasNormInf() const{Warning::throw_error("Hamiltonian_Matrix::HasNormInf"); return false;} ;
        const Epetra_Comm & Comm() const{Warning::throw_error("Hamiltonian_Matrix::Comm"); return *comm;} ;
        const Epetra_Map & OperatorDomainMap() const {return *map;};
        const Epetra_Map & OperatorRangeMap() const { return *map;};


        Teuchos::RCP<Epetra_Comm> comm;
        Teuchos::RCP<Epetra_Map> map;
        int ispin;
        Teuchos::RCP< Epetra_Vector > local_potential;
        Teuchos::RCP<Core_Hamiltonian_Matrix> core_hamiltonian_matrix;
        bool gpu_enable;
//        Teuchos::RCP<Epetra_CrsMatrix> hamiltonian_matrix;
};

/*
namespace Anasazi{
template < >
class OperatorTraits<double, Epetra_MultiVector,Anasazi::Operator >{
//class OperatorTraits<double, Epetra_MultiVector,Implicit_Hamiltonian_Matrix >{
    public:
        static void Apply ( const Anasazi::Operator& Op,
                      const Epetra_MultiVector& x,
                      Epetra_MultiVector& y )
        { 

            Op.Apply( x, y ); 

        }
        
};
};
}*/
