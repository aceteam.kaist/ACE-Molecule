#include "External_Field_Cube.hpp"
#include <fstream>
#include "Teuchos_ParameterList.hpp"

#include "../../Basis/Create_Basis.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Interpolation/Tricubic_Interpolation.hpp"
#include "../../Utility/Interpolation/Trilinear_Interpolation.hpp"
#include "../../Utility/Time_Measure.hpp"
#include "../../Utility/Read/Read_Cube.hpp"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::ParameterList;
using Teuchos::NO_TRANS;
using std::vector;
using std::array;
using std::string;

External_Field_Cube::External_Field_Cube(
        Teuchos::RCP<const Basis> basis,
        std::string cube_filename,
        int interpolation,
        double scaling_factor
): External_Field(std::string("Cube"),basis){
    this -> cube_filename = cube_filename;
    this -> interpolation_scheme = interpolation;
    this-> scaling_factor = scaling_factor;

    calculate_potential();
    have_potential = true;
}

std::ostream& External_Field_Cube::print(std::ostream &c) const{
    c << "=================================== ExternalField ==" << std::endl;
    c << " ExternalField: " << this -> type << std::endl;
    c << " Filed from cube: " << this -> cube_filename << " x " << this -> scaling_factor << std::endl;
    if(interpolation_scheme == 0){
        c << " Interpolation: Trilinear" << std::endl;
    } else {
        c << " Interpolation: Tricubic" << std::endl;
    }
    c << "====================================================" << std::endl;
    return c;
}

void External_Field_Cube::calculate_potential(){

    std::array<int,3> cube_points;
    std::array<std::array<double,3>,3> cube_lattice_vec;

    Verbose::single(Verbose::Detail)<< "External_Field_Cube:: checking validity of filename (" << cube_filename << ") " <<std::endl;
    //Teuchos::RCP<Epetra_Vector> potential ;
    Teuchos::RCP<const Atoms> atoms = Teuchos::rcp(new Atoms(Read::Cube::read_cube_atoms(cube_filename) ));
    Read::Cube::read_cube_mesh(cube_filename, cube_points, cube_lattice_vec); //unit is bohr

//    for debug
//    std::cout << cube_lattice_vec[0][0] << "\t" << cube_lattice_vec[0][1] <<"\t" << cube_lattice_vec[0][2] <<std::endl;
//    std::cout << cube_lattice_vec[1][0] << "\t" << cube_lattice_vec[1][1] <<"\t" << cube_lattice_vec[1][2] <<std::endl;
//    std::cout << cube_lattice_vec[2][0] << "\t" << cube_lattice_vec[2][1] <<"\t" << cube_lattice_vec[2][2] <<std::endl;

    // check cube contains orthogonal (diagonal) lattice vectors
    for(int i = 0; i < 3; ++i){
        int j = (i+1)%3; int k = (i+2)%3;
        if (fabs(cube_lattice_vec[i][j]) > 1e-8 or
                fabs(cube_lattice_vec[i][k]) > 1e-8){
            Verbose::all() << "lattice in cube file should be orthogonal" <<std::endl;
            Verbose::all() << "cube filename:" << cube_filename <<std::endl;
            exit(-1);
        }
    }

    RCP<Time_Measure> timer = rcp(new Time_Measure());

    RCP<ParameterList> cube_basis_param = rcp(new ParameterList("cube_basis"));
//    cube_basis_param -> sublist("BasicInformation").set<string>("Grid", "Basic");
//    cube_basis_param -> sublist("BasicInformation").set<string>("Basis", "Sinc");
//    cube_basis_param -> sublist("BasicInformation").set<int>("AllowOddPoints", 1);
//    cube_basis_param -> sublist("BasicInformation").set<int>("PointX", cube_points[0]);
//    cube_basis_param -> sublist("BasicInformation").set<int>("PointY", cube_points[1]);
//    cube_basis_param -> sublist("BasicInformation").set<int>("PointZ", cube_points[2]);
//    cube_basis_param -> sublist("BasicInformation").set<double>("ScalingX", cube_lattice_vec[0][0]);
//    cube_basis_param -> sublist("BasicInformation").set<double>("ScalingY", cube_lattice_vec[1][1]);
//    cube_basis_param -> sublist("BasicInformation").set<double>("ScalingZ", cube_lattice_vec[2][2]);

    cube_basis_param ->set<string>("Grid", "Basic");
    cube_basis_param ->set<string>("Basis", "Sinc");
    cube_basis_param ->set<int>("AllowOddPoints", 1);
    cube_basis_param ->set<int>("PointX", cube_points[0]);
    cube_basis_param ->set<int>("PointY", cube_points[1]);
    cube_basis_param ->set<int>("PointZ", cube_points[2]);
    cube_basis_param ->set<double>("ScalingX", cube_lattice_vec[0][0]);
    cube_basis_param ->set<double>("ScalingY", cube_lattice_vec[1][1]);
    cube_basis_param ->set<double>("ScalingZ", cube_lattice_vec[2][2]);

    RCP<Basis> cube_basis = Create_Basis::Create_Basis(*this -> basis -> get_map() -> Comm().Clone(), cube_basis_param);
    RCP<Epetra_Vector> original_values = rcp(new Epetra_Vector(*cube_basis->get_map()));

    //std::cout << *cube_basis <<std::endl;
    //exit(-1);

    cube_basis->read_cube(cube_filename, original_values, true);
    int NumMyElements = basis -> get_map() -> NumMyElements();
    int* MyGlobalElements = basis -> get_map() -> MyGlobalElements();
    vector<double> val(NumMyElements);
    int j_x,j_y,j_z;

    this -> potential = rcp( new Epetra_Vector( *basis->get_map()));
    timer -> start("Interpolation");
    if(this -> interpolation_scheme == 0){
       Interpolation::Trilinear::interpolate(cube_basis, original_values, basis, potential );
    } else if(this -> interpolation_scheme == 1){
        Interpolation::Tricubic::interpolate(cube_basis, original_values, basis, potential );
    }
    // scaling
    potential->Scale(scaling_factor);
    timer -> end("Interpolation");
    timer -> print(Verbose::single(Verbose::Simple), -1, "Interpolation");

    return;
}
