#include "External_Field_From_Input.hpp"
#include <fstream>
#include "Teuchos_ParameterList.hpp"

#include "../../Basis/Create_Basis.hpp"
#include "../../Utility/Parallel_Manager.hpp"
#include "../../Utility/Parallel_Util.hpp"
#include "../../Utility/Verbose.hpp"
#include "../../Utility/Interpolation/Tricubic_Interpolation.hpp"
#include "../../Utility/Interpolation/Trilinear_Interpolation.hpp"
#include "../../Utility/Time_Measure.hpp"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::ParameterList;
using Teuchos::NO_TRANS;
using std::vector;
using std::array;
using std::string;

External_Field_From_Input::External_Field_From_Input(
        Teuchos::RCP<const Basis> basis,
        std::string pot_filename,
        std::array<int,3> points,
        std::array<std::array<double,3>,3> lattice_vec,
        std::array<double,3> center,
        int interpolation
): External_Field(std::string("From_Input"),basis){
    this -> pot_filename = pot_filename;
    this -> points = points;
    this -> lattice_vec = lattice_vec;
    this -> center = center;
    this -> interpolation_scheme = interpolation;

    calculate_potential();
    have_potential = true;
}

std::ostream& External_Field_From_Input::print(std::ostream &c) const{
    c << "=================================== ExternalField ==" << std::endl;
    c << " ExternalField: " << this -> type << std::endl;
    c << " Filed from file: " << this -> pot_filename << std::endl;
    if(interpolation_scheme == 0){
        c << " Interpolation: Trilinear" << std::endl;
    } else {
        c << " Interpolation: Tricubic" << std::endl;
    }
    c << " Centered on: " << this -> center[0] << ", " << this -> center[1] << ", " << this -> center[2] << std::endl;
    for(int i = 0; i < 3; ++i){
        c << " Lattice vector " << i << ": " << this -> lattice_vec[i][0] << ", " << this -> lattice_vec[i][1] << ", " << this -> lattice_vec[i][2] << " on " << this -> points[i] << std::endl;
    }
    c << "====================================================" << std::endl;
    return c;
}

void External_Field_From_Input::calculate_potential(){
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    const double** scaled_grid = basis->get_scaled_grid();
    int size = basis->get_map()->NumMyElements();
    int* MyGlobalElements = basis->get_map()->MyGlobalElements();

    array<double,3> vasp_spacing;
    vasp_spacing.fill(0.0);
    for(int i = 0; i < 3; ++i){
        double tmp = 0.0;
        for(int j = 0; j < 3; ++j){
            tmp += pow(this -> lattice_vec[i][j],2);
        }
        vasp_spacing[i] = sqrt(tmp)/this -> points[i];
    }

    timer -> start("Potential read and grid init");
    vector<double> orig_pot = this -> read(this -> pot_filename);
    Teuchos::SerialDenseMatrix<int,double> inv_T = this -> get_transformation_matrix(this -> lattice_vec, this -> points);
    // Trilinear/Tricubic interpolation은 평행사변체 grid이기만 하면 된다.
    vector<int> index_list;
    //vector< array<double,3> > pos_list;
    vector< Teuchos::SerialDenseMatrix<int,double> > pos_list;
    for(int i=0;i<size;i++){
        Teuchos::SerialDenseMatrix<int,double> orig_pos(3,1);
        this -> basis->get_position(MyGlobalElements[i],orig_pos(0,0), orig_pos(1,0), orig_pos(2,0));
        Teuchos::SerialDenseMatrix<int,double> pos(3,1);
        for(int d = 0; d < 3; ++d){
            pos(0,0) = orig_pos(d, 0) - this -> center[d];
        }
        if(this -> is_inside(pos)){
            Teuchos::SerialDenseMatrix<int,double> tmp(3,1);
            tmp.multiply(NO_TRANS, NO_TRANS, 1.0, inv_T, pos, 0.0);
            index_list.push_back(i);
            pos_list.push_back(tmp);
        }
        /*else{
            Verbose::single(Verbose::Detail) << pos(0,0) << ", " << pos(1,0) << ", " << pos(2,0) << std::endl;
        }*/
    }
    timer -> end("Potential read and grid init");

    timer -> print(Verbose::single(), -1, "Potential read and grid init");

    RCP<ParameterList> basis_param = rcp(new ParameterList("BasicInformation"));
    basis_param -> set<string>("Grid", "Basic");
    basis_param -> set<string>("Basis", "Sinc");
    basis_param -> set<int>("AllowOddPoints", 1);
    basis_param -> set<int>("PointX", points[0]);
    basis_param -> set<int>("PointY", points[1]);
    basis_param -> set<int>("PointZ", points[2]);
    basis_param -> set<double>("ScalingX", vasp_spacing[0]);
    basis_param -> set<double>("ScalingY", vasp_spacing[1]);
    basis_param -> set<double>("ScalingZ", vasp_spacing[2]);
    //basis_param -> sublist("BasicInformation").set<double>("ScalingX", 1.0);
    //basis_param -> sublist("BasicInformation").set<double>("ScalingY", 1.0);
    //basis_param -> sublist("BasicInformation").set<double>("ScalingZ", 1.0);
    RCP<Basis> vasp_basis = Create_Basis::Create_Basis(*this -> basis -> get_map() -> Comm().Clone(), basis_param);

    timer -> start("Interpolation");
    vector<double> val(size);
    if(this -> interpolation_scheme == 0){
        for(int j = 0; j < index_list.size(); ++j){
            val[index_list[j]] = Interpolation::Trilinear::calculate(vasp_basis, orig_pot.data(), pos_list[j](0,0), pos_list[j](1,0), pos_list[j](2,0));
        }
    } else if(this -> interpolation_scheme == 1){
        for(int j = 0; j < index_list.size(); ++j){
            val[index_list[j]] = Interpolation::Tricubic::calculate(vasp_basis, orig_pot.data(), pos_list[j](0,0), pos_list[j](1,0), pos_list[j](2,0));
        }
        //if(std::abs(val[index_list[j]]) < 1.0E-15) Verbose::single(Verbose::Detail) << index_list[j] << ": " << pos_list[j](0,0) << ", " << pos_list[j](1,0) << ", " << pos_list[j](2,0) << ": " << val[index_list[j]] << std::endl;
    }
    timer -> end("Interpolation");
    timer -> print(Verbose::single(Verbose::Simple), -1, "Interpolation");
    // Put val to potential
    this -> potential = rcp( new Epetra_Vector(Copy, *basis->get_map(), val.data()));
    return;
}

std::vector<double> External_Field_From_Input::read(std::string filename){
    vector<double> pot_from_file;
    pot_from_file.reserve(points[0]*points[1]*points[2]);
    //if(Parallel_Manager::info().get_total_mpi_rank() == 0 or true){
    if(true){
        std::ifstream ifs(filename);
        double tmp;
        while(ifs >> tmp){
            pot_from_file.push_back(tmp);
        }
        if(pot_from_file.size() != points[0]*points[1]*points[2]){
            std::ostringstream oss;
            oss << "Potential file size and grid size mismatch! " << pot_from_file.size() << " vs. " << points[0]*points[1]*points[2] << ".\n";
            Verbose::all() << oss.str() << std::endl;
            throw std::runtime_error(oss.str());
        }
    }
    //Parallel_Manager::info().all_bcast(pot_from_file.data(), pot_from_file.size(), 0);
    return pot_from_file;
}

Teuchos::SerialDenseMatrix<int, double>
External_Field_From_Input::get_transformation_matrix(std::array<std::array<double,3>,3> lattice_vec, std::array<int,3> ngrid){
    Teuchos::SerialDenseMatrix<int, double> T(3,3);
    Teuchos::SerialDenseMatrix<int, double> inv_T(3,3);
    array<double,3> lvec_norm;
    for(int i = 0; i < 3; ++i){
        lvec_norm[i] = 0.0;
        for(int j = 0; j < 3; ++j){
            lvec_norm[i] += lattice_vec[i][j]*lattice_vec[i][j];
        }
    }
    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            T(i,j) = lattice_vec[i][j]/std::sqrt(lvec_norm[i]);
        }
    }
    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            int i1 = (i+1)%3; int i2 = (i+2)%3;
            int j1 = (j+1)%3; int j2 = (j+2)%3;
            inv_T(j,i) = T(i1,j1)*T(i2,j2)-T(i1,j2)*T(i2,j1);
        }
    }
    inv_T.scale(1./(T(0,0)*inv_T(0,0)+T(1,0)*inv_T(0,1)+T(2,0)*inv_T(0,2)));
    return inv_T;
}

// This consideres smallest cubic box enclosing lattice vector.
bool External_Field_From_Input::is_inside(Teuchos::SerialDenseMatrix<int, double> pos){
    array<double, 3> max_point;
    max_point.fill(0.0);
    for(int i = 0; i < 3; ++i){
        for(int d = 0; d < 3; ++d){
            max_point[d] += this -> lattice_vec[i][d];
        }
    }
    if(pos(0,0) < -max_point[0]/2 or pos(1,0) < -max_point[1]/2 or pos(2,0) < -max_point[2]/2){
        return false;
    }
    if(pos(0,0) > max_point[0]/2 or pos(1,0) > max_point[1]/2 or pos(2,0) > max_point[2]/2){
        return false;
    }
    return true;
}
