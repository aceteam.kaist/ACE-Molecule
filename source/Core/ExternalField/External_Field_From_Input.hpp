#pragma once
#include <string>
#include <array>
#include "../../Basis/Basis.hpp"
#include "External_Field.hpp"

#include "Teuchos_RCP.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#include "Epetra_Vector.h"


/**
 * @author Sungwoo Kang
 * @date 18/01/05
 * @brief Read external field potential from file.
 * @note Potential file loop order should be tested in conjunction of index order and cube file loop order.
 **/
class External_Field_From_Input: public External_Field{
  public:
    External_Field_From_Input(
        Teuchos::RCP<const Basis> basis,
        std::string pot_filename,
        std::array<int,3> points,
        std::array<std::array<double,3>,3> lattice_vec,
        std::array<double,3> center,
        int interpolation
    );
    virtual std::ostream &print(std::ostream& c) const;

  protected:
    //Teuchos::RCP<Teuchos::ParameterList> parameters;
    /**
     * @breif Interpolate the electric field from the given file to our basis.
     * @details Please note that our interpolation methods only accepts the rectangular basis.
     * To use our interpolation methods, this function makes a rectangular auxilary basis with same edge length with external potential basis.
     * Simulation basis point is expressed in terms of this auxilary basis basis.
     * @note See also get_transformation_matrix
     **/
    void calculate_potential();
    std::vector<double> read(std::string filename);
    /**
     * @brief Transforms the simulation basis coordinates to auxilary basis.
     **/
    Teuchos::SerialDenseMatrix<int, double> get_transformation_matrix(std::array<std::array<double,3>,3> lattice_vec, std::array<int,3> ngrid);
    bool is_inside(Teuchos::SerialDenseMatrix<int, double> pos);

    std::string pot_filename;
    std::array<int,3> points;
    std::array<std::array<double,3>,3> lattice_vec;
    std::array<double,3> center;
    int interpolation_scheme;
};
