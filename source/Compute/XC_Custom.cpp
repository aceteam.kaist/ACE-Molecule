#include "XC_Custom.hpp"
#include "xc.h"

#include <stdexcept>
#include <cmath>

#include "../Utility/Parallel_Util.hpp"
#include "../Utility/Math/Faddeeva.hpp"
#include "../Utility/Calculus/Lagrange_Derivatives.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/String_Util.hpp"

using std::abs;
using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

#define _XC_RHO_CUTOFF (1.0E-10)
#define _XC_SIGMA_CUTOFF (1.0E-10)

/*
 * Guide to read LIBXC
 * 1. XC(X) is either xc_ or XC_ in double precision (xc_s_ or XC_S_ in single precision)
 * 2. LC_WPBE implemented in hyb_gga_xc_hse.c
 *    XC_GGA_X_WPBEH 1.0, XC_GGA_C_PBE 1.0
 *    cam_omega = 0.4 (RS \mu), cam_alpha = 1.0 (LR EXX portion), cam_beta = -1.0 (SR EXX portion: DISABLED)
 * 3. See hyb_gga_x_ityh.c to see 10.1063/1.1383587 which talks about making LDA/GGA X into ERF-type range-separated functional.
 * 4. See lda_x.c lda_x_attenuation_function for LDA screened coulomb interaction implementations.
 * 5. For GGA, change 3(3/4PI)^(1/3) to K^GGA. If K^GGA depend on density or aa !~ \rho^(1/3), we are fucked.
 *    Since K^PBE = K^LDA * (1 + kappa - kappa/(1+mu s^2/kappa)) where s = |\grad n|/(2 k_F n), we are fucked.
 * 6. Fuck you.
 */

// mGGA exchange-correlation potential CANNOT be computed.
// mGGA exchange-correlation energy can be computed.
// mGGA exchange-correlation energy with NLCC CANNOT be computed.

/*
 * NOTE on LIBXC index:
 * For UNPOLARIZED case, it is simply rho[i], sigma[i] where i is point index, and so on.
 * For POLARIZED case, rho[2*i+is], sigma[3*i+is] where is is spin index, and so on.
 * POLARIZED spin index list:
 * - GGA
 *   rho, vrho(2)     = (u, d)
 *   sigma, vsigma(3) = (uu, ud, dd)
 *
 *   v2rho2(3)        = (u_u, u_d, d_d)
 *   v2rhosigma(6)    = (u_uu, u_ud, u_dd, d_uu, d_ud, d_dd)
 *   v2sigma2(6)      = (uu_uu, uu_ud, uu_dd, ud_ud, ud_dd, dd_dd)
 *
 *   v3rho3(4)        = (u_u_u, u_u_d, u_d_d, d_d_d)
 *   v3rho2sigma(9)   = (u_u_uu, u_u_ud, u_u_dd, u_d_uu, u_d_ud, u_d_dd, d_d_uu, d_d_ud, d_d_dd)
 *   v3rhosigma2(12)  = (u_uu_uu, u_uu_ud, u_uu_dd, u_ud_ud, u_ud_dd, u_dd_dd, d_uu_uu, d_uu_ud, d_uu_dd, d_ud_ud, d_ud_dd, d_dd_dd)
 *   v3sigma(10)      = (uu_uu_uu, uu_uu_ud, uu_uu_dd, uu_ud_ud, uu_ud_dd, uu_dd_dd, ud_ud_ud, ud_ud_dd, ud_dd_dd, dd_dd_dd)
 *
 */

/*
XC_Custom::XC_Custom(RCP<const Basis> basis, int function_id, std::vector<double> alpha, std::vector<double> beta, int NGPU):Libxc(basis, function_id, NGPU), alpha(alpha), beta(beta){
    if(this -> function_id != 10478){
        throw std::invalid_argument("Only LC-wPBE-2Gau is supported from XC_Custom!");
    }
#ifdef DEBUG
    test();
#endif
}
*/

void XC_Custom::compute_exc(RCP<XC_State> &xc_info){
    auto map = basis->get_map();
    int NumMyElements = map->NumMyElements();

    RCP<Epetra_Vector> output = rcp(new Epetra_Vector(*map));
    this -> spin_size = xc_info -> all_density.size();

    if(xc_info -> all_density.size() == 1){
        if(xc_info -> get_functional_type() == "LDA"){
            for(int i=0; i<NumMyElements; i++){
                double rho = xc_info -> all_density[0] -> operator[](i);
                if(rho > _XC_RHO_CUTOFF){
                    this -> xc_lda_ex(NULL, 1, &rho, &(output -> operator[](i)));
                }
            }
        } else if(xc_info -> get_functional_type() == "GGA"){
            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
            double rho = 0.0, sigma = 0.0;

            for(int i=0; i<NumMyElements; i++){
                rho = xc_info -> all_density[0]->operator[](i);
                if(rho > _XC_RHO_CUTOFF){
                    sigma = contracted_gradient[0] -> operator[](i);
                    this -> xc_gga_ex(NULL, 1, &rho, &sigma, &(output->operator[](i)));
                }
            }
        } else {
            Verbose::all() << "XC_Custom: only LDA and GGA implemented" << std::endl;
            exit(EXIT_FAILURE);
        }
    } else if(xc_info -> all_density.size() == 2){
        std::vector<double> rho(2);
        if(xc_info -> get_functional_type() == "LDA"){
            for(int i=0; i<NumMyElements; i++){
                rho[0] = xc_info -> all_density[0] -> operator[](i);
                rho[1] = xc_info -> all_density[1] -> operator[](i);
                if(rho[0] + rho[1] > _XC_RHO_CUTOFF){
                    this -> xc_lda_ex(NULL, 1, rho.data(), &(output -> operator[](i)));
                }
            }
        } else if(xc_info -> get_functional_type() == "GGA"){
            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
            std::vector<double> sigma(3);
            //Verbose::all() << "PID " << map->Comm().MyPID() << ", " << xc_info -> all_density.size() << ", " << contracted_gradient.size() << std::endl;
            //Verbose::all() << "PID " << map->Comm().MyPID() << ", " << NumMyElements << ", " << xc_info -> all_density[1]->Map().NumMyElements() << ", " << contracted_gradient[2]->Map().NumMyElements() << ", " << output->Map().NumMyElements() << std::endl;
    //Parallel_Manager::info().all_barrier();
            for(int i=0; i<NumMyElements; ++i){
                rho[0] = xc_info -> all_density[0]->operator[](i);
                rho[1] = xc_info -> all_density[1]->operator[](i);

                if(rho[0]+rho[1] > _XC_RHO_CUTOFF){
                    sigma[0] = contracted_gradient[0] -> operator[](i);
                    sigma[1] = contracted_gradient[1] -> operator[](i);
                    sigma[2] = contracted_gradient[2] -> operator[](i);

                    this -> xc_gga_ex(NULL, 1, rho.data(), sigma.data(), &(output->operator[](i)));
                }
            }
        } else {
            Verbose::all() << "XC_Custom: only LDA and GGA implemented" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    else{
        Verbose::all() << "all_density.size() is not 1 or 2 ! VALUE : " << xc_info->all_density.size() << std::endl;
    }
//Parallel_Manager::info().all_barrier();
//        Verbose::all() << "before set ex " << map->Comm().MyPID() << std::endl;
    xc_info -> set_energy_density(output);

    return;
}

void XC_Custom::compute_vxc(RCP<XC_State> &xc_info){
    int NumMyElements = basis->get_map()->NumMyElements();
    auto map = basis->get_map();
    this -> spin_size = xc_info -> all_density.size();

    Array< RCP<Epetra_Vector> > output_potential;
    for(int s = 0; s < xc_info -> all_density.size(); ++s){
        output_potential.append(rcp(new Epetra_Vector(*map)));
    }
    Verbose::single(Verbose::Normal) << "\n#---------------------------------------- XC_Custom::compute_xc_potential_vector\n";
    Verbose::single(Verbose::Normal) << " The functional information: " << this -> get_info_string(false) << "\t" << this -> get_functional_id() << std::endl;
    if(xc_info -> all_density.size() == 1){
        if(xc_info -> get_functional_type() == "LDA"){
            double rho = 0.0;
#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime) firstprivate(rho)
#endif
            for(int i=0; i<NumMyElements; ++i){
                rho = xc_info -> all_density[0]->operator[](i);
                if(rho > _XC_RHO_CUTOFF){
                    this -> xc_lda_vx(NULL, 1, &rho, &(output_potential[0]->operator[](i)));
                }
            }
        } else if(xc_info -> get_functional_type() == "GGA"){
            RCP<Epetra_Vector> vector_ = rcp(new Epetra_Vector(*map));
            double rho = 0.0, sigma = 0.0;
            double vsigma = 0.0;

            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);

            Array< RCP<Epetra_MultiVector> > dedgd;
            Array< RCP<Epetra_Vector> > div_dedgd;
            for(int i_spin=0; i_spin<xc_info -> all_density.size(); i_spin++){
                dedgd.append(rcp(new Epetra_MultiVector(*map, 3)));
                div_dedgd.append(rcp(new Epetra_Vector(*map)));
            }

#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime) firstprivate(rho,sigma,vsigma)
#endif
            for(int i=0; i<NumMyElements; ++i){
                rho = xc_info -> all_density[0]->operator[](i);
                if(rho > _XC_RHO_CUTOFF){
                    sigma = contracted_gradient[0] -> operator[](i);
                    vsigma = 0.0;

                    this -> xc_gga_vx(NULL, 1, &rho, &sigma, &(vector_->operator[](i)), &vsigma);
                    dedgd[0]->ReplaceMyValue(i, 0, 2.0 * vsigma * xc_info -> density_grad[0]->operator[](0)[i]);
                    dedgd[0]->ReplaceMyValue(i, 1, 2.0 * vsigma * xc_info -> density_grad[0]->operator[](1)[i]);
                    dedgd[0]->ReplaceMyValue(i, 2, 2.0 * vsigma * xc_info -> density_grad[0]->operator[](2)[i]);
                }
            }
            Lagrange_Derivatives::divergence(basis, dedgd, true, div_dedgd, NGPU);
#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(runtime)
#endif
            for(int i=0; i<NumMyElements; ++i){
                output_potential[0]->SumIntoMyValue(i, 0, vector_->operator[](i) - div_dedgd[0]->operator[](i));
            }
        } else {
            Verbose::all() << "XC_Custom: only LDA and GGA implemented" << std::endl;
            exit(EXIT_FAILURE);
        }
    } else if(xc_info -> all_density.size() == 2){
        Array< RCP<Epetra_Vector> > vector_;
        for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
            vector_.append(rcp(new Epetra_Vector(*map)));
        }

        std::vector<double> rho(2,0.0);
        if(xc_info -> get_functional_type() == "LDA"){
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime) firstprivate(rho)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                rho[0] = xc_info -> all_density[0]->operator[](i);
                rho[1] = xc_info -> all_density[1]->operator[](i);
                std::vector<double> vrho(2);

                if(rho[0]+rho[1] > _XC_RHO_CUTOFF){
                    this -> xc_lda_vx(NULL, 1, rho.data(), vrho.data());

                    for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
                        vector_[i_spin]->ReplaceMyValue(i, 0, vrho[i_spin]);
                    }
                }
            }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for schedule(runtime)
    #endif
            for(int i=0; i<NumMyElements; ++i){
                output_potential[0]->SumIntoMyValue(i, 0, vector_[0]->operator[](i));
                output_potential[1]->SumIntoMyValue(i, 0, vector_[1]->operator[](i));
            }
        } else if(xc_info -> get_functional_type() == "GGA"){
            std::vector<double> sigma(3,0.0);
            std::vector<double> vrho(2,0.0);
            std::vector<double> vsigma(3,0.0);

            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);

            Array< RCP<Epetra_MultiVector> > dedgd;
            Array< RCP<Epetra_Vector> > div_dedgd;
            for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
                dedgd.append(rcp(new Epetra_MultiVector(*map, 3)));
                div_dedgd.append(rcp(new Epetra_Vector(*map)));
            }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for schedule(runtime) firstprivate(rho,vrho,sigma,vsigma)
    #endif
            for(int i=0; i<NumMyElements; ++i){
                rho[0] = xc_info -> all_density[0]->operator[](i);
                rho[1] = xc_info -> all_density[1]->operator[](i);

                if(rho[0]+rho[1] > _XC_RHO_CUTOFF){
                    sigma[0] = contracted_gradient[0] -> operator[](i);
                    sigma[1] = contracted_gradient[1] -> operator[](i);
                    sigma[2] = contracted_gradient[2] -> operator[](i);

                    vrho[0] = 0.0; vrho[1] = 0.0;
                    vsigma[0] = 0.0; vsigma[1] = 0.0; vsigma[2] = 0.0;

                    this -> xc_gga_vx(NULL, 1, rho.data(), sigma.data(), vrho.data(), vsigma.data());

                    for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
                        vector_[i_spin]->ReplaceMyValue(i, 0, vrho[i_spin]);
                    }
                    dedgd[0]->ReplaceMyValue(i, 0, 2.0 * vsigma[0] * xc_info -> density_grad[0]->operator[](0)[i] + vsigma[1] * xc_info -> density_grad[1]->operator[](0)[i]);
                    dedgd[0]->ReplaceMyValue(i, 1, 2.0 * vsigma[0] * xc_info -> density_grad[0]->operator[](1)[i] + vsigma[1] * xc_info -> density_grad[1]->operator[](1)[i]);
                    dedgd[0]->ReplaceMyValue(i, 2, 2.0 * vsigma[0] * xc_info -> density_grad[0]->operator[](2)[i] + vsigma[1] * xc_info -> density_grad[1]->operator[](2)[i]);
                    dedgd[1]->ReplaceMyValue(i, 0, 2.0 * vsigma[2] * xc_info -> density_grad[1]->operator[](0)[i] + vsigma[1] * xc_info -> density_grad[0]->operator[](0)[i]);
                    dedgd[1]->ReplaceMyValue(i, 1, 2.0 * vsigma[2] * xc_info -> density_grad[1]->operator[](1)[i] + vsigma[1] * xc_info -> density_grad[0]->operator[](1)[i]);
                    dedgd[1]->ReplaceMyValue(i, 2, 2.0 * vsigma[2] * xc_info -> density_grad[1]->operator[](2)[i] + vsigma[1] * xc_info -> density_grad[0]->operator[](2)[i]);
                }
            }

            Lagrange_Derivatives::divergence(basis, dedgd, true, div_dedgd, NGPU);

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for schedule(runtime)
    #endif
            for(int i=0; i<NumMyElements; ++i){
                output_potential[0]->SumIntoMyValue(i, 0, vector_[0]->operator[](i) - div_dedgd[0]->operator[](i));
                output_potential[1]->SumIntoMyValue(i, 0, vector_[1]->operator[](i) - div_dedgd[1]->operator[](i));
            }
        } else {
            Verbose::all() << "XC_Custom: only LDA and GGA implemented" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    else{
        Verbose::all() << "all_density.size() is not 1 or 2 ! VALUE : " << xc_info->all_density.size() << std::endl;
    }
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
    xc_info -> set_potential(output_potential);
    return;
}

void XC_Custom::compute_kernel(RCP<XC_State> &xc_info){
    int NumMyElements = basis->get_map()->NumMyElements();
    auto map = basis->get_map();
    this -> spin_size = xc_info -> all_density.size();

    Verbose::single(Verbose::Normal) << "\n#---------------------------------------- XC_Custom::compute_kernel\n";
    Verbose::single(Verbose::Normal) << " The functional information: " << this -> get_info_string(false) << "\t" << this -> get_functional_id() << std::endl;
    if(xc_info -> all_density.size() == 1){
        if(xc_info -> get_functional_type() == "LDA"){
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc;
            fxc.push_back(rcp(new Epetra_Vector(*map)));

            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                double rho = xc_info -> all_density[0] -> operator[](i);
                if(rho > _XC_RHO_CUTOFF){
                    this -> xc_lda_fx(NULL, 1, &rho, &(fxc[0]->operator[](i)));
                }
            }
            xc_info -> set_lda_kernel(fxc);
        } else if(xc_info -> get_functional_type() == "GGA"){
            Array< RCP<Epetra_Vector> > vsigma;
            Array< RCP<Epetra_Vector> > v2rho2;
            Array< RCP<Epetra_Vector> > v2rhosigma;
            Array< RCP<Epetra_Vector> > v2sigma2;
            vsigma.push_back(rcp(new Epetra_Vector(*map)));
            v2rho2.push_back(rcp(new Epetra_Vector(*map)));
            v2rhosigma.push_back(rcp(new Epetra_Vector(*map)));
            v2sigma2.push_back(rcp(new Epetra_Vector(*map)));

            double rho = 0.0, sigma = 0.0;
            double exc = 0.0, vrho = 0.0;

            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for schedule(runtime) firstprivate(rho,vrho,sigma,exc)
    #endif
            for(int i=0; i<NumMyElements; ++i){
                rho = xc_info -> all_density[0]->operator[](i);
                if(rho > _XC_RHO_CUTOFF){
                    sigma = contracted_gradient[0] -> operator[](i);
                    exc = 0.0, vrho = 0.0;

                    this -> xc_gga_x(NULL, 1, &rho, &sigma, &exc, &vrho, &(vsigma[0]->operator[](i)), &(v2rho2[0]->operator[](i)), &(v2rhosigma[0]->operator[](i)), &(v2sigma2[0]->operator[](i)), NULL, NULL, NULL, NULL);
                }
            }
            xc_info -> set_gga_kernel(vsigma, v2rho2, v2rhosigma, v2sigma2);
        } else {
            Verbose::all() << "XC_Custom: only LDA and GGA implemented" << std::endl;
            exit(EXIT_FAILURE);
        }
    } else if(xc_info -> all_density.size() == 2){
        std::vector<double> rho(2,0.0);
        if(xc_info -> get_functional_type() == "LDA"){
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc;
            fxc.push_back(rcp(new Epetra_Vector(*map)));
            fxc.push_back(rcp(new Epetra_Vector(*map)));
            fxc.push_back(rcp(new Epetra_Vector(*map)));
            std::vector<double> kernel(3, 0.0);

            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                rho[0] = xc_info -> all_density[0] -> operator[](i);
                rho[1] = xc_info -> all_density[1] -> operator[](i);
                if(rho[0]+rho[1] > _XC_RHO_CUTOFF){
                    kernel[0] = 0.0; kernel[1] = 0.0; kernel[2] = 0.0;
                    this -> xc_lda_fx(NULL, 1, rho.data(), kernel.data());
                    fxc[0] -> ReplaceMyValue(i, 0, kernel[0]);
                    fxc[1] -> ReplaceMyValue(i, 0, kernel[1]);
                    fxc[2] -> ReplaceMyValue(i, 0, kernel[2]);
                }
            }
            xc_info -> set_lda_kernel(fxc);
        } else if(xc_info -> get_functional_type() == "GGA"){
            std::vector<double> sigma(3,0.0);
            std::vector<double> exc(2,0.0);
            std::vector<double> vrho(2,0.0);
            std::vector<double> vsigma_tmp(3,0.0);
            std::vector<double> v2rho2_tmp(3,0.0);
            std::vector<double> v2rhosigma_tmp(6,0.0);
            std::vector<double> v2sigma2_tmp(6,0.0);

            Array< RCP<Epetra_Vector> > vsigma;
            Array< RCP<Epetra_Vector> > v2rho2;
            Array< RCP<Epetra_Vector> > v2rhosigma;
            Array< RCP<Epetra_Vector> > v2sigma2;
            for(int s = 0; s < 3; ++s){
                vsigma.push_back(rcp(new Epetra_Vector(*map)));
                v2rho2.push_back(rcp(new Epetra_Vector(*map)));
            }
            for(int s = 0; s < 6; ++s){
                v2rhosigma.push_back(rcp(new Epetra_Vector(*map)));
                v2sigma2.push_back(rcp(new Epetra_Vector(*map)));
            }

            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);

            Array< RCP<Epetra_MultiVector> > dedgd;
            Array< RCP<Epetra_Vector> > div_dedgd;
            //Array< RCP<Epetra_Vector> > vector;
            for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
                dedgd.append(rcp(new Epetra_MultiVector(*map, 3)));
                div_dedgd.append(rcp(new Epetra_Vector(*map)));
                //vector.append(rcp(new Epetra_Vector(*map)));
            }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for schedule(runtime)
    #endif
            for(int i=0; i<NumMyElements; ++i){
                rho[0] = xc_info -> all_density[0]->operator[](i);
                rho[1] = xc_info -> all_density[1]->operator[](i);

                if(rho[0] + rho[1] > _XC_RHO_CUTOFF){
                    sigma[0] = contracted_gradient[0] -> operator[](i);
                    sigma[1] = contracted_gradient[1] -> operator[](i);
                    sigma[2] = contracted_gradient[2] -> operator[](i);

                    this -> xc_gga_x(NULL, 1, rho.data(), sigma.data(), exc.data(), vrho.data(), vsigma_tmp.data(), v2rho2_tmp.data(), v2rhosigma_tmp.data(), v2sigma2_tmp.data(), NULL, NULL, NULL, NULL);
                    for(int s = 0; s < 3; ++s){
                        vsigma[s] -> ReplaceMyValue(i, 0, vsigma_tmp[s]);
                        v2rho2[s] -> ReplaceMyValue(i, 0, v2rho2_tmp[s]);
                    }
                    for(int s = 0; s < 6; ++s){
                        v2rhosigma[s] -> ReplaceMyValue(i, 0, v2rhosigma_tmp[s]);
                        v2sigma2[s] -> ReplaceMyValue(i, 0, v2sigma2_tmp[s]);
                    }
                }
            }
            xc_info -> set_gga_kernel(vsigma, v2rho2, v2rhosigma, v2sigma2);
        } else {
            Verbose::all() << "XC_Custom: only LDA and GGA implemented" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    else{
        Verbose::all() << "all_density.size() is not 1 or 2 ! VALUE : " << xc_info->all_density.size() << std::endl;
    }
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
    return;
}

void XC_Custom::xc_gga_ex(void *dummy, int np, double *rho, double *sigma, double *ex){
    this -> xc_gga_x(NULL, np, rho, sigma, ex, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
}

void XC_Custom::xc_gga_vx(void *dummy, int np, double *rho, double *sigma, double *vrho, double *vsigma){
    this -> xc_gga_x(NULL, np, rho, sigma, NULL, vrho, vsigma, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
}

void XC_Custom::xc_lda_ex(void *dummy, int np, double *rho, double *ex){
    this -> xc_lda_x(NULL, np, rho, ex, NULL, NULL, NULL);
}

void XC_Custom::xc_lda_vx(void *dummy, int np, double *rho, double *vrho){
    this -> xc_lda_x(NULL, np, rho, NULL, vrho, NULL, NULL);
}

void XC_Custom::xc_lda_fx(void *dummy, int np, double *rho, double *v2rho2){
    this -> xc_lda_x(NULL, np, rho, NULL, NULL, v2rho2, NULL);
}
