#pragma once 
#include "Scf.hpp"
#include "Create_Compute.hpp"
#include "Compute_Interface.hpp"

class Sp: public Compute_Interface{
    public:
        Sp(RCP<const Basis> basis, RCP<Atoms> atoms, RCP<ParameterList> parameters);
        int compute(RCP<const Basis> basis,Array<RCP<State> >& states);

    protected:
        RCP<Guess> guess;
        RCP<Scf> scf;
        RCP<const Atoms> atoms;

};

