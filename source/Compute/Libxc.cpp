#include "Libxc.hpp"
#include <stdexcept>
#include <cmath>

#include "../Utility/Calculus/Lagrange_Derivatives.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/String_Util.hpp"

using std::abs;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

// mGGA exchange-correlation potential CANNOT be computed.
// mGGA exchange-correlation energy can be computed.
// mGGA exchange-correlation energy with NLCC CANNOT be computed.
/*
 * NOTE on LIBXC index:
 * For UNPOLARIZED case, it is simply rho[i], sigma[i] where i is point index, and so on.
 * For POLARIZED case, rho[2*i+is], sigma[3*i+is] where is is spin index, and so on.
 * POLARIZED spin index list:
 * - LDA
 *   rho, vxc(2) = (u, d)
 *   fxc(3)      = (uu, ud, dd)
 *   kxc(4)      = (uuu, uud, udd, ddd)
 * - GGA
 *   rho, vrho(2)     = (u, d)
 *   sigma, vsigma(3) = (uu, ud, dd)
 *
 *   v2rho2(3)        = (u_u, u_d, d_d)
 *   v2rhosigma(6)    = (u_uu, u_ud, u_dd, d_uu, d_ud, d_dd)
 *   v2sigma2(6)      = (uu_uu, uu_ud, uu_dd, ud_ud, ud_dd, dd_dd)
 *
 *   v3rho3(4)        = (u_u_u, u_u_d, u_d_d, d_d_d)
 *   v3rho2sigma(9)   = (u_u_uu, u_u_ud, u_u_dd, u_d_uu, u_d_ud, u_d_dd, d_d_uu, d_d_ud, d_d_dd)
 *   v3rhosigma2(12)  = (u_uu_uu, u_uu_ud, u_uu_dd, u_ud_ud, u_ud_dd, u_dd_dd, d_uu_uu, d_uu_ud, d_uu_dd,     d_ud_ud, d_ud_dd, d_dd_dd)
 *   v3sigma(10)      = (uu_uu_uu, uu_uu_ud, uu_uu_dd, uu_ud_ud, uu_ud_dd, uu_dd_dd, ud_ud_ud, ud_ud_dd,     ud_dd_dd, dd_dd_dd)
 *
 */

Libxc::Libxc(RCP<const Basis> basis, int function_id, int NGPU, double cutoff): Compute_XC(basis, function_id), NGPU(NGPU), cutoff(cutoff)
{
    if(this -> cutoff < 0.0){
        this -> cutoff = 1.0E-10;
    }
    Verbose::single(Verbose::Normal) << "Libxc::Libxc - XC Functional was set.\n";
}

/*
int Libxc::compute(RCP<const Basis> basis, Array<RCP<State> >& states){
    if(states.size()==0){
        Verbose::single() << "No given density!\n";
        exit(EXIT_FAILURE);
    }
    states.append(rcp(new State(*states[states.size()-1])));

    this->basis = basis;

    auto state = states[states.size()-1];
    auto map = basis->get_map();

    Array< RCP<Epetra_Vector> > exchange_potential;
    Array< RCP<Epetra_Vector> > correlation_potential;
    double exchange_energy = 0.0, correlation_energy = 0.0;

    for(int i_spin=0; i_spin<state->occupations.size(); i_spin++){
        exchange_potential.push_back(rcp(new Epetra_Vector(*map)));
        correlation_potential.push_back(rcp(new Epetra_Vector(*map)));
    }

    RCP<XC_State> xc_info = rcp(new XC_State(this -> function_id));
    xc_info -> initialize(basis, states[states.size()-1]);

    compute_vxc(xc_info);
    compute_Exc(xc_info);

    for(int i_spin=0; i_spin<exchange_potential.size(); i_spin++){
        state->local_potential[i_spin]->Update(1.0,*xc_info -> get_potential()[i_spin],1.0);
    }
    state->total_energy += xc_info -> get_energy();

    return 0;
}

int Libxc::compute(RCP<const Basis> basis, Array<RCP<Scf_State> >& states){

    if(states.size()==0){
        Verbose::single() << "No given density!\n";
        exit(EXIT_FAILURE);
    }
    states.append(rcp(new Scf_State(*states[states.size()-1])));

    this->basis = basis;

    auto state = states[states.size()-1];
    auto map = basis->get_map();

    RCP<XC_State> xc_info = rcp(new XC_State(this -> function_id));
    xc_info -> initialize(basis, state);
    compute_vxc(xc_info);
    compute_Exc(xc_info);

    xc_func_type func;
    xc_func_init(&func, xc_info -> xc_id, XC_UNPOLARIZED);
    if(func.info->kind == XC_EXCHANGE){
        state->x_potential = xc_info -> get_potential();
        state->x_energy = xc_info -> get_energy();
    }else if(func.info->kind == XC_CORRELATION){
        state->c_potential = xc_info -> get_potential();
        state->c_energy = xc_info -> get_energy();
    }else if(func.info->kind == XC_EXCHANGE_CORRELATION){
        state->x_potential = xc_info -> get_potential();
        state->x_energy = xc_info -> get_energy();
        state->c_potential.clear();
        for(int s = 0; s < state -> get_occupations().size(); ++s){
            state->c_potential.append(rcp(new Epetra_Vector(*map)));
        }
        state->c_energy = 0.0;
    }else if(func.info->kind == XC_KINETIC){
        std::cout << "XC_KINETIC not implemented" << std::endl;
        exit(EXIT_FAILURE);
    }else{
        std::cout << "Unknown func.info->kind " << func.info->kind << std::endl;
        exit(EXIT_FAILURE);
    }

    return 0;
}
*/
int Libxc::compute(RCP<XC_State>& xc_info){
    compute_vxc(xc_info);
    compute_Exc(xc_info);

    return 0;
}

void Libxc::compute_Exc(RCP<XC_State> &xc_info){
    RCP<const Epetra_Map> map= basis->get_map();
    double scaling = this -> basis -> get_scaling()[0] * this -> basis -> get_scaling()[1] * this -> basis -> get_scaling()[2];

    double energy = 0.0, total_energy = 0.0;

    this -> compute_exc(xc_info);
    RCP<const Epetra_Vector> energy_density = xc_info -> get_energy_density();

    for(int i_spin=0; i_spin<xc_info -> all_density.size(); i_spin++){
        xc_info -> all_density[i_spin] -> Dot(*energy_density, &energy);
        total_energy += energy;
    }
    total_energy *= scaling;


    xc_info -> set_energy(total_energy);
    return;
}

void Libxc::compute_exc(RCP<XC_State> &xc_info){
    auto map = basis->get_map();
    int NumMyElements = map->NumMyElements();

    RCP<Epetra_Vector> vector = rcp(new Epetra_Vector(*map));

    if(xc_info -> all_density.size() == 1){
        xc_func_init(&func, xc_info -> get_xc_id(), XC_UNPOLARIZED);
        xc_func_set_dens_threshold(&func, this -> cutoff);
        /*
        if(xc_info -> get_xc_id() == XC_GGA_X_WPBEH){
            XC(gga_x_wpbeh_set_params)(&func, xc_info->LRC_omega);
        }
        */

        switch(func.info->family){
        case XC_FAMILY_LDA:{
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime)
            #endif
            for(int i=0; i<NumMyElements; i++){
                xc_lda_exc(&func, 1, &(xc_info -> all_density[0]->operator[](i)), &(vector->operator[](i)));
            }
        } break;

        case XC_FAMILY_GGA:
        case XC_FAMILY_HYB_GGA:{
            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
            double rho = 0.0, sigma = 0.0;

            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime) private(rho, sigma)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                rho = xc_info -> all_density[0]->operator[](i);
                sigma = contracted_gradient[0] -> operator[](i);

                xc_gga_exc(&func, 1, &rho, &sigma, &(vector->operator[](i)));
            }
        } break;

        case XC_FAMILY_MGGA:{
            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
            double rho = 0.0, sigma = 0.0, lapl_rho = 0.0, tau = 0.0;

            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime) private(rho, sigma, lapl_rho, tau)
            #endif
            for(int i=0;i<NumMyElements;++i){
                rho = xc_info -> all_density[0]->operator[](i);
                sigma = contracted_gradient[0]->operator[](i);
                lapl_rho = xc_info -> density_laplacian[0]->operator[](i);
                tau = xc_info -> kinetic_energy_density[0]->operator[](i);

                xc_mgga_exc(&func, 1, &rho, &sigma, &lapl_rho, &tau, &(vector->operator[](i)));
            }
        } break;
        }
    }
    else if(xc_info -> all_density.size() == 2){
        xc_func_init(&func, xc_info -> get_xc_id(), XC_POLARIZED);
        xc_func_set_dens_threshold(&func, this -> cutoff);
        /*
        if(xc_info -> get_xc_id() == XC_GGA_X_WPBEH){
            XC(gga_x_wpbeh_set_params)(&func, xc_info->LRC_omega);
        }
        */
        Verbose::single(Verbose::Normal) << "Libxc::compute_xc_energy_density - The functional information: " << func.info->name << "\t" << func.info->number << std::endl;

        switch(func.info->family){
        case XC_FAMILY_LDA:{
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                double* rho = new double [2];
                rho[0] = xc_info -> all_density[0]->operator[](i);
                rho[1] = xc_info -> all_density[1]->operator[](i);

                xc_lda_exc(&func, 1, rho, &(vector->operator[](i)));
                delete[] rho;
            }
        } break;

        case XC_FAMILY_GGA:
        case XC_FAMILY_HYB_GGA:{
            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                double* rho = new double [2];
                double* sigma = new double [3];
                rho[0] = xc_info -> all_density[0]->operator[](i);
                rho[1] = xc_info -> all_density[1]->operator[](i);

                sigma[0] = contracted_gradient[0] -> operator[](i);
                sigma[1] = contracted_gradient[1] -> operator[](i);
                sigma[2] = contracted_gradient[2] -> operator[](i);

                xc_gga_exc(&func, 1, rho, sigma, &(vector->operator[](i)));
                delete[] rho;
                delete[] sigma;
            }
        } break;

        case XC_FAMILY_MGGA:{
            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                double* rho = new double [2];
                double* sigma = new double [3];
                double* lapl_rho = new double [2];
                double* tau = new double [2];
                rho[0] = xc_info -> all_density[0]->operator[](i);
                rho[1] = xc_info -> all_density[1]->operator[](i);

                sigma[0] = contracted_gradient[0] -> operator[](i);
                sigma[1] = contracted_gradient[1] -> operator[](i);
                sigma[2] = contracted_gradient[2] -> operator[](i);

                lapl_rho[0] = xc_info -> density_laplacian[0]->operator[](i);
                lapl_rho[1] = xc_info -> density_laplacian[1]->operator[](i);
                tau[0] = xc_info -> kinetic_energy_density[0]->operator[](i);
                tau[1] = xc_info -> kinetic_energy_density[1]->operator[](i);

                xc_mgga_exc(&func, 1, rho, sigma, lapl_rho, tau, &(vector->operator[](i)));
                delete[] rho;
                delete[] sigma;
                delete[] lapl_rho;
                delete[] tau;
            }
        } break;
        } // end switch
    }else{
        Verbose::all() << "XC_State illegal density size " << xc_info -> all_density.size() << std::endl;
        throw std::invalid_argument("Illegal density size detected in XC calculation " + String_Util::to_string(static_cast<int>(xc_info -> all_density.size())));
    }

    xc_func_end(&func);
    xc_info -> set_energy_density(vector);

    return;
}

void Libxc::compute_vxc(RCP<XC_State> &xc_info){
    int NumMyElements = basis->get_map()->NumMyElements();
    auto map = basis->get_map();
    internal_timer->start("compute_vxc::init");

    Array< RCP<Epetra_Vector> > output_potential;
    for(int s = 0; s < xc_info -> all_density.size(); ++s){
        output_potential.append(rcp(new Epetra_Vector(*map)));
    }
    internal_timer->end("compute_vxc::init");
    if(xc_info -> all_density.size() == 1){
        xc_func_init(&func, xc_info -> get_xc_id(), XC_UNPOLARIZED);
        xc_func_set_dens_threshold(&func, this -> cutoff);
        /*
        if(xc_info -> get_xc_id() == XC_GGA_X_WPBEH){
            XC(gga_x_wpbeh_set_params)(&func, xc_info->LRC_omega);
        }
        */

        Verbose::single(Verbose::Normal) << "\n#---------------------------------------- Libxc::compute_xc_potential_vector\n";
        Verbose::single(Verbose::Normal) << " The functional information: " << func.info->name << "\t" << func.info->number << "\n";

        switch(func.info->family){

            case XC_FAMILY_LDA:{
                double vxc = 0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime) private(vxc)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    vxc = 0.0;
                    xc_lda_vxc(&func, 1, &(xc_info -> all_density[0]->operator[](i)), &vxc);
                    output_potential[0]->ReplaceMyValue(i, 0, vxc);
                }
            } break;

            case XC_FAMILY_GGA:
            case XC_FAMILY_HYB_GGA:{
                //internal_timer->start("XC_FAMILY_GGA1");
                double rho = 0.0, sigma = 0.0;
                double vsigma = 0.0;

                Array< RCP<Epetra_Vector> > contracted_gradient;
                this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);

                //internal_timer->end("XC_FAMILY_GGA1");
                //internal_timer->start("XC_FAMILY_GGA2");
                Array< RCP<Epetra_MultiVector> > dedgd;
                Array< RCP<Epetra_Vector> > div_dedgd;
                for(int i_spin=0; i_spin<xc_info -> all_density.size(); i_spin++){
                    dedgd.append(rcp(new Epetra_MultiVector(*map, 3)));
                    div_dedgd.append(rcp(new Epetra_Vector(*map)));
                }
                //internal_timer->end("XC_FAMILY_GGA2");
                //internal_timer->start("XC_FAMILY_GGA3");

                RCP<Epetra_Vector> vector = rcp(new Epetra_Vector(*map));
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime) firstprivate(rho,sigma,vsigma)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    rho = xc_info -> all_density[0]->operator[](i);
                    sigma = contracted_gradient[0] -> operator[](i);
                    vsigma = 0.0;

                    xc_gga_vxc(&func, 1, &rho, &sigma, &(vector->operator[](i)), &vsigma);
                    dedgd[0]->ReplaceMyValue(i, 0, 2.0 * vsigma * xc_info -> density_grad[0]->operator[](0)[i]);
                    dedgd[0]->ReplaceMyValue(i, 1, 2.0 * vsigma * xc_info -> density_grad[0]->operator[](1)[i]);
                    dedgd[0]->ReplaceMyValue(i, 2, 2.0 * vsigma * xc_info -> density_grad[0]->operator[](2)[i]);
                }
                /*
                double norm;
                vector -> Norm2(&norm);
                Verbose::single() << "VXC vector norm2 = " << norm << std::endl;
                vector -> NormInf(&norm);
                Verbose::single() << "VXC vector normInf = " << norm << std::endl;
                for(int i = 0; i < NumMyElements; ++i){
                    std::cout << vector -> Map().GID(i) << ": " << xc_info -> all_density[0] -> operator[](i) << ", " << contracted_gradient[0] -> operator[](i) << " = " << vector -> operator[](i) << std::endl;
                }
                // */
                //internal_timer->end("XC_FAMILY_GGA3");


                //internal_timer->start("XC_FAMILY_GGA4");
                Lagrange_Derivatives::divergence(basis, dedgd, true, div_dedgd,NGPU);
                //internal_timer->end("XC_FAMILY_GGA4");
                //internal_timer->start("XC_FAMILY_GGA5");

                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    output_potential[0]->ReplaceMyValue(i, 0, vector->operator[](i) - div_dedgd[0]->operator[](i));
                }
                //internal_timer->end("XC_FAMILY_GGA5");
            } break;

            case XC_FAMILY_MGGA:{
                Verbose::all() << "Libxc::compute_xc_potential_vector - mGGA is not implemented.\n";
                throw std::logic_error("Libxc::compute_xc_potential_vector - mGGA is not implemented.");
            } break;
        }
    }
    else if(xc_info -> all_density.size() == 2){
        xc_func_init(&func, xc_info -> get_xc_id(), XC_POLARIZED);
        xc_func_set_dens_threshold(&func, this -> cutoff);
        /*
        if(xc_info -> get_xc_id() == XC_GGA_X_WPBEH){
            XC(gga_x_wpbeh_set_params)(&func, xc_info->LRC_omega);
        }
        */

        Verbose::single(Verbose::Normal) << "\n#---------------------------------------- Libxc::compute_xc_potential_vector\n";
        Verbose::single(Verbose::Normal) << " The functional information: " << func.info->name << "\t" << func.info->number << "\n";

        switch(func.info->family){
            case XC_FAMILY_LDA:{
                std::vector<double> rho(2,0.0);
                std::vector<double> vrho(2,0.0);
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime) firstprivate(rho,vrho)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    rho[0] = xc_info -> all_density[0]->operator[](i);
                    rho[1] = xc_info -> all_density[1]->operator[](i);

                    vrho[0] = 0.0; vrho[1] = 0.0;

                    xc_lda_vxc(&func, 1, rho.data(), vrho.data());
                    output_potential[0]->ReplaceMyValue(i, 0, vrho[0]);
                    output_potential[1]->ReplaceMyValue(i, 0, vrho[1]);
                }
            } break;

            case XC_FAMILY_GGA:
            case XC_FAMILY_HYB_GGA:{
                std::vector<double> rho(2,0.0);
                std::vector<double> sigma(3,0.0);
                std::vector<double> vrho(2,0.0);
                std::vector<double> vsigma(3,0.0);

                Array< RCP<Epetra_Vector> > contracted_gradient;
                this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);

                Array< RCP<Epetra_MultiVector> > dedgd;
                Array< RCP<Epetra_Vector> > div_dedgd;
                Array< RCP<Epetra_Vector> > vector;
                for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
                    dedgd.append(rcp(new Epetra_MultiVector(*map, 3)));
                    div_dedgd.append(rcp(new Epetra_Vector(*map)));
                    vector.append(rcp(new Epetra_Vector(*map)));
                }

                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime) firstprivate(rho,vrho,sigma,vsigma)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    rho[0] = xc_info -> all_density[0]->operator[](i);
                    rho[1] = xc_info -> all_density[1]->operator[](i);

                    sigma[0] = contracted_gradient[0] -> operator[](i);
                    sigma[1] = contracted_gradient[1] -> operator[](i);
                    sigma[2] = contracted_gradient[2] -> operator[](i);

                    vrho[0] = 0.0; vrho[1] = 0.0;
                    vsigma[0] = 0.0; vsigma[1] = 0.0; vsigma[2] = 0.0;

                    xc_gga_vxc(&func, 1, rho.data(), sigma.data(), vrho.data(), vsigma.data());

                    for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
                        vector[i_spin]->ReplaceMyValue(i, 0, vrho[i_spin]);
                    }
                    dedgd[0]->ReplaceMyValue(i, 0, 2.0 * vsigma[0] * xc_info -> density_grad[0]->operator[](0)[i] + vsigma[1] * xc_info -> density_grad[1]->operator[](0)[i]);
                    dedgd[0]->ReplaceMyValue(i, 1, 2.0 * vsigma[0] * xc_info -> density_grad[0]->operator[](1)[i] + vsigma[1] * xc_info -> density_grad[1]->operator[](1)[i]);
                    dedgd[0]->ReplaceMyValue(i, 2, 2.0 * vsigma[0] * xc_info -> density_grad[0]->operator[](2)[i] + vsigma[1] * xc_info -> density_grad[1]->operator[](2)[i]);
                    dedgd[1]->ReplaceMyValue(i, 0, 2.0 * vsigma[2] * xc_info -> density_grad[1]->operator[](0)[i] + vsigma[1] * xc_info -> density_grad[0]->operator[](0)[i]);
                    dedgd[1]->ReplaceMyValue(i, 1, 2.0 * vsigma[2] * xc_info -> density_grad[1]->operator[](1)[i] + vsigma[1] * xc_info -> density_grad[0]->operator[](1)[i]);
                    dedgd[1]->ReplaceMyValue(i, 2, 2.0 * vsigma[2] * xc_info -> density_grad[1]->operator[](2)[i] + vsigma[1] * xc_info -> density_grad[0]->operator[](2)[i]);
                }

                Lagrange_Derivatives::divergence(basis, dedgd, true, div_dedgd, NGPU);

                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    output_potential[0]->ReplaceMyValue(i, 0, vector[0]->operator[](i) - div_dedgd[0]->operator[](i));
                    output_potential[1]->ReplaceMyValue(i, 0, vector[1]->operator[](i) - div_dedgd[1]->operator[](i));
                }
            } break;

            case XC_FAMILY_MGGA:{
                Verbose::all() << "Libxc::compute_xc_potential_vector - mGGA is not implemented.\n";
                throw std::logic_error("Libxc::compute_xc_potential_vector - mGGA is not implemented.");
            } break;
        } // end switch
    }else{
        Verbose::all() << "XC_State illegal density size " << xc_info -> all_density.size() << std::endl;
        throw std::invalid_argument("Illegal density size detected in XC calculation " + String_Util::to_string(static_cast<int>(xc_info -> all_density.size())));
    }

    internal_timer->start("compute_vxc postprocessing");
    xc_func_end(&func);
    xc_info -> set_potential(output_potential);
    internal_timer->end("compute_vxc postprocessing");

    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
    internal_timer->print(Verbose::single(Verbose::Normal));

    return;
}

void Libxc::compute_kernel(RCP<XC_State> &xc_info){
    int NumMyElements = basis->get_map()->NumMyElements();
    auto map = basis->get_map();

    if(xc_info -> all_density.size() == 1){
        xc_func_init(&func, xc_info -> get_xc_id(), XC_UNPOLARIZED);
        xc_func_set_dens_threshold(&func, this -> cutoff);

        Verbose::single(Verbose::Normal) << "\n#---------------------------------------- Libxc::compute_kernel\n";
        Verbose::single(Verbose::Normal) << " The functional information: " << func.info->name << "\t" << func.info->number << "\n";

        switch(func.info->family){
        case XC_FAMILY_LDA:{
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc;
            fxc.push_back(rcp(new Epetra_Vector(*map)));

            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                xc_lda_fxc(&func, 1, &(xc_info -> all_density[0]->operator[](i)), &(fxc[0]->operator[](i)));
            }

            xc_info -> set_lda_kernel(fxc);
        } break;

        case XC_FAMILY_GGA:
        case XC_FAMILY_HYB_GGA:{
            Array< RCP<Epetra_Vector> > vsigma;
            Array< RCP<Epetra_Vector> > v2rho2;
            Array< RCP<Epetra_Vector> > v2rhosigma;
            Array< RCP<Epetra_Vector> > v2sigma2;
            vsigma.push_back(rcp(new Epetra_Vector(*map)));
            v2rho2.push_back(rcp(new Epetra_Vector(*map)));
            v2rhosigma.push_back(rcp(new Epetra_Vector(*map)));
            v2sigma2.push_back(rcp(new Epetra_Vector(*map)));

            double rho = 0.0, sigma = 0.0;
            double exc = 0.0, vrho = 0.0;

            Array< RCP<Epetra_Vector> > contracted_gradient;
            this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for schedule(runtime) firstprivate(rho,vrho,sigma,exc)
            #endif
            for(int i=0; i<NumMyElements; ++i){
                rho = xc_info -> all_density[0]->operator[](i);
                sigma = contracted_gradient[0] -> operator[](i);
                exc = 0.0, vrho = 0.0;

                // Important note: do not enter NULL in place of &vrho. It skips vsigma calculations.
                xc_gga(&func, 1, &rho, &sigma, &exc, &vrho, &(vsigma[0]->operator[](i)), &(v2rho2[0]->operator[](i)), &(v2rhosigma[0]->operator[](i)), &(v2sigma2[0]->operator[](i)), NULL, NULL, NULL, NULL);
                //xc_gga_vxc(&func, 1, &rho, &sigma, &vrho, &(vsigma[0]->operator[](i)));
                //xc_gga_fxc(&func, 1, &rho, &sigma, &(v2rho2[0]->operator[](i)), &(v2rhosigma[0]->operator[](i)), &(v2sigma2[0]->operator[](i)));
                /*
                if(!std::isfinite(v2rho2[0] -> operator[](i))){
                    std::cout << basis -> get_map() -> GID(i) << " gives v2rho2 = " << v2rho2[0] -> operator[](i) << " with " << xc_info -> all_density[0] -> operator[](i) << " and " << contracted_gradient[0] -> operator[](i) << std::endl;
                }
                // */
            }
            xc_info -> set_gga_kernel(vsigma, v2rho2, v2rhosigma, v2sigma2);
        } break;
            case XC_FAMILY_MGGA:{
                Verbose::all() << "Libxc::compute_kernel - mGGA is not implemented.\n";
                throw std::logic_error("Libxc::compute_kernel - mGGA is not implemented.");
            } break;
        } // end switch
    }
    else if(xc_info -> all_density.size() == 2){
        xc_func_init(&func, xc_info -> get_xc_id(), XC_POLARIZED);
        xc_func_set_dens_threshold(&func, this -> cutoff);

        Verbose::single(Verbose::Normal) << "\n#---------------------------------------- Libxc::compute_kernel_vector\n";
        Verbose::single(Verbose::Normal) << " The functional information: " << func.info->name << "\t" << func.info->number << "\n";

        switch(func.info->family){
            case XC_FAMILY_LDA:{
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc;
                fxc.push_back(rcp(new Epetra_Vector(*map)));
                fxc.push_back(rcp(new Epetra_Vector(*map)));
                fxc.push_back(rcp(new Epetra_Vector(*map)));

                double rho[2];
                double kernel[3];

                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    rho[0] = xc_info -> all_density[0] -> operator[](i);
                    rho[1] = xc_info -> all_density[1] -> operator[](i);
                    kernel[0] = 0.0; kernel[1] = 0.0; kernel[2] = 0.0;
                    xc_lda_fxc(&func, 1, rho, kernel);
                    fxc[0] -> ReplaceMyValue(i, 0, kernel[0]);
                    fxc[1] -> ReplaceMyValue(i, 0, kernel[1]);
                    fxc[2] -> ReplaceMyValue(i, 0, kernel[2]);
                }
                xc_info -> set_lda_kernel(fxc);
            } break;

            case XC_FAMILY_GGA:
            case XC_FAMILY_HYB_GGA:{

                Array< RCP<Epetra_Vector> > vsigma;
                Array< RCP<Epetra_Vector> > v2rho2;
                Array< RCP<Epetra_Vector> > v2rhosigma;
                Array< RCP<Epetra_Vector> > v2sigma2;
                for(int s = 0; s < 3; ++s){
                    vsigma.push_back(rcp(new Epetra_Vector(*map)));
                    v2rho2.push_back(rcp(new Epetra_Vector(*map)));
                }
                for(int s = 0; s < 6; ++s){
                    v2rhosigma.push_back(rcp(new Epetra_Vector(*map)));
                    v2sigma2.push_back(rcp(new Epetra_Vector(*map)));
                }

                Array< RCP<Epetra_Vector> > contracted_gradient;
                this -> contract_gradient(xc_info -> get_density_gradient(), contracted_gradient);

                Array< RCP<Epetra_MultiVector> > dedgd;
                Array< RCP<Epetra_Vector> > div_dedgd;
                Array< RCP<Epetra_Vector> > vector;
                for(int i_spin=0; i_spin<xc_info -> all_density.size(); ++i_spin){
                    dedgd.append(rcp(new Epetra_MultiVector(*map, 3)));
                    div_dedgd.append(rcp(new Epetra_Vector(*map)));
                    vector.append(rcp(new Epetra_Vector(*map)));
                }

                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for schedule(runtime)
                #endif
                for(int i=0; i<NumMyElements; ++i){
                    std::vector<double> rho(2);
                    std::vector<double> sigma(3);
                    rho[0] = xc_info -> all_density[0]->operator[](i);
                    rho[1] = xc_info -> all_density[1]->operator[](i);

                    sigma[0] = contracted_gradient[0] -> operator[](i);
                    sigma[1] = contracted_gradient[1] -> operator[](i);
                    sigma[2] = contracted_gradient[2] -> operator[](i);

                    std::vector<double> vrho(2);
                    std::vector<double> exc(2);
                    std::vector<double> vsigma_tmp(3);// v/sigma_aa, v/sigma_ab, v/sigma_bb
                    std::vector<double> v2rho2_tmp(3);// v2/rho_a2, v2/rho_a rho_b, v2/rho_b2
                    std::vector<double> v2rhosigma_tmp(6);// v2/rho_a sigma_aa, v2/rho_a sigma_ab, v2/rho_a sigma_bb, v2/rho_b sigma_aa, v2/rho_b sigma_ab, v2/rho_b sigma_bb
                    std::vector<double> v2sigma2_tmp(6);// v2/sigma_aa2, v2/sigma_aa sigma_ab, v2/sigma_aa sigma_bb, v2/sigma_ab2, v2/sigma_ab sigma_bb, v2/sigma_bb

                    // Important note: do not enter NULL in place of &vrho. It skips vsigma calculations.
                    xc_gga(&func, 1, rho.data(), sigma.data(), exc.data(), vrho.data(), vsigma_tmp.data(), v2rho2_tmp.data(), v2rhosigma_tmp.data(), v2sigma2_tmp.data(), NULL, NULL, NULL, NULL);

                    for(int s = 0; s < 3; ++s){
                        vsigma[s] -> ReplaceMyValue(i, 0, vsigma_tmp[s]);
                        v2rho2[s] -> ReplaceMyValue(i, 0, v2rho2_tmp[s]);
                        assert(std::isfinite(vsigma_tmp[s])); assert(std::isfinite(v2rho2_tmp[s]));
                    }
                    for(int s = 0; s < 6; ++s){
                        v2rhosigma[s] -> ReplaceMyValue(i, 0, v2rhosigma_tmp[s]);
                        v2sigma2[s] -> ReplaceMyValue(i, 0, v2sigma2_tmp[s]);
                        assert(std::isfinite(v2rhosigma_tmp[s])); assert(std::isfinite(v2sigma2_tmp[s]));
                    }
                }
                xc_info -> set_gga_kernel(vsigma, v2rho2, v2rhosigma, v2sigma2);
            } break;

            case XC_FAMILY_MGGA:{
                Verbose::all() << "Libxc::compute_kernel - mGGA is not implemented.\n";
                throw std::logic_error("Libxc::compute_kernel - mGGA is not implemented.");
            } break;
        } // end switch
    }

    xc_func_end(&func);

    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
    return;
}

void Libxc::contract_gradient(
        Array< RCP<const Epetra_MultiVector> > density_grad,
        Array< RCP<Epetra_Vector> > &cont_grad
){
    cont_grad.clear();
    for(int i = 0; i < 2*density_grad.size()-1; ++i){
        cont_grad.append(rcp(new Epetra_Vector(density_grad[0]->Map())));
    }
    RCP<Epetra_MultiVector> dot_product = rcp(new Epetra_MultiVector(density_grad[0]->Map(), 3));
    dot_product -> Multiply(1.0, *density_grad[0], *density_grad[0], 0.0);
    for(int d = 0; d < 3; ++d){
        cont_grad[0] -> Update(1.0, *dot_product -> operator()(d), 1.0);
    }

    if(density_grad.size() == 2){
        dot_product -> Multiply(1.0, *density_grad[1], *density_grad[0], 0.0);
        for(int d = 0; d < 3; ++d){
            cont_grad[1] -> Update(1.0, *dot_product -> operator()(d), 1.0);
        }
        dot_product -> Multiply(1.0, *density_grad[1], *density_grad[1], 0.0);
        for(int d = 0; d < 3; ++d){
            cont_grad[2] -> Update(1.0, *dot_product -> operator()(d), 1.0);
        }
    }

    /*
    for(int i = 0; i < density_grad[0] -> Map().NumMyElements(); ++i){
        if(cont_grad[0] -> operator[](i) > 0.0005){
           std::cout << density_grad[0] -> Map().GID(i) << ": " << density_grad[0] -> operator[](0)[i] << ", " << density_grad[0] -> operator[](1)[i] << ", " << density_grad[0] -> operator[](2)[i] << std::endl;
        }
    }
    */
}

std::string Libxc::get_info_string(bool verbose/* = true*/){
    xc_func_type func;
    xc_func_init(&func, this -> function_id, XC_UNPOLARIZED);
    std::string info = func.info->name;
    if(!verbose){
        xc_func_end(&func);
        return info;
    }
    info += "\n";
    for(int i = 0; i < 5; ++i){
        if(func.info->refs[i] != NULL){
            info += "Reference " + String_Util::to_string((long long int)(i+1)) + ": " + func.info->refs[i]->ref+"\n";
        }
    }
    switch(func.info->family){
        case XC_FAMILY_LDA:
            info += "LDA functional\n";
            break;
        case XC_FAMILY_GGA:
            info += "GGA functional\n";
            break;
        case XC_FAMILY_MGGA:
            info += "mGGA functional\n";
            break;
        case XC_FAMILY_LCA:
            info += "LCA functional\n";
            break;
        case XC_FAMILY_OEP:
            info += "OEP functional\n";
            break;
        case XC_FAMILY_HYB_GGA:
            info += "Hybrid GGA functional\n";
            break;
        case XC_FAMILY_HYB_MGGA:
            info += "Hybrid mGGA functional\n";
            break;
        default:
            info += "Unknown functional\n";
            break;
    }
    int flag = func.info -> flags;
    if(flag & XC_FLAGS_HAVE_EXC) info += "XC_FLAGS_HAVE_EXC ";
    if(flag & XC_FLAGS_HAVE_VXC) info += "XC_FLAGS_HAVE_VXC ";
    if(flag & XC_FLAGS_HAVE_FXC) info += "XC_FLAGS_HAVE_FXC ";
    if(flag & XC_FLAGS_HAVE_KXC) info += "XC_FLAGS_HAVE_KXC ";
    if(flag & XC_FLAGS_HAVE_LXC) info += "XC_FLAGS_HAVE_LXC ";
    if(flag & XC_FLAGS_HYB_CAM) info += "XC_FLAGS_HYB_CAM ";
    if(flag & XC_FLAGS_HYB_CAMY) info += "XC_FLAGS_HYB_CAMY ";
    if(flag & XC_FLAGS_HYB_LC) info += "XC_FLAGS_HYB_LC ";
    if(flag & XC_FLAGS_HYB_LCY) info += "XC_FLAGS_HYB_LCY ";
    info += "\n";
    double EXX_portion = xc_hyb_exx_coef(&func);
    if(EXX_portion > 0.0) info += "Exact exchange portion required for this functional: " + String_Util::to_string(EXX_portion)+"\n";
    double CAM_omega, CAM_alpha, CAM_beta;
    xc_hyb_cam_coef(&func, &CAM_omega, &CAM_alpha, &CAM_beta);
    if(CAM_omega > 0.0) info += "Range-separation parameter: " + String_Util::to_string(CAM_omega)+"\n";
    if(CAM_alpha > 0.0 and CAM_beta > 0.0) info += "Range-separation global exact exchange portion: " + String_Util::to_string(CAM_alpha)+"\n";
    if(CAM_beta > 0.0)  info += "Range-separation long-range exact exchange portion: " + String_Util::to_string(CAM_beta)+"\n";
    xc_func_end(&func);
    return info;
}

int Libxc::get_functional_kind(){
    xc_func_type func;
    xc_func_init(&func, this -> function_id, XC_UNPOLARIZED);
    int rv = func.info -> kind;
    xc_func_end(&func);
    return rv;
}
