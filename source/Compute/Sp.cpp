#include "Sp.hpp"

Sp::Sp(RCP<const Basis> basis, RCP<const Atoms> atoms, RCP<ParameterList> parameters){
    this->basis = basis;
    this->parameters = parameters;
    int guess_type = parameters->sublist("Sp").get<int>("");
    if(guess_type==0){
        guess= Create_Compute::Create_Core_Hamiltonian(basis, atoms, parameters);
    }
    else if(guess_type==1){
        guess = Create_Compute::Create_Atomic_Density(atoms,"sublist",parameters);
    }
    else{
        exit(-1);
//        guess = Create_Core_Hamiltnoian("",parameter);
    }

    if(guess_type==0){
        scf = rcp(new Scf(basis, parameters, rcp_dynamic_cast<Core_Hamiltonian>(guess) ) );    
    }
    else{
        scf = rcp(new Scf(basis, parameters) );    
    }
}

int Sp::compute(RCP<const Basis> basis,Array<RCP<State> >& states){
    Array<RCP<State> > tmp_states;
    
    tmp_states.append( rcp( new State( *states[states.size()-1] )  ) );
    
    guess->compute(basis, tmp_states);
    scf->compute(basis, tmp_states);

    states.append(rcp(new State(*tmp_states[tmp_states.size()-1]) ) );
    
    return 0;
}

