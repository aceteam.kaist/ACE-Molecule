#pragma once
#include "Epetra_MultiVector.h"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_LAPACK.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#ifdef HAVE_MPI
#include "Epetra_MpiComm.h"
#include "mpi.h"
#else
#include "Epetra_SerialComm.h"
#endif

#include "CIS_Occupation.hpp"
#include "Core_Hamiltonian.hpp"
#include "Compute_Interface.hpp"
#include "Poisson_Solver.hpp"
#include "Exchange_Correlation.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Time_Measure.hpp"

/**
 * @brief CIS calculator.
 * @author Jaechang Lim. Modification and documantation by Sungwoo Kang.
 * @note See also CIS_Occupation. Many calculations are done there.
 **/
class CIS: public Compute_Interface{
    public:
        CIS(Teuchos::RCP<const Basis> basis,
                Teuchos::RCP<Teuchos::ParameterList> parameters,
                Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvector,
                Teuchos::Array< std::vector<double> > orbital_energies,
                Teuchos::RCP<Poisson_Solver> poisson_solver=Teuchos::null,
                Teuchos::RCP<Exchange_Correlation> exchange_correlation = Teuchos::null);
        /**
         * @brief Compute member function. This is called.
         * @details Workflow:
         * 1. Call initializer(), and create Poisson. Remove Exchange_Correlation object?!
         * 2. Call cal_CI_Hcore, cal_two_centered_integ, cal_CI_modified_Hcore from CIS_Occupation.
         * 3. Call CIS_Occupation -> cal_H0 to calculate diagonal part of CIS matrix.
         * 4. Diagonalize. It is always direct_matrix_diagonalize(). Options are: Davidson, Full_matrix_diagonalize().
         *    These matrix_diagonalize routines call cis_occupation->cal_sorted_CI_element to calculate CIS elements.
         * 5. Print CI energy only?!
         **/
        int compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states);
        ~CIS();
    protected:
        //initializer
        /**
         * @brief Initializer.
         * @details Calculates number of single excitation, get string size, and initialize Epetra_Map for CI matrix.
         **/
        void initializer();

        /**
         * @brief Perform Davidson diagonalization.
         * @details Unlike other diagonalizer, this does not make matrix.
         * Even cal_pure_Hcore() is not called, but H0 is used.
         * @note Currently not used.
         * @todo Move to appropriate another class.
         **/
        std::vector<double> Davidson();

        /**
         * @brief Gram_Schmidt process. Used once in Davidson().
         **/
        void Gram_Schmidt(Teuchos::RCP<Epetra_Vector> new_vector);
        //void diagonalize(Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > Dmatrix, double* eval, double** evec, int matrix_size);

        /**
         * @brief Print CI eigenvector values and call cis_occupation -> print_configuration.
         **/
        void print_eval(Teuchos::Array< Teuchos::RCP<Epetra_Vector > > eigenvectors);

        //CIS occupation
        Teuchos::RCP<CI_Occupation> cis_occupation;

        //basis
        Teuchos::RCP<const Basis> basis;

        //core hamiltonian
        Teuchos::RCP<Core_Hamiltonian> core_hamiltonian;

        //poisson solver
        Teuchos::RCP<Poisson_Solver> poisson_solver;

        //exchange correlation
        Teuchos::RCP<Exchange_Correlation> exchange_correlation;

        //parameters
        Teuchos::RCP<Teuchos::ParameterList> parameters;

        //two centered integral (ijkl)
        //double**** two_centered_integ;

        //Hcore
        //double** Hcore;

        //CI energy
        double CI_energy;

        //number of eigenvalues
        int num_eigenvalues;

        //block size for diagonlization of CI matrix
        int num_blocks;

        //max iteration
        int max_iteration;

        //total number of string which is same as number of configuration
        int total_num_string;

        //collected coefficient of CI eigenvector
        //double* combine_coefficient;

        //sigma
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > sigma;

        //Diagonal of core hamiltonian
        Teuchos::RCP<Epetra_Vector> H0;

        //orbitals
        Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > orbital;

        //distributed CI coefficient
        Teuchos::Array< Teuchos::RCP< Epetra_Vector> > CI_coefficient;

        /**
         * @brief Use ACE-Molecule diagonalizer to diagonalize CI matrix.
         * @details Workflow:
         * 1. Call CIS_Occupation -> cal_pure_Hcore().
         * 2. Get CI matrix in CrsMatrix form.
         * 3. Diagonalize using diagonalizer from Create_Diagonalize.
         * 4. Call CIS_Occupation -> print_configuration to print which orbitals are excited.
         * This prints configurations without any cutoff.
         **/
        std::vector<double> Full_matrix_diagonalize();
        /**
         * @brief Use LAPACK diagonalizer to diagonalize CI matrix.
         * @details Workflow:
         * 1. Call CIS_Occupation -> cal_pure_Hcore().
         * 2. Get CI matrix in Teuchos_SerialDenseMatrix form.
         * 3. Check CI matrix is symmetric.
         * 3. Call direct_diagonalize to diagonalize.
         * 4. Call CIS_Occupation -> print_configuration to print which orbitals are excited.
         * This prints non-excitation contribution higher than cutoff 0.1, and single excitation contribution higher than cutoff 0.01.
         **/
        std::vector<double> direct_matrix_diagonalize();
        /**
         * @brief Call Teuchos::LAPACK SYEV() to diagonalize.
         * @todo Isn't this exactly same with ACE-Molecule Direct_Diagonalize, instead of its input is Epetra_CrsMatrix.
         **/
        void direct_diagonalize(Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > Dmatrix, double* eval, double** evec, int matrix_size);
        Teuchos::RCP<Diagonalize> diagonalize;

        /**
         * @brief Make CI matrix as Epetra_FECrsMatrix from CIS_Occupation -> cal_sorted_CI_element. Diagonal values are H0.
         * @param H0 Diagonal part of CI matrix.
         * @return CIS matrix in Epetra_FECrsMatrix form.
         * @todo Merge with make_CI_matrix_dense. These two should act same, with only difference of output type.
         **/
        Teuchos::RCP<Epetra_CrsMatrix> make_CI_matrix_crs(Teuchos::RCP<Epetra_Vector> H0);
        /**
         * @brief Make CI matrix as Teuchos_SerialDenseMatrix from CIS_Occupation -> cal_sorted_CI_element. Diagonal values are H0.
         * @param H0 Diagonal part of CI matrix.
         * @return CIS matrix in Epetra_FECrsMatrix form.
         * @todo Merge with make_CI_matrix_crs. These two should act same, with only difference of output type.
         **/
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int,double> > make_CI_matrix_dense(Teuchos::RCP<Epetra_Vector> H0);

        //number of electrons
        int num_electrons;

        Teuchos::RCP<Time_Measure> timer;
};
