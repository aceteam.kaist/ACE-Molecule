#pragma once
//#include "../Basis/Basis.hpp"
//#include "../State/State.hpp"
#include "Compute.hpp"
//class State;
//class Mesh;

/**
@brief This is pure abstract class to be inherited by all Modes. 
       Every Compute Mode that are called in main should interit this class.
**/
class Compute_Interface: public Compute{
    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Compute_Interface(){};
    protected:
        Teuchos::RCP<Teuchos::ParameterList> parameters;
};
