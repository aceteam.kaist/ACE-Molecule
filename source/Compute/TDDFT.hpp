#pragma once
#include <vector>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_SerialDenseSolver.hpp"
#include "Epetra_CrsMatrix.h"

#include "Compute_Interface.hpp"
#include "Create_Compute.hpp"
#include "Poisson_Solver.hpp"
#include "../State/State.hpp"
#include "../Core/Solvation/Solvation.hpp"
#include "../Core/Pseudo-potential/Nuclear_Potential.hpp"

/**
 * @brief This class provides TDDFT routines. See TDDFT_* (actual implementations) for details.
 * @author Kwangwoo Hong, Jaewook Kim, Sungwoo Kang.
 * @date 2017/01/05
 **/
class TDDFT: public Compute_Interface{
    public:
        TDDFT(Teuchos::RCP<const Basis> basis, Teuchos::RCP<Exchange_Correlation> exchange_correlation, Teuchos::RCP<Poisson_Solver> poisson_solver, Teuchos::RCP<Teuchos::ParameterList> parameters);
        virtual ~TDDFT();

        /**
         * @brief Interface for compute routine.
         * @details Implementations should:
         * 2. Initialize suitable state.
         * 3. Calculate matrix_dimension and overlap matrix.
         * 4. Initilize hybrid, TD matrix, and diagonalize it.
         * 5. Process return values of diagonalize.
         * @callergraph
         * @callgraph
         **/
        virtual int compute(Teuchos::RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<State> >& states);

    protected:
        virtual void get_excitations(Teuchos::RCP<Diagonalize> diagonalize, std::vector<double> &energies, Teuchos::RCP<Epetra_MultiVector> &eigenvectors) = 0;

        /**
         * @brief Fill TDDFT matrix.
         * @callergraph
         * @callgraph
         **/
        virtual void fill_TD_matrix(
                Teuchos::Array< std::vector<double> > orbital_energies,
                double Hartree_contrib, double xc_contrib,
                int matrix_index_c, int matrix_index_r,
                Teuchos::RCP<Epetra_CrsMatrix> &sparse_matrix
        ) = 0;

        /**
         * @brief Fill restricted TDDFT matrix. Governs matrix index.
         * @callergraph
         * @callgraph
         **/
        virtual void fill_TD_matrix_restricted(
                std::vector<double> orbital_energies,
                double Hartree_contrib, std::vector<double> xc_contrib,
                int matrix_index_c, int matrix_index_r,
                Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix
        ) = 0;

        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<Poisson_Solver> poisson_solver;
        //Teuchos::RCP<const Poisson_Solver> exchange_poisson_solver;
        Teuchos::RCP<Poisson_Solver> exchange_poisson_solver;
        Teuchos::RCP<Exchange_Correlation> exchange_correlation;
        Teuchos::RCP<Teuchos::ParameterList> parameters;

        /**
         * @brief Orbital gradients. [spin index][direction index][orbital index].
         **/
        std::vector< Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > > gradient;
        //std::vector<int> number_of_electrons; //(i.e. with alpha spin)
        std::vector<int> number_of_orbitals; //(i.e. with alpha spin)
        std::vector<int> number_of_occupied_orbitals; //(i.e. of alpha spin)
        //std::vector<int> index_of_HOMO; //(i.e. of alpha spin)

        //std::vector<int> num_unocc_orb; 
        //std::vector<int> num_occ_orb;
        /**
         * Lowest orbital index to start calculations.
         **/
        int iroot = 0;

        double cutoff;

        //void initialize_orbital_numbers(Teuchos::Array< Teuchos::RCP<const Occupation> > occupations);
        void initialize_orbital_numbers(Teuchos::RCP<State> &state);

        void print_contribution(Teuchos::RCP< Epetra_MultiVector> eigen_vectors, int index, std::vector< std::vector<int> > determinant_index, int num_request, double tolerance, bool upper_half = false);

        /**
         * @brief Casida matrix calculation routine.
         * @param functionals Functional types, which is LDA, GGA, or PGG
         * @param portions Portions for each functional types. Ignored in this case.
         * @param state Input state.
         * @param sparse_matrix Output TDDFT kernel matrix.
         * @todo Implement mGGA.
         * @note See also Exchange_Correlation, especially if mGGA should be implemented.
         * @note See also calculate_PGG_kernel_ia for PGG computation routine
         * @note Non-diagonal term is doubled (4.0 for Casida, 2.0 for TammDancoff) because of 2 spins.
         * @note TheoryLevel "Casida" solves \f[ CZ = \Omega^2 Z \f] and "TammDancoff" solves \f[ AX = \Omega X \f].
         * @callergraph
         * @callgraph
         **/
        void get_full_TD_matrix(
                std::vector<std::string> functionals,
                std::vector<double> portions,
                int iroot,
                Teuchos::RCP<State> state,
                Teuchos::RCP<Epetra_CrsMatrix>& sparse_matrix
        );

        void postprocessing(
                std::vector<double>excitation_energies,
                Teuchos::RCP<Epetra_MultiVector> eigenvectors,
                Teuchos::RCP<State> state,
                int num_excitation
        );
//old comment!
        /**
         * @brief HF X kernel. See J. Chem. Phys. 134, 034120 (2011) equation 44.
         * @param state Input state.
         * @param sparse_matrix Output TDDFT kernel matrix.
         * @param
         **/
//  171113 : old function! the function is merged with Kernel_TDHF_X()
//        virtual void Kernel_HF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, double scale = 1.0) = 0;

         /**
          * @brief Calculates TDHF kernel
          * @param state Input state.
          * @param sparse_matrix Output TDHF matrix
          * @param kernel_is_not_hybrid False for hybrid kernel, true for TDHF(EXX)-like kernel w/ DFT orbital
          * @param add_delta_term True for TDHF(EXX), False for pristine TDHF
          * @param scale Output matrix is scaled with this factor.
          * @author Sungwoo Kang, Jaewook Kim
          * @date 2017/11/13
          * @note Kernel_KSCI_X() and Kernel_HF_X() is merged (171113)
          * @note delta_term is potential_difference norm. \delta_ij<a|v_GKS-v_KS|b>-\delta_ab<i|v_GKS-v_KS|j>
          * @details
          *          Potential difference norm = <i|v^EXX-v^HF|a>
          *          A = (\epsilon_a - \epsilon_i) \delta_{ab} \delta{ij} \delta{\sigma \sigma^\prime} + (ia\sigma | jb\sigma^\prime)
          *              + f^{xc}_{ia\sigma,jb\sigma^\prime} - c_x (ab\sigma|ij\sigma^\prime) \delta_{\sigma \sigma^\prime}
          *          B =  (ia\sigma | jb\sigma^\prime) + f^{xc}_{ia\sigma,jb\sigma^\prime} - c_x (ja\sigma | ib \sigma^\prime_ \delta_{\sigma \sigma^\prime}
         * @callergraph
         * @callgraph
          **/
        virtual void Kernel_TDHF_X(Teuchos::RCP<State> state, Teuchos::RCP<Epetra_CrsMatrix>& sparse_matrix, bool kernel_is_not_hybrid, bool add_delta_term, double scale = 1.0) = 0;

        /**
         * @brief Calculates oscillator strength and overlap of the states
         * @param TD_excitation Eigenvector of TDDFT matrix.
         * @param state State. Orbitals and orbital eigenvalues are required.
         * @param start Minimum index of orbital that TDDFT excitations are allowed. (0 always for now)
         * @param ispin Spin index
         * @param num_excitation num_excitation in compute function.
         * @param state_overlap Output. \f[ \langle \Psi_I | r_i \Psi_0 \rangle \f].
         * @param oscillator_strength Output. Oscillator strength for each excitations.
         * @param is_deexcit True for ABBA, false for others.
         * @author Sungwoo Kang
         * @date 2017/01/06
         * @note Casida oscillator strength is fixed 2017/01/06. Reference: DOI: 10.1063/1.471140
         * @note Better equatoin for oscillator strength: 10.1063/1.3517312 eq.19
         * @callergraph
         * @callgraph
         **/
        void calculate_oscillator_strength(
                std::vector<double> excitation_energies,
                Teuchos::RCP<Epetra_MultiVector> TD_excitation,
                Teuchos::RCP<State> state,
                int num_excitation,
                std::vector< std::vector<double> > &state_overlap,
                std::vector<double> &oscillator_strength,
                bool is_deexcit = false
        );

        /**
         * @brief Calculates CT character analysis.
         * @param TD_excitation Eigenvector of TDDFT matrix.
         * @param state State. Orbitals and orbital eigenvalues are required.
         * @param start Minimum index of orbital that TDDFT excitations are allowed. (0 always for now)
         * @param ispin Spin index
         * @param num_excitation num_excitation in compute function.
         * @param ct_character Output. Spatial overlap, r-index, Lambda-index.
         * @param spatial_overlap Output. Spatial overlap of orbital pair.
         * @param is_deexcit True for ABBA, false for others.
         * @author Sungwoo Kang
         * @date 2017/01/06
         * @note Spatial overlap: 10.1063/1.2831900 r-index: 10.1021/ct400337e Lambda-index 10.1063/1.4922780
         * @callergraph
         * @callgraph
         **/
        void CT_analysis(
                std::vector<double> excitation_energies,
                Teuchos::RCP<Epetra_MultiVector> TD_excitation,
                Teuchos::RCP<State> state, int num_excitation,
                std::vector< std::vector<double> > &ct_character,
                std::vector<double> &spatial_overlap,
                bool is_deexcit = false
        );

        /**
         * @brief Calculates PGG kernel_ia.
         * @param ia_over_density density / orb_i orb_j.
         * @param orbitals Orbitals.
         * @param end Index to end.
         * @param is_upper_only true for TDA, false for others.
         * @todo Should check is_upper_only is false for ABBA. It is really uncanny situation though.
         * @callergraph
         * @callgraph
         **/
        Teuchos::RCP<Epetra_Vector> calculate_PGG_kernel_ia(
                Teuchos::RCP<Epetra_Vector> ia_over_density,
                Teuchos::RCP<Epetra_MultiVector> orbitals,
                int end, bool is_upper_only
        );

        /**
         * @brief print function of TDDFT excitation.
         **/
        void print_excitation_info(
            double tolerance, int order,
            Teuchos::RCP<Epetra_MultiVector> eigenvectors,
            int iroot, int num_occ_orbitals, int num_orbitals,
            std::vector<double> excitation_energies,
            std::vector< std::vector<double> > ct_character,
            std::vector< std::vector<double> > state_overlap,
            std::vector<double> oscillator_strength,
            bool upper_half = false
        );

        Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> > gradient_matrix;
        
        /**
         * @brief Resolution of identity (RI, density fitting) to calculate gradient.
         * @details Requires gradient_matrix, or/and gradient value to be initialized.
         *          For RI technique, see 10.1088/1367-2630/14/5/053020 for example.
         * @param[in] orbital_coeff Orbital coefficient.
         * @param[in] s Spin index
         * @param[in] i Orbital index 1.
         * @param[in] a Orbital index 2.
         * @param[out] grad_ia Gradient of orbital(i) * orbital(a).
         **/
        void get_gradient_ia_RI_using_matrix(
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff, 
                int s, int i, int a, 
                Teuchos::RCP<Epetra_MultiVector>& grad_ia
        );

        /**
         * @brief Initialize gradient_matrix, and optionally gradient member variable.
         * @param[in] orbitals Orbitals.
         * @param[in] initialize_gradient Initialize gradient member variable.
         **/
        void construct_gradient(
                Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals, 
                bool multiply_orbitals
        );


        std::string change_exx_kernel(std::string functional_type, std::string exchange_kernel, std::string exx_default = "PGG");

        // Not intended to use in this class, but for derived class.
        // return DFT local potential, which is used to calculate orbitals.
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_dft_potential(Teuchos::RCP<State> state);

        // return potential difference
        /**
         * @brief Calculates potential difference (v_kernel - v_orbital)  relavant to the unitary transformation of casida matrix
         * @param[in] state State. Orbitals and orbital eigenvalues are required.
         * @param[in] without_EXX Parameter used for KSCI scheme [a.k.a. TDHF(EXX)] Option for the calculation of xc_wo_EXX potential
         * @param[in] kernel_is_not_hybrid False for hybrid kernel, true for TDHF(EXX)-like kernel w/ DFT orbital
         * @return[out] potential differences
         * @author Jaewook Kim
         * @date 2017/11/13
         * @details
         * This function calculates potential difference to calculate the following correction term
         * \f[
         *      \Delta \mathbf{v}_{pq}&= \left\langle \psi_p^{\mathrm{KLI}}\right| \hat{H}^{\mathrm{Kernel}}- \hat{H}^{\mathrm{orbital}}\left|\psi_q^{\mathrm{KLI}}\right\rangle
         * \f]
         * Here, the difference between the TDDFT kernel potential and orbital potentials are calculated using the information in "OrbitalInfo"
         * This function gives correction term in the case of the TDDFT kernel is different with the functional that we used for orbital calculation.
         * For example, the orbital could be calculated by using KLI potential but we want to perform TDHF calculation, intead of using exact kernel corresponds to KLI potential.
         * @callergraph
         * @callgraph
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > calculate_v_diff(Teuchos::RCP<State> state, bool without_EXX, bool kernel_is_not_hybrid);

        // calculate < i | v_GKS - V_KS | j>
        /**
         * @brief Calculates potential difference norm
         * @param[in] orbital_coeff vector of orbital coefficients.
         * @param[in] i orbital index i
         * @param[in] j orbital idnex j
         * @param[in] potential_diff potential difference calculated in calculate_v_diff()
         * @return[out] potential difference norm
         * @author Jaewook Kim
         * @date 2017/11/13
         * @details
         * This method calculates 
         * \f[
         *      \Delta \mathbf{v}_{ij}= \left\langle \psi_i^{\mathrm{KLI}}\right| \hat{H}^{\mathrm{GKS}}- \hat{H}^{\mathrm{KLI}}\left|\psi_j^{\mathrm{KLI}}\right\rangle
         * \f]
         * by using the orbital informations and potential difference, calculated in calculate_v_diff()
         * See the calculate_v_diff() for detailed explanation.
         * @callergraph
         * @callgraph
         */
        double potential_difference_norm(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int i, int j, Teuchos::RCP<Epetra_Vector> potential_diff);

        /**
         * @brief calculate (ia|jb) (two centered integral), and prepare PCMSolver with V_H(ia).
         * @param orbital_value[in] Orbital, in value representation.
         * @param exchange[in] Use exchange_poisson and does not initialize PCMSolver. Used for (RSH) HF kernel.
         * @note i,j = occ, a,b = unocc, uses iroot.
         * @note output index starts from (iroot,index_of_HOMO|iroot,index_of_HOMO), see the quadruple for statements.
         **/
        std::vector<double> get_iajb(
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbital_value, 
            bool exchange
        );

        /** 
         * @param[in] orbital_value Orbitals, value, not basis coefficient.
         * @param[in] use_exchange_poisson If true, (ia|jb) from exchange_poisson will be used instead. Useful for computing RSH.
         * @param[in] use_solvation If true, list of V[ia] will be returned to mep_list.
         * @param[out] mep_list list of V[ia] if use_solvation is true. Untouched otherwise.
         **/
        std::vector<double> calculate_iajb_highmem(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_value, 
            bool use_exchange_poisson, bool use_solvation, 
            std::vector< std::vector<double> > &mep_list
        );
        /** 
         * @param[in] orbital_value Orbitals, value, not basis coefficient.
         * @param[in] use_exchange_poisson If true, (ia|jb) from exchange_poisson will be used instead. Useful for computing RSH.
         * @param[in] use_solvation If true, list of V[ia] will be returned to mep_list.
         * @param[out] mep_list list of V[ia] if use_solvation is true. Untouched otherwise.
         **/
        std::vector<double> calculate_iajb_lowmem(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_value, 
            bool use_exchange_poisson, bool use_solvation, 
            std::vector< std::vector<double> > &mep_list
        );

        /**
         * @brief Get Z vector from eigenvectors and spread over processors.
         * @param input Input eigenvector.
         * @param eigvals GS eigenvalues.
         * @return output Z vector.
         * @callergraph
         * @callgraph
         **/
        virtual std::vector< std::vector<double> > gather_z_vector(
            Teuchos::RCP<Epetra_MultiVector> input,
            Teuchos::Array< std::vector<double> > eigvals
        ) = 0;

        void print_tddft_theory_info(std::string functional_type);

        bool is_tdhf_kli = false;
        bool is_rtriplet = false;
        bool is_open_shell = false;
        std::vector< std::vector<bool> > orbital_pair_filter;
        double orbital_pair_cutoff = -1.0;


        int num_excitation = 0;
        int matrix_dimension = 0;
        /**
         * @brief Contains which class is used? C, ABBA, or TDA?
         **/
        std::string type;
        /**
         * @brief Contains which matrix is used? Casida or TammDancoff?
         **/
        std::string theorylevel;
        std::string _exchange_kernel;
        /**
         * @brief Matrix dimension = mat_dim_scale * [# of occupied orbitals] * [# of virtual orbitals]
         **/
        int mat_dim_scale = 1;
        Teuchos::RCP<State> state;
        std::vector<double> eps_diff;
        std::vector<double> spatial_overlap;

        /**
         * @brief Convert spin, occupied orbital, and virtual orbital index to matrix index.
         * @param[in] s Spin index.
         * @param[in] i Occupied orbital index.
         * @param[in] a Vritual orbital index.
         * @return Matrix index.
         **/
        int pack_index(int s, int i, int a);

        /**
         * @brief Convert matrix index to spin, occupied orbital, and virtual orbital index.
         * @param[in] mat_ind Matrix index.
         * @param[out] s Spin index.
         * @param[out] i Occupied orbital index.
         * @param[out] a Virtual orbital index.
         **/
        void unpack_index(int mat_ind, int &s, int &i, int &a);

        /**
         * @brief Change how to calculate the gradient of orbital pair.
         * @details Orb * grad(orb) if true, grad(orb*orb) if false.
         **/
        bool new_orbital_grad = true;
        Teuchos::RCP<Nuclear_Potential> nuclear_potential;
        Teuchos::RCP<Solvation> solvation = Teuchos::null;
};
