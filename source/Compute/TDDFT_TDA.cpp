#include "TDDFT_TDA.hpp"
#include <cmath>
#include <complex>

#include "Teuchos_Time.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Epetra_CrsMatrix.h"

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Utility/Two_e_integral.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/String_Util.hpp"

#define DEBUG (1)// 0 truns off fill-time output

using std::abs;
using std::vector;
using std::endl;
using std::string;
using std::setw;
using std::setiosflags;
using std::ios;
using Teuchos::SerialDenseMatrix;
using Teuchos::Time;
using Teuchos::TimeMonitor;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

TDDFT_TDA::TDDFT_TDA(RCP<const Basis> basis, RCP<Exchange_Correlation> exchange_correlation, RCP<Poisson_Solver> poisson_solver, RCP<Teuchos::ParameterList> parameters)
:TDDFT(basis, exchange_correlation, poisson_solver, parameters){
    if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") != "TammDancoff"){
        string errmsg = "TDDFT::compute - Class and TheoryLevel mismatch! (TDDFT_TDA with " + this -> theorylevel + ")!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
    this -> type = "TDA";
}

void TDDFT_TDA::get_excitations(
        Teuchos::RCP<Diagonalize> diagonalize, 
        std::vector<double> &energies, 
        Teuchos::RCP<Epetra_MultiVector> &eigenvectors
){
    energies = diagonalize->get_eigenvalues();

    if(num_excitation!=energies.size()){
        Verbose::single(Verbose::Simple) << "Original NumberOfStates(" << num_excitation<< ") is changed to " << energies.size() << "." << std::endl;
        num_excitation = energies.size();
    }

    std::vector<double> excitation_energies(energies);
    excitation_energies.resize(num_excitation);
    // Verify excitation energies
    double min_element = *std::min(excitation_energies.begin(), excitation_energies.end());
    if(min_element < 0.0){
        Verbose::single(Verbose::Simple) << "TDDFT::compute - WARNING: For whatever reason, excitation energy " << min_element << " is negative.\n";
        Verbose::single(Verbose::Simple) << "TDDFT::compute - WARNING: This should not happen.\n";
    }
    // end
    energies = excitation_energies;
    eigenvectors = diagonalize -> get_eigenvectors();
}

void TDDFT_TDA::fill_TD_matrix(
        Teuchos::Array< vector<double> > orbital_energies,
        double Hartree_contrib, double xc_contrib,
        int matrix_index_c, int matrix_index_r,
        RCP<Epetra_CrsMatrix>& sparse_matrix
){
    int s1, i, a, s2, j, b;
    this -> unpack_index(matrix_index_c, s1, i, a);
    this -> unpack_index(matrix_index_r, s2, j, b);

    double element_tmp = (Hartree_contrib + xc_contrib);
    // *2 because spin restricted.
    if(!this -> is_open_shell){
        element_tmp *= 2;
    }
    if(i==j and a==b and s1 == s2){
        double eps_ai = orbital_energies[s1][a] - orbital_energies[s1][i];
        element_tmp += eps_ai;
    }
    if(std::fabs(element_tmp) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            if(sparse_matrix->MyGlobalRow(matrix_index_c) ){
                sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
            }
            if(sparse_matrix->MyGlobalRow(matrix_index_r)) {
                sparse_matrix->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
            }
        }
        else{
            sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
        }
    }
}

void TDDFT_TDA::fill_TD_matrix_restricted(
        vector<double> orbital_energies,
        double Hartree_contrib, vector<double> xc_contrib,
        int matrix_index_c, int matrix_index_r,
        Array< RCP<Epetra_CrsMatrix> >& sparse_matrix
){
    int s1, i, a, s2, j, b;
    this -> unpack_index(matrix_index_c, s1, i, a);
    this -> unpack_index(matrix_index_r, s2, j, b);
    assert(s1 == 0); assert(s2 == 0);
    double eps_ai = orbital_energies[a] - orbital_energies[i];

    // *2 because both up and down spin should be considered.
    double element_tmp = 2.0 * (Hartree_contrib + xc_contrib[0]);
    if(i==j and a==b){
        element_tmp += eps_ai;
    }
    if(std::fabs(element_tmp) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_c) ){
                sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
            }
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_r)) {
                sparse_matrix[0]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
            }
        }
        else{
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
        }
    }
    if(sparse_matrix.size() > 1){
        throw std::invalid_argument("Not implemented");
        double element_tmp = 2.0 * xc_contrib[1];
        if(i==j and a==b){
            element_tmp += eps_ai;
        }
        if(std::fabs(element_tmp) > this -> cutoff){
            if(matrix_index_c!=matrix_index_r){
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_c) ){
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
                }
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_r)) {
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
                }
            }
            else{
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
            }
        }
    }
}

void TDDFT_TDA::Kernel_TDHF_X(
        Teuchos::RCP<State> state, 
        Teuchos::RCP<Epetra_CrsMatrix>& sparse_matrix, 
        bool kernel_is_not_hybrid, 
        bool add_delta_term, double scale/* = 1.0*/
){
    auto map = basis->get_map();
    Array< RCP<Epetra_MultiVector> > orbitals;
    for(int i_spin=0; i_spin<state->get_occupations().size(); ++i_spin){
        orbitals.push_back(rcp(new Epetra_MultiVector(*map, state->get_orbitals()[i_spin]->NumVectors())));
        orbitals[i_spin]->Update(1.0, *state->get_orbitals()[i_spin], 0.0);
        Value_Coef::Value_Coef(basis, orbitals[i_spin], false, true, orbitals[i_spin]);
    }

    // For HF
    RCP<Epetra_Vector> Kib = rcp(new Epetra_Vector(*map)); //occ-vir
    // end

    // For LDA & GGA
    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*map)); //occ-vir
    RCP<Epetra_Vector> Kij = rcp(new Epetra_Vector(*map)); //occ-occ

    RCP<Epetra_Vector> ia_coeff = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> jb_coeff = rcp(new Epetra_Vector(*map));
    // end

    double EXX_portion = scale;
    // V_GKS - V_KS + a0 * V_HF
    Array< RCP<Epetra_Vector> > potential_diff_wo_EXX;
    if(add_delta_term){
        potential_diff_wo_EXX = calculate_v_diff(state, true, kernel_is_not_hybrid);
        if(potential_diff_wo_EXX.size() > 2){
            Verbose::all() << "TDDFT::Kernel_KSCI, something wrong ! EXX_portion : " << EXX_portion << "    potential_diff_wo_EXX.size() : " << potential_diff_wo_EXX.size() << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    // Hartree<-fill td matrix restricted()
    // -sum_k(ak|kb)
    int number_of_virtual_orbitals = number_of_orbitals[0] - number_of_occupied_orbitals[0];

/***** 171114, get iajb is waste of time
    Verbose::single(Verbose::Detail) << "TDDFT::get_iajb" << std::endl;
#if DEBUG == 1
    internal_timer -> start("get_iajb");
#endif
    std::vector<double> iajb = get_iajb(orbitals[0], true);
#if DEBUG == 1
    internal_timer -> end("get_iajb");
    Verbose::single(Verbose::Detail) << "TDDFT_TDA::Kernel_TDHF_X get_iajb time = " << internal_timer -> get_elapsed_time("get_iajb",-1) << std::endl;
#endif
    int index_iajb = 0;
****/
    if(add_delta_term){
        for(int s = 0; s < (is_open_shell? 2: 1); ++s){ 
            for(int i=iroot; i<number_of_occupied_orbitals[s]; ++i){
#if DEBUG == 1
                internal_timer -> start("i-loop");
#endif
                for(int a=number_of_occupied_orbitals[s]; a<number_of_orbitals[s]; ++a){
                    Kia->PutScalar(0.0);
                    Two_e_integral::compute_Kji(basis, orbitals[s]->operator()(i), orbitals[s]->operator()(a), exchange_poisson_solver, Kia);
                    int delta_matrix_index_r = this -> pack_index(s, i, a);

                    for(int j=i; j < number_of_occupied_orbitals[s]; ++j){
                        for(int b = (j==i)? a: number_of_occupied_orbitals[s]; b<number_of_orbitals[s]; ++b){
                            int delta_matrix_index_c = this -> pack_index(s, j, b);

                            // Delta v_ab =  -sum_k(ak|kb) + potential_difference_norm(a,b)

                            if(i==j){
                                double element_iajb = Two_e_integral::compute_two_center_integral(basis, Kia, orbitals[s]->operator()(b), orbitals[s]->operator()(j));
                                double element_tmp_vdiff = potential_difference_norm(state->get_orbitals()[s],a,b,potential_diff_wo_EXX[s]);
                                    // A
                                    sparse_matrix->InsertGlobalValues(delta_matrix_index_r,1,&element_tmp_vdiff,&delta_matrix_index_c);
                                    if(delta_matrix_index_r!=delta_matrix_index_c){
                                        sparse_matrix->InsertGlobalValues(delta_matrix_index_c,1,&element_tmp_vdiff,&delta_matrix_index_r);
                                    }

                                    for(int k=0;k<number_of_occupied_orbitals[s];++k){
                                        //ia,jb = ka,kb , (bj|ia) = (bk|ka)
                                        // A
                                        double element_HFx = - element_iajb * scale;
                                        int delta_matrix_index_c2 = this -> pack_index(s, k, a);
                                        int delta_matrix_index_r2 = this -> pack_index(s, k, b);
                                        sparse_matrix->InsertGlobalValues(delta_matrix_index_r2,1,&element_HFx,&delta_matrix_index_c2);
                                        if(delta_matrix_index_r2!=delta_matrix_index_c2){
                                            sparse_matrix->InsertGlobalValues(delta_matrix_index_c2,1,&element_HFx,&delta_matrix_index_r2);
                                        }
                                    }
                                    //-sum_k(ak|kb) end
                                }
                            }
                        }
                    }
#if DEBUG == 1
                    internal_timer -> end("i-loop");
                    Verbose::single(Verbose::Detail) << "TDDFT_TDA::Kernel_TDHF_X index for Delta v + B i = " << i << " time = "<< internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
#endif
                }
            }
        // end
    }

    // exchange - A
    // sum_k(jk|ki)
    /*
       int index_i = i * number_of_virtual_orbitals;
       int index_j = j * number_of_virtual_orbitals;
       int index_a = a - number_of_occupied_orbitals[0];
       int index_b = b - number_of_occupied_orbitals[0];
       matrix_index_c = index_i + index_a = i * number_of_virtual_orbitals + a - number_of_occupied_orbitals[0];
       matrix_index_r = index_j + index_b = j * number_of_virtual_orbitals + b - number_of_occupied_orbitals[0];
     */

    for(int s = 0; s < (is_open_shell? 2: 1); ++s){
        for(int i=iroot; i<number_of_occupied_orbitals[s]; ++i){
    #if DEBUG == 1
            internal_timer -> start("i-loop");
    #endif
            int index_i = i * number_of_virtual_orbitals;

            for(int j=i; j<number_of_occupied_orbitals[s]; ++j){
                Kij->PutScalar(0.0);
                Two_e_integral::compute_Kji(basis, orbitals[s]->operator()(i), orbitals[s]->operator()(j), exchange_poisson_solver, Kij);

                for(int a=number_of_occupied_orbitals[s]; a<number_of_orbitals[s]; ++a){
                    for(int b = (j==i)? a: number_of_occupied_orbitals[s]; b<number_of_orbitals[s]; ++b){
                        int index_b = b - number_of_occupied_orbitals[s];
                        int matrix_index_c = this -> pack_index(s, i, a);
                        int matrix_index_r = this -> pack_index(s, j, b);
                        double element_ijab = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(a), orbitals[s]->operator()(b));

                        double element_tmp1 = - element_ijab * scale;
                        if(std::fabs(element_tmp1) > this -> cutoff){
                            if(matrix_index_c!=matrix_index_r){
                                sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_tmp1,&matrix_index_r);
                                sparse_matrix->InsertGlobalValues(matrix_index_r,1,&element_tmp1,&matrix_index_c);
                            }
                            else{
                                sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_tmp1,&matrix_index_c);
                            }
                        }

                        if(add_delta_term){
                            if(a==b){
                                // Delta v_ij =  sum_k(jk|ki) - potential_difference_norm(j,i)
                                // delta_ij가 없을 때 or delta_ij가 있을 때 ij 같음.
                                if(!this -> is_tdhf_kli or i == j){
                                    double element_tmp_m_vdiff = - potential_difference_norm(state->get_orbitals()[s],j,i,potential_diff_wo_EXX[s]);
                                    // A
                                    sparse_matrix->InsertGlobalValues(matrix_index_r,1,&element_tmp_m_vdiff,&matrix_index_c);
                                    if(matrix_index_r!=matrix_index_c){
                                        sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_tmp_m_vdiff,&matrix_index_r);
                                    }
                                }
                                // sum_k(jk|ki)
                                double element_ijij = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(i), orbitals[s]->operator()(j)) * scale;

                                //ia,jb = ia,ia , (ij|ji)
                                //A
                                matrix_index_c = this -> pack_index(s, i, a);
                                matrix_index_r = matrix_index_c;
                                sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_ijij,&matrix_index_r);

                                if(i!=j){
                                    //ia,jb = ja,ja , (ij|ji)
                                    //A
                                    matrix_index_c = this -> pack_index(s, j, a);
                                    matrix_index_r = matrix_index_c;
                                    sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_ijij,&matrix_index_r);
                                }

                                if(!this -> is_tdhf_kli or i == j){
                                    for(int k=j+1;k<number_of_occupied_orbitals[s];++k){
                                        double element_kiij = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(i), orbitals[s]->operator()(k)) * scale ;
                                        //ia,jb = ka,jb, (ki|ij)
                                        //A
                                        matrix_index_c = this -> pack_index(s, k, a);
                                        matrix_index_r = this -> pack_index(s, j, b);
                                        sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_kiij,&matrix_index_r);
                                        if(matrix_index_r!=matrix_index_c){
                                            sparse_matrix->InsertGlobalValues(matrix_index_r,1,&element_kiij,&matrix_index_c);
                                        }
                                    }//for(k)
                                }//if(!is_tdhf_kli or i==j)
                                if(!this -> is_tdhf_kli and i!=j){
                                    for(int k=i+1;k<number_of_occupied_orbitals[s];k++){
                                        double element_kjij = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(j), orbitals[s]->operator()(k)) * scale;
                                        //ia,jb = ia,kb, (kj|ij)
                                        //A
                                        matrix_index_c = this -> pack_index(s, i, a);
                                        matrix_index_r = this -> pack_index(s, k, b);
                                        sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_kjij,&matrix_index_r);
                                        if(matrix_index_r!=matrix_index_c){
                                            sparse_matrix->InsertGlobalValues(matrix_index_r,1,&element_kjij,&matrix_index_c);
                                        }
                                    }//for(k)
                                }//if(!is_tdhf_kli and i!=j)
                            }//if(a==b)
                        }//if(add_delta_term)
                    }//for(b)
                }//for(j)
            }//for(a)
    #if DEBUG == 1
            internal_timer -> end("i-loop");
            Verbose::single(Verbose::Detail) << "TDDFT_TDA::Kernel_TDHF_X index for rest exchange  i = " << i << " time = " << internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
    #endif
        }//for(i)
    }//for(s)
    // end
    return;
}

std::vector< std::vector<double> > TDDFT_TDA::gather_z_vector(
        RCP<Epetra_MultiVector> input, 
        Teuchos::Array< std::vector<double> > eigvals
){
    int num_vec = input -> NumVectors();
    int total_size = input->Map().NumGlobalElements();
    double ** recv = new double*[num_vec];
    for(int i = 0; i < num_vec; ++i){
        recv[i] = new double[total_size];
    }
    Parallel_Util::group_allgather_multivector(input, recv);

    vector<double> eps_diff(total_size);
    int matrix_index_r = 0;
    for(int matrix_index_r = 0; matrix_index_r < total_size; ++matrix_index_r){
        int s, i, a;
        this -> unpack_index(matrix_index_r, s, i, a);
        eps_diff[matrix_index_r] = sqrt(eigvals[s][a] - eigvals[s][i]);
    }
    for(int j = 0; j < num_vec; ++j){
        for(int i = 0; i < total_size; ++i){
            recv[j][i] /= eps_diff[i];
        }
        double norm = 0.0;
        for(int i = 0; i < total_size; ++i){
            norm += recv[j][i]*recv[j][i];
        }
        for(int i = 0; i < total_size; ++i){
            recv[j][i] /= sqrt(norm);
        }
    }

    vector< vector<double> > output(num_vec);
    for(int i = 0; i < num_vec; ++i){
        output[i] = vector<double>(recv[i], recv[i]+total_size);
    }
    for(int i = 0; i < num_vec; ++i){
        delete[] recv[i];
    }
    delete[] recv;
    return output;
}
