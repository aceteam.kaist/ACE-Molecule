#include "../Utility/Value_Coef.hpp"
#include "Compute_XC.hpp"

using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

Compute_XC::Compute_XC(RCP<const Basis> basis, int xc_id): basis(basis), function_id(xc_id){
}

int Compute_XC::compute(
   Teuchos::RCP<const Basis> basis,
   Teuchos::Array<Teuchos::RCP<State> >& states
){
    std::cout << "Compute_XC::compute: state version is not used" << std::endl;
    return -10;
};


int Compute_XC::compute(Teuchos::RCP<XC_State>& xc_info){
    compute_vxc(xc_info);
    compute_Exc(xc_info);
    return 0;
};

int Compute_XC::get_functional_id(){
    return this -> function_id;
}

Teuchos::RCP<Poisson_Solver> Compute_XC::get_rsh_poisson_solver(){
    return this -> poisson_solver;
}
