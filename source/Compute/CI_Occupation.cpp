#include "CI_Occupation.hpp"
#include <array>
#include "../Utility/Density/Density_From_Orbitals.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Time_Measure.hpp"

using std::array;
using std::vector;
using std::endl;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

double CI_Occupation::cal_CI_element_value1(vector<int> beta_occ1, vector<int> alpha_occ2, vector<int> same_alpha_occ,vector<int> diff_alpha_occ, vector<int> diff_alpha_index,vector<int> same_alpha_index){
    double retval = 0.0;
    retval += cal_CI_element_value1_hcore( beta_occ1, alpha_occ2, same_alpha_occ, diff_alpha_occ, diff_alpha_index,same_alpha_index);
    retval += cal_CI_element_value1_exchange( beta_occ1, alpha_occ2, same_alpha_occ, diff_alpha_occ, diff_alpha_index,same_alpha_index);
    retval += cal_CI_element_value1_repulsion( beta_occ1, alpha_occ2, same_alpha_occ, diff_alpha_occ, diff_alpha_index,same_alpha_index);
    return retval;
}

double CI_Occupation::cal_CI_element_value1_hcore(vector<int> beta_occ1, vector<int> alpha_occ2, vector<int> same_alpha_occ,vector<int> diff_alpha_occ, vector<int> diff_alpha_index,vector<int> same_alpha_index){

    return pure_Hcore[diff_alpha_occ[0]][diff_alpha_occ[1]]*cal_gamma(diff_alpha_occ[0],diff_alpha_occ[1], alpha_occ2);
}

double CI_Occupation::cal_CI_element_value1_exchange(vector<int> beta_occ1, vector<int> alpha_occ2, vector<int> same_alpha_occ,vector<int> diff_alpha_occ, vector<int> diff_alpha_index,vector<int> same_alpha_index){
    double gamma = cal_gamma(diff_alpha_occ[0],diff_alpha_occ[1], alpha_occ2);
    double retval = 0.0;
    if(same_alpha_occ.size()!=0){
        for(int i=0; i<same_alpha_occ.size(); i++){
            retval -=two_centered_integ[diff_alpha_occ[0]][same_alpha_occ[i]][same_alpha_occ[i]][diff_alpha_occ[1]]*gamma*0.5;
        }
    }
    return retval;
}

double CI_Occupation::cal_CI_element_value1_repulsion(vector<int> beta_occ1, vector<int> alpha_occ2, vector<int> same_alpha_occ,vector<int> diff_alpha_occ, vector<int> diff_alpha_index,vector<int> same_alpha_index){
    double retval = 0.0;
    double gamma = cal_gamma(diff_alpha_occ[0],diff_alpha_occ[1], alpha_occ2);
    if(same_alpha_occ.size()!=0){
        for(int i=0; i<same_alpha_occ.size(); i++){
            retval += two_centered_integ[diff_alpha_occ[0]][diff_alpha_occ[1]][same_alpha_occ[i]][same_alpha_occ[i]]*gamma;
        }
        for(int i=0; i<same_alpha_occ.size(); i++){
            vector<int> temp_occ = alpha_occ2;
            temp_occ[same_alpha_index[i]]=diff_alpha_occ[0];
            Sorting(temp_occ);
            retval += two_centered_integ[diff_alpha_occ[0]][same_alpha_occ[i]][same_alpha_occ[i]][diff_alpha_occ[1]]*cal_gamma(same_alpha_occ[i],diff_alpha_occ[1], temp_occ)*cal_gamma(diff_alpha_occ[0], same_alpha_occ[i], alpha_occ2)*0.5;
        }
    }
    for(int i=0; i<num_alpha_electrons; i++){
        retval += two_centered_integ[diff_alpha_occ[0]][diff_alpha_occ[1]][beta_occ1[i]][beta_occ1[i]]*gamma;
    }
    return retval;
}

double CI_Occupation::cal_CI_element_value2(vector<int> alpha_occ1 ,vector<int> beta_occ2, vector<int> same_beta_occ,vector<int> diff_beta_occ,vector<int> diff_beta_index ,vector<int> same_beta_index){

    double retval = 0.0;
    retval += cal_CI_element_value2_hcore( alpha_occ1 , beta_occ2, same_beta_occ, diff_beta_occ, diff_beta_index ,same_beta_index);
    retval += cal_CI_element_value2_exchange( alpha_occ1 , beta_occ2, same_beta_occ, diff_beta_occ, diff_beta_index ,same_beta_index);
    retval += cal_CI_element_value2_repulsion( alpha_occ1 , beta_occ2, same_beta_occ, diff_beta_occ, diff_beta_index ,same_beta_index);
    return retval;
}
double CI_Occupation::cal_CI_element_value2_hcore(vector<int> alpha_occ1 ,vector<int> beta_occ2, vector<int> same_beta_occ,vector<int> diff_beta_occ,vector<int> diff_beta_index ,vector<int> same_beta_index){

    return pure_Hcore[diff_beta_occ[0]][diff_beta_occ[1]]*cal_gamma(diff_beta_occ[0],diff_beta_occ[1], beta_occ2);
}

double CI_Occupation::cal_CI_element_value2_exchange(vector<int> alpha_occ1 ,vector<int> beta_occ2, vector<int> same_beta_occ,vector<int> diff_beta_occ,vector<int> diff_beta_index ,vector<int> same_beta_index){

    double retval = 0.0;
    double gamma = cal_gamma(diff_beta_occ[0],diff_beta_occ[1], beta_occ2);
    if(same_beta_occ.size()!=0){
        for(int i=0; i<same_beta_occ.size(); i++){
            retval -=two_centered_integ[diff_beta_occ[0]][same_beta_occ[i]][same_beta_occ[i]][diff_beta_occ[1]]*gamma*0.5;
        }
    }
    return retval;
}

double CI_Occupation::cal_CI_element_value2_repulsion(vector<int> alpha_occ1 ,vector<int> beta_occ2, vector<int> same_beta_occ,vector<int> diff_beta_occ,vector<int> diff_beta_index ,vector<int> same_beta_index){

    double retval = 0.0;
    double gamma = cal_gamma(diff_beta_occ[0],diff_beta_occ[1], beta_occ2);
    if(same_beta_occ.size()!=0){
        for(int i=0; i<same_beta_occ.size(); i++){
            retval += two_centered_integ[diff_beta_occ[0]][diff_beta_occ[1]][same_beta_occ[i]][same_beta_occ[i]]*gamma;
        }
        //            cout << temp << endl;
        for(int i=0; i<same_beta_occ.size(); i++){
            vector<int> temp_occ = beta_occ2;
            temp_occ[same_beta_index[i]]=diff_beta_occ[0];
            Sorting(temp_occ);
            retval += two_centered_integ[diff_beta_occ[0]][same_beta_occ[i]][same_beta_occ[i]][diff_beta_occ[1]]*cal_gamma(same_beta_occ[i],diff_beta_occ[1], temp_occ)*cal_gamma(diff_beta_occ[0], same_beta_occ[i], beta_occ2)*0.5;
        }
    }
    for(int i=0; i<num_alpha_electrons; i++){
        retval += two_centered_integ[diff_beta_occ[0]][diff_beta_occ[1]][alpha_occ1[i]][alpha_occ1[i]]*gamma;
    }
    return retval;
}

double CI_Occupation::cal_CI_element_value3(vector<int> alpha_occ2,vector<int> beta_occ2,vector<int> diff_alpha_occ,vector<int> diff_beta_occ,vector<int> diff_alpha_index,vector<int> diff_beta_index){
    double retval = 0.0;
    retval += two_centered_integ[diff_alpha_occ[0]][diff_alpha_occ[1]][diff_beta_occ[0]][diff_beta_occ[1]]*cal_gamma(diff_beta_occ[0],diff_beta_occ[1], beta_occ2)*cal_gamma(diff_alpha_occ[0],diff_alpha_occ[1], alpha_occ2);
    return retval;
}
double CI_Occupation::cal_CI_element_value4(vector<int> alpha_occ2 ,vector<int> diff_alpha_occ,vector<int> diff_alpha_index){
    double retval = 0.0;
    vector<int> temp_occ = alpha_occ2;
    temp_occ[diff_alpha_index[0]]=diff_alpha_occ[0];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_alpha_occ[0]][diff_alpha_occ[2]][diff_alpha_occ[1]][diff_alpha_occ[3]]*cal_gamma(diff_alpha_occ[1],diff_alpha_occ[3], temp_occ)*cal_gamma(diff_alpha_occ[0], diff_alpha_occ[2], alpha_occ2)*0.5;

    temp_occ = alpha_occ2;
    temp_occ[diff_alpha_index[0]]=diff_alpha_occ[1];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_alpha_occ[1]][diff_alpha_occ[2]][diff_alpha_occ[0]][diff_alpha_occ[3]]*cal_gamma(diff_alpha_occ[0],diff_alpha_occ[3], temp_occ)*cal_gamma(diff_alpha_occ[1], diff_alpha_occ[2], alpha_occ2)*0.5;

    temp_occ = alpha_occ2;
    temp_occ[diff_alpha_index[1]]=diff_alpha_occ[0];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_alpha_occ[1]][diff_alpha_occ[2]][diff_alpha_occ[0]][diff_alpha_occ[3]]*cal_gamma(diff_alpha_occ[1],diff_alpha_occ[2], temp_occ)*cal_gamma(diff_alpha_occ[0], diff_alpha_occ[3], alpha_occ2)*0.5;

    temp_occ = alpha_occ2;
    temp_occ[diff_alpha_index[1]]=diff_alpha_occ[1];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_alpha_occ[0]][diff_alpha_occ[2]][diff_alpha_occ[1]][diff_alpha_occ[3]]*cal_gamma(diff_alpha_occ[0],diff_alpha_occ[2], temp_occ)*cal_gamma(diff_alpha_occ[1], diff_alpha_occ[3], alpha_occ2)*0.5;
    return retval ;
}
double CI_Occupation::cal_CI_element_value5(vector<int> beta_occ2 ,vector<int> diff_beta_occ,vector<int> diff_beta_index){

    double retval = 0.0;
    vector<int> temp_occ = beta_occ2;
    temp_occ[diff_beta_index[0]]=diff_beta_occ[0];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_beta_occ[0]][diff_beta_occ[2]][diff_beta_occ[1]][diff_beta_occ[3]]*cal_gamma(diff_beta_occ[1],diff_beta_occ[3], temp_occ)*cal_gamma(diff_beta_occ[0], diff_beta_occ[2], beta_occ2)*0.5;

    temp_occ = beta_occ2;
    temp_occ[diff_beta_index[0]]=diff_beta_occ[1];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_beta_occ[1]][diff_beta_occ[2]][diff_beta_occ[0]][diff_beta_occ[3]]*cal_gamma(diff_beta_occ[0],diff_beta_occ[3], temp_occ)*cal_gamma(diff_beta_occ[1], diff_beta_occ[2], beta_occ2)*0.5;

    temp_occ = beta_occ2;
    temp_occ[diff_beta_index[1]]=diff_beta_occ[0];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_beta_occ[1]][diff_beta_occ[2]][diff_beta_occ[0]][diff_beta_occ[3]]*cal_gamma(diff_beta_occ[1],diff_beta_occ[2], temp_occ)*cal_gamma(diff_beta_occ[0], diff_beta_occ[3], beta_occ2)*0.5;

    temp_occ = beta_occ2;
    temp_occ[diff_beta_index[1]]=diff_beta_occ[1];
    Sorting(temp_occ);
    retval += two_centered_integ[diff_beta_occ[0]][diff_beta_occ[2]][diff_beta_occ[1]][diff_beta_occ[3]]*cal_gamma(diff_beta_occ[0],diff_beta_occ[2], temp_occ)*cal_gamma(diff_beta_occ[1], diff_beta_occ[3], beta_occ2)*0.5;
    return retval;
}
void CI_Occupation::reduce_memory(){
    this -> two_centered_integ.clear();
    return;
}

inline void CI_Occupation::Sorting(vector<int>& occupation){
    for(int i=0; i<num_alpha_electrons-1;i++){
        if(occupation[i]>occupation[i+1]){
            int temp = occupation[i];
            occupation[i] = occupation[i+1];
            occupation[i+1] = temp;
        }
    }
    for(int i=num_alpha_electrons-1; i>0;i--){
        if(occupation[i]<occupation[i-1]){
            int temp = occupation[i];
            occupation[i] = occupation[i-1];
            occupation[i-1] = temp;
        }
    }
}
int CI_Occupation::cal_gamma(int p, int q, vector<int> occupation)
{
    int count1 = 0;
    int count2 = 0;
    int a = occupation.size();
    for(int i=0; i< std::min(a, p); i++)
    {
        if(occupation[i]<p)
            count1++;
    }
    for(int i=0; i<std::min(a, q); i++)
    {
        if(occupation[i]<q)
            count2++;
    }
    int ret_val = 1;
    if((count1+count2)%2==1)
        ret_val = -1;
    if (p>q)
        ret_val=ret_val*-1;
    return ret_val;
}
int CI_Occupation::get_num_occupied_orbitals(){
    return total_occupation[0][0][0].size();

}
int CI_Occupation::get_num_orbitals(){
    return num_orbitals;
}
int CI_Occupation::get_string_number(int excitation){
    return string_number[excitation];

}
void CI_Occupation::cal_two_centered_integ(Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > eigenvectors, RCP<Poisson_Solver> poisson_solver,RCP<const Basis> basis ){
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer -> start("time");

    this -> two_centered_integ.clear();
    this -> two_centered_integ.resize(num_orbitals);
    for(int i=0; i<num_orbitals; ++i){
        this -> two_centered_integ.at(i).resize(num_orbitals);
        for(int j=0; j<num_orbitals;++j){
            this -> two_centered_integ.at(i).at(j).resize(num_orbitals);
            for(int k = 0; k < num_orbitals; ++k){
                this -> two_centered_integ.at(i).at(j).at(k).resize(num_orbitals);
            }
        }
    }

    Teuchos::RCP<Epetra_MultiVector>  value_wavefunctions;
    for(int alpha=0; alpha < eigenvectors.size(); alpha++){
        int number_of_eigenvalues = eigenvectors[alpha]->NumVectors();
        value_wavefunctions =  Teuchos::rcp( new Epetra_MultiVector(eigenvectors[alpha]->Map(),number_of_eigenvalues) );
        Value_Coef::Value_Coef(basis,eigenvectors[alpha],false,true,value_wavefunctions);
    }
    for(int k=0; k<num_orbitals; k++)
    {
        for(int l=0; l<=k; l++)
        {
            //Verbose::single() << num_orbitals << " " << k << " " << l << endl;
            double temp = 0;
            Teuchos::RCP<Epetra_Vector> wavefunction_products2 = Teuchos::rcp(new Epetra_Vector(eigenvectors[0]->Map()));
            wavefunction_products2->Multiply(1.0, *(value_wavefunctions->operator()(k)), *(value_wavefunctions->operator()(l)), 0.0);
            Teuchos::RCP<Epetra_Vector>  K= Teuchos::rcp(new Epetra_Vector(eigenvectors[0]->Map()));
            Array<RCP<Epetra_Vector> > density;
            density.append(wavefunction_products2);
            poisson_solver->compute( density,K,temp);
            for(int i=0; i<=k; i++)
            {
                for(int j=0; j<=k;j++)
                {
                    Teuchos::RCP<Epetra_Vector> wavefunction_products1 = Teuchos::rcp(new Epetra_Vector(eigenvectors[0]->Map()));
                    wavefunction_products1->Multiply(1.0, *(eigenvectors[0]->operator()(j)), *(eigenvectors[0]->operator()(i)), 0.0);
                    double integrated_value = 0.0;
                    K->Dot(*wavefunction_products1->operator()(0), &integrated_value);
                    //integrated_value = Integration::integrate(basis, grid_setting, wavefunction_products1, true, K, true);
                    two_centered_integ[i][j][k][l] = integrated_value;
                    two_centered_integ[i][j][l][k] = integrated_value;
                    two_centered_integ[j][i][k][l] = integrated_value;
                    two_centered_integ[j][i][l][k] = integrated_value;
                    two_centered_integ[k][l][i][j] = integrated_value;
                    two_centered_integ[l][k][i][j] = integrated_value;
                    two_centered_integ[k][l][j][i] = integrated_value;
                    two_centered_integ[l][k][j][i] = integrated_value;
                    //two_centered_integ[i][j][k][l] = 0.003*(i+j+k+l);
                    //two_centered_integ[i][j][k][l] = 1.0;
                }
            }
        }
    }
    timer -> end("time");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "Calculate two centered integral time: " << timer -> get_elapsed_time("time", -1) << " s" << endl;
    //    cout << "aa" << endl;
    return;

}
void CI_Occupation::cal_CI_Hcore(Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > orbitals, Array< vector<double > > orbital_energies, RCP<const Basis> basis, bool is_DFT,Array<RCP<State> >& states, RCP<Exchange_Correlation> exchange_correlation, RCP<Poisson_Solver> poisson_solver, RCP<Core_Hamiltonian> core_hamiltonian){
    Verbose::single(Verbose::Detail) << "start Hcore" << endl;
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer -> start("time");

    this -> Hcore.clear();
    this -> Hcore.resize(num_orbitals);
    for(int i = 0; i < num_orbitals; ++i){
        this -> Hcore.at(i).resize(num_orbitals);
    }

    if(is_DFT){
        if(parameters->sublist("CIS").isParameter("HcoreByEigenvalues")){
            Array< RCP<Epetra_Vector> > density;
            Array< RCP<Epetra_MultiVector> > temp_orbitals;  //to convert from const multivector to multivector
            temp_orbitals.append(rcp(new Epetra_MultiVector(*orbitals[0])));
            Density_From_Orbitals::compute_total_density(basis, states[states.size()-1]->occupations,temp_orbitals, density);
            Teuchos::Array<Teuchos::RCP< const Epetra_Vector> > temp_density;  // to convert from vector to const vector
            temp_density.append(rcp(new Epetra_Vector(*density[0])));
            //cout << "4" << endl;
            auto map = basis->get_map();
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > x_potential;
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > c_potential;
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > xc_potential;
            Teuchos::RCP<Epetra_Vector>  hartree_potential;
            double hartree_energy = 0;
            poisson_solver->compute(density,hartree_potential,hartree_energy);
            x_potential.push_back(rcp(new Epetra_Vector(*map)));
            c_potential.push_back(rcp(new Epetra_Vector(*map)));
            xc_potential.push_back(rcp(new Epetra_Vector(*map)));
            //cout << "5" << endl;

            if (states[states.size()-1]->orbitals.size()==0){
                exchange_correlation->compute_vxc(states[states.size()-1], x_potential, c_potential);
                //exchange_correlation->compute_vxc(state->density, x_potential, c_potential, state->core_density, state->core_density_grad);
            }
            else{
                exchange_correlation->compute_vxc(states[states.size()-1], x_potential, c_potential);
                //exchange_correlation->compute_vxc(state->density, state->orbitals, state->occupations, x_potential, c_potential, state->core_density, state->core_density_grad);
            }
            xc_potential[0]->Update(1.0, *x_potential[0], 0.0);
            xc_potential[0]->Update(1.0, *c_potential[0], 1.0);
            //cout << "6" << endl;
            for(int i=0; i<num_orbitals; i++)
            {
                for(int j=0; j<=i; j++)
                {
                    Teuchos::RCP<Epetra_Vector> temp1 = Teuchos::rcp(new Epetra_Vector(orbitals[0]->Map()));
                    Teuchos::RCP<Epetra_Vector> temp2 = Teuchos::rcp(new Epetra_Vector(orbitals[0]->Map()));
                    Teuchos::RCP<Epetra_Vector> overlap_density = Teuchos::rcp(new Epetra_Vector(orbitals[0]->Map()));
                    temp1->Multiply(1.0, *temp_orbitals[0]->operator()(j), *xc_potential[0], 0.0);
                    double vxc = 0;
                    double hartree_energy = 0;
                    temp1->Dot(*temp_orbitals[0]->operator()(i), &vxc);

                    temp1->PutScalar(0.0);
                    temp1->Multiply(1.0, *temp_orbitals[0]->operator()(j),*hartree_potential, 0.0);
                    temp1->Dot(*temp_orbitals[0]->operator()(i), &hartree_energy);
                    Hcore[i][j] = -vxc - hartree_energy;
                    temp1 = Teuchos::null;
                    //cout << i << " " << j << endl;
                    //cout << orbital_energies[0][i] << endl;
                    if(i==j)
                        Hcore[i][j] += orbital_energies[0][i];
                    //out << i << " " << j << endl;
                    Hcore[j][i] = Hcore[i][j];
                }
            }
        }
        else{
            int number_of_eigenvalues = orbitals[0]->NumVectors();
            Teuchos::RCP<Epetra_MultiVector> temp1 = Teuchos::rcp(new Epetra_MultiVector(orbitals[0]->Map(), number_of_eigenvalues));
            Teuchos::RCP<Epetra_MultiVector> temp2 = Teuchos::rcp(new Epetra_MultiVector(*orbitals[0]));
            RCP<Epetra_CrsMatrix> core_hamiltonian_matrix = rcp(new Epetra_FECrsMatrix (Copy, *basis->get_map(),0 ));
            core_hamiltonian->get_core_hamiltonian(0,core_hamiltonian_matrix);
            core_hamiltonian_matrix->Multiply(false, *temp2, *temp1);

            //cout << "check2" << endl;
            for(int i=0; i<num_orbitals; i++)
            {
                for(int j=0; j<=i; j++)
                {
                    double temp = 0;
                    temp2->operator()(i)->Dot(*temp1->operator()(j), &temp);
                    Hcore[i][j] =temp;
                    //Hcore[i][j] = -0.3*(i+j);
                    Hcore[j][i] = Hcore[i][j];
                }
            }
            core_hamiltonian_matrix = Teuchos::null;

        }
    }
    else{
        for(int i=0; i<num_orbitals; i++){
            for(int j=0; j<=i; j++){
                Hcore[i][j] = 0.0;
                for(int k=0; k<num_alpha_electrons; k++){
             //       Hcore[i][j] -=two_centered_integ[i][k][j][j];// + two_centered_integ[i][j][j][k];
                    Hcore[i][j] -= (2*two_centered_integ[i][j][k][k] - two_centered_integ[i][k][k][j]);
                }
                if(i==j)
                    Hcore[i][j] += orbital_energies[0][i];
                Hcore[j][i] = Hcore[i][j];
            }
        }
    }

    timer -> end("time");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "Calculate CI_Hcore time: " << timer -> get_elapsed_time("time", -1) << " s" << endl;
}

void CI_Occupation::cal_CI_modified_Hcore(){
    for(int i=0; i<num_orbitals; i++){
        for(int j=0; j<num_orbitals; j++){
            for(int k=0; k<num_orbitals; k++){
                Hcore[i][j] -= 0.5*two_centered_integ[i][k][k][j];
            }
        }

    }
}

int CI_Occupation::distinguish_excitation(vector<int> occupation){
    int count = 0;
    for(int i=0; i<num_alpha_electrons; i++){
        if(occupation[i]>=num_alpha_electrons)
            count++;
    }
    return count;
}

double CI_Occupation::cal_sorted_CI_element(int index1, int index2){
    int alpha1, beta1, alpha2, beta2, alpha_address1, beta_address1,alpha_address2, beta_address2;
    check_category(index1, &alpha1, &beta1, &alpha_address1, &beta_address1);
    check_category(index2, &alpha2, &beta2, &alpha_address2, &beta_address2);
    vector<int> alpha_occ1;
    vector<int> alpha_occ2;
    vector<int> beta_occ1;
    vector<int> beta_occ2;
    vector<int> same_alpha_occ;
    vector<int> same_beta_occ;
    vector<int> diff_alpha_occ;
    vector<int> diff_beta_occ;
    vector<int> diff_beta_index;
    vector<int> diff_alpha_index;
    vector<int> same_beta_index;
    vector<int> same_alpha_index;
    //cout << get_sign(alpha2, alpha_address2) << " " << get_sign(alpha1, alpha_address1) << endl;
    check_same_diff_occupation(index1, index2, alpha_occ1, beta_occ1, alpha_occ2, beta_occ2, same_alpha_occ, diff_alpha_occ, same_beta_occ, diff_beta_occ, diff_alpha_index, diff_beta_index, same_alpha_index, same_beta_index);
    double retval = 0;
    if(diff_alpha_occ.size() + diff_beta_occ.size() > 4){
        return retval;
    }
    else if(diff_alpha_occ.size()==2 and diff_beta_occ.size()==0){
        retval = cal_CI_element_value1(beta_occ1, alpha_occ2, same_alpha_occ, diff_alpha_occ, diff_alpha_index, same_alpha_index)*get_sign(alpha2, alpha_address2)*get_sign(alpha1, alpha_address1);
    }
    else if(diff_alpha_occ.size()==0 and diff_beta_occ.size()==2){
        retval = cal_CI_element_value2(alpha_occ1 , beta_occ2, same_beta_occ, diff_beta_occ,diff_beta_index ,same_beta_index)*get_sign(beta2, beta_address2)*get_sign(beta1, beta_address1);
    }
    else if(diff_alpha_occ.size()==2 and diff_beta_occ.size()==2){
        retval = cal_CI_element_value3(alpha_occ2,beta_occ2, diff_alpha_occ, diff_beta_occ, diff_alpha_index, diff_beta_index)*get_sign(alpha2, alpha_address2)*get_sign(alpha1, alpha_address1)*get_sign(beta2, beta_address2)*get_sign(beta1, beta_address1);
    }
    else if(diff_alpha_occ.size()==4 and diff_beta_occ.size()==0){
        retval = cal_CI_element_value4(alpha_occ2, diff_alpha_occ, diff_alpha_index)*get_sign(alpha1, alpha_address1)*get_sign(alpha2, alpha_address2);
    }
    else if(diff_alpha_occ.size()==0 and diff_beta_occ.size()==4){
        retval = cal_CI_element_value5(beta_occ2 ,diff_beta_occ, diff_beta_index)*get_sign(beta1, beta_address1)*get_sign(beta2, beta_address2);
    }
    return retval;
}
void CI_Occupation::check_same_diff_occupation(vector<int> alpha_occu1,vector<int> beta_occu1,vector<int> alpha_occu2,vector<int> beta_occu2, vector<int>& same_alpha_occ, vector<int>& diff_alpha_occ, vector<int>& same_beta_occ,vector<int>& diff_beta_occ, vector<int>& diff_alpha_index,vector<int>& diff_beta_index, vector<int>& same_alpha_index,vector<int>& same_beta_index){
    for(int i=0; i<num_alpha_electrons; i++){
        vector<int>::iterator temp = find(alpha_occu2.begin(), alpha_occu2.end(), alpha_occu1[i]);

        if( temp!= alpha_occu2.end()){
            same_alpha_occ.push_back(alpha_occu1[i]);
        }
        else{
            diff_alpha_occ.push_back(alpha_occu1[i]);
        }
    }
    for(int i=0; i<num_alpha_electrons; i++){
        vector<int>::iterator temp = find(same_alpha_occ.begin(), same_alpha_occ.end(), alpha_occu2[i]);
        if(temp == same_alpha_occ.end()){
            diff_alpha_occ.push_back(alpha_occu2[i]);
            diff_alpha_index.push_back(i);
        }
        else{

            //size_t index = temp - alpha_occu2.begin();
            same_alpha_index.push_back(i);
        }
    }
    for(int i=0; i<num_alpha_electrons; i++){
        vector<int>::iterator temp = find(beta_occu2.begin(), beta_occu2.end(), beta_occu1[i]);
        if( temp!= beta_occu2.end()){
            same_beta_occ.push_back(beta_occu1[i]);
        }
        else{
            diff_beta_occ.push_back(beta_occu1[i]);
        }
    }
    for(int i=0; i<num_beta_electrons; i++){
        vector<int>::iterator temp = find(same_beta_occ.begin(), same_beta_occ.end(), beta_occu2[i]);
        if(temp == same_beta_occ.end()){
            diff_beta_occ.push_back(beta_occu2[i]);
            //size_t index = temp - beta_occu2.begin();
            diff_beta_index.push_back(i);
        }
        else{
            same_beta_index.push_back(i);
        }
    }
    return;
}

void CI_Occupation::check_same_diff_occupation(int index1, int index2, int& alpha1, int& alpha2, int& beta1, int& beta2, int& alpha_address1, int& alpha_address2, int& beta_address1, int& beta_address2, vector<int>& alpha_occu1, vector<int>& beta_occu1,vector<int>& alpha_occu2,vector<int>& beta_occu2, vector<int>& same_alpha_occ, vector<int>& diff_alpha_occ, vector<int>& same_beta_occ,vector<int>& diff_beta_occ, vector<int>& diff_alpha_index,vector<int>& diff_beta_index, vector<int>& same_alpha_index,vector<int>& same_beta_index){
    check_category(index1, &alpha1, &beta1, &alpha_address1, &beta_address1);
    check_category(index2, &alpha2, &beta2, &alpha_address2, &beta_address2);
    alpha_occu1 = total_occupation[0][alpha1][alpha_address1];
    beta_occu1 = total_occupation[0][beta1][beta_address1];
    alpha_occu2 = total_occupation[0][alpha2][alpha_address2];
    beta_occu2 = total_occupation[0][beta2][beta_address2];
    check_same_diff_occupation(alpha_occu1, beta_occu1, alpha_occu2, beta_occu2, same_alpha_occ, diff_alpha_occ, same_beta_occ, diff_beta_occ, diff_alpha_index, diff_beta_index, same_alpha_index, same_beta_index);
}
void CI_Occupation::check_same_diff_occupation(int index1, int index2, vector<int>& alpha_occu1, vector<int>& beta_occu1,vector<int>& alpha_occu2,vector<int>& beta_occu2, vector<int>& same_alpha_occ, vector<int>& diff_alpha_occ, vector<int>& same_beta_occ,vector<int>& diff_beta_occ, vector<int>& diff_alpha_index,vector<int>& diff_beta_index, vector<int>& same_alpha_index,vector<int>& same_beta_index){
    int alpha1, alpha2, beta1, beta2, alpha_address1, alpha_address2, beta_address1, beta_address2;
    check_category(index1, &alpha1, &beta1, &alpha_address1, &beta_address1);
    check_category(index2, &alpha2, &beta2, &alpha_address2, &beta_address2);
    alpha_occu1 = total_occupation[0][alpha1][alpha_address1];
    beta_occu1 = total_occupation[0][beta1][beta_address1];
    alpha_occu2 = total_occupation[0][alpha2][alpha_address2];
    beta_occu2 = total_occupation[0][beta2][beta_address2];
    check_same_diff_occupation(alpha_occu1, beta_occu1, alpha_occu2, beta_occu2, same_alpha_occ, diff_alpha_occ, same_beta_occ, diff_beta_occ, diff_alpha_index, diff_beta_index, same_alpha_index, same_beta_index);
}

int CI_Occupation::get_sign(int excitation, int address){
    int abs_address = address;
    for(int i=0; i<excitation;i++){
        abs_address += string_number[i];
    }
    return sign_list[abs_address];
}
double CI_Occupation::cal_H0_value(vector<int> alpha_occupation,vector<int> beta_occupation){
    return (cal_H0_value_hcore(alpha_occupation, beta_occupation) + cal_H0_value_exchange(alpha_occupation, beta_occupation) +cal_H0_value_repulsion(alpha_occupation, beta_occupation) );
}

double CI_Occupation::cal_H0_value_hcore(vector<int> alpha_occupation,vector<int> beta_occupation){
    double temp = 0;
    //cal core energy of alpha and beta electrons
    for(int i=0; i<num_alpha_electrons; i++){
        temp +=Hcore[alpha_occupation[i]][alpha_occupation[i]];
        temp +=Hcore[beta_occupation[i]][beta_occupation[i]];
        for(int l =0; l<num_orbitals; l++)
            temp += (two_centered_integ[alpha_occupation[i]][l][l][alpha_occupation[i]] + two_centered_integ[beta_occupation[i]][l][l][beta_occupation[i]])*0.5;
    }
    return temp;
}
double CI_Occupation::cal_H0_value_exchange(vector<int> alpha_occupation,vector<int> beta_occupation){
    double temp = 0.0;
    for(int i=0; i<num_alpha_electrons; i++){
        for(int j=0; j<num_alpha_electrons; j++){
            temp -= two_centered_integ[alpha_occupation[i]][alpha_occupation[j]][alpha_occupation[j]][alpha_occupation[i]]*0.5;
            temp -= two_centered_integ[beta_occupation[i]][beta_occupation[j]][beta_occupation[j]][beta_occupation[i]]*0.5;
        }
    }
    return temp;
}
double CI_Occupation::cal_H0_value_repulsion(vector<int> alpha_occupation,vector<int> beta_occupation){
    double temp = 0.0;
    for(int i=0; i<num_alpha_electrons; i++){
        for(int j=0; j<num_alpha_electrons; j++){
            temp += two_centered_integ[alpha_occupation[i]][alpha_occupation[i]][alpha_occupation[j]][alpha_occupation[j]]*0.5;
            temp += two_centered_integ[beta_occupation[i]][beta_occupation[i]][beta_occupation[j]][beta_occupation[j]]*0.5;
        }
    }
    for(int i=0; i<num_alpha_electrons; i++){
        for(int j=0; j<num_alpha_electrons; j++){
            temp += two_centered_integ[alpha_occupation[i]][alpha_occupation[i]][beta_occupation[j]][beta_occupation[j]];
        }
    }
    return temp;

}
void CI_Occupation::cal_H0(Teuchos::RCP<Epetra_Vector> H0){
    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer -> start("time");

    int alpha1, beta1, alpha2, beta2, alpha_address1, beta_address1,alpha_address2, beta_address2;
    for(int k=0; k<total_num_string;k++){
        if(H0->Map().LID(k)>=0){
            //cout << k << endl;
            double temp = 0;
            check_category(k, &alpha1, &beta1, &alpha_address1, &beta_address1);
            vector<int> alpha_occupation = total_occupation[0][alpha1][alpha_address1];
            vector<int> beta_occupation = total_occupation[0][beta1][beta_address1];
            H0->operator[](H0->Map().LID(k)) =cal_H0_value(alpha_occupation, beta_occupation) ;
        }

    }
    timer -> end("time");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "Calculate H0 time: " << timer -> get_elapsed_time("time", -1) << " s" << endl;
    return ;
}

std::vector< std::vector<int> > CI_Occupation::get_check_address(){
    return check_address;
}

int CI_Occupation::find_pair_spin(int index){
    int alpha1, beta1, alpha2, beta2, alpha_address1, beta_address1,alpha_address2, beta_address2;
    check_category(index, &alpha1, &beta1, &alpha_address1, &beta_address1);
    alpha2 = beta1;
    beta2 = alpha1;
    alpha_address2 = beta_address1;
    beta_address2 = alpha_address1;
    return check_address[alpha2][beta2]+alpha_address2+string_number[alpha2]*beta_address2;
}
void CI_Occupation::print_configuration(vector<int> occupation1, vector<int> occupation2, int alpha1, int beta1){
    Verbose::set_numformat(Verbose::Occupation);
    if(alpha1!=0){
        Verbose::single(Verbose::Simple) << "alpha occupation : ";
        for(int i=0; i<num_alpha_electrons; i++){
            int count = 0;
            for(int j=0; j<num_alpha_electrons; j++){
                if(occupation1[j]==i){
                    count++;
                }
            }
            if(count == 0)
                Verbose::single(Verbose::Simple) << "[" << i << "]";
        }
        Verbose::single(Verbose::Simple) << " => ";
        for(int i=0; i<num_alpha_electrons; i++){
            if(occupation1[i]>=num_alpha_electrons)
                Verbose::single(Verbose::Simple) << "[" << occupation1[i] << "]";
        }
    }
    else
        Verbose::single(Verbose::Simple) << "alpha occupation is ground ";
    Verbose::single(Verbose::Simple) << " , ";
    if(beta1!=0){
        Verbose::single(Verbose::Simple) << "beta occupation : ";
        for(int i=0; i<num_alpha_electrons; i++){
            int count = 0;
            for(int j=0; j<num_alpha_electrons; j++){
                if(occupation2[j]==i){
                    count++;
                }
            }
            if(count == 0)
                Verbose::single(Verbose::Simple) << "[" << i << "]";
        }
        Verbose::single(Verbose::Simple) << " => ";
        for(int i=0; i<num_alpha_electrons; i++){
            if(occupation2[i]>=num_alpha_electrons)
                Verbose::single(Verbose::Simple) << "[" << occupation2[i] << "]";
        }
        //Verbose::single() << endl;
    }
    else
        Verbose::single(Verbose::Simple) << "beta occupation is ground ";
}

void CI_Occupation::print_configuration(int configuration){
    int alpha1, beta1, alpha2, beta2, alpha_address1, beta_address1,alpha_address2, beta_address2;
    check_category(configuration, &alpha1, &beta1, &alpha_address1, &beta_address1);
    vector<int> occupation1 = total_occupation[0][alpha1][alpha_address1];
    vector<int> occupation2 = total_occupation[0][beta1][beta_address1];
    print_configuration(occupation1, occupation2, alpha1, beta1);
}

int CI_Occupation::get_address(int excitation,vector<int> occupation){
    int address;
    address = 0;
    if(excitation==0)
        address = 0;
    else if(excitation==1){
        for(int i=0; i<num_alpha_electrons; i++){
            address = address + SY1[occupation[i]+1][i+1];
        }
    }
    else if(excitation==2){
        for(int i=0; i<num_alpha_electrons; i++)
            address = address + DY1[occupation[i]+1][i+1];

    }
    return address;
}
void CI_Occupation::cal_pure_Hcore(){
    this -> pure_Hcore.clear();
    this -> pure_Hcore.resize(num_orbitals);
    for(int i=0; i<num_orbitals; i++){
        this -> pure_Hcore.at(i).resize(num_orbitals);
    }

    for(int i=0; i<num_orbitals;i++){
        for(int j=0; j<num_orbitals; j++){
            pure_Hcore[i][j] = Hcore[i][j];
            for(int k=0; k<num_orbitals; k++)
            {
                pure_Hcore[i][j] = pure_Hcore[i][j] + 0.5*two_centered_integ[i][k][k][j];
            }
        }
    }
    return;
}
int CI_Occupation::get_total_num_string(){
    return total_num_string;
}

std::vector< std::vector<int> > CI_Occupation::cal_W(int nelectrons, int norbitals, int excitation){
    vector< vector<int> > W(norbitals+1);
    for(int i=0; i<norbitals+1;i++){
        W[i].resize(nelectrons+1);
    }

    W[0][0] = 1;
    for(int k = 0; k<nelectrons+1; k++){
        for(int m=0; m<nelectrons+1-excitation; m++){
            if (k<1 or m<1){
                W.at(k).at(m) = 1;
            } else {
                W.at(k).at(m) = W[k-1][m] + W[k-1][m-1];
            }
            if(m>k or k>excitation+m){
                W.at(k).at(m) = 0;
            }
        }
    }
    for(int k = nelectrons+1; k<norbitals+1; k++){
        for(int m=nelectrons-excitation; m<nelectrons+1; m++){
            if (m==nelectrons-excitation or -k+m==norbitals-nelectrons){
                W.at(k).at(m) = W[nelectrons][nelectrons-excitation];
            } else {
                W.at(k).at(m) = W[k-1][m] + W[k-1][m-1];
            }
            if(-k+m>-excitation or -k+m<-(norbitals-nelectrons)){
                W.at(k).at(m) = 0;
            }
        }
    }
    return W;
}

std::vector< std::vector<int> >  CI_Occupation::cal_Y(int nelectrons, int norbitals, int excitation){
    vector< vector<int> > W = cal_W(nelectrons, norbitals, excitation);
    vector< vector<int> > Y(num_orbitals+1);
    for(int i = 0; i < norbitals+1; ++i){
        Y[i].resize(nelectrons+1);
    }

    for(int k = 0; k<nelectrons+1; k++){
        for(int m=0; m<nelectrons+1-excitation; m++){
            if (k-1>=0 and m-1>=0){
                Y.at(k).at(m) = W.at(k).at(m) - W.at(k-1).at(m-1);
            } else {
                Y.at(k).at(m) = 0;
            }
        }
    }

    for(int k = nelectrons+1; k<norbitals+1; k++){
        for(int m=nelectrons+1-excitation; m<nelectrons+1; m++){
            if (m>=nelectrons-excitation and k-m>excitation){
                Y.at(k).at(m) = W.at(k).at(m) - W.at(k-1).at(m-1);
            } else {
                Y.at(k).at(m) = 0;
            }
        }
    }
    Y.at(nelectrons).at(nelectrons-excitation) = W.at(nelectrons).at(nelectrons-excitation)-W.at(nelectrons-1).at(nelectrons-excitation-1);
    return Y;
}

int CI_Occupation::Eij(vector<int> occupation, int p, int q){
    for(int i=0; i<num_alpha_electrons; i++){
        if(occupation[i]==p and i!=q){
            return 0;
        }
    }
    //return 1;
    return cal_gamma(p, occupation[q], occupation);
}

vector< vector<int> > CI_Occupation::make_occupation(int num_electrons, int num_orbitals,int start_orbitals){
    vector<int> new_occupation;
    vector< vector<int> > occupation_list;
    for(int i=0; i<num_electrons; i++)
        new_occupation.push_back(i+start_orbitals);

    occupation_list.push_back(new_occupation);
    int i=0;
    while(i<num_electrons){
        if (i<num_electrons-1){
            if(new_occupation[i]+1<new_occupation[i+1]){
                new_occupation[i] = new_occupation[i]+1;
                if(i>0){
                    for(int j=0; j<i; j++){
                        new_occupation[j] = j+start_orbitals;
                    }
                }
                occupation_list.push_back(new_occupation);
                i=0;
            } else {
                i++;
            }
        } else if(i==num_electrons-1 and new_occupation[i] <num_orbitals-1+start_orbitals) {
            new_occupation[i] = new_occupation[i]+1;
            for(int j=0; j<i; j++){
                new_occupation[j] = j+start_orbitals;
            }
            occupation_list.push_back(new_occupation);
            i=0;
        } else {
            break;
        }
    }

    return occupation_list;
}

void CI_Occupation::make_restricted_occupation(int max_excitation){
    vector< vector< vector<int> > > alpha_occupation;
    vector< vector< vector<int> > > beta_occupation;
    vector<int> temp_merge;
    vector< vector<int> > temp_occupation;
    vector< vector<int> > temp_occupation2;
    temp_occupation = make_occupation(num_alpha_electrons, num_alpha_electrons, 0);
    alpha_occupation.push_back(temp_occupation);

    vector< vector<int> > middle_occupation;

    string_number = array<int, 3>();

    string_number[0] =1;
    for(int excite=1; excite < max_excitation+1; excite++) {
        if(num_alpha_electrons < excite){
            string_number[excite] = 0;
            break;
        }
        temp_occupation.clear();
        temp_occupation2.clear();
        temp_occupation = make_occupation(num_alpha_electrons-excite, num_alpha_electrons, 0);
        temp_occupation2 = make_occupation(excite, num_orbitals-num_alpha_electrons, num_alpha_electrons);
        for(int j=0; j<temp_occupation2.size(); j++) {
            for(int i=0; i<temp_occupation.size(); i++) {
                temp_merge.clear();
                temp_merge = temp_occupation[i];
                temp_merge.insert(temp_merge.end(), temp_occupation2[j].begin(), temp_occupation2[j].end());
                middle_occupation.push_back(temp_merge);
            }
        }
        alpha_occupation.push_back(middle_occupation);
        middle_occupation.clear();

        string_number[excite] = alpha_occupation[excite].size();
    }
    total_occupation.push_back(alpha_occupation);
}

int CI_Occupation::cal_single_sign(int address){
    vector<int> occupation;
    vector<int> ground_occupation;
    occupation = total_occupation[0][1][address];
    ground_occupation = total_occupation[0][0][0];

    int different;
    for(int i=0; i<num_alpha_electrons; i++) {
        if(ground_occupation[i]!=occupation[i]) {
            different = i;
            break;
        }
    }
    int count = 0;
    int sign = -1;
    if((different + num_alpha_electrons)%2==1) {
        sign = 1;
    }
    return sign;
}

void CI_Occupation::check_category(int coordinate, int* alpha, int* beta, int* alpha_address, int* beta_address, vector<int>& alpha_occ, vector<int>& beta_occ){
    this -> check_category(coordinate, alpha, beta, alpha_address, beta_address);
    alpha_occ = total_occupation[0][*alpha][*alpha_address];
    beta_occ = total_occupation[0][*beta][*beta_address];
}
