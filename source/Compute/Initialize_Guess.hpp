#pragma once
//#include <time.h>
#include "Epetra_MultiVector.h"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Guess.hpp"
#include "../State/State.hpp"

//class State;

class Initialize_Guess: public Guess{
  public:
    Initialize_Guess(
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations,
        Teuchos::RCP<const Atoms> atoms
    );

    int compute(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::Array< Teuchos::RCP<State> >& states
    );
};

