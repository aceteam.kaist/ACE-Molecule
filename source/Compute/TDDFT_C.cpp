#include "TDDFT_C.hpp"
#include <complex>
#include <stdexcept>
#include <cmath>

#include "Epetra_CrsMatrix.h"

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Basis/Basis_Function/Finite_Difference.hpp"
#include "../Basis/Basis_Function/Sinc.hpp"
#include "../Utility/Two_e_integral.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Calculus/Integration.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/String_Util.hpp"

#define DEBUG (1)// 0 turns off fill-time output.

using std::abs;
using std::vector;
using std::endl;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

TDDFT_C::TDDFT_C(RCP<const Basis> basis, RCP<Exchange_Correlation> exchange_correlation, RCP<Poisson_Solver> poisson_solver, RCP<Teuchos::ParameterList> parameters)
: TDDFT::TDDFT(basis, exchange_correlation, poisson_solver, parameters){
    if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") == "TammDancoff"){
        string errmsg = "TDDFT - Class and TheoryLevel mismatch! (TDDFT_C with TammDancoff)!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
    this -> type = "C";
    this -> parameters -> sublist("TDDFT").sublist("Diagonalize").get<bool>("SymmReal", true);
    if(this -> _exchange_kernel == "HF" or this -> _exchange_kernel == "HF-EXX"){
        string errmsg = "TDDFT::compute - Class and ExchangeKernel mismatch! (TDDFT_C with " + this -> _exchange_kernel + ")!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
}

void TDDFT_C::get_excitations(
        Teuchos::RCP<Diagonalize> diagonalize, 
        std::vector<double> &energies, 
        Teuchos::RCP<Epetra_MultiVector> &eigenvectors
){
    energies = diagonalize->get_eigenvalues();

    if(num_excitation!=energies.size()){
        Verbose::single(Verbose::Simple) << "NumberOfStates(" << num_excitation<< ") is changed to "<< energies.size() << "." <<std::endl;
        num_excitation = energies.size();
    }

    std::vector<double> excitation_energies(energies);
    // Verify excitation energies
    for(int i=0; i<num_excitation; ++i){
        if(excitation_energies[i] < 0.0){
            Verbose::single(Verbose::Simple) << "TDDFT::compute - WARNING: Casida excitation energy " << excitation_energies[i] << " is negative, which should not.\n";
            excitation_energies[i] = -sqrt(-excitation_energies[i]);
        }
        else{
            excitation_energies[i] = sqrt(excitation_energies[i]);
        }
    }
    // end
    energies = excitation_energies;
    eigenvectors = diagonalize -> get_eigenvectors();
}

void TDDFT_C::fill_TD_matrix(
    Teuchos::Array< vector<double> > orbital_energies,
    double Hartree_contrib, double xc_contrib,
    int matrix_index_c, int matrix_index_r,
    RCP<Epetra_CrsMatrix>& sparse_matrix
){
    int s1, i, a, s2, j, b;
    this -> unpack_index(matrix_index_c, s1, i, a);
    this -> unpack_index(matrix_index_r, s2, j, b);
    double eps_ai = orbital_energies[s1][a] - orbital_energies[s1][i];

    double element_tmp = 2. * sqrt(eps_ai)
        * sqrt(orbital_energies[s2][b] - orbital_energies[s2][j]) * (Hartree_contrib + xc_contrib);
    // *2 from restricted.
    if(!this -> is_open_shell){
        element_tmp *= 2;
    }
    if(i==j and a==b and s1 == s2){
        element_tmp += eps_ai*eps_ai;
    }
    if(std::fabs(element_tmp) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            if(sparse_matrix->MyGlobalRow(matrix_index_c) ){
                sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
            }
            if(sparse_matrix->MyGlobalRow(matrix_index_r)) {
                sparse_matrix->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
            }
        }
        else{
            sparse_matrix->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
        }
    }
}

void TDDFT_C::fill_TD_matrix_restricted(
    vector<double> orbital_energies,
    double Hartree_contrib, vector<double> xc_contrib,
    int matrix_index_c, int matrix_index_r,
    Array< RCP<Epetra_CrsMatrix> >& sparse_matrix
){
    int s1, i, a, s2, j, b;
    this -> unpack_index(matrix_index_c, s1, i, a);
    this -> unpack_index(matrix_index_r, s2, j, b);
    assert(s1 == 0); assert(s2 == 0);
    double eps_ai = orbital_energies[a] - orbital_energies[i];

    // *2 because both up and down spin should be considered.
    double element_tmp = 4.0 * sqrt(eps_ai)
        * sqrt(orbital_energies[b] - orbital_energies[j]) * (Hartree_contrib + xc_contrib[0]);
    if(i==j and a==b){
        element_tmp += eps_ai*eps_ai;
    }
    if(std::fabs(element_tmp) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_c) ){
                sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
            }
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_r)) {
                sparse_matrix[0]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
            }
        }
        else{
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
        }
    }
    if(sparse_matrix.size() > 1){
        element_tmp = xc_contrib[1];
        if(i==j and a==b){
            element_tmp += eps_ai*eps_ai;
        }
        element_tmp += 4.0 * sqrt(eps_ai)
            * sqrt(orbital_energies[b] - orbital_energies[j]) * xc_contrib[1];
        if(std::fabs(element_tmp) > this -> cutoff){
            if(matrix_index_c!=matrix_index_r){
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_c) ){
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
                }
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_r)) {
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
                }
            }
            else{
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
            }
        }
    }
}

void TDDFT_C::Kernel_TDHF_X(
        Teuchos::RCP<State> state, 
        Teuchos::RCP<Epetra_CrsMatrix>& sparse_matrix, 
        bool kernel_is_not_hybrid, 
        bool add_delta_term, double scale/* = 1.0*/
){
    throw std::logic_error("Not implemented!");
    return;
}

std::vector< std::vector<double> > TDDFT_C::gather_z_vector(
        RCP<Epetra_MultiVector> input, 
        Teuchos::Array< std::vector<double> > eigvals
){
    int num_vec = input -> NumVectors();
    int total_size = input -> Map().NumGlobalElements();
    double ** recv = new double*[num_vec];
    for(int i = 0; i < num_vec; ++i){
        recv[i] = new double[total_size];
    }
    Parallel_Util::group_allgather_multivector(input, recv);
    vector< vector<double> > output(num_vec);
    for(int i = 0; i < num_vec; ++i){
        output[i] = vector<double>(recv[i], recv[i]+total_size);
    }
    for(int i = 0; i < num_vec; ++i){
        delete[] recv[i];
    }
    delete[] recv;
    return output;
}
