#pragma once
#include <vector>

#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"

#include "../Basis/Basis.hpp"

//#include "../Io/Io.hpp"
#include "../Io/Atoms.hpp"
#include "../State/State.hpp"
#include "Create_Compute_Interface.hpp"
#include <gsl/gsl_vector.h>


class Opt: public Compute_Interface{
    public:
        Opt(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<Atoms> atoms,
            Teuchos::Array< Teuchos::RCP<State> > states,
            Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > array_parameters
        );
 
        int compute(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<State> >& states
        );

    private:
 
        void update_geometry(std::vector<double> updated_geometry);

        static double single_point_calculation(
            const gsl_vector *x,
            void *params
        );

        static void gradient_calculation(
            const gsl_vector *x,
            void *params,
            gsl_vector *gsl_gradient
        );
    
        static void energy_and_gradient(
            const gsl_vector *x,
            void *params,
            double *total_energy,
            gsl_vector *gsl_gradient
        );

        void spcal(
            double *energy
        );

        void gradcal(
            gsl_vector *gsl_gradient
        );

        double get_maxval(
            std::vector<double> target
        );

    protected:
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<Atoms> atoms;
        Teuchos::Array< Teuchos::RCP<State> > states;
        Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > array_parameters;
        Teuchos::Array< Teuchos::RCP<Compute> > opt_computes;
        int num_sp_cal = 0;
        int num_grad_cal = 0;
    
        double tot_energy;
        Teuchos::Array< Teuchos::RCP<State> > before_states;
        std::vector<double> before_geometry;
        std::vector<double> current_geometry;
        std::vector<double> current_gradient;
        double maxdr = 0.0;
        double maxgrad = 0.0;

        int opt_method;
        int max_cycle;
        double force_tolerance;
        double dPosition_tolerance;
};
