#pragma once
#include <string>
#include "Compute_XC.hpp"
#include "../State/XC_State.hpp"
#include "Poisson_Solver.hpp"

/**
 * @brief The class actually performs the calculation of KLI-approximated exact-exchange functionals.
 * @details KLI approximation is implemented.<br/>
 * Reference for KLI exchange functional : J. B. Krieger, Y. Li, and G. J. Iafrate, <i>Phys. Lett. A</i> <b>148</b>, 470–474 (1990).<br/>
 * Variable names and functions are obtained from the following reference. <br>
 *    Y. Kim, M. Staedele, and R. Martin, <i>Phys. Rev. A</i> <b>60</b>(5), 3633–3640 (1999).<br/>
 * See also : J. Kim, K. Hong, S. choi, and W. Y. Kim, <i>Bull. Korean Chem. Soc.</i> <b>36</b>(3), 998-1007 (2015)
 */


class KLI: public Compute_XC{
    public:
        /**
         * @brief Constructor of KLI class
         * @param[in] basis Basis data
         * @param[in] function_id Functional ID of KLI
         * @param[in] poisson_solver Poisson_Solver that perform two-electron intergral.
         * @param[in] PD_cutoff pair density cutoff value
         * @callergraph
         * @callgraph
         */
        KLI(
            Teuchos::RCP<const Basis> basis,
            int function_id,
            Teuchos::RCP<Poisson_Solver> poisson_solver,
            double PD_cutoff/*=-1.0*/
        );

        /**
         * @brief Calculate exchange-correlation energy and potential of given XC_State.
         * @return 0 (normal termination) The computed exchange energy is replaced to exchange_energy, and the exchange_potential is replaced for the last Scf_State.
         * @param[in,out] xc_info State type object containing all exchange-correlation infomration
         * @callergraph
         * @callgraph
         */
        int compute(
            Teuchos::RCP<XC_State> &xc_info
        );

        /**
         * @details Note that this routine DOES NOT WORK! Without orbital informaiton, KLI exchange cannot be calculated.
         * @return Error termination
         * @param[in,out] xc_info State type object containing all exchange-correlation infomration
         * @callergraph
         * @callgraph
         */
        void compute_Exc(
            Teuchos::RCP<XC_State> &xc_info
        );

        /**
         * @brief Calculate exchange-correlation potential.
         * @details Note that this routine DOES NOT WORK! Without orbital informaiton, KLI exchange cannot be calculated.
         * @return Error termination
         * @param[in,out] xc_info State type object containing all exchange-correlation infomration
         * @callergraph
         * @callgraph
         */
        void compute_vxc(
            Teuchos::RCP<XC_State> &xc_info
        );


        /**
         * @brief Calculate exchange-correlation kernel with orbitals
         * @details It gives nothing but print that "KLI-PGG exchange kernel will be used."
         * @return Nothing.
         * @param[in,out] xc_info State type object containing all exchange-correlation infomration
         * @callergraph
         * @callgraph
         */
        void compute_kernel(
            Teuchos::RCP<XC_State> &xc_info
        );

        /**
         * @brief Return functional type for TDDFT class
         * @return "KLI-PGG"
         * @callergraph
         * @callgraph
         */
        std::string get_functional_type();

        /**
         * @brief Return functional type for human-readable output
         * @return Description about functional type defined in this object.
         * @param[in] verbose boolean type variable, default = true. If verbose = false, this method will return "KLI" only.
         * @callergraph
         * @callgraph
         */
        virtual std::string get_info_string(bool verbose = true);

        /**
         * @brief Return the kind of the functional in this class.
         * @detail See Exchange_Corrlation class
         * @return 0
         * @callergraph
         * @callgraph
         */
        virtual int get_functional_kind();

    private:
        /**
         * @brief Functional ID of Libxc.
         * @details
         * - -10 : no exchange potential(only energy)
         * - -11 : Slater potential
         * - -12 : OEP-EXX potential with KLI approximation
         * - -13 : OEP-EXX potential(not implemented)
         */
        int function_id;

        /**
         * @brief Pair density value cutoff.
         * @details Default value is -1, which is defined in Create_Compute class.
         * Used in the method check_PD_norm. <br/>
         * If PD_cutoff>0, the term including the negligible pair density will be ignored. See check_PD_norm.
         * @callergraph
         * @callgraph
         */
        double PD_cutoff;

        /**
         * @brief Number of electorns in the system for each spin
         * @details neutral, ground state hydrogen molecule -> number of orbitals (could be) 12, index of homo and number of electrons is equal to 1
         */
        std::vector<int> number_of_electrons; ///< Number of electorns in the system (i.e. with alpha spin)
        /**
         * @brief Number of orbitals in State for each spin
         * @details neutral, ground state hydrogen molecule -> number of orbitals (could be) 12, index of homo and number of electrons is equal to 1
         */
        std::vector<int> number_of_orbitals;
        /**
         * @brief index of HOMO for each spin orbitals
         * @details neutral, ground state hydrogen molecule -> number of orbitals (could be) 12, index of homo and number of electrons is equal to 1
         */
        std::vector<int> index_of_HOMO;

        /**
         * @brief Orbital index that will start KLI computation from here.
         **/
        std::vector<int> ind_start;
        /**
         * @brief NOT USED VARIABLE
         * @todo Check any possibility that this variable is useful.
         * @detail This variable modify the orbiatl range for KLI exchange calculation. NOT USED, and cannot be modified from program input.
         **/
        std::array<int, 2> orbital_range_mod;

/////////////////////////////////////////////////////

        /**
         * @brief Initialize three variables: number_of_electrons, number_of_orbitals, and index_of_HOMO
         * @param[in] occupations occupation number
         * @details Using the occupation numbers and number of orbitals, extract informations about number of electrons, orbitals and HOMO index for each spin indicies.
         * These numbers are used in orbital-index based diagonalization.
         * @details neutral, ground state hydrogen molecule -> number of orbitals (could be) 12, index of homo and number of electrons is equal to 1
         * @note If number_of_electrons = 0, there is no exchange potential or energy.
         * @note If number_of_electrons = 1, the exchange potential or energy should be calculated using slater_potential()
         * @callergraph
         * @callgraph
         */
        void initialize_orbital_numbers(
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );

        /**
         * @brief calculate exchange energy
         * @detail The exchange energy is calculated by using the Hartree-Fock exchange energy equation 
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] occupations occupation number
         * @return Hartree-Fock exchange energy
         * @callergraph
         * @callgraph
         */
        //EXX : require waveftnprod, occupation, K
        double obtain_exchange_energy(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations
        );

        /**
         * @brief Alternative parallelization for obtain_exchange_energy method
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] occupations occupation number
         * @return Hartree-Fock exchange energy
         * @callergraph
         * @callgraph
         **/
        double obtain_exchange_energy2(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations
        );

        /**
         * @brief calculate u*psi value
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] occupations occupation number
         * @return u*psi value
         * @details
         * \f[
         *     u\psi_{\sigma,i} (\vec{r}) = -\sum_j^{N_\sigma} f_{\sigma,j} \psi_{\sigma,j}(\vec{r})K_{\sigma,ji}(\vec{r})
         * \f]
         * it need K matrix information, could be obtained from Two_e_integral::compute_Kji()
         * @callergraph
         */
        //upsi_sigma,i(r) = -sum ( occ_j * psi_sigma,j(r) * K_sigma,ji(r) , j=1..N_sigma)
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > obtain_u_psi(
            Teuchos::Array<Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );
        /**
         * @brief Alternative parallelization for obtain_u_psi
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] occupations occupation number
         * @return u*psi value
         * @callergraph
         * @callgraph
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > obtain_u_psi2(
            Teuchos::Array<Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );

        /**
         * @brief calculate and return slater potential, XPotential = -11
         * @param[in] density electric (spin) density
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] occupations occupation number
         * @param[out] slater type exchange potential
         * @details
         * Accesible function for slater potential. Call slater_potential_internal() function after calculate u*psi by using the function obtain_u_psi()
         * \f[
         *     v_{x,\sigma}^{\mathrm slater}(\vec{r}) = \sum_i^{N_\sigma} \frac{\rho_{\sigma, i}(\vec{r})  u_{\sigma, i}(\vec{r})}{ \rho_{\sigma}(\vec{r})}
         * \f]
         * If number_of_electrons = 1 for given spin, 
         * \f[
         *     v_{x,\sigma}^{\mathrm slater}(\vec{r}) = v_{x}^{\mathrm exact}(\vec{r})
         * \f]
         * @callergraph
         * @callgraph
         */
        //slater_potential : require waveftnprod, u, density, occpuations
        //slater potential_sigma(r) = sum(rho_sigma,i(r) * u_sigma,i(r) / rho_sigma(r), i=1..N_sigma)
        //XPotential = -11
        void slater_potential(
            Teuchos::Array<Teuchos::RCP<const Epetra_Vector> > density,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array<Teuchos::RCP<Epetra_Vector> > & exchange_potential
        );
        /**
         * @brief calculate slater potential for KLI calculation, XPotential = -11
         * @param[in] density electric (spin) density
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] u_psi u times psi value. See @ref obtain_u_psi()
         * @param[in] occupations occupation number
         * @return exchange_potential output, slater type exchange potential
         * @details
         * \f[
         *     v_{x,\sigma}^{\mathrm slater} = \sum_i^{N_\sigma} \frac{\rho_{\sigma, i}(\vec{r})  u_{\sigma, i}(\vec{r})}{ \rho_{\sigma}(\vec{r})}
         * \f]
         * @callergraph
         * @callgraph
         */
        //slater potential_sigma(r) = sum(rho_sigma,i(r) * u_sigma,i(r) / rho_sigma(r), i=1..N_sigma)
        //inside KLI
        Teuchos::Array<Teuchos::RCP<Epetra_Vector> > slater_potential_internal(
            Teuchos::Array<Teuchos::RCP<const Epetra_Vector> > density,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > u_psi,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );

        /**
         * @brief calculate KLI potential for KLI calculation, XPotential = -12
         * @param[in] density electric (spin) density
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] occupations occupation number
         * @param[out] exchange_potential, KLI type exchange potential
         * @details KLI-EXX potential is calculated as the following scheme. First of all, the potential have the following form.
         * \f[
         * v_{x,\sigma}^{KLI}(\vec{r}) = \frac{1}{2\rho_\sigma(\vec{r})} \sum_i^{N_\sigma} |\phi_{i\sigma}^{\mathrm KS}(\vec{r})|^2 \left[v_{x,i\sigma}^{\mathrm HF}(\vec{r}) -\left(\bar{v}_{x,i\sigma}^{\mathrm HF}(\vec{r}) - \bar{v}_{x,i\sigma}^{KLI}(\vec{r}) \right)  \right]
         * \f]
         * First of all, u_psi is calculated using obtain_u_psi2(), which is the term corresponds to the product of HF exchange operator and KS orbitals. <br/>
         * slater_potential is calculated using slater_potential_internal(). <br/>
         * orbital_density is calculated inside the function using Value_coef().  <br/>
         * delta_M matrix is calculated using obatin_delta_M(), which is corresponds to the term \f$(\delta_{ji}-M_{ji\sigma})\f$ <br/>
         * slater_norm is calculated using obtain_slater_norm() and slater_potential. <br/>
         * Using delat_M matrix and slater_norm, kli_norm is calculated using obtain_kli_norm()
         * \f[
         * \sum_j^{N_\sigma-1}(\delta_{ji}-M_{ji\sigma})\left(\bar{v}_{x,j\sigma}^{\rm HF} -\bar{v}_{x,j\sigma}^{\rm KLI}\right) = \bar{v}_{x,i\sigma}^{\rm HF} -\bar{v}_{x,i\sigma}^{Slater}.
         * \f]
         * slater_potential, kli_norm, and orbital_densities are used to calculate kli exchange potential.
         * @callergraph
         * @callgraph
         */
        //kli_potential : require slater, kli_norm, waveftnprod, density, occupuations
        void kli_potential(
            Teuchos::Array<Teuchos::RCP<const Epetra_Vector> > density,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array<Teuchos::RCP<Epetra_Vector> > & exchange_potential
        );
        /**
         * @brief calculate delta M matrix for KLI calculation
         * @param[in] density electric (spin) density
         * @param[in] orbital_density electric density corresponds to one given orbital
         * @param[in] occupations occupation number
         * @param[in] alpha spin number
         * @return Identity matrix - M matrix. See the details
         * @details
         * delta_M method calculate
         * \f[
         *     \delta_{ji}-M_{\sigma, ji}
         * \f]
         * where \f$M_{\sigma, ji}\f$ is
         * \f[
         *      \int d\vec{r} \frac{\rho_{\sigma, j}(\vec{r}) \rho_{\sigma, i}(\vec{r})}{\rho_{\sigma}(\vec{r})}
         * \f]
         * Note that i,j should be smaller than index_of_HOMO, which means \f$i,j\leq N_\sigma\f$.
         * @callergraph
         * @callgraph
         */
        //delta_M = delta_ji - M_sigma,ji
        //M_sigma_ji = int(rho_sigma,j(r) * rho_sigma,i(r) / rho_sigma(r), dr)
        //i, j = 1..N_sigma-1(HOMO-1)
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int, double> > obtain_delta_M(
            Teuchos::RCP<const Epetra_Vector> density,
            Teuchos::RCP<Epetra_MultiVector> orbital_density,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations,
            int alpha
        );

        /**
         * @brief calculate slater norm for KLI calculation
         * @param[in] orbital_coeff orbital written as coefficient form
         * @param[in] slater slater potential
         * @param[in] u_psi u_psi function
         * @param[in] alpha spin number
         * @return slater_norm vector
         * @details
         * slater_norm method calculate the following equation.
         * \f[
         *     \textrm{(slater norm)}_{\sigma,i} = \left<\sigma,i | v_{x,\sigma}^{\mathrm{slater}} - u_{\sigma,i} | \sigma,i\right>
         * \f]
         * Note that i should be smaller than index_of_HOMO, \f$i\leq N_\sigma\f$.
         * @callergraph
         * @callgraph
         */
        //slater_norm_sigma,i = <sigma,i|slater_sigma - u_sigma,i|sigma,i>
        //i = 1..N_sigma-1(HOMO-1)
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int, double> > obtain_slater_norm(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<Epetra_Vector> > slater,
            Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > u_psi,
            int alpha
        );

        /**
         * @brief calculate kli norm for KLI calculation
         * @param[in] delta_M delta_M (see obtain_delta_M)
         * @param[in] slater_norm slater norm (see obtain_slater_norm)
         * @return kli_norm vector
         * @details
         kli_norm method calculate the following equation.
         * \f[
         *     \textrm{(kli norm)}_{\sigma,i} = \left<\sigma,i | v_{x,\sigma}^{\mathrm{KLI}} - u_{\sigma,i} | \sigma,i\right>
         * \f]
         * using the M matrix.
         * \f[
         *     \sum_j^{N_\sigma-1}(\delta_{ji}-M_{ji\sigma})\left(\bar{v}_{x,j\sigma}^{HF} -\bar{v}_{x,j\sigma}^{KLI}\right) = \bar{v}_{x,i\sigma}^{HF} -\bar{v}_{x,i\sigma}^{Slater}.
         * \f]
         * Note that i should be smaller than index_of_HOMO, which means \f$i\leq N_\sigma\f$.
         * @callergraph
         * @callgraph
         */
        //kli_norm_sigma,i = <sigma,i|kli_sigma - u_sigma,i|sigma,i>
        //i = 1..N_sigma-1(HOMO-1)
        Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > obtain_kli_norm(
            Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > delta_M,
            Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > slater_norm
        );

        /**
         * @brief check whether norm of ij pair density is large enough or not
         * @param[out] index_list index_list of orbital pair i,j whose pair density is large enough
         * @param[in] orbital orbital values
         * @param[in] i_spin spin_index
         * @callergraph
         * @callgraph
         */
        void check_PD_norm(std::vector< std::vector<int> >& index_list, Teuchos::RCP<Epetra_MultiVector> orbitals, int i_spin);
};
