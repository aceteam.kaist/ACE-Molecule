#include "TDDFT_ABBA.hpp"

#include <cmath>
#include <complex>

#include "Teuchos_Time.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Epetra_CrsMatrix.h"

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Utility/Two_e_integral.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/String_Util.hpp"

#define DEBUG (1)// 0 truns off fill-time output

using std::abs;
using std::vector;
using std::endl;
using std::string;
using std::setw;
using std::setiosflags;
using std::ios;
using Teuchos::SerialDenseMatrix;
using Teuchos::Time;
using Teuchos::TimeMonitor;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

TDDFT_ABBA::TDDFT_ABBA(RCP<const Basis> basis, RCP<Exchange_Correlation> exchange_correlation, RCP<Poisson_Solver> poisson_solver, RCP<Teuchos::ParameterList> parameters)
: TDDFT::TDDFT(basis, exchange_correlation, poisson_solver, parameters){
    if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") == "TammDancoff"){
        string errmsg = "TDDFT::compute - Class and TheoryLevel mismatch! (TDDFT_ABBA with TammDancoff)!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
    this -> type = "ABBA";
    parameters->sublist("TDDFT").sublist("Diagonalize").set<string>("Solver","Direct");
    this -> parameters -> sublist("TDDFT").sublist("Diagonalize").get<bool>("SymmReal", false);
    this -> mat_dim_scale = 2;
}

void TDDFT_ABBA::get_excitations(
        Teuchos::RCP<Diagonalize> diagonalize,
        std::vector<double> &energies,
        Teuchos::RCP<Epetra_MultiVector> &eigenvectors
){
    energies = diagonalize->get_eigenvalues();

    // Verify excitation energies
    vector<double> excitation_energies;
    for(int i = 0; i < matrix_dimension/2; ++i){
        if(std::real(energies[i]) > 0.0){
            Verbose::single(Verbose::Simple) << "Eigenvalue of #" << i-matrix_dimension/2 << " is positive, which should not!" << std::endl;
        }
    }
    for(int i = matrix_dimension/2; i < num_excitation; ++i){
        excitation_energies.push_back(std::real(energies[i]));
    }
    eigenvectors = rcp(new Epetra_MultiVector(diagonalize->get_eigenvectors()->Map(), num_excitation - matrix_dimension/2));
    for(int i = matrix_dimension/2; i < num_excitation; ++i){
        eigenvectors -> operator()(i-matrix_dimension/2) -> Update(1.0, *diagonalize -> get_eigenvectors() -> operator()(i), 0.0);
    }
    energies = excitation_energies;
}

void TDDFT_ABBA::fill_TD_matrix(
    Teuchos::Array< vector<double> > orbital_energies,
    double Hartree_contrib, double xc_contrib,
    int matrix_index_c, int matrix_index_r,
    RCP<Epetra_CrsMatrix>& sparse_matrix
){
    int s1, i, a, s2, j, b;
    this -> unpack_index(matrix_index_c, s1, i, a);
    this -> unpack_index(matrix_index_r, s2, j, b);
    int num_excits = this -> matrix_dimension/2;

    double element_B1 = Hartree_contrib + xc_contrib;
    // *2 because restricted singlet.
    if(!this -> is_open_shell){
        element_B1 *= 2;
    }
    double element_A1 = element_B1;
    if(i==j and a==b and s1 == s2){
        element_A1 += std::real(orbital_energies[s1][a]) - std::real(orbital_energies[s1][i]);
    }

    // 170807 FIX: A B // B A
    int matrix_index_c2 = matrix_index_c + num_excits;
    int matrix_index_r2 = matrix_index_r + num_excits;
    if(std::fabs(element_B1) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            sparse_matrix->InsertGlobalValues(matrix_index_c ,1,&element_A1,&matrix_index_r );
            sparse_matrix->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
            sparse_matrix->InsertGlobalValues(matrix_index_c ,1,&element_B1,&matrix_index_r2);
            sparse_matrix->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
            sparse_matrix->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_r );
            sparse_matrix->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
            sparse_matrix->InsertGlobalValues(matrix_index_c2,1,&element_A1,&matrix_index_r2);
            sparse_matrix->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
        }
        else{
            sparse_matrix->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
            sparse_matrix->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
            sparse_matrix->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
            sparse_matrix->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
        }
    }
}

void TDDFT_ABBA::fill_TD_matrix_restricted(
    vector<double> orbital_energies,
    double Hartree_contrib, vector<double> xc_contrib,
    int matrix_index_c, int matrix_index_r,
    Array< RCP<Epetra_CrsMatrix> >& sparse_matrix
){
    int s1, i, a, s2, j, b;
    this -> unpack_index(matrix_index_c, s1, i, a);
    this -> unpack_index(matrix_index_r, s2, j, b);
    assert(s1 == 0); assert(s2 == 0);
    int num_excits = this -> matrix_dimension/2;

    // *2 because restricted singlet.
    double element_B1 = 2.0 * (Hartree_contrib + xc_contrib[0]);
    //double element_B2 = -element_B1;
    double element_A1 = element_B1;
    if(i==j and a==b){
        element_A1 += std::real(orbital_energies[a]) - std::real(orbital_energies[i]);
    }
    //double element_A2 = -element_A1;

    // 170807 FIX: A B // B A
    int matrix_index_c2 = matrix_index_c + num_excits;
    int matrix_index_r2 = matrix_index_r + num_excits;
    if(std::fabs(element_B1) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c ,1,&element_A1,&matrix_index_r );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c ,1,&element_B1,&matrix_index_r2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_r );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_A1,&matrix_index_r2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
        }
        else{
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
        }
    }
    if(sparse_matrix.size() > 1){
        element_B1 = 2.0 * xc_contrib[1];
        //element_B2 = -element_B1;
        element_A1 = element_B1;
        if(i==j and a==b){
            element_A1 += std::real(orbital_energies[a]) - std::real(orbital_energies[i]);
        }
        //double element_A2 = -element_A1;
        // A B -> A B
        // B A   -B-A
        // 170807 FIX: A B // B A
        int matrix_index_c2 = matrix_index_c + num_excits;
        int matrix_index_r2 = matrix_index_r + num_excits;
        if(std::fabs(element_B1) > this -> cutoff){
            if(matrix_index_c!=matrix_index_r){
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c ,1,&element_A1,&matrix_index_r );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c ,1,&element_B1,&matrix_index_r2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_r );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_A1,&matrix_index_r2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
            }
            else{
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
            }
        }
    }
}

void TDDFT_ABBA::Kernel_TDHF_X(
        Teuchos::RCP<State> state, 
        Teuchos::RCP<Epetra_CrsMatrix>& sparse_matrix, 
        bool kernel_is_not_hybrid, 
        bool add_delta_term, double scale/* = 1.0*/
){
    auto map = basis->get_map();

    Array< RCP<Epetra_MultiVector> > orbitals;
    for(int i_spin=0; i_spin<state->get_occupations().size(); ++i_spin){
        orbitals.push_back(rcp(new Epetra_MultiVector(*map, state->get_orbitals()[i_spin]->NumVectors())));
        orbitals[i_spin]->Update(1.0, *state->get_orbitals()[i_spin], 0.0);
        Value_Coef::Value_Coef(basis, orbitals[i_spin], false, true, orbitals[i_spin]);
    }

    // For HF
    RCP<Epetra_Vector> Kib = rcp(new Epetra_Vector(*map)); //occ-vir
    // For HF, LDA & GGA
    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*map)); //occ-vir
    RCP<Epetra_Vector> Kij = rcp(new Epetra_Vector(*map)); //occ-occ
    // For LDA & GGA
    RCP<Epetra_Vector> ia_coeff = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> jb_coeff = rcp(new Epetra_Vector(*map));
    // end

    double EXX_portion = scale;
    // V_GKS - V_KS + a0 * V_HF
    Array< RCP<Epetra_Vector> > potential_diff_wo_EXX;
    if(add_delta_term){
        potential_diff_wo_EXX = calculate_v_diff(state, true, kernel_is_not_hybrid);
        if(potential_diff_wo_EXX.size() > 2){
            Verbose::all() << "TDDFT::Kernel_TDHF_X, something wrong ! EXX_portion : " << EXX_portion << "    potential_diff_wo_EXX.size() : " << potential_diff_wo_EXX.size() << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    //170822 : 1st iajb! (ia|jb)
            // Delta v_ab delta_ij
            //  \delta_ij < \phi_a | vx^NL - vx | \phi_b >
            // Delta v_ab =  -sum_k(ak|kb) + potential_difference_norm(a,b)
    //170822 : 6st iajb! (ib|ja)
            // exchange - Bterm : b-->a, a-->b
    //openmp + mpi, precaclaulate (ia|jb)
    // tnx to jaechang, see TDDFT::get_iajb()

    Verbose::single(Verbose::Detail) << "TDDFT::get_iajb" << std::endl;
#if DEBUG == 1
        internal_timer -> start("get_iajb");
#endif
    std::vector<double> iajb = get_iajb(orbitals, true);
#if DEBUG == 1
        internal_timer -> end("get_iajb");
        Verbose::single(Verbose::Detail) << "TDDFT_ABBA::Kernel_TDHF_X get_iajb time = " << internal_timer -> get_elapsed_time("get_iajb",-1) << std::endl;
#endif
    const int spin_size = is_open_shell? 2: 1;
    const int half_size = this -> matrix_dimension / 2;

    for(int s = 0; s < spin_size; ++s){
        for(int i = iroot; i < number_of_occupied_orbitals[s]; ++i){
#if DEBUG == 1
            internal_timer -> start("i-loop");
#endif
            for(int a=number_of_occupied_orbitals[s]; a<number_of_orbitals[s]; ++a){
                int delta_matrix_index_c = this -> pack_index(s, i, a);

                for(int j=i; j<number_of_occupied_orbitals[s]; ++j){
                    for(int b = (j==i)? a: number_of_occupied_orbitals[s]; b<number_of_orbitals[s]; ++b){
                        int delta_matrix_index_r = this -> pack_index(s, j, b);
                        int index_iajb = delta_matrix_index_r + half_size * delta_matrix_index_c - delta_matrix_index_c * (delta_matrix_index_c + 1)/2;
                        ////from A
                        double element_iajb = iajb.at(index_iajb);
                        int delta_matrix_index_c2 = delta_matrix_index_c + half_size;
                        int delta_matrix_index_r2 = delta_matrix_index_r + half_size;

                        ////from B
                        int B_matrix_index_r = this -> pack_index(s, i, b);
                        int B_matrix_index_c = this -> pack_index(s, j, a);
                        double element_tmp1 = -element_iajb * scale;
                        // only B terms
                        int B_matrix_index_c2 = B_matrix_index_c + half_size;
                        int B_matrix_index_r2 = B_matrix_index_r + half_size;

                        if(std::fabs(element_tmp1) > this -> cutoff){
                            if(B_matrix_index_c!=B_matrix_index_r){
                                sparse_matrix->InsertGlobalValues(B_matrix_index_c ,1,&element_tmp1,&B_matrix_index_r2);
                                sparse_matrix->InsertGlobalValues(B_matrix_index_r ,1,&element_tmp1,&B_matrix_index_c2);
                                sparse_matrix->InsertGlobalValues(B_matrix_index_c2,1,&element_tmp1,&B_matrix_index_r );
                                sparse_matrix->InsertGlobalValues(B_matrix_index_r2,1,&element_tmp1,&B_matrix_index_c );
                            }
                            else{
                                sparse_matrix->InsertGlobalValues(B_matrix_index_r ,1,&element_tmp1,&B_matrix_index_c2);
                                sparse_matrix->InsertGlobalValues(B_matrix_index_r2,1,&element_tmp1,&B_matrix_index_c );
                            }
                        }
                        ////from B end

                        //  \delta_ij < \phi_a | vx^NL - vx | \phi_b >
                        // Delta v_ab =  -sum_k(ak|kb) + potential_difference_norm(a,b)
                        if(add_delta_term){
                            if(i==j){
                                double element_tmp_vdiff = potential_difference_norm(state->get_orbitals()[s],a,b,potential_diff_wo_EXX[s]);
                                //double element_tmp_m_vdiff = - element_tmp_vdiff;
                                // A
                                sparse_matrix->InsertGlobalValues(delta_matrix_index_r ,1,&element_tmp_vdiff, &delta_matrix_index_c );
                                // -A
                                sparse_matrix->InsertGlobalValues(delta_matrix_index_r2 ,1,&element_tmp_vdiff, &delta_matrix_index_c2 );
                                if(delta_matrix_index_r!=delta_matrix_index_c){
                                    sparse_matrix->InsertGlobalValues(delta_matrix_index_c ,1,&element_tmp_vdiff,&delta_matrix_index_r );
                                    sparse_matrix->InsertGlobalValues(delta_matrix_index_r ,1,&element_tmp_vdiff,&delta_matrix_index_c );
                                }

                                //-sum_k(ak|kb) start
                                for(int k=0;k<number_of_occupied_orbitals[s];++k){
                                    //ia,jb = ka,kb , (bj|ia) = (bk|ka)
                                    double element_HFx = - element_iajb * scale;
                                    // A
                                    int delta_matrix_index_c3 = this -> pack_index(s, k, a);
                                    int delta_matrix_index_r3 = this -> pack_index(s, k, b);
                                    sparse_matrix->InsertGlobalValues(delta_matrix_index_r3,1,&element_HFx,&delta_matrix_index_c3);
                                    if(a!=b){
                                        sparse_matrix->InsertGlobalValues(delta_matrix_index_c3,1,&element_HFx,&delta_matrix_index_r3);
                                    }
                                    // -A
                                    delta_matrix_index_c3 += half_size;
                                    delta_matrix_index_r3 += half_size;
                                    sparse_matrix->InsertGlobalValues(delta_matrix_index_r3,1,&element_HFx,&delta_matrix_index_c3);
                                    if(delta_matrix_index_r3!=delta_matrix_index_c3){
                                        sparse_matrix->InsertGlobalValues(delta_matrix_index_c3,1,&element_HFx,&delta_matrix_index_r3);
                                    }
                                }
                                //-sum_k(ak|kb) end
                            }// if(i == j)
                        }// if(add_delta_term)
                    }//for(b)
                }//for(j)
            }//for (a)
#if DEBUG == 1
            internal_timer -> end("i-loop");
            Verbose::single(Verbose::Detail) << "TDDFT_ABBA::Kernel_TDHF_X index for Delta v + B i = " << i << " time = "<< internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
#endif
        }//for(i)
    }//for(s)
    // */ /// KSW TEST
    // end


    // exchange - A
    // Delta v_ij =  sum_k(jk|ki) - potential_difference_norm(j,i)
    //170822 : 2st iajb! (ij|ab)
    //170822 : 3st iajb! (ij|ij)
    //170822 : 4st iajb! (ij|ik)
    //170822 : 5st iajb! (ij|jk)
    for(int s = 0; s < spin_size; ++s){
        for(int i = iroot; i<number_of_occupied_orbitals[s]; ++i){
#if DEBUG == 1
            internal_timer -> start("i-loop");
#endif
            for(int j=i; j<number_of_occupied_orbitals[s]; ++j){
                Kij->PutScalar(0.0);
                Two_e_integral::compute_Kji(basis, orbitals[s]->operator()(i), orbitals[s]->operator()(j), exchange_poisson_solver, Kij);

                for(int a=number_of_occupied_orbitals[s]; a<number_of_orbitals[s]; ++a){

                    int b = (j==i)? a: number_of_occupied_orbitals[s];
                    for(; b<number_of_orbitals[s]; ++b){
                        int matrix_index_c = this -> pack_index(s, i, a);
                        int matrix_index_r = this -> pack_index(s, j, b);
                        double element_ijab = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(a), orbitals[s]->operator()(b));

                        double element_tmp1 = -element_ijab * scale;
                        // only A terms
                        int matrix_index_c2 = matrix_index_c + half_size;
                        int matrix_index_r2 = matrix_index_r + half_size;
                        if(std::fabs(element_tmp1) > this -> cutoff){
                            if(matrix_index_c!=matrix_index_r){
                                sparse_matrix->InsertGlobalValues(matrix_index_c ,1,&element_tmp1,&matrix_index_r );
                                sparse_matrix->InsertGlobalValues(matrix_index_r ,1,&element_tmp1,&matrix_index_c );
                                sparse_matrix->InsertGlobalValues(matrix_index_c2,1,&element_tmp1,&matrix_index_r2);
                                sparse_matrix->InsertGlobalValues(matrix_index_r2,1,&element_tmp1,&matrix_index_c2);
                            }
                            else{
                                sparse_matrix->InsertGlobalValues(matrix_index_r ,1,&element_tmp1,&matrix_index_c );
                                sparse_matrix->InsertGlobalValues(matrix_index_r2,1,&element_tmp1,&matrix_index_c2);
                            }
                        }

                        //* /// KSW TEST  // Barely changed.
                        //  - \delta_ab < \phi_i | vx^NL - vx | \phi_j >
                        if(add_delta_term){
                            // 이 뒤쪽에서 i랑 j가 vx^NL의 k랑 섞이므로 단순 i==j는 안됨.
                            if(a==b){
                                // Delta v_ij =  sum_k(jk|ki) - potential_difference_norm(j,i)
                                // delta_ij가 없을 때 or delta_ij가 있을 때 ij 같음.
                                if(!this -> is_tdhf_kli or i == j){
                                    double element_tmp_vdiff = -potential_difference_norm(state->get_orbitals()[s],j,i,potential_diff_wo_EXX[s]);
                                    // upper A
                                    sparse_matrix->InsertGlobalValues(matrix_index_r , 1, &element_tmp_vdiff, &matrix_index_c );
                                    // lower A
                                    sparse_matrix->InsertGlobalValues(matrix_index_r2, 1, &element_tmp_vdiff, &matrix_index_c2);
                                    if(matrix_index_r!=matrix_index_c){
                                        sparse_matrix->InsertGlobalValues(matrix_index_c , 1, &element_tmp_vdiff, &matrix_index_r );
                                        sparse_matrix->InsertGlobalValues(matrix_index_c2, 1, &element_tmp_vdiff, &matrix_index_r2);
                                    }
                                }
                                // sum_k(jk|ki)
                                // 이 부분이 sum_k (jk|ki)를 계산하는 부분이라면, 왜 (ij|ij) = (ji|ji)를 계산하는가?
                                // 여기서는 i가 i==j일 때이고 j가 k기 때문이다. 실제로는 (jk|ik) = (jk|ki) for i==j
                                double element_ijij = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(i), orbitals[s]->operator()(j)) * scale;

                                //ia,jb = ia,ia , (ij|ji)
                                // upper A
                                int matrix_index_ijij_c = this -> pack_index(s, i, a);
                                sparse_matrix->InsertGlobalValues(matrix_index_ijij_c , 1, &element_ijij, &matrix_index_ijij_c );
                                // lower A
                                int matrix_index_ijij_c2 = matrix_index_ijij_c + half_size;
                                sparse_matrix->InsertGlobalValues(matrix_index_ijij_c2, 1, &element_ijij, &matrix_index_ijij_c2);

                                // 이 부분이 sum_k (jk|ki)를 계산하는 부분이라면, 왜 (ij|ij) = (ji|ji)를 계산하는가?
                                // 여기서는 j가 i==j일 때이고 i가 k기 때문이다. 실제로는 (jk|ik) = (jk|ki) for i==j
                                if(i!=j){
                                    //ia,jb = ja,ja , (ij|ji)
                                    // upper A
                                    matrix_index_ijij_c = this -> pack_index(s, j, a);
                                    sparse_matrix->InsertGlobalValues(matrix_index_ijij_c , 1, &element_ijij, &matrix_index_ijij_c );
                                    // lower A
                                    matrix_index_ijij_c2 = matrix_index_ijij_c + half_size;
                                    sparse_matrix->InsertGlobalValues(matrix_index_ijij_c2, 1, &element_ijij, &matrix_index_ijij_c2);
                                }//if(i != j)
                                // sum_k(jk|ki)를 계산하는 부분이라면, (ki|ij) = (ji|ki)가 무슨 관련인가?
                                // 이 for(k)에서만 k가 i기 때문이다. 2e integral은 (ki|ij)지만 matrix에 들어가는건 (ik|kj)
                                // delta_ij가 없을 때 or delta_ij가 있을 때 ij 같음.
                                if(!this -> is_tdhf_kli or i == j){
                                    for(int k=j+1;k<number_of_occupied_orbitals[s];++k){
                                        double element_kiij = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(i), orbitals[s]->operator()(k)) * scale;
                                        //ia,jb = ka,jb, (ki|ij)
                                        // upper A
                                        int matrix_index_kiij_c = this -> pack_index(s, k, a);
                                        int matrix_index_kiij_r = this -> pack_index(s, j, b);
                                        sparse_matrix->InsertGlobalValues(matrix_index_kiij_r , 1, &element_kiij, &matrix_index_kiij_c );
                                        if(matrix_index_kiij_r!=matrix_index_kiij_c){
                                            sparse_matrix->InsertGlobalValues(matrix_index_kiij_c , 1, &element_kiij, &matrix_index_kiij_r );
                                        }
                                        // lower A
                                        int matrix_index_kiij_c2 = matrix_index_c + half_size;
                                        int matrix_index_kiij_r2 = matrix_index_r + half_size;
                                        sparse_matrix->InsertGlobalValues(matrix_index_kiij_r2, 1, &element_kiij, &matrix_index_kiij_c2);
                                        if(matrix_index_kiij_r2!=matrix_index_kiij_c2){
                                            sparse_matrix->InsertGlobalValues(matrix_index_kiij_c2, 1, &element_kiij, &matrix_index_kiij_r2);
                                        }
                                    }//for(k)
                                }//if(!tdhf_kli or i==j)
                                // sum_k(jk|ki)를 계산하는 부분이라면, (kj|ij) = (jk|ji)가 무슨 관련인가?
                                // 이 for(k)에서만 k가 j기 때문이다. 2e integral은 (kj|ij)지만 matrix에 들어가는건 (jk|ik) = (jk|ki)
                                // delta_ij가 없을 때 i != j인 경우.
                                if(!this -> is_tdhf_kli and i!=j){
                                    for(int k=i+1;k<number_of_occupied_orbitals[s];++k){
                                        double element_kjij = Two_e_integral::compute_two_center_integral(basis, Kij, orbitals[s]->operator()(j), orbitals[s]->operator()(k)) * scale;
                                        //ia,jb = ia,kb, (kj|ij)
                                        // upper A
                                        int matrix_index_kjij_c = this -> pack_index(s, i, a);
                                        int matrix_index_kjij_r = this -> pack_index(s, k, b);
                                        if(matrix_index_kjij_r!=matrix_index_kjij_c){
                                            sparse_matrix->InsertGlobalValues(matrix_index_kjij_r, 1,&element_kjij, &matrix_index_kjij_c);
                                        }
                                        sparse_matrix->InsertGlobalValues(matrix_index_kjij_c, 1,&element_kjij, &matrix_index_kjij_r);
                                        // lower A
                                        int matrix_index_kjij_c2 = matrix_index_kjij_c + half_size;
                                        int matrix_index_kjij_r2 = matrix_index_kjij_r + half_size;
                                        sparse_matrix->InsertGlobalValues(matrix_index_kjij_r2, 1, &element_kjij, &matrix_index_kjij_c2);
                                        if(matrix_index_kjij_r!=matrix_index_kjij_c){
                                            sparse_matrix->InsertGlobalValues(matrix_index_kjij_c2, 1, &element_kjij, &matrix_index_kjij_r2);
                                        }
                                    }
                                }//if(!is_tdhf_kli and i != j)
                            }//if(a == b)
                            // */ /// KSW TEST
                        }//if(add_delta_term)
                    }//for(b)
                }//for(j)
            }//for(a)
#if DEBUG == 1
            internal_timer -> end("i-loop");
            Verbose::single(Verbose::Detail) << "TDDFT_ABBA::Kernel_TDHF_X index for rest exchange  i = " << i << " time = " << internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
#endif
        }//for(i)
    }//for(s)
    // end
    return;
}

std::vector< std::vector<double> > TDDFT_ABBA::gather_z_vector(
        RCP<Epetra_MultiVector> input, 
        Teuchos::Array< std::vector<double> > eigvals
){
    int num_vec = input -> NumVectors();
    int total_size = input->Map().NumGlobalElements();
    double ** recv = new double*[num_vec];
    for(int i = 0; i < num_vec; ++i){
        recv[i] = new double[total_size];
    }
    Parallel_Util::group_allgather_multivector(input, recv);

    total_size /= 2;
    vector<double> eps_diff(total_size);
    for(int matrix_index_r = 0; matrix_index_r < total_size; ++matrix_index_r){
        int s, i, a;
        this -> unpack_index(matrix_index_r, s, i, a);
        eps_diff[matrix_index_r] = sqrt(eigvals[s][a] - eigvals[s][i]);
    }
    for(int j = 0; j < num_vec; ++j){
        for(int i = 0; i < total_size; ++i){
            recv[j][i] += recv[j][i+total_size];
            recv[j][i] /= eps_diff[i];
        }
        double norm = 0.0;
        for(int i = 0; i < total_size; ++i){
            norm += recv[j][i]*recv[j][i];
        }
        for(int i = 0; i < total_size; ++i){
            recv[j][i] /= sqrt(norm);
        }
    }

    vector< vector<double> > output(num_vec);
    for(int i = 0; i < num_vec; ++i){
        output[i] = vector<double>(recv[i], recv[i]+total_size);
    }
    for(int i = 0; i < num_vec; ++i){
        delete[] recv[i];
    }
    delete[] recv;
    return output;
}
