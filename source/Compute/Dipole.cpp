#include "Dipole.hpp"
#include "Scf.hpp"
#include <string>
#include <stdexcept>
#include <EpetraExt_MatrixMatrix.h>

#include "../Utility/Parallel_Util.hpp"
//#include "../State/State.hpp"
//#include "../State/Scf_State.hpp"
#include "../Basis/Basis.hpp"
#include "../Utility/Time_Measure.hpp"
 
#define _ENERGY_WIDTH_ (10)

using std::string;
using std::vector;
using std::setw;

using Teuchos::RCP;
/*
double Dipole::get_dipole_moment() const {
    std::vector<double> dipole_vector=this->get_dipole_moments()
    dipole_moment=sqrt(pow(dipole_vector[0],2)+pow(dipole_vector[1],2)+pow(dipole_vector[2],2));
    return dipole_moment;
}

std::vector<double> Dipole::get_dipole_moments() const {
    return dipole_moments;
}
*/
bool Dipole::Compute_dipole(Teuchos::RCP<State> &state,Teuchos::RCP<const Basis> basis){
    // dipole_x = -sigma(xi)+sigma(XnZn) first term for e- second term for Nu - Hellmann_Feynman theorem
//    internal_timer -> start("Dipole::Compute_dipole");
    double Dipole_moment = 0.0;
    // e-
    std::vector<double> dipole_e(3);

    auto scaling = basis->get_scaling();

    int *MyGlobalElements = basis->get_map()->MyGlobalElements();
    int NumMyElements = basis->get_map()->NumMyElements();

    for(int j=0; j<NumMyElements;j++){
        std::array<double, 3> r;
        basis->get_position(MyGlobalElements[j], r[0], r[1], r[2]);

        for(int s=0; s< state -> occupations.size() ;s++){
            for(int k=0; k<3;k++){
                dipole_e[k] -= r[k] * state -> get_density()[s] -> operator[](j);
            }   
        }
    }
    std::vector<double> dipole_e_total(3);
    for(int k=0; k<3;k++){
        Parallel_Util::group_sum(&dipole_e[k], &dipole_e_total[k],1);
        dipole_e_total[k] *= scaling[0]*scaling[1]*scaling[2];
    }

    //  nucleaus
    std::vector<double> dipole_Nu(3);
    vector<std::array<double,3> > positions = state->atoms->get_positions();
    for(int j=0; j<state->atoms->get_size();j++){
        for (int k=0; k<3;k++){
            dipole_Nu[k] += state -> atoms -> operator[](j).get_valence_charge()*positions[j][k];
        }
    }
    // summation
    
    std::vector<double> dipole_moment(3);
    for(int i=0; i<3;i++){
        dipole_moment[i]=dipole_e_total[i]+dipole_Nu[i];
    }

    Dipole_moment=sqrt(pow(dipole_moment[0],2)+pow(dipole_moment[1],2)+pow(dipole_moment[2],2));

 //   internal_timer -> end("Dipole::Compute_dipole");
   
    Verbose::single(Verbose::Simple) << "=============================================================" << std::endl;
    Verbose::single(Verbose::Simple) << "Dipole moment       = " << setw(_ENERGY_WIDTH_)<<Dipole_moment << " a.u.    = " << setw(_ENERGY_WIDTH_)<<Dipole_moment/0.393430307 << " Debye" << std::endl;
    Verbose::single(Verbose::Simple) << "     X component    = " << setw(_ENERGY_WIDTH_)<<dipole_moment[0]<< " a.u.    = " << setw(_ENERGY_WIDTH_)<<dipole_moment[0]/0.393430307 << " Debye" <<  std::endl;
    Verbose::single(Verbose::Simple) << "     Y component    = " << setw(_ENERGY_WIDTH_)<<dipole_moment[1]<< " a.u.    = " << setw(_ENERGY_WIDTH_)<<dipole_moment[1]/0.393430307 << " Debye" <<  std::endl;
    Verbose::single(Verbose::Simple) << "     Z component    = " << setw(_ENERGY_WIDTH_)<<dipole_moment[2]<< " a.u.    = " << setw(_ENERGY_WIDTH_)<<dipole_moment[2]/0.393430307 << " Debye" <<  std::endl;
    Verbose::single(Verbose::Simple) << "=============================================================" << std::endl<< std::endl;
//    Verbose::single(Verbose::Simple) << "Scf::Compute_dipole time: " << this -> internal_timer -> get_elapsed_time("Scf::Compute_dipole", -1) << "s" << std::endl;
    return true;
}
