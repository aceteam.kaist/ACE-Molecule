#include "Hessian.hpp"
#include <string>
#include <cmath>

#include "../Utility/String_Util.hpp"
#include "../Utility/Matrix_Multiplication.hpp"
#include "../Utility/Verbose.hpp"

using std::string;
using std::vector;
using std::abs;

vector< vector< double> > Hessian::initial_guess(vector<double> force, double tolerance){
    vector< vector< double> > hessian;
    int a = force.size();
    hessian.resize(a);
    for(int i=0; i<a; i++){
        hessian.at(i).resize(a);
    }
    for(int i=0; i<a; i++){
        for(int j=0; j<a; j++){
            if(i==j){
                if(abs(force[i]) >= tolerance){
                     hessian[i][j] = 1/abs(force[i])*0.01;
                     //hessian[i][j] = -1/abs(force[i])*0.01;
                }
                else{
                     hessian[i][j] = 0.0;
                }
            }
            else{
                hessian[i][j] = 0.0;
            }
        }
    }
    return hessian;
}


vector< vector<double> > Hessian::update_hessian(vector< vector<double> > old_Hessian, vector<double> old_force, vector<double> force, vector<double> old_position, vector<double> position, double tolerance){
    if(old_force.size()==0){
        return initial_guess(force, tolerance); 
        //return this->initial_guess(force, tolerance); 
    }
    else{
        return bfgs(old_Hessian, old_force, force, old_position, position, tolerance);
        //return this->bfgs(old_Hessian, old_force, force, old_position, position, tolerance);
    }
}

vector< vector< double> > Hessian::bfgs(vector< vector<double> > old_Hessian, vector<double> old_Force, vector<double> new_Force, vector<double> old_Position, vector<double> new_Position, double tolerance){
    vector< vector< double> > new_Hessian;
    int a = old_Hessian.size();
    new_Hessian.resize(a);
    for (int i=0; i<a; i++){
        new_Hessian.at(i).resize(a);
    } 

    vector<double> dPosition;
    vector<double> dForce;

    for(int i =0; i < old_Position.size(); i++){
        dPosition.push_back(new_Position.at(i)-old_Position.at(i));
        dForce.push_back(new_Force[i]-old_Force[i]);
    }


    vector< vector<double> > numerator1 = Matrix_Multiplication::outer(dPosition,dPosition);
    vector< vector<double> > numerator2 = Matrix_Multiplication::multiply(Matrix_Multiplication::multiply(old_Hessian,Matrix_Multiplication::outer(dForce,dForce)),old_Hessian);

    double numerator3 = Matrix_Multiplication::dot(dForce,dForce);

    double denomiator1 = Matrix_Multiplication::dot(dPosition,dForce);
    double denomiator2 = Matrix_Multiplication::dot(dForce,Matrix_Multiplication::multiply(old_Hessian,dForce));

    for(int i=0; i< a; i++){
        Verbose::single() << "force[" << i << "] : " << new_Force[i] << std::endl;
        for(int j=0; j<a; j++){
        /*
            if(abs(new_Force[i]) > tolerance & abs(new_Force[j]) > tolerance){
                Verbose::single() << "here1" << std::endl;
                new_Hessian[i][j] = old_Hessian[i][j] + numerator1[i][j]/denomiator1 - numerator2[i][j]/denomiator2;
            }
            else{
                Verbose::single() << "here2" << std::endl;
                new_Hessian[i][j] = 0;  
            } 
        */
                new_Hessian[i][j] = old_Hessian[i][j] + numerator1[i][j]/denomiator1 - numerator2[i][j]/denomiator2;

        }
    }
    return new_Hessian;
}
