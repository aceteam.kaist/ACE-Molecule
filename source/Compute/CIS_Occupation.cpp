#include "CIS_Occupation.hpp"
#include <vector>
#include <iostream>
#include <algorithm>
#include "../Utility/Density/Density_From_Orbitals.hpp"

using std::string;
using std::vector;
using std::endl;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::ParameterList;

/*
   int main(){
   CISD_Occupation test = CISD_Occupation(8,4,4);
   return 0;
   }
   */
CIS_Occupation::CIS_Occupation(RCP<ParameterList> parameters, int num_orbitals, int num_alpha_electrons, int num_beta_electrons){
    //    cout<< "1" << endl;
    this->num_orbitals = num_orbitals;
    this->num_alpha_electrons = num_alpha_electrons;
    this->num_beta_electrons = num_beta_electrons;
    this->parameters = parameters;
    if(num_orbitals < num_alpha_electrons+1 or num_orbitals < num_beta_electrons+1){
        Verbose::all() << "too small number of orbitals, number of orbitals should be large than 1+num_alapha_electrons and 2+beta_electrons" << endl;
        exit(-1);
    }
    initializer();
}
void CIS_Occupation::initializer(){
    this -> SY1 = cal_Y(num_alpha_electrons,num_orbitals, 1);
    Verbose::single(Verbose::Detail) << "start make occupation" << endl;
    make_restricted_occupation(1);
    /*
    Verbose::single() << "single" << endl;
    for(int i=0; i<string_number[1];i++){
        for(int j = 0; j<num_alpha_electrons; j++){
            Verbose::single() << total_occupation[0][1][i][j] << " ";
        }
        Verbose::single() << "  ";
        Verbose::single() << get_address(1,total_occupation[0][1][i]) << endl;
    }
    */

    check_address.clear();
    check_address.resize(2);
    for(int i = 0; i < 2; ++i){
        check_address.at(i).resize(2);
    }

    check_address[0][0] = 0;
    check_address[1][0] = check_address[0][0] + string_number[0]*string_number[0];
    check_address[0][1] = check_address[1][0] + string_number[1]*string_number[0];
    spin_check_address = vector<int>(string_number[0] + string_number[1]);
    spin_check_address[0] = 0;
    spin_check_address[1] = 1;
    total_num_string = string_number[0]*string_number[0]
        + string_number[1]*string_number[0]
        + string_number[0]*string_number[1];
    make_sign_list();
    Verbose::single(Verbose::Detail) << "make cis occupation end" << endl;
}

Teuchos::Array< Teuchos::RCP<Epetra_Vector> > CIS_Occupation::cal_sigma(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > CI_coefficient){
    Teuchos::Array< Teuchos::RCP< Epetra_Vector> > sigma;
    for(int i=0; i<CI_coefficient.size(); i++)
        sigma.append(Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map())));
    int* MyGlobalElements = CI_coefficient[0]->Map().MyGlobalElements();

    combine_coefficient = new double*[CI_coefficient.size()];
    for(int k=0; k<CI_coefficient.size(); k++){
        combine_coefficient[k] = new double[total_num_string]();
        double* tmp_combine_coefficient = new double[total_num_string]();
        int NumMyElements = CI_coefficient[0]->Map().NumMyElements();
        for(int i=0; i<NumMyElements; i++)
            tmp_combine_coefficient[MyGlobalElements[i]] = CI_coefficient[k]->operator[](i);
        CI_coefficient[0]->Map().Comm().SumAll(tmp_combine_coefficient, combine_coefficient[k], total_num_string);
        delete [] tmp_combine_coefficient;
    }

    int alpha1, beta1,alpha_address1, beta_address1;
    for(int i=0; i<total_num_string;i++){
        if(CI_coefficient[0]->Map().LID(i)>=0){
            check_category(i, &alpha1, &beta1, &alpha_address1, &beta_address1);
            vector<double> temp = vector<double>(CI_coefficient.size());
            temp = cal_sigma1_alpha_alpha(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp.at(j);
            temp = cal_sigma1_beta_beta(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp.at(j);
            temp = cal_sigma2_alpha_alpha(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp.at(j);
            temp = cal_sigma2_beta_beta(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp.at(j);
            temp = cal_sigma2_alpha_beta(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp.at(j);
        }
    }
    for(int i=0; i<CI_coefficient.size(); i++)
        delete [] combine_coefficient[i];
    delete combine_coefficient;
    return sigma;
//    exit(-1);
}

std::vector<double> CIS_Occupation::cal_sigma1_alpha_alpha(int excitation1, int address1, int excitation2, int address2, int num_blocks){
    vector<int> occupation = total_occupation[0][excitation1][address1];
    vector<double> retval(num_blocks);
    for(int p=0; p<num_orbitals;p++){
        for(int q=0; q<num_alpha_electrons;q++){
            int eij = Eij(occupation,p,q);
            if(eij!=0){
                vector<int> temp_occupation = total_occupation[0][excitation1][address1];
                temp_occupation[q] = p;

                sort(temp_occupation.begin(), temp_occupation.end());
                int new_excitation = distinguish_excitation(temp_occupation);
                if(new_excitation + excitation2<2 ){
                    int address = get_address(new_excitation,temp_occupation);
                    for(int l=0; l<num_blocks; l++){
                        retval[l] += cal_sign(new_excitation,address)*Hcore[p][occupation[q]]*combine_coefficient[l][check_address[new_excitation][excitation2]+address+string_number[new_excitation]*address2]*eij;
                    }

                }
            }
        }
    }
    for(int l=0; l<num_blocks; l++){
        if(excitation1==1){
            retval[l] = retval[l]*cal_single_sign(address1);
        }
    }
    return retval;
}

std::vector<double> CIS_Occupation::cal_sigma1_beta_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks){
    vector<int> occupation = total_occupation[0][excitation2][address2];
    vector<double> retval(num_blocks);
    for(int p=0; p<num_orbitals;p++){
        for(int q=0; q<num_alpha_electrons;q++){
            int eij = Eij(occupation,p,q);
            if(eij!=0){
                vector<int> temp_occupation = total_occupation[0][excitation2][address2];
                temp_occupation[q] = p;
                sort(temp_occupation.begin(), temp_occupation.end());
                int new_excitation = distinguish_excitation(temp_occupation);
                if(new_excitation + excitation1<2 ){
                    int address = get_address(new_excitation,temp_occupation);
                    for(int l=0; l<num_blocks; l++){
                        retval[l] += cal_sign(new_excitation,address)*Hcore[p][occupation[q]]*combine_coefficient[l][check_address[excitation1][new_excitation]+address1+string_number[excitation1]*address]*eij;
                    }

                }
            }
        }
    }
    for(int l=0; l<num_blocks; l++){
        if(excitation2==1){
            retval[l] = retval[l]*cal_single_sign(address2);
        }
    }
    return retval;
}

std::vector<double> CIS_Occupation::cal_sigma2_alpha_alpha(int excitation1, int address1, int excitation2, int address2, int num_blocks){
    vector<int> occupation = total_occupation[0][excitation1][address1];
    vector<double> retval(num_blocks);
    for(int p=0; p<num_orbitals;p++){
        for(int q=0; q<num_alpha_electrons;q++){
            for(int r=0; r<num_orbitals;r++){
                for(int s=0; s<num_alpha_electrons;s++){
                    int ers = Eij(occupation,r,s);
                    if(ers!=0){
                        vector<int> temp_occupation = total_occupation[0][excitation1][address1];
                        temp_occupation[s] = r;
                        sort(temp_occupation.begin(), temp_occupation.end());
                        int new_excitation1 = distinguish_excitation(temp_occupation);
                        int temp_address1 = get_address(new_excitation1, temp_occupation);
                        int epq = Eij(temp_occupation,p,q);
                        if(epq!=0){
                            vector<int> ttemp_occupation = total_occupation[0][excitation1][address1];
                            ttemp_occupation[s] = r;
                            sort(ttemp_occupation.begin(), ttemp_occupation.end());
                            ttemp_occupation[q] = p;
                            sort(ttemp_occupation.begin(), ttemp_occupation.end());
                            int new_excitation2 = distinguish_excitation(ttemp_occupation);
                            if(new_excitation2 + excitation2<2 ){
                                int temp_address2 = get_address(new_excitation2,ttemp_occupation);
                                for(int l=0; l<num_blocks; l++){
                                        retval[l] += cal_sign(new_excitation2,temp_address2)*two_centered_integ[p][temp_occupation[q]][r][occupation[s]]*combine_coefficient[l][check_address[new_excitation2][excitation2]+temp_address2+string_number[new_excitation2]*address2]*ers*epq;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    for(int l=0; l<num_blocks; l++){
        if(excitation1==1){
            retval[l] = retval[l]*cal_single_sign(address1);
        }
    }
    for(int l=0; l<num_blocks; l++){
        retval[l] = retval[l]*0.5;
    }
    return retval;
}

std::vector<double> CIS_Occupation::cal_sigma2_beta_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks){
    vector<int> occupation = total_occupation[0][excitation2][address2];
    vector<double> retval(num_blocks);
    for(int p=0; p<num_orbitals;p++){
        for(int q=0; q<num_alpha_electrons;q++){
            for(int r=0; r<num_orbitals;r++){
                for(int s=0; s<num_alpha_electrons;s++){
                    int ers = Eij(occupation,r,s);
                    if(ers!=0){
                        vector<int> temp_occupation = total_occupation[0][excitation2][address2];
                        temp_occupation[s] = r;
                        sort(temp_occupation.begin(), temp_occupation.end());
                        int new_excitation1 = distinguish_excitation(temp_occupation);
                        int temp_address1 = get_address(new_excitation1, temp_occupation);
                        int epq = Eij(temp_occupation,p,q);
                        if(epq!=0){
                            vector<int> ttemp_occupation = total_occupation[0][excitation2][address2];
                            ttemp_occupation[s] = r;
                            sort(ttemp_occupation.begin(), ttemp_occupation.end());
                            ttemp_occupation[q] = p;
                            sort(ttemp_occupation.begin(), ttemp_occupation.end());
                            int new_excitation2 = distinguish_excitation(ttemp_occupation);
                            if(new_excitation2 + excitation1<2 ){
                                int temp_address2 = get_address(new_excitation2,ttemp_occupation);
                                for(int l=0; l<num_blocks; l++){
                                        retval[l] += cal_sign(new_excitation2,temp_address2)*two_centered_integ[p][temp_occupation[q]][r][occupation[s]]*combine_coefficient[l][check_address[excitation1][new_excitation2]+address1+string_number[excitation1]*temp_address2]*ers*epq;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    for(int l=0; l<num_blocks; l++){
        if(excitation2==1){
            retval[l] = retval[l]*cal_single_sign(address2);
        }
    }
    for(int l=0; l<num_blocks; l++){
        retval[l] = retval[l]*0.5;
    }
    return retval;
}

std::vector<double> CIS_Occupation::cal_sigma2_alpha_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks){
    vector<int> occupation1 = total_occupation[0][excitation1][address1];
    vector<int> occupation2 = total_occupation[0][excitation2][address2];
    vector<double> retval(num_blocks);
    for(int p=0; p<num_orbitals;p++){
        for(int q=0; q<num_alpha_electrons;q++){
            for(int r=0; r<num_orbitals;r++){
                for(int s=0; s<num_alpha_electrons;s++){
                    int ers = Eij(occupation2,r,s);
                    if(ers!=0){
                        int epq = Eij(occupation1,p,q);
                        if(epq!=0){
                            vector<int> temp_occupation2 = total_occupation[0][excitation2][address2];
                            vector<int> temp_occupation1 = total_occupation[0][excitation1][address1];
                            temp_occupation2[s] = r;
                            temp_occupation1[q] = p;
                            sort(temp_occupation1.begin(), temp_occupation1.end());
                            sort(temp_occupation2.begin(), temp_occupation2.end());
                            int new_excitation1 = distinguish_excitation(temp_occupation1);
                            int new_excitation2 = distinguish_excitation(temp_occupation2);
                                if(new_excitation1 + new_excitation2<2 ){
                                    int temp_address1 = get_address(new_excitation1, temp_occupation1);
                                    int temp_address2 = get_address(new_excitation2, temp_occupation2);
                                    for(int l=0; l<num_blocks; l++){
                                        retval[l] += cal_sign(new_excitation1,temp_address1)*cal_sign(new_excitation2,temp_address2)*two_centered_integ[p][occupation1[q]][r][occupation2[s]]*combine_coefficient[l][check_address[new_excitation1][new_excitation2]+temp_address1+string_number[new_excitation1]*temp_address2]*ers*epq;

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    for(int l=0; l<num_blocks; l++){
        retval[l] = retval[l]*cal_sign(excitation2,address2)*cal_sign(excitation1,address1);
    }
    /*
    for(int l=0; l<num_blocks; l++){
        retval[l] = retval[l];
    }
    */
    return retval;
}

int CIS_Occupation::cal_sign(int excitation, int address){
    if(excitation==1)
        return cal_single_sign(address);
    else
        return 1;
}

void CIS_Occupation::check_category(int coordinate, int* alpha, int* beta, int* alpha_address, int* beta_address){
    if(coordinate >= check_address[0][1]){
        int temp = coordinate - check_address[0][1];

        *alpha_address = temp%(string_number[0]);
        *beta_address = temp/(string_number[0]);
        *alpha = 0;
        *beta = 1;
    }
    else if(coordinate >= check_address[1][0]){

        int temp = coordinate - check_address[1][0];
        *alpha_address = temp%(string_number[1]);
        *beta_address = temp/(string_number[1]);
        *alpha = 1;
        *beta = 0;
    }
    else if(coordinate == check_address[0][0]){
        *alpha_address = 0;
        *beta_address = 0;
        *alpha = 0;
        *beta = 0;
    }

    return;
}

void CIS_Occupation::make_sign_list(){
    sign_list = vector<int>(string_number[0]+string_number[1]);
    sign_list[0] = 1;
    for(int i=0; i<string_number[1]; i++){
        sign_list[i+1] = cal_single_sign(i);
    }
    return;
}
std::vector<double> CIS_Occupation::cal_Hcore_energy(Teuchos::RCP< Epetra_MultiVector > CI_coefficient, int num_eigenvalues){
    std::cout << "This part is not completed...jaechang" <<std::endl;
    exit(-1);
    return vector<double>();
}
Teuchos::RCP<Epetra_Vector> CIS_Occupation::cal_CI_density(RCP<const Basis> basis, Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_of_orbital,Teuchos::RCP<Epetra_Vector> CI_coefficient, Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital) {

    Teuchos::RCP<Epetra_Vector> CI_density = Teuchos::rcp(new Epetra_Vector(density_of_orbital[0]->Map()));
    int num_alpha_string = string_number[0] + string_number[1];
    double* combine_coefficient = new double[total_num_string]();
    double* tmp_combine_coefficient = new double[total_num_string]();

    int NumMyElements = CI_coefficient->Map().NumMyElements();
    int* MyGlobalElements = CI_coefficient->Map().MyGlobalElements();
    for(int i=0; i<NumMyElements; i++)
        tmp_combine_coefficient[MyGlobalElements[i]] = CI_coefficient->operator[](i);
    CI_coefficient->Map().Comm().SumAll(tmp_combine_coefficient, combine_coefficient, total_num_string);
    delete [] tmp_combine_coefficient;
    //int check = 0;
    /*
    for(int i=0; i<total_num_string; i++)
        combine_coefficient[i] = 0.0;
    combine_coefficient[0] = 1.0;
    */
    for(int i=0; i< num_alpha_string; i++){
        int alpha_address;
        int alpha_excitation;
        address_to_excitation(&alpha_excitation, &alpha_address, i);

        for(int beta_excitation = 0; beta_excitation<2 - alpha_excitation; beta_excitation++){
            if(num_alpha_electrons==1 and beta_excitation==2)
                continue;
            else{
                for(int beta_address = 0; beta_address < string_number[beta_excitation]; beta_address++){
                    int coordinate = check_address[alpha_excitation][beta_excitation]+alpha_address+string_number[alpha_excitation]*beta_address;
  //                  cout << coordinate << endl;
                    for(int j=0; j<num_alpha_electrons; j++){
                        CI_density->Update(combine_coefficient[coordinate]*combine_coefficient[coordinate], *density_of_orbital[total_occupation[0][alpha_excitation][alpha_address][j]], 1.0);
                        CI_density->Update(combine_coefficient[coordinate]*combine_coefficient[coordinate], *density_of_orbital[total_occupation[0][beta_excitation][beta_address][j]], 1.0);
                    }
                }
            }
        }
    }
//    exit(-1);
    //return CI_density;
    vector< vector< vector<int> > > alpha_occupation;
    vector<int> temp_merge;
    vector< vector<int> > temp_occupation;
    vector< vector<int> > temp_occupation2;
    temp_occupation = make_occupation(num_alpha_electrons-1, num_alpha_electrons-1, 0);
    alpha_occupation.push_back(temp_occupation);
    vector< vector<int> > middle_occupation;
    int excite = 1;

    if(num_alpha_electrons-1 < excite){
        string_number[excite] = 0;
    }
    else{
        temp_occupation.clear();
        temp_occupation2.clear();
        temp_occupation = make_occupation(num_alpha_electrons-excite-1, num_alpha_electrons-1, 0);
        temp_occupation2 = make_occupation(excite, num_orbitals-2-num_alpha_electrons+1, num_alpha_electrons-1);

        for(int j=0; j<temp_occupation2.size(); j++)
        {
            for(int i=0; i<temp_occupation.size(); i++)
            {
                temp_merge.clear();
                temp_merge = temp_occupation[i];
                temp_merge.insert(temp_merge.end(), temp_occupation2[j].begin(), temp_occupation2[j].end());
                middle_occupation.push_back(temp_merge);
            }
        }
    }
    alpha_occupation.push_back(middle_occupation);
    middle_occupation.clear();
    /*
    vector< vector<int> > check(total_num_string);
    for(int i = 0; i < total_num_string; ++i){
        check.resize(total_num_string);
    }
    */

    int time = 0;
    int time2 = 0;
    for(int i=0; i<num_orbitals; i++){
        for(int j=i+1; j<num_orbitals; j++){
            Teuchos::RCP<Epetra_Vector> overlap_density = Teuchos::rcp(new Epetra_Vector(orbital[0]->Map()));
            Teuchos::RCP<Epetra_Vector> temp1 = Teuchos::rcp(new Epetra_Vector(*orbital[0]->operator()(i)));
            Teuchos::RCP<Epetra_Vector> temp2 = Teuchos::rcp(new Epetra_Vector(*orbital[0]->operator()(j)));
            Density_From_Orbitals::compute_overlap_density(basis, temp1, false,temp2,false, overlap_density);
            vector<vector<vector<int>>> occupation1 ;
            vector<vector<vector<int>>> occupation2 ;
            occupation1 = alpha_occupation;
            occupation2 = alpha_occupation;
            //    Verbose::single() << i << " " << j << endl;
            for(int k=0; k < occupation1.size(); k++){
                for(int l=0; l<occupation1[k].size(); l++){
                    for(int n=0; n<occupation1[k][l].size(); n++){
                        if(occupation1[k][l][n] >= i){
                            for(int x=n; x< occupation1[k][l].size(); x++){
                                occupation1[k][l][x] += 1;
                            }
                            occupation1[k][l].insert(occupation1[k][l].begin()+n,i);
                            break;
                        }

                    }
                    for(int n=0; n<occupation1[k][l].size(); n++){
                        if(occupation1[k][l][n]>=j)
                            occupation1[k][l][n] += 1;
                    }
                    if(occupation1[k][l][occupation1[k][l].size()-1]<i){
                        occupation1[k][l].push_back(i);
                    }

                    for(int n=0; n<occupation2[k][l].size(); n++){
                        if(occupation2[k][l][n] >= i)
                            occupation2[k][l][n] +=1;
                    }
                    for(int n=0; n<occupation2[k][l].size(); n++){
                        if(occupation2[k][l][n]>=j){
                            for(int x=n; x< alpha_occupation[k][l].size(); x++){
                                occupation2[k][l][x] += 1;
                            }
                            occupation2[k][l].insert(occupation2[k][l].begin()+n,j);
                            break;
                        }
                    }
                    if(occupation2[k][l][occupation2[k][l].size()-1]<j){
                        occupation2[k][l].push_back(j);
                    }

                }
            }
            /*
            if(i==3 and j==4){
                Verbose::single() << "check1" << endl;
                for(int x=0; x<occupation1.size(); x++){
                    for(int y=0; y<occupation1[x].size(); y++){
                        for(int z=0; z<occupation1[x][y].size(); z++){
                            //if(occupation1[x][y][z]!=i)
                                Verbose::single() << occupation1[x][y][z] << " ";
                        }
                        Verbose::single() << endl;
                    }
                }
                Verbose::single() << "check2" << endl;
                for(int x=0; x<occupation2.size(); x++){
                    for(int y=0; y<occupation2[x].size(); y++){
                        for(int z=0; z<occupation2[x][y].size(); z++){

                            //if(occupation2[x][y][z]!=j)
                                Verbose::single() << occupation2[x][y][z] << " ";
                        }
                        Verbose::single() << endl;
                    }
                }

                exit(-1);
            }
            */
            for(int k=0; k<occupation1.size(); k++){
                for(int l=0; l<occupation1[k].size(); l++){
                    int beta_excitation;
                    int alpha_excitation1 = distinguish_excitation(occupation1[k][l]);
                    int alpha_excitation2 = distinguish_excitation(occupation2[k][l]);
                    if(alpha_excitation1>alpha_excitation2){
                        beta_excitation = 1- distinguish_excitation(occupation1[k][l]);
                    }
                    else{
                        beta_excitation = 1- distinguish_excitation(occupation2[k][l]);
                    }
  //                  cout << "beta excitation " << beta_excitation << endl;
                    if(beta_excitation==1){
                        std::cout << "beta_excitation == 1" << endl;
                        exit(-1);
                    }
                    for(int n=0; n<beta_excitation+1;n++){
                        for(int beta_address=0; beta_address<string_number[n]; beta_address++){
                            int coordinate1 = check_address[alpha_excitation1][n] + get_address(alpha_excitation1, occupation1[k][l]) + string_number[alpha_excitation1]*beta_address;
                            int coordinate2 = check_address[alpha_excitation2][n] + get_address(alpha_excitation2, occupation2[k][l]) + string_number[alpha_excitation2]*beta_address;
                            int alpha_address1 = get_address(alpha_excitation1, occupation1[k][l]);
                            int alpha_address2 = get_address(alpha_excitation2, occupation2[k][l]);

                            //CI_density->Update(4.0*combine_coefficient[coordinate1]*combine_coefficient[coordinate2]*get_sign(alpha_excitation1, alpha_address1)*get_sign(alpha_excitation2, alpha_address2), *overlap_density, 1.0);
                            CI_density->Update(4.0*combine_coefficient[coordinate1]*combine_coefficient[coordinate2], *overlap_density, 1.0);
                            //cout << endl;
                            //cout << "coordinate check : " << coordinate1 << " " << coordinate2 << " " << combine_coefficient[coordinate1] << " " << combine_coefficient[coordinate2] << endl;
                            //check[coordinate1][coordinate2] += 1;
                            //check[coordinate2][coordinate1] += 1;
                            coordinate1 = check_address[beta_excitation][alpha_excitation1] + beta_address + string_number[beta_excitation]*get_address(alpha_excitation1, occupation1[k][l]);
                            coordinate2 = check_address[beta_excitation][alpha_excitation2] + beta_address + string_number[beta_excitation]*get_address(alpha_excitation2, occupation2[k][l]);
                            //cout << "coordinate check : " << coordinate1 << " " << coordinate2 << " " << combine_coefficient[coordinate1] << " " << combine_coefficient[coordinate2] << endl;
                            //CI_density->Update(2*combine_coefficient[coordinate1]*combine_coefficient[coordinate2], *overlap_density, 1.0);
                            time = time + 4;
                            //check[coordinate1][coordinate2] += 1;
                            //check[coordinate2][coordinate1] += 1;
                        }
                    }
                }
            }
//            cout << i << " " << j << " " << time << endl;
        }
    }
//    cout << "time : " << time << endl;
    /*
    delete combine_coefficient;
    for(int i=0; i<total_num_string; i++){
        int sum = 0;
        for(int j=0; j<total_num_string;j++){
            cout << check[i][j] << " ";
            if(check[i][j]!=0)
                sum++;
        }
        cout << "         " << sum << endl;
    }
    */
//    exit(-1);
    //CI_density->Scale(2.0);//bcause alpha density and beta density is same;
    return CI_density;
}
void CIS_Occupation::make_ST_list(vector< vector< vector<int> > > & ST_list) {
    std::cout << "This part is not complete ...jaechang" <<std::endl;
    exit(-1);
    return ;
}
vector< vector<int> > CIS_Occupation::make_GD_list(){
    std::cout << "This part is not complete ...jaechang" <<std::endl;
    exit(-1);
    vector< vector<int> > tmp;
    return tmp;
}
int CIS_Occupation::get_ST_size(){
    std::cout << "This part is not complete ...jaechang" <<std::endl;
    exit(-1);
    return 0;

}
int CIS_Occupation::get_GD_size(){
    std::cout << "This part is not complete ...jaechang" <<std::endl;
    exit(-1);
    return 0;
}
void CIS_Occupation::address_to_excitation(int* excitation, int* ret_address, int address){
    if(address >=spin_check_address[1])
        *excitation = 1, *ret_address = address - spin_check_address[1];
    else
        *excitation = 0, *ret_address = 0;
    return;
}
