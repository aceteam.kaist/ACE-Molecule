#include "Scf.hpp"
#include <string>
#include <stdexcept>
#include <EpetraExt_MatrixMatrix.h>

#include "Create_Compute.hpp"
#include "../Core/Convergence/Create_Convergence.hpp"
#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Core/Mixing/Create_Mixing.hpp"
#include "../Core/Solvation/Create_Solvation.hpp"
#include "../Core/Occupation/Create_Occupation.hpp"
//#include "../Core/Diagonalize/Pure_Diagonalize.hpp"

#include "../Io/Atoms.hpp"
#include "../Utility/Density/Density_From_Orbitals.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Two_e_integral.hpp"
#include "../Utility/SymMMMul.hpp"
#include "../Utility/Parallel_Manager.hpp"
#include "../Utility/Parallel_Util.hpp"

#ifdef ACE_DISPERSION
#include "../Utility/Dispersion/dispersion.hpp"
#endif
//using std::ofstream;
#define _ENERGY_WIDTH_ (10)

using std::string;
using std::vector;
using std::setw;

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
//using Teuchos::ParameterList;

Scf::Scf(RCP<const Basis> basis,RCP<Teuchos::ParameterList> parameters,RCP<Core_Hamiltonian> core_hamiltonian): parameters(parameters), core_hamiltonian(core_hamiltonian){
    this -> initialize(basis, Teuchos::sublist(parameters,"Scf"));
}

void Scf::initialize(RCP<const Basis> basis, RCP<Teuchos::ParameterList> scf_param){
    this->basis = basis;
    if(scf_param -> get<int>("EnergyDecomposition") != 1 and scf_param -> get<string>("ConvergenceType")=="Energy"){
        throw std::invalid_argument("Energy is not computed for every step but convergence criteria is Energy!");
    }
    this -> make_hamiltonian_matrix = (scf_param -> get<int>("MakingHamiltonianMatrix") == 0)? false: true;
    if(this -> make_hamiltonian_matrix){
        Verbose::single(Verbose::Normal) << "Diagonalization will be done without making hamiltonian matrix" << std::endl;
    }

    this->poisson_solver = Create_Compute::Create_Poisson(basis,scf_param);
    this->exchange_correlation = Create_Compute::Create_Exchange_Correlation(basis,scf_param,poisson_solver);
    this->external_field = Create_External_Field::Create_External_Field(basis,Teuchos::sublist(parameters,"BasicInformation"));
    this->diagonalize = Create_Diagonalize::Create_Diagonalize(Teuchos::sublist(scf_param,"Diagonalize"));
    this -> convergence = Create_Convergence::Create_Convergence(Teuchos::sublist(parameters,"Scf") );
    this -> mixing = Create_Mixing::Create_Mixing(Teuchos::sublist(parameters,"Scf"));
    Verbose::single(Verbose::Simple) << mixing -> get_mixing_info() << std::endl;
    if (scf_param->isParameter("SolvationModel")){
        this -> solvation = Create_Solvation::Create_Solvation(basis, Teuchos::sublist(scf_param, "SolvationModel"));
    }
    this -> is_final_diag = parameters -> sublist("Scf").isSublist("FinalDiagonalize");
    if(parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("Pseudopotential") == 3
       and parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("HartreePotentialMethod", 0) == 2){
        this -> poisson_solver = Teuchos::null;
    }
}

Scf::~Scf(){
}

int Scf::compute(RCP<const Basis> basis,Array<RCP<State> >& states){
    if (*basis!=(*this->basis)){
        Verbose::single(Verbose::Simple) << "Scf:: basis in Scf is different to given basis" << std::endl;
        Verbose::single(Verbose::Simple) << this->basis.get() << std::endl;
        Verbose::single(Verbose::Simple) << basis.get() << std::endl;
    }
    Verbose::single(Verbose::Simple) << "Scf:: compute start" << std::endl;
    RCP<State> initial_state = rcp( new State(*states[states.size()-1]) );

    RCP<State> final_state;
    if (core_hamiltonian == Teuchos::null){
        RCP<const Atoms> atoms = initial_state->get_atoms();
        Verbose::single(Verbose::Detail) << "Scf:: Core_Hamiltonian is not constructed yet. It will be constructed " <<std::endl;
        this -> core_hamiltonian = Create_Compute::Create_Core_Hamiltonian(basis, atoms, parameters);
    }

    core_hamiltonian->get_core_electron_info(initial_state->core_density,initial_state->core_density_grad);

    // initialize solvation
    if(this -> solvation != Teuchos::null){
        this -> solvation -> initialize(initial_state -> get_atoms(), core_hamiltonian->get_nuclear_potential()->get_Zeffs());
    }

    //set valence charge
    auto new_atoms = rcp(new Atoms(*initial_state->get_atoms()));
    auto valence_charges = this -> core_hamiltonian ->get_nuclear_potential()-> get_Zeffs();

    for(int i=0; i<new_atoms->get_size(); i++){
        new_atoms->operator[](i).set_valence_charge(valence_charges[i]);
    }
    initial_state->set_atoms(new_atoms);
    /* generate new occupations and put it in in_state */
    Teuchos::RCP<Teuchos::ParameterList> occ_param;
    if(parameters->isSublist("Scf")){
        if(parameters->sublist("Scf").isSublist("Occupation")){
            occ_param = sublist(sublist(parameters,"Scf"),"Occupation");

            occ_param->set("NumElectrons",parameters->sublist("BasicInformation").get<double>("NumElectrons") );
            occ_param->set("SpinMultiplicity",parameters->sublist("BasicInformation").get<double>("SpinMultiplicity") );
            occ_param->set("Polarize", parameters -> sublist("BasicInformation").get<int>("Polarize",0));
            if(parameters -> sublist("Scf").isParameter("NumberOfEigenvalues")){
                occ_param -> get("OccupationSize", parameters->sublist("Scf").get<int>("NumberOfEigenvalues"));
            }

            auto occupations = Create_Occupation::Create_Occupation(occ_param );
            initial_state -> occupations = occupations;
        }
    }

    int num_eigval_default_ = 0;
    for(int s = 0; s < initial_state -> occupations.size(); ++s){
        num_eigval_default_ = std::max(num_eigval_default_, initial_state -> occupations[s] -> get_size());
    }
    int num_eigval_param_ = parameters->sublist("Scf").get<int>("NumberOfEigenvalues", -1);
    if(num_eigval_param_ <= 0){
        string warnmsg = "Scf NumberOfEigenvalues use default value of " + String_Util::to_string(num_eigval_default_);
        Verbose::single(Verbose::Simple) << warnmsg << std::endl;
        std::cerr << warnmsg << std::endl;
        parameters -> sublist("Scf").set<int>("NumberOfEigenvalues", num_eigval_default_);
    }

    int ierr = iterate(initial_state,final_state);
    
    states.append(final_state);
    Verbose::set_numformat(Verbose::Occupation);
    Verbose::single(Verbose::Simple) << "SCF final state occupation = \n";
    for(int s = 0; s < final_state -> occupations.size(); ++s){
        Verbose::single(Verbose::Simple) << *final_state -> occupations[0] << std::endl;
    }
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "SCF total time:" << std::endl;
    this -> internal_timer -> print(Verbose::single(Verbose::Simple));
    Verbose::set_numformat();
    return ierr;
}

int Scf::iterate(RCP<State> initial_state, RCP<State>& final_state){
    Verbose::single(Verbose::Simple) << std::endl;
    Verbose::single(Verbose::Simple) << "#-------------------------------------------------------------- Scf::iterate" << std::endl;
    auto timer = Teuchos::rcp(new Time_Measure() );
    timer->start("Scf::iterate");
    this -> internal_timer -> start("Scf::iterate::initialization");
    //const int spin_size = initial_state->occupations.size();
        //core_hamiltonian->update_hamiltonian() does not use occupations and orbitals.
    const int max_cycle = parameters->sublist("Scf").get<int>("IterateMaxCycle");
    const int num_eigval = parameters->sublist("Scf").get<int>("NumberOfEigenvalues");
    int return_val=-1;
    
    // set potential_from_field that is used during scf iteration
    Array<RCP<Epetra_Vector> > external_potential;
    for(int i_spin=0;i_spin<initial_state->occupations.size();i_spin++){
        external_potential.append(rcp( new Epetra_Vector(*basis->get_map())) );
    }
    if(this->external_field !=Teuchos::null){
        external_field->get_potential( external_potential);
        initial_state->potential_from_field  = external_potential;
    }

    /////////////////////////////End/////////////////////////////////////////
    /**
    //Scf iteration, start!
    //Definitions of the "in_state" and "out_state" are written in
    //http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf
    //3rd page
    //for linear mixing, we mix "in_state" and "out_state"
    //for pulay mixing, we mix "in_state", which are stored in states
    //  (be aware that pulay mixing class stores "residue" = "out_state" - "in_state"
    //in scf_states, ONLY the "in_state" should be stored (to restart the calculation),
    // except for the last state(last state is "out_state", which is converged state)
    //
    // nth scf_states component contain follwing information:
    //    n-1th out_state orbitals & orbital energies
    //    n-1th mixed_state(or out_state) density = nth in_state density
    //    n-1th mixed_state(or out_state) local_potential = nth in_state local_potential
    //    occupations (occupation information would not be changed)
    //    core_density & core_density_grad (which come from psudopotential)
    //    n-1th mixed_state(or out_state) hartree, x, c_potential,
    //                           which are components of the local_potential.
    */
    //initialization!
    scf_states.clear();
    RCP<Epetra_CrsMatrix> overlap_matrix;
    this -> core_hamiltonian -> get_nuclear_potential() -> get_overlap_matrix( overlap_matrix );

    //in_state should contain rho_0^in, psi_0^in, V_loc,0^in.
    //use prho_0^in and psi_0^in from initial_state.
    //calculate V_loc,0^in
    RCP<Scf_State> in_state = rcp(new Scf_State(*initial_state));

    if(parameters -> sublist("Scf").get<int>("IgnoreInternalInit", 0) == 1){
        this -> core_hamiltonian -> update_orbital_info(in_state -> get_occupations(), in_state -> get_orbitals());
    }

    if(parameters->sublist("Scf").get<int>("ComputeInitialEnergy")!=0){
        calculate_energy( in_state );
    }

    Verbose::single(Verbose::Normal)<<  "#-------------------------------------------------------------- Scf::iterate" << std::endl;
    Verbose::single(Verbose::Normal) << "initialization," <<std::endl;
    Verbose::single(Verbose::Normal) << "    calculate xc and hartree potential of initial_state " <<std::endl;
    Verbose::single(Verbose::Normal)<<  "#---------------------------------------------------------------------------" << std::endl;

    generate_local_potential(in_state);
    scf_states.append( rcp(new Scf_State(*in_state)) );
//    duplicate?
//    bool make_hamiltonian_matrix = true;
//    if(parameters->sublist("Scf").get<int>("Making_Hamiltonian_Matrix")==0){
//        Verbose::single() << "Diagonalization will be done without making hamiltonian matrix" << std::endl;
//        make_hamiltonian_matrix = false;
//    }
    this -> internal_timer -> end("Scf::iterate::initialization");
    Verbose::single(Verbose::Simple) << "Scf::iterate::initialization time: " << this -> internal_timer -> get_elapsed_time("Scf::iterate::initialization", -1) << "s" << std::endl;

    RCP<Epetra_CrsMatrix> hamiltonian_matrix;// = rcp(new Epetra_CrsMatrix(Copy, *basis -> get_map(), 0) );
    RCP<Epetra_Vector> diagonal_of_hamiltonian = rcp(new Epetra_Vector(*basis -> get_map()));

    // scf_states[stats.size()-1] is identical to in_state
    for (int i_scf=0; i_scf<max_cycle; ++i_scf){
        Verbose::single(Verbose::Simple) << "#------------------------------------------------------------- Scf::iterate Step #" << setw(4) << i_scf + 1 << std::endl;
        internal_timer->start("Scf iteration cycle");
        RCP<Scf_State> out_state = rcp(new Scf_State(*in_state));
        // scf_states[stats.size()-1] is idendical to in_state and out_state
        out_state->orbitals.clear();
        out_state->orbital_energies.clear();
        this -> internal_timer -> start("Scf::iterate::Hamiltonian update");
        this -> core_hamiltonian -> update_hamiltonian(in_state -> get_occupations(), in_state -> get_orbitals());
        this -> internal_timer -> end("Scf::iterate::Hamiltonian update");
        Verbose::single(Verbose::Detail) << "Scf::iterate::Hamiltonian update time" << this -> internal_timer -> get_elapsed_time("Scf::iterate::Hamiltonian update", -1) << "s" << std::endl;
        convergence -> is_diagonalize_converged = true;

        for(int i_spin=0; i_spin < in_state->occupations.size();++i_spin){
            RCP<Epetra_Vector> local_potential = rcp(new Epetra_Vector( *(in_state->local_potential[i_spin]))); 
            
            RCP<Epetra_MultiVector> initial_orbitals;
            if( i_spin < in_state->orbitals.size() ){
                initial_orbitals = in_state->orbitals[i_spin];
            }
            else{
                initial_orbitals = Teuchos::null;
            }

            if(make_hamiltonian_matrix){
                Verbose::single(Verbose::Detail) << "Diagonalization will be done with making hamiltonian matrix" << std::endl;
                this -> internal_timer -> start("Scf::iterate::Hamiltonian construction");
                ////////////////  extract diagonal element of core hamiltonian ///////////
                core_hamiltonian->get_core_hamiltonian(i_spin,hamiltonian_matrix);
                RCP<Epetra_Vector> core_diagonal = rcp( new Epetra_Vector( *this -> basis -> get_map(), false ) );
                hamiltonian_matrix -> ExtractDiagonalCopy( *core_diagonal );
                core_diagonal->Update(1.0,*local_potential,1.0);
                core_diagonal->Update(1.0,*external_potential[i_spin],1.0);
                hamiltonian_matrix->ReplaceDiagonalValues(*core_diagonal);
                
                this -> internal_timer -> end("Scf::iterate::Hamiltonian construction");

                Verbose::single(Verbose::Detail)<<  "orbital_initialization" << std::endl;
                Verbose::set_numformat(Verbose::Scientific);
                Verbose::single(Verbose::Normal) << "Hamiltonian matrix sparsity = " << static_cast<double>(hamiltonian_matrix -> NumGlobalNonzeros64())/this -> basis -> get_original_size()/this -> basis -> get_original_size() << std::endl;
                internal_timer->start("Diagonalize");

                bool is_diag_success = diagonalize->diagonalize(hamiltonian_matrix, num_eigval, initial_orbitals, overlap_matrix );
                
                internal_timer->end("Diagonalize");

                //std::cout << *hamiltonian_matrix <<std::endl; //shchoi
                convergence -> is_diagonalize_converged = convergence -> is_diagonalize_converged and is_diag_success;
                out_state->orbitals.append(diagonalize->get_eigenvectors() );
                out_state->orbital_energies.append(diagonalize->get_eigenvalues() );
                // debug test
                //out_state->occupations[i_spin]->set_eigenvalues(out_state->orbital_energies[i_spin]);

                double* norm = new double[ out_state->orbital_energies[out_state->orbital_energies.size()-1].size()];
                out_state->orbitals[out_state->orbital_energies.size()-1]->Norm2(norm);
                for(int i =0; i< out_state->orbital_energies[out_state->orbital_energies.size()-1].size(); i++){
                    assert(std::fabs(norm [i] - 1)<0.00001);
                    Verbose::set_numformat(Verbose::Energy);
                    Verbose::single(Verbose::Detail) << out_state->orbital_energies[out_state->orbital_energies.size()-1][i] <<"  Hartree\t";
                    Verbose::set_numformat(Verbose::Occupation);
                    Verbose::single(Verbose::Detail) << norm[i] << std::endl;

                }
                delete[] norm;
            }
            else{
                RCP<Epetra_Vector> local_ext_pot = rcp(new Epetra_Vector(*out_state->local_potential[i_spin]));
                local_ext_pot->Update(1.0,*external_potential[i_spin],1.0);
                internal_timer->start("Diagonalize");
                Verbose::single(Verbose::Detail) << "Diagonalization will be done without making hamiltonian matrix" << std::endl;
                bool is_diag_success = diagonalize->diagonalize(core_hamiltonian->get_core_hamiltonian_matrix(), i_spin, num_eigval, local_ext_pot, initial_orbitals, overlap_matrix );
                internal_timer->end("Diagonalize");

                //std::cout << *hamiltonian_matrix <<std::endl;
                convergence -> is_diagonalize_converged = convergence -> is_diagonalize_converged and is_diag_success;
                out_state->orbitals.append(diagonalize->get_eigenvectors() );
                out_state->orbital_energies.append(diagonalize->get_eigenvalues() );
                // debug test
                //out_state->occupations[i_spin]->set_eigenvalues(out_state->orbital_energies[i_spin]);
                double orbital_energy_sum = 0.0;

                for(int i =0; i< out_state->orbital_energies[out_state->orbital_energies.size()-1].size(); i++){
                    Verbose::single(Verbose::Detail) << out_state->orbital_energies[out_state->orbital_energies.size()-1][i] <<"  Hartree"<<std::endl;
                    orbital_energy_sum+=out_state->orbital_energies[out_state->orbital_energies.size()-1][i];
                }
                Verbose::single(Verbose::Detail) << "Sum of orbital energy : " << orbital_energy_sum << " Hartree" << std::endl;
            }
        }
        this -> core_hamiltonian -> update_orbital_info(out_state -> get_occupations(), out_state -> get_orbitals()); //For PAW orbital correction

    // set eigenvalues
    // 여기서 set_eigenvalues를 통해 occupations를 채워준다.
    // 만약 fermi occupation을 사용한다면 소수 개수로 들어있는 occupations를 만듦.
    // In general, occupations.size() = 1 or 2  (polarize 0 or 1)
        for(int i_spin=0; i_spin< out_state->occupations.size(); i_spin++){
            out_state->occupations[i_spin]->set_eigenvalues(out_state->orbital_energies[i_spin]);
        }

        // Generate Density from Orbitals
        Density_From_Orbitals::compute_total_density(basis,out_state->occupations,out_state->orbitals,out_state->density);
        // compute and print total energy
        bool is_compute_energy_this_step = print_energy(i_scf,out_state);
        // append out_states to scf_states
        append_states(out_state);
        /*
        //If the mixing class does not need previous scf_states[i] anymore, we erase it.
        if( scf_states.size() > mixing -> get_used_step_number() + 1 ){
            scf_states[scf_states.size()-mixing -> get_used_step_number() - 2] = Teuchos::null;
        }
        */

        if(convergence_check(i_scf, is_compute_energy_this_step) ) {
            internal_timer->end("Scf iteration cycle");
            Verbose::single(Verbose::Simple) << i_scf+1 << "th iteration (time = " << internal_timer->get_elapsed_time("Scf iteration cycle",-1)  << " s)" << std::endl;
            Verbose::single(Verbose::Normal) << "SCF time profile up to " << i_scf+1 << " step:" << std::endl;
            Verbose::set_numformat(Verbose::Time);
            this -> internal_timer -> print(Verbose::single(Verbose::Normal));
            Verbose::set_numformat();
            Verbose::single(Verbose::Simple) << std::endl << "#-------------------------------------------------------------- Scf::iterate" << std::endl;
            Verbose::single(Verbose::Simple) << " Total " << i_scf+1 << " iterations." << std::endl;
            Verbose::single(Verbose::Simple) << " Scf satisfies convergence criteria." << std::endl;
            Verbose::single(Verbose::Simple) <<  "#---------------------------------------------------------------------------" << std::endl;
            
            return_val= 0;
            break;
        }

        // generate_in_state() : mixing을 하고  in_state 를 return함.
        in_state = generate_in_state();

        //new in_state was made
        Verbose::single(Verbose::Normal) << "Scf::iterate::Mixing time: " << internal_timer -> get_elapsed_time("Scf::iterate::Mixing", -1) << "s" << std::endl;

        internal_timer->end("Scf iteration cycle");
        Verbose::single(Verbose::Simple) << std::endl << std::endl;
        Verbose::set_numformat(Verbose::Time);
        Verbose::single(Verbose::Simple) << i_scf+1 << "th iteration (time = " << internal_timer->get_elapsed_time("Scf iteration cycle",-1)  << " s)" << std::endl;
        Verbose::single(Verbose::Normal) << "SCF time profile of " << i_scf+1 << " step:" << std::endl;
        Verbose::set_numformat(Verbose::Time);
        internal_timer -> print(Verbose::single(Verbose::Normal), -1);
        Verbose::set_numformat();
        Verbose::single(Verbose::Simple) << "#--------------------------------------------------------- Scf::iterate Step #" << setw(4) << i_scf + 1 << " end" << std::endl;
    }
    if (this->is_final_diag){
        final_diagonalize( external_potential, overlap_matrix);
    }
    timer->end("Scf::iterate");
    final_state = scf_states[scf_states.size()-1];
    Verbose::set_numformat(Verbose::Occupation);
    Verbose::single(Verbose::Simple) << *(final_state->get_occupations()[0]) <<std::endl;
    Verbose::set_numformat();
    final_state->timer = timer;
    return return_val;
/*
    if( convergence -> converge(basis, scf_states) and convergence -> is_diagonalize_converged ){
        return 0;
    } else {
        return -1;
    }
    return -2;
*/
}
//int Scf::generate_local_potential(RCP<Scf_State> state,int i_spin,RCP<Epetra_Vector>& local_potential){
//int Scf::generate_local_potential(RCP<Scf_State> state,int i_spin){
int Scf::generate_local_potential(RCP<Scf_State> state){
    internal_timer -> start("Scf::generate_local_potential");
    internal_timer -> start("Scf::generate_local_potential::alloc");
    RCP<const Epetra_Map> map = basis->get_map();
    //ArrayRCP<Epetra_Vector> local_potential = rcp(new Epetra_Vector( *basis->get_map()  ) );
    Array< RCP<Epetra_Vector> > local_potential;
    for(int i_spin=0; i_spin<state->occupations.size(); i_spin++){
        local_potential.push_back(rcp(new Epetra_Vector(*map)));
    }

    internal_timer -> end("Scf::generate_local_potential::alloc");
    //    local_potential->PutScalar(0.0);

    if (state->density.size()==0){
        if (state->orbitals.size()==0){
            if(state->local_potential.size()!=0){
                //local_potential = state->local_potential[i_spin];
                return 0;// What is the meaning of this???
            }
            else{
                Verbose::all()<< "Scf:: generate_local_potential Error!!!" <<std::endl;
                exit(-1);
            }
        }
        else{
            Density_From_Orbitals::compute_total_density(basis,state->occupations,state->orbitals,state->density);
        }
    }
    internal_timer -> start("Scf::generate_local_potential::hartree potential");


    // For the case that evaluation of Hartree potential is performed in solvation
    // Zero vector is now assigned 
    RCP<Epetra_Vector> hartree_potential = rcp ( new Epetra_Vector(*basis->get_map()) );  
    double hartree_energy;
    if(poisson_solver != Teuchos::null){
        poisson_solver->compute(state->density, hartree_potential, hartree_energy);
    } else {
        hartree_potential = rcp(new Epetra_Vector(*this -> basis -> get_map()));
    }
    this -> core_hamiltonian -> get_nuclear_potential() -> get_hartree_potential_correction(state -> density, hartree_potential);
    Verbose::single(Verbose::Detail)<< "Scf::Computing Hartree potential is done" <<std::endl;
    internal_timer -> end("Scf::generate_local_potential::hartree potential");

    internal_timer -> start("Scf::generate_local_potential::alloc2");
    Array< RCP<Epetra_Vector> > x_potential;
    Array< RCP<Epetra_Vector> > c_potential;
    /* 170913
    for(int i_spin=0; i_spin<state->occupations.size(); i_spin++){
        x_potential.push_back(rcp(new Epetra_Vector(*map)));
        c_potential.push_back(rcp(new Epetra_Vector(*map)));
    }
    */

    //exchange_correlation->set(states[-1]); 를 짜자.
    RCP<State> just_state = Teuchos::rcp_implicit_cast<State>(state);
    internal_timer -> end("Scf::generate_local_potential::alloc2");
    //exchange_corrlation->compute_vxc(x_potential,c_potential);
    //Verbose::single() << "orbital size is not zero\n";
    internal_timer -> start("Scf::generate_local_potential::XC potential");
    exchange_correlation->compute_vxc(just_state, x_potential, c_potential);
    internal_timer -> end("Scf::generate_local_potential::XC potential");
    //exchange_correlation->compute_vxc(state->density, state->orbitals, state->occupations, x_potential, c_potential, state->core_density, state->core_density_grad);

    internal_timer -> start("Scf::generate_local_potential::assign");
    state->hartree_energy = hartree_energy;
    state->hartree_potential = hartree_potential;
    state->x_potential = x_potential;
    state->c_potential = c_potential;

	this -> internal_timer -> end("Scf::generate_local_potential::assign");
    
    
	this -> internal_timer -> start("Scf::calculate_solvation");
    if(this -> solvation != Teuchos::null){
        double solv_energy = this -> solvation -> compute(state, hartree_potential);
    }

	this -> internal_timer -> end("Scf::calculate_solvation");

	this -> internal_timer -> start("Scf::generate_local_potential::update");
    for(int i_spin=0; i_spin<state->occupations.size(); i_spin++){
        local_potential[i_spin]->Update(1.0,*hartree_potential,0.0); 
        local_potential[i_spin]->Update(1.0,*x_potential[i_spin],1.0, *c_potential[i_spin],1.0);
    }
    state -> local_potential = local_potential;

    this -> internal_timer -> end("Scf::generate_local_potential::update");
    this -> internal_timer -> end("Scf::generate_local_potential");
    Verbose::set_numformat(Verbose::Occupation);
    Verbose::single(Verbose::Normal) << "Scf::generate_local_potential time: " << this -> internal_timer -> get_elapsed_time("Scf::generate_local_potential", -1) << "s" << std::endl;
    return 0;
}

int Scf::calculate_energy(RCP<Scf_State>& state,
    double &hartree_energy,
    vector<double> &kinetic_energies,
    double &x_energy,
    double &c_energy,
    double &int_n_vxc,
    double &external_energy,
    double &orbital_energy_sum,
    double &numerical_correction,
    double &solvation_energy,
    double &dispersion_energy
){
    vector<double> energies;
    vector<int> num_orbitals;

    internal_timer->start("Scf::calculate_energy") ;
    Verbose::single(Verbose::Detail) <<  "start of calculate_energy" << std::endl;
    Verbose::set_numformat(Verbose::Time);
    //    RCP<const Epetra_Map> map = basis->get_map();
    const int spin_size = state->occupations.size();
    //    int NumMyElements = map->NumMyElements();

    Array< RCP<const Occupation> > occupations =state->get_occupations();
    Array< RCP<const Epetra_MultiVector> > orbitals = state->get_orbitals();
    Array< RCP<const Epetra_Vector> > density = state->get_density();
    Array< vector<double> > orbital_energies = state->orbital_energies;
    for(int i_spin=0; i_spin<spin_size; i_spin++){
        num_orbitals.push_back(occupations[i_spin]->get_size());
    }
    Verbose::single(Verbose::Detail) << "initialization of calculate_energy" << std::endl;

    // Calculate Hartree energy
    //double hartree_energy = poisson_solver->get_energy(state->density);
    internal_timer->start("Scf::calculate_hartree_energy") ;
    hartree_energy = 0.0;
    RCP<Epetra_Vector> hartree_potential;
    if(poisson_solver != Teuchos::null){
        poisson_solver->compute(state->density, hartree_potential, hartree_energy);
    } else {
        hartree_potential = rcp(new Epetra_Vector(*this -> basis -> get_map()));
    }
//    if(Parallel_Manager::info().get_mpi_rank()==0){
//    }

    this -> core_hamiltonian -> get_nuclear_potential() -> get_hartree_potential_correction(state -> density, hartree_potential);
    internal_timer->end("Scf::calculate_hartree_energy") ;
    Verbose::single(Verbose::Detail) << "calculate_energy, hartree = " << internal_timer -> get_elapsed_time("Scf::calculate_hartree_energy", -1) << " s" << std::endl;

    internal_timer->start("Scf::calculate_ion_ion") ;
    core_hamiltonian->calculate_ion_ion(state->nuclear_nuclear_repulsion );
    internal_timer->end("Scf::calculate_ion_ion") ;
    Verbose::single(Verbose::Detail) <<  "calculate_energy, ion ion = " << internal_timer -> get_elapsed_time("Scf::calculate_ion_ion", -1) << " s" << std::endl;
    //hartree_energy*=0.5;
    // end

    // Calculate kinetic energy
    internal_timer->start("Scf::calculate_kinetic_energy");
    kinetic_energies.clear();
    core_hamiltonian->get_kinetic_energy(state,kinetic_energies);
    double kinetic_energy = 0.0;
    for(int s = 0; s < spin_size; ++s){
        kinetic_energy += kinetic_energies[s];
    }
    internal_timer->end("Scf::calculate_kinetic_energy") ;
    Verbose::single(Verbose::Detail)<<  "calculate_energy, kinetic time = " << internal_timer -> get_elapsed_time("Scf::calculate_kinetic_energy", -1) << " s" << std::endl;
    // end
    // Calculate Exc
    RCP<State> just_state = Teuchos::rcp_implicit_cast<State>(state);
    x_energy = c_energy = 0.0;
    internal_timer->start("Scf::calculate_energy_Exc") ;
    exchange_correlation->compute_Exc(just_state, x_energy, c_energy);
    Verbose::single(Verbose::Detail) << "calculate_energy, Exc,done" << std::endl;

    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Detail) << "Exchange energy = " << x_energy << std::endl;
    Verbose::single(Verbose::Detail) << "Correlation energy = " << c_energy << std::endl;
    //exchange_correlation->compute_Exc(state->density,state->orbitals,state->core_density,core_density_grad,x_energy,c_energy);
    // end
    internal_timer->end("Scf::calculate_energy_Exc") ;
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Detail) << "calculate_energy, exchange correlation = " << internal_timer -> get_elapsed_time("Scf::calculate_energy_Exc", -1) << " s" << std::endl;

    internal_timer->start("Calculate int[n*v_xc]") ;
    // Calculate \int[n*v_xc]
    //exchange_correlation->integrate_rho_vxc(just_state, int_n_vxc);
    internal_timer->end("Calculate int[n*v_xc]");
    int_n_vxc = 0.0;
    Verbose::single(Verbose::Detail) << "int_n_vxc = " << int_n_vxc << std::endl;
    Verbose::single(Verbose::Detail) << "calculate_energy, rho integrate = " << internal_timer -> get_elapsed_time("Calculate int[n*v_xc]", -1) << " s" << std::endl;
    // end
    
    internal_timer->start("calculate_energy_nuclear_potential") ;
    external_energy = 0.0;
    core_hamiltonian->calculate_nuclear_density_interaction(state,external_energy);
    internal_timer->end("calculate_energy_nuclear_potential") ;
    Verbose::single(Verbose::Detail) << "calculate_energy, nuclear potential = " << internal_timer -> get_elapsed_time("calculate_energy_nuclear_potential", -1)<< " s" << std::endl;

    Verbose::set_numformat(Verbose::Energy);
    if(external_field != Teuchos::null ){
        for(int i_spin=0; i_spin<state->occupations.size(); i_spin++){
            double energy_from_external_field= external_field->get_energy(state, i_spin);
            Verbose::single(Verbose::Detail) << "External field interaction energy = " <<  energy_from_external_field << " Hartree" << std::endl;
            external_energy+=energy_from_external_field;
        }
    }
    /*
    // Calculate Exc
    double Ex = 0.0, Ec = 0.0;
    exchange_correlation->set_use_before_K(true);
    exchange_correlation->compute_Exc(all_density, orbitals, Ex, Ec, occupations, core_density, core_density_grad);
    // end

    // Calculate external energy
    double external_energy = 0.0;

    for(int i_spin=0; i_spin<spin_size; i_spin++){
    RCP<Epetra_CrsMatrix> pp_matrix = rcp(new Epetra_CrsMatrix(*core_hamiltonian->core_hamiltonians[0]));
    //Teuchos::RCP<Epetra_CrsMatrix> pp_matrix = Teuchos::rcp(new Epetra_CrsMatrix(*core_hamiltonian->core_hamiltonians[i_spin]));
    EpetraExt::MatrixMatrix::Add(*core_hamiltonian->kinetic_matrix,false,-1.0,*pp_matrix,1.0);

    RCP<Epetra_MultiVector> external_tmp = rcp(new Epetra_MultiVector(*map, orbitals[i_spin]->NumVectors() ));
    pp_matrix->Multiply(false,*orbitals[i_spin],*external_tmp);

    double* Result=new double [orbitals[i_spin]->NumVectors()];
    external_tmp->Dot(*orbitals[i_spin],Result);

    for(int i=0;i<num_orbitals[i_spin];i++){
    external_energy += Result[i] * occupations[i_spin]->operator[](i);
    }
    delete[] Result;

    // Energy due to external field
    external_energy += external_field->get_energy(state, i_spin);
    // end
    }
    // Correction of external energy from compensation charge
    RCP<Epetra_Vector> comp_potential = rcp(new Epetra_Vector(*map));
    //core_hamiltonian->kbprojector->get_comp_potential(comp_potential);

    double my_correction = 0.0, total_correction = 0.0;
    for(int i_spin=0; i_spin<spin_size; i_spin++){
    for(int i=0; i<num_orbitals[i_spin]; i++){
    for(int j=0; j<NumMyElements; j++){
    my_correction += occupations[i_spin]->operator[](i) * orbitals[i_spin]->operator[](i)[j] * orbitals[i_spin]->operator[](i)[j] * comp_potential->operator[](j);
    }
    }
    }

    map->Comm().SumAll(&my_correction, &total_correction, 1);
    external_energy -= total_correction;
    // end

    // end
    */
    // Put together

    orbital_energy_sum = 0.0;
    for(int i_spin=0; i_spin<spin_size; i_spin++){
        for(int i=0;i<num_orbitals[i_spin];i++){
            orbital_energy_sum += occupations[i_spin]->operator[](i) * std::real(orbital_energies[i_spin][i]); // eigenvalues, energies[0]
        }
    }
    /*
       energies.push_back(hartree_energy); // energies[1]
       energies.push_back(n_vxc); // energies[2]
       energies.push_back(x_energy); // energies[3]
       energies.push_back(c_energy); // energies[4]
       energies.push_back(total_kinetic_energy); // energies[5]
       energies.push_back(external_energy); // energies[6]
    //energies.push_back(-total_n_vxc_core - total_Ex_nlcc_core - total_Ec_nlcc_core); // Correction from NLCC
    // end

    // Calculate total energy
    double total1 = state->nuclear_nuclear_repulsion + energies[1] + energies[3] + energies[4] + energies[5] + energies[6];
    double total2 = 0.0;
    if (energies[0]!=0.0){
    state->nuclear_nuclear_repulsion + energies[0] - energies[1] + energies[3] + energies[4] - energies[2];
    }
*/
    numerical_correction = 0.0;
    internal_timer->start("Calculate PP/PAW correction");
    this -> core_hamiltonian -> get_nuclear_potential() -> get_energy_correction(
            state->get_occupations(), state-> get_orbitals(), hartree_potential,
            hartree_energy, int_n_vxc, x_energy, c_energy, kinetic_energy, kinetic_energies,
            numerical_correction, external_energy);
    internal_timer -> end("Calculate PP/PAW correction");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Detail) << "calculate_energy, PP/PAW correction = " << this -> internal_timer -> get_elapsed_time("Calculate PP/PAW correction", -1) << " s" << std::endl;
//    double solvation_energy=0.0;


    dispersion_energy = 0.0;
#ifdef ACE_DISPERSION
    Dispersion_Calculation dispersion;
    string functional;
    Verbose::single() << "scf_param is fine" << std::endl;
    Verbose::single() << parameters -> isSublist("Scf") << std::endl;
    Verbose::single() << *parameters << std::endl;
    Verbose::single() << &parameters <<std::endl;
    Verbose::single() << parameters <<std::endl;    
    
    if(parameters -> sublist("Scf").isSublist("Dispersion")){
        Verbose::single() << "Dispersion on" <<std::endl;    

        //if( parameters -> sublist("Scf").sublist("Dispersion").isSublist("Dispersionfunctional")){
        Verbose::single() << "Dispersionfunctional on" <<std::endl;    
        functional = this-> parameters -> sublist("Scf").sublist("Dispersion").get<string>("Dispersionfunctional");
        dispersion_energy = dispersion.dispersion_interaction_c(state,functional,1,0,0);
        //}
    }
#endif

    // Print energies
    state->total_energy = state->nuclear_nuclear_repulsion+hartree_energy+x_energy+c_energy+kinetic_energy+numerical_correction+external_energy+dispersion_energy;
    solvation_energy = 0.0;

    if(this -> solvation != Teuchos::null){
        solvation_energy = this -> solvation -> compute(state, hartree_potential );
        state -> total_free_energy = state -> total_energy + solvation_energy;
    }
    return 0;
}

int Scf::calculate_energy(RCP<Scf_State>& state){
    const int spin_size = state->occupations.size();
    double hartree_energy, x_energy, c_energy, external_energy, solvation_energy, orbital_energy_sum, int_n_vxc, numerical_correction, dispersion_energy;
    vector<double> kinetic_energies;
    this -> calculate_energy(state, hartree_energy, kinetic_energies, x_energy, c_energy, int_n_vxc, external_energy, orbital_energy_sum, numerical_correction, solvation_energy, dispersion_energy);
    this -> display_energy(state, hartree_energy, kinetic_energies, x_energy, c_energy, int_n_vxc, external_energy, orbital_energy_sum, numerical_correction, solvation_energy, dispersion_energy);

    if(!std::isfinite(state -> total_energy)){
        Verbose::all() << "Wrong energy found." << std::endl;
        throw std::runtime_error("Wrong energy found.\n");
    }
    this -> internal_timer -> end("Scf::calculate_energy");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Detail) << "Scf::calculate_energy time: " << this -> internal_timer -> get_elapsed_time("Scf::calculate_energy", -1) << "s" << std::endl;
    this -> internal_timer -> print(Verbose::single(Verbose::Normal), -1);

    return 0;
}

int Scf::display_energy(
    RCP<Scf_State> state,
    double hartree_energy, 
    vector<double> kinetic_energies, 
    double x_energy, 
    double c_energy, 
    double int_n_vxc, 
    double external_energy, 
    double orbital_energy_sum,
    double numerical_correction,
    double solvation_energy,
    double dispersion_energy
){
    const int spin_size = state -> occupations.size();
    Array< vector<double> > orbital_energies = state->orbital_energies;
    double kinetic_energy = 0.0;
    for(int s = 0; s < spin_size; ++s){
        kinetic_energy += kinetic_energies[s];
    }

    Verbose::set_numformat(Verbose::Energy);
    //Verbose::single(Verbose::Detail) << "Total energy = " << total1 << " / " << total2 << " (using eigenvalues)" << std::endl;
    Verbose::single(Verbose::Simple) << std::endl << "==================" << std::endl;
    Verbose::single(Verbose::Simple) << "Eigenvalue(s)" << std::endl;
    for(int i_spin=0; i_spin<spin_size; i_spin++){
        for(int i=0; i<orbital_energies[i_spin].size(); i++){
            Verbose::single(Verbose::Simple) << setw(_ENERGY_WIDTH_) << std::real(orbital_energies[i_spin][i]) << "  "; // eigenvalues
        }
        Verbose::single(Verbose::Simple) << std::endl;
    }
    Verbose::single(Verbose::Simple) << "==================" << std::endl;
    Verbose::single(Verbose::Simple) << std::endl;
    Verbose::single(Verbose::Simple) << "=============================================================" << std::endl;

    Verbose::single(Verbose::Simple) << "Total energy       = " << state->total_energy << std::endl;
    if(this -> solvation != Teuchos::null){
        Verbose::single() << "Total free energy  = " << state->total_free_energy << std::endl;
    }

    if(this -> core_hamiltonian -> get_nuclear_potential()->get_type() == "KBprojector"){
        Verbose::single(Verbose::Simple) << "=============================================================" << std::endl;
        Verbose::single(Verbose::Simple) << "     Ion-ion                     = " << setw(_ENERGY_WIDTH_) << std::right << state->nuclear_nuclear_repulsion << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "     Eigenvalues                 = " << setw(_ENERGY_WIDTH_) <<  orbital_energy_sum << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "     Hartree energy              = " << setw(_ENERGY_WIDTH_) << hartree_energy << " Hartree" << std::endl;
//        Verbose::single(Verbose::Simple) << "     \\int[n*v_xc]                = " << setw(15) << int_n_vxc  << " Hartree" << std::endl; It is not calculated
        Verbose::single(Verbose::Simple) << "     Exchange-Correlation energy = " << setw(_ENERGY_WIDTH_) << x_energy+c_energy << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "          Exchange energy        = " << setw(_ENERGY_WIDTH_) << x_energy << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "          Correlation energy     = " << setw(_ENERGY_WIDTH_) << c_energy << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "     Kinetic energy              = " << setw(_ENERGY_WIDTH_) << kinetic_energy << " Hartree" << std::endl;
        for(int i_spin = 0; i_spin < kinetic_energies.size(); i_spin++){
            Verbose::single(Verbose::Simple) << "          Spin " << i_spin << "                 = " << setw(_ENERGY_WIDTH_) << kinetic_energies[i_spin] << " Hartree" << std::endl;
        }
        Verbose::single(Verbose::Simple) << "     External energy             = " << setw(_ENERGY_WIDTH_) << external_energy << " Hartree" << std::endl;

        if(this -> solvation != Teuchos::null){
            Verbose::single(Verbose::Simple) << "     Solvation energy            = " << setw(_ENERGY_WIDTH_) << solvation_energy << " Hartree" << std::endl;
        }
#ifdef ACE_DISPERSION
        Verbose::single(Verbose::Simple) << "     Dispersion energy           = " << setw(_ENERGY_WIDTH_) << dispersion_energy << " Hartree" << std::endl;
#endif
        //Verbose::single(Verbose::Simple) << "     Correction (comp. charge)   = " << setw(15) << total_correction << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "=============================================================" << std::endl << std::endl;;
        // end
    }
    else{
        Verbose::single(Verbose::Simple) << "=============================================================" << std::endl;
        Verbose::single(Verbose::Simple) << "     Eigenvalues                 = " << setw(_ENERGY_WIDTH_) << std::right << orbital_energy_sum << " Hartree" << std::endl;
        // Hartree energy includes ion-ion in PAW
        Verbose::single(Verbose::Simple) << "     Hartree energy + ion-ion    = " << setw(_ENERGY_WIDTH_) << hartree_energy << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "     Exchange-Correlation energy = " << setw(_ENERGY_WIDTH_) << x_energy+c_energy << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "          Exchange energy        = " << setw(_ENERGY_WIDTH_) << x_energy << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "          Correlation energy     = " << setw(_ENERGY_WIDTH_) << c_energy << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "     Kinetic energy              = " << setw(_ENERGY_WIDTH_) << kinetic_energy << " Hartree" << std::endl;
        for(int i_spin =0; i_spin < kinetic_energies.size(); i_spin++){
            Verbose::single(Verbose::Simple) << "          Spin " << i_spin << "                 = " << setw(_ENERGY_WIDTH_) << kinetic_energies[i_spin] << " Hartree" << std::endl;
        }
        Verbose::single(Verbose::Simple) << "     Numerical correction        = " << setw(_ENERGY_WIDTH_) << numerical_correction << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "     External energy             = " << setw(_ENERGY_WIDTH_) << external_energy << " Hartree" << std::endl;

        if(this -> solvation != Teuchos::null){
            Verbose::single(Verbose::Simple) << "     Solvation energy            = " << setw(_ENERGY_WIDTH_) << solvation_energy << " Hartree" << std::endl;
        }
        
        //Verbose::single() << "     Correction (comp. charge)   = " << setw(15) << total_correction << " Hartree" << std::endl;
        Verbose::single(Verbose::Simple) << "=============================================================" << std::endl << std::endl;;
        // end
    }
    return 0;
}

RCP<Core_Hamiltonian> Scf::get_core_hamiltonian(){
    return this -> core_hamiltonian;
}

int Scf::final_diagonalize(Array<RCP<Epetra_Vector> > external_potential,RCP<Epetra_CrsMatrix> overlap_matrix){
    RCP<Epetra_Vector> diagonal_of_hamiltonian = rcp(new Epetra_Vector(*basis -> get_map()));

    internal_timer->start("Scf:: final diagonalize");

    int num_eigval_final = Teuchos::sublist(parameters,"Scf")->
                                sublist("FinalDiagonalize").get<int>("NumberOfEigenvalues",
                                parameters->sublist("Scf").get<int>("NumberOfEigenvalues"));

    auto last_state = scf_states[scf_states.size()-1];
    auto out_state = Teuchos::rcp(new Scf_State(*last_state) );
    out_state->orbitals.clear();
    out_state->orbital_energies.clear();
    final_diagonalizer = Create_Diagonalize::Create_Diagonalize(Teuchos::sublist(Teuchos::sublist(parameters,"Scf" ), "FinalDiagonalize"), last_state -> orbital_energies);

    this -> core_hamiltonian -> update_orbital_info( last_state -> get_occupations(), last_state -> get_orbitals() );
    generate_local_potential(last_state);
    this -> core_hamiltonian -> update_hamiltonian( last_state -> get_occupations(), last_state -> get_orbitals() );
    Verbose::set_numformat(Verbose::Energy);
    for(int i_spin =0; i_spin < last_state->occupations.size();i_spin++){
        if(make_hamiltonian_matrix){
            RCP<Epetra_CrsMatrix> hamiltonian_matrix = rcp(new Epetra_CrsMatrix(Copy, *basis -> get_map(), 0) );

            core_hamiltonian->get_core_hamiltonian(i_spin,hamiltonian_matrix);
            hamiltonian_matrix -> ExtractDiagonalCopy( *diagonal_of_hamiltonian );
            diagonal_of_hamiltonian->Update(1.0,*last_state->local_potential[i_spin],1.0);
            diagonal_of_hamiltonian->Update(1.0,*external_potential[i_spin],1.0);
            hamiltonian_matrix->ReplaceDiagonalValues(*diagonal_of_hamiltonian);

            final_diagonalizer->diagonalize(hamiltonian_matrix,num_eigval_final, last_state->orbitals[i_spin] ,overlap_matrix);
        } else {
            RCP<Epetra_Vector> local_ext_pot = rcp(new Epetra_Vector(*last_state->local_potential[i_spin]));
            local_ext_pot->Update(1.0,*external_potential[i_spin],1.0);
            final_diagonalizer->diagonalize(core_hamiltonian->get_core_hamiltonian_matrix(), i_spin, num_eigval_final, local_ext_pot, last_state->orbitals[i_spin], overlap_matrix);
        }
        out_state->orbitals.append(final_diagonalizer->get_eigenvectors() );
        out_state->orbital_energies.append(final_diagonalizer->get_eigenvalues() );
        double* norm = new double[ out_state->orbital_energies[out_state->orbital_energies.size()-1].size()];
        out_state->orbitals[out_state->orbital_energies.size()-1]->Norm2(norm);
        for(int i =0; i< out_state->orbital_energies[i_spin].size(); i++){
            Verbose::single(Verbose::Normal) << out_state->orbital_energies[i_spin][i] <<"  Hartree\t" << norm[i]<<std::endl;
        }
        delete[] norm;
        //Verbose::single() << out_state->orbital_energies[i_spin].size() <<std::endl;
        //Verbose::single() << out_state->orbitals[i_spin] <<std::endl;
    }
    internal_timer->end("Scf:: final diagonalize");
    for(int i_spin =0; i_spin< out_state->occupations.size();i_spin++){
        out_state->occupations[i_spin]->resize(num_eigval_final);
    }
    this -> core_hamiltonian -> update_orbital_info(out_state -> get_occupations(), out_state -> get_orbitals());
    Density_From_Orbitals::compute_total_density(basis,out_state->occupations,out_state->orbitals,out_state->density);
    calculate_energy(out_state);
    scf_states.append(out_state);
    return 0;
}


int Scf::append_states(Teuchos::RCP<Scf_State> out_state ){
    //notice that scf_states[0], ..., [n-1] contain "in_state" potential/density

    //out_state should be appended(for convergence check!)
    scf_states.append( rcp(new Scf_State(*out_state )));
    //If the mixing class does not need previous scf_states[i] anymore, we erase it.
    if( scf_states.size() > mixing -> get_used_step_number() + 1 ){
        scf_states[scf_states.size()-mixing -> get_used_step_number() - 2] = Teuchos::null;
    }
    return 0;
}

bool Scf::print_energy(int i_scf, Teuchos::RCP<Scf_State> out_state){
    bool is_compute_energy_this_step = true;
    if(parameters->sublist("Scf").get<int>("EnergyDecomposition")==0){
        is_compute_energy_this_step= false;
    } else if(i_scf % parameters->sublist("Scf").get<int>("EnergyDecomposition")!=0){
        is_compute_energy_this_step= false;
    }
    if(is_compute_energy_this_step){
        calculate_energy(out_state);
    }
    return is_compute_energy_this_step;
}

bool Scf::convergence_check(int i_scf, bool is_compute_energy_this_step){
    //convergence check!
    //notice that scf_states[0], ..., [n-1] contain "in_state" potential/density
    if(parameters->sublist("Scf").get<int>("DiagonalizeShouldNotBeConverged")!=0){
        convergence->is_diagonalize_converged = true;
    }

    //From now on, scf_states[scf_states.size()-1] should be modified
    Verbose::set_numformat(Verbose::Scientific);
    if ( convergence->converge(basis, scf_states)){
        if( convergence -> is_diagonalize_converged ){
            if(is_compute_energy_this_step==false){
                calculate_energy(scf_states[scf_states.size()-1]);
            }
            return true;
        } else {
            Verbose::single(Verbose::Simple) << std::endl << "#-------------------------------------------------------------- Scf::iterate" << std::endl;
            Verbose::single(Verbose::Simple) << " Scf satisfies convergence criteria, but diagonalization unconverged!" << std::endl;
            Verbose::single(Verbose::Simple) << " Increase the maximum iteration cycle twice and retry                " << std::endl;
            Verbose::single(Verbose::Simple) <<  "#---------------------------------------------------------------------------" << std::endl;
            int diag_iterno = diagonalize -> get_max_iter();
            diagonalize -> set_max_iter( 2*diag_iterno );
        }
    }
    return false;
}
Teuchos::RCP<Scf_State> Scf::generate_in_state(){
    //mixing start!
    internal_timer -> start("Scf::iterate::Mixing");
    if(mixing->get_mixing_type() == "Potential"){
        Verbose::single(Verbose::Normal) << std::endl << "#-------------------------------------------------------------- Scf::iterate" << std::endl;
        Verbose::single(Verbose::Normal) << "Mixing type is \"" <<  mixing->get_mixing_type() << "\"\n";
        Verbose::single(Verbose::Normal) << "calculate xc and hartree potential of out_state " <<std::endl;
        Verbose::single(Verbose::Normal) <<  "#---------------------------------------------------------------------------" << std::endl;

        internal_timer -> start("Scf::iterate::Mixing (potential update)");
        generate_local_potential(scf_states[scf_states.size()-1]);
//        for(int i_spin=0; i_spin < in_state->occupations.size();i_spin++){
            //generate_local_potential(out_state, i_spin);
//            generate_local_potential(scf_states[scf_states.size()-1], i_spin);
//        }
        internal_timer -> end("Scf::iterate::Mixing (potential update)");
//        Verbose::single()<<  "out, generate_local_potential" << std::endl;
    }

    //scf_states.append( rcp(new Scf_State(*out_state )));
    for(int i_spin=0; i_spin<scf_states[scf_states.size()-1]->occupations.size(); i_spin++){
        mixing->update(basis, scf_states, i_spin, core_hamiltonian -> get_core_hamiltonian_matrix() -> nuclear_potential);
    }
    // now, scf_states[stats.size()-1] becomes new in_state
    auto in_state = scf_states[scf_states.size()-1];
    Verbose::single(Verbose::Normal) << "occupation of input state" <<std::endl;
    Verbose::single(Verbose::Normal) << * in_state->occupations[0] <<std::endl;

    if(mixing->get_mixing_type() == "Density" or mixing -> get_mixing_type() == "PAW_Density" or mixing->get_mixing_type() == "Orbital"){
        Verbose::single(Verbose::Normal)<<  std::endl << "#-------------------------------------------------------------- Scf::iterate" << std::endl;
        Verbose::single(Verbose::Normal) << "Mixing type is \"" <<  mixing->get_mixing_type() << "\"\n";
        Verbose::single(Verbose::Normal) << "calculate xc and hartree potential of new in_state " <<std::endl;
        Verbose::single(Verbose::Normal) <<  "#---------------------------------------------------------------------------" << std::endl;

        internal_timer -> start("Scf::iterate::Mixing (potential update)");
        generate_local_potential(in_state);
//        for(int i_spin=0; i_spin < in_state->occupations.size();i_spin++){
//        }
        internal_timer -> end("Scf::iterate::Mixing (potential update)");
        Verbose::single(Verbose::Detail) << "out, generate_local_potential" << std::endl;
    }
    internal_timer -> end("Scf::iterate::Mixing");
    //mixing end!

    return in_state;
}
