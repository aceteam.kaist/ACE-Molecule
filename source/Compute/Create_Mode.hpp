/*
 * Create_Mode.hpp
 *
 *  Created on: 2017. 2. 28.
 *      Author: ksw
 */

#include <string>
#include <unordered_map>
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Create_Compute_Interface.hpp"
#include "Compute.hpp"
#include "../Io/Atoms.hpp"
#include "../Basis/Basis.hpp"

namespace Create_Mode{
    Teuchos::Array< Teuchos::RCP<Compute> > get_array(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > array_parameters
            );
    std::unordered_map<std::string, int> get_parameter_names( Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > array_parameters);
}
