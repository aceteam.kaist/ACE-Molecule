#pragma once
#include "Cube.hpp"

//class State;

class Cube_Density_from_Orbitals: public Cube{
  public:
    Cube_Density_from_Orbitals(
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations, 
        Teuchos::RCP<const Atoms> atoms, 
        Teuchos::Array< Teuchos::RCP<Occupation> > input_occupations, 
        Teuchos::Array<std::string> cube_filenames,
        bool is_bohr = true
    );
    int compute(
        Teuchos::RCP<const Basis> basis, 
        Teuchos::Array< Teuchos::RCP<State> >& states
    );

  protected:
    //void initialize();
    Cube_Density_from_Orbitals(){};
    //Teuchos::Array<std::string> cube_filenames;

    Teuchos::Array< Teuchos::RCP<Occupation> > out_occupations;
};

