#include "KLI.hpp"
//#include "Epetra_Time.h"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Calculus/Integration.hpp"
#include "../Utility/Parallel_Util.hpp"
#include "../Utility/Two_e_integral.hpp"
#include "../State/Scf_State.hpp"
#include "Teuchos_SerialDenseSolver.hpp"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;
using Teuchos::SerialDenseMatrix;
using std::vector;

KLI::KLI(RCP<const Basis> basis, int function_id, RCP<Poisson_Solver> poisson_solver, double PD_cutoff/*=-1.0*/): Compute_XC(basis, function_id){
    // no exchange potential(only energy) : -10
    // slater poential : -11
    // kli approximateion : -12
    // OEP-EXX : -13
    this->function_id = function_id;
    this->poisson_solver = poisson_solver;
    this->PD_cutoff = PD_cutoff;
    this -> orbital_range_mod[0] = 0;
    this -> orbital_range_mod[1] = 0;

    switch(function_id){
        case -10:
            Verbose::single(Verbose::Simple) << "KLI::KLI - No xc potential, exchange energy only \n";
            break;
        case -11:
            Verbose::single(Verbose::Simple) << "KLI::KLI - Slater potential\n";
            break;
        case -12:
            Verbose::single(Verbose::Simple) << "KLI::KLI - OEP-EXX potential, KLI approximation\n";
            break;
        case -13:
            Verbose::all() << "KLI::KLI - OEP-EXX potential, not implemented\n";
            exit(EXIT_FAILURE);
            break;
        default:
            Verbose::all() << "KLI::KLI - Undefined functional " << function_id << "!\n";
            exit(EXIT_FAILURE);
    }
    Verbose::single(Verbose::Normal) << "KLI::initialize_parameters - XC Functional was set.\n";
}

/*
int KLI::compute(RCP<const Basis> basis, Array<RCP<State> >& states){
    if(states.size()==0){
        Verbose::all() << "No given Orbitals!\n";
        exit(EXIT_FAILURE);
    }
    states.append(rcp(new State(*states[states.size()-1])));

    this->basis = basis;

    auto state = states[states.size()-1];
    auto map = basis->get_map();

    RCP<XC_State> xc_info = rcp(new XC_State(this -> function_id));
    xc_info -> initialize(basis, states[states.size()-1]);
    compute_vxc(xc_info);
    compute_Exc(xc_info);

    for(int i_spin=0; i_spin<xc_info -> potential.size(); i_spin++){
        state->local_potential[i_spin]->Update(1.0,*xc_info -> potential[i_spin],1.0);
    }
    state->total_energy += xc_info -> get_energy();

    return 0;
}

int KLI::compute(RCP<const Basis> basis, Array<RCP<Scf_State> >& states){

    if(states.size()==0){
        Verbose::all() << "No given density!\n";
        exit(EXIT_FAILURE);
    }
    states.append(rcp(new Scf_State(*states[states.size()-1])));

    this->basis = basis;

    auto state = states[states.size()-1];
    auto map = basis->get_map();

    RCP<XC_State> xc_info = rcp(new XC_State(this -> function_id));
    xc_info -> initialize(basis, states[states.size()-1]);

    compute_vxc(xc_info);
    compute_Exc(xc_info);

    state->x_potential = xc_info -> get_potential();
    state->x_energy = xc_info -> get_energy();

    return 0;
}
*/
int KLI::compute(RCP<XC_State> &xc_info){
    compute_vxc(xc_info);
    compute_Exc(xc_info);
    return 0;
}

void KLI::compute_Exc(RCP<XC_State> &xc_info){
    //core_density, core_density_grad는 안씀
    initialize_orbital_numbers(xc_info -> get_occupations());

//  Array< RCP<Epetra_MultiVector> > wavefunction_products = obtain_wavefunction_products(orbital_coeff);
//  obtain_K(wavefunction_products);
    //xc_info -> set_energy( obtain_exchange_energy(xc_info -> orbitals, xc_info -> occupations) );
    xc_info -> set_energy( obtain_exchange_energy2(xc_info -> get_orbitals(), xc_info -> get_occupations()) );
    //double energy1 = obtain_exchange_energy(xc_info -> orbitals, xc_info -> occupations);
    //double energy2 = obtain_exchange_energy2(xc_info -> orbitals, xc_info -> occupations);
    //Verbose::all() << "energy comparison : " << energy1 << " " << energy2 << std::endl;
    //exit(-1);
    return;
}

void KLI::compute_vxc(RCP<XC_State> &xc_info){
    //spin index :  occupations.size();
    //for(int i_spin=0; i_spin<exchange_potential.size(); i_spin++){
    //core_density와 core_density_grad는 함수에서 사용하지 않는다.
    auto map = basis->get_map();
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();

    Array< RCP<Epetra_Vector> > potential;
    for(int i_spin=0; i_spin < xc_info -> occupations.size(); i_spin++){
        potential.push_back(rcp(new Epetra_Vector(*map)));
    }
    initialize_orbital_numbers(xc_info -> get_occupations());

    switch(function_id){
        case -10:
            for(int i_spin=0; i_spin<potential.size(); i_spin++){
                potential[i_spin]->PutScalar(0.0);
            }
            break;
        case -11:
            slater_potential(xc_info -> get_val_density(), xc_info -> get_orbitals(), xc_info -> get_occupations(), potential);
            break;
        case -12:
            kli_potential(xc_info -> get_val_density(), xc_info -> get_orbitals(), xc_info -> get_occupations(), potential);
            break;
        default:
            Verbose::all() << function_id << " is unknown OEP_Level\n";
            exit(EXIT_FAILURE);
            break;
    }
    for(int i_spin = 0; i_spin < potential.size(); i_spin++){
        if(number_of_electrons[i_spin] == 0){
            potential[i_spin]->PutScalar(0.0);
        }
        else{
            Array<RCP<Epetra_Vector> > spin_density;
            spin_density.push_back(rcp(new Epetra_Vector(*xc_info -> val_density[i_spin])));

            RCP<Epetra_Vector> spin_density_coulomb = rcp(new Epetra_Vector(*map));
            double tmp_hartree_energy;
            poisson_solver->compute(spin_density, spin_density_coulomb, tmp_hartree_energy);
            for(int i=0;i<NumMyElements;i++){
                if(spin_density[0]->operator[](i)<1E-10){
                    double tmpval = (spin_density_coulomb->operator[](i))/(-1.0*number_of_electrons[i_spin]);
                    if (potential.size()==1)   tmpval *= 0.5;
                    potential[i_spin]->ReplaceGlobalValue(MyGlobalElements[i], 0, tmpval);
                }
            }
        }
    }

    xc_info -> set_potential(potential);

    return;
}

void KLI::compute_kernel(RCP<XC_State> &xc_info){
    Verbose::single(Verbose::Simple) << "\n#---------------------------------------------------- KLI::compute_fxc\n";
    Verbose::single(Verbose::Simple) << " !!! WARNING !!! KLI exchange kernel is not implemented.\n";
    Verbose::single(Verbose::Simple) << " If not \"ExchangeKernel\" is specified, PGG kernel will be used.\n";
    Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";
    return;
}

/**
 * @todo
 * - If the calculated value of "number_of_electrons" for alpha spin, or beta spin, is 1, KLI potential is same as Slater potential(Do not use whole KLI scheme)
 * - NUMBER_OF_ELECTRONS, NUMBER_OF_ORBITALS, NUMBER_OF_HOMO should be initialized properly ->done(?)
 * - Also, should be modified for ExDFT
 */
void KLI::initialize_orbital_numbers(Array<RCP<const Occupation> > occupations){
    //NUMBER_OF_ELECTRONS을 alpha spin, beta spin에 대해 각각 구해서 1이면 slater만 계산하게 하도록 해야함!
    //NUMBER_OF_ELECTRONS, NUMBER_OF_ORBITALS, NUMBER_OF_HOMO는 각각 다른값이고 처음에 계산해서 그 값을 사용하게 할것!

    number_of_orbitals = vector<int>(occupations.size());
    number_of_electrons = vector<int>(occupations.size());
    index_of_HOMO = vector<int>(occupations.size());
    this -> ind_start = vector<int>(occupations.size());

    //should be modified for ExDFT
    Verbose::single(Verbose::Normal) << "\n#------------------------------------------- KLI::initialize_orbital_numbers\n";
    for(int i_spin=0 ; i_spin<occupations.size() ; i_spin++){
        number_of_orbitals[i_spin] = occupations[i_spin]->get_size();
        number_of_electrons[i_spin] = ceil(occupations[i_spin]->get_total_occupation());
        //for exmaple, sum of total occupation number = 2.3 -> number_of_electrons = 3;
        index_of_HOMO[i_spin] = number_of_orbitals[i_spin];
        if (occupations.size()==1){
            number_of_electrons[0] = ceil(0.5*(occupations[i_spin]->get_total_occupation()));
            //total electron = 3.7 -> number_of_electrons[0] == 2;
        }

        if(number_of_electrons[i_spin] < index_of_HOMO[i_spin]){
            for(int i=number_of_orbitals[i_spin]-1 ; i>=0 ; i--){
                if(occupations[i_spin]->operator[](i)<1E-20){ //if occupations[i_spin][i]==0
                    index_of_HOMO[i_spin]--;
                }
                else{
                    break;
                }
            }
        }
        this -> ind_start[i_spin] += this -> orbital_range_mod[0];
        this -> index_of_HOMO[i_spin] -= this -> orbital_range_mod[1];
        Verbose::single(Verbose::Normal) << " number_of_orbitals[" << i_spin << "] : " << number_of_orbitals[i_spin] << "\tindex_of_HOMO : " << index_of_HOMO[i_spin]
            << "\tstarting index : " << this -> ind_start[i_spin] << "\tnumber_of_electrons : " << number_of_electrons[i_spin] << '\n';
    }
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
    return;
}

//Ex=-(1/2) sum(sum(occ_sigma,i * occ_sigma,j * int dr int dr' wfp_sigma,ij(r)*wfp_sigma,ji(r') / |r-r'| , ij..N_sigma), sigma)
//  =-(1/2) sum(sum(occ_sigma,i * occ_sigma,j * int dr wfp_sigma,ij(r)*K_sigma,ji(r) , ij..N_sigma), sigma)
//  =-(1/2) sum( occ_sigma,i*occ_sigma,j* (ij|ji) , ijsigma)
//using poisson solver
/**
 * @details
 * \f{eqnarray*}{
 * E_x&=&-\frac{1}{2} \sum_\sigma \sum_i^{N_\sigma} \sum_j^{N_\sigma} f_{\sigma,i} f_{\sigma,j} \times \int d\vec{r} \int d\vec{r'} \frac{\psi_{\sigma, ij}(\vec{r}) \psi_{\sigma,ji}(\vec{r'})}{|\vec{r}-\vec{r'}|} \\
 *    &=&-\frac{1}{2} \sum_\sigma \sum_i^{N_\sigma} \sum_j^{N_\sigma} f_{\sigma,i} f_{\sigma,j} \times \int d\vec{r} (\textrm{wavefunction\_prod})_{\sigma,ij} (\vec{r}) K_{\sigma,ji}(\vec{r})\\
 *    &=&-\frac{1}{2} \sum_\sigma \sum_i^{N_\sigma} \sum_j^{N_\sigma} f_{\sigma,i} f_{\sigma,j} \times (ij|ji)
 * \f}
 */
double KLI::obtain_exchange_energy(Array< RCP<const Epetra_MultiVector> > orbital_coeff, Array< RCP<const Occupation> > occupations){
    double energy = 0.0;
    for(int i_spin=0 ; i_spin < occupations.size() ; i_spin++){
        if(index_of_HOMO[i_spin] <= this -> ind_start[i_spin]){
            // energy = 0.0
        }
        else{
//      int index=0;
            for(int j = this -> ind_start[i_spin]; j<index_of_HOMO[i_spin]; j++){
                for(int i = j; i < index_of_HOMO[i_spin]; i++){
                    double integrated_value = 0.0;
//              RCP<Epetra_Vector> waveftn_product_component = rcp(new Epetra_Vector(*wavefunction_products[i_spin]->operator()(index)));
//              //K_sigma,ji = integrated part for r'
//              RCP<Epetra_Vector> K_component = rcp(new Epetra_Vector(*K[i_spin]->operator()(index)));
//              integrated_value = Integration::integrate(basis, grid_setting, waveftn_product_component, true, K_component, true);
                    integrated_value = Two_e_integral::compute_two_center_integral(basis, orbital_coeff[i_spin],i,j,j,i,poisson_solver);
                    integrated_value *= occupations[i_spin]->operator[](i);
                    integrated_value *= occupations[i_spin]->operator[](j);
                    if(occupations.size() == 1) integrated_value*=0.5;
                    if(i!=j) integrated_value *= 2.0;
                    energy += integrated_value;
                    //Verbose::single() << "energy1 : " << i_spin << " " << integrated_value << std::endl;
//                index++;
                }
//            index = index + number_of_orbitals[i_spin] - index_of_HOMO[i_spin];
            }
        }
    }
    return -0.5*energy;
}

//upsi_sigma,i(r) = -sum ( occ_j * psi_sigma,j(r) * K_sigma,ji(r) , j=1..N_sigma)
// for i = 1..N_sigma
Array< RCP<Epetra_MultiVector> > KLI::obtain_u_psi(Array<RCP<const Epetra_MultiVector> > orbital_coeff, Array<RCP<const Occupation> > occupations){
//Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > KLI::obtain_u_psi(Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvectors, Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > wavefunction_products, Teuchos::Array<Occupation*> occupations){
    Array<RCP<Epetra_MultiVector> > u_psi;
//    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > K = obtain_K(wavefunction_products);
//    obtain_K(wavefunction_products);
    auto map = basis->get_map();
    for(int i_spin = 0; i_spin < occupations.size(); i_spin++){
//      u_psi.push_back( rcp( new Epetra_MultiVector(eigenvectors[i_spin]->Map(), eigenvectors[i_spin]->NumVectors()) ) );
//      u_psi.push_back( rcp( new Epetra_MultiVector(map, number_of_orbitals[i_spin])));
        if(index_of_HOMO[i_spin] <= ind_start[i_spin]){
            u_psi.push_back( rcp( new Epetra_MultiVector(*map, 1)));
            u_psi[i_spin]->operator()(0)->PutScalar(0.0);
        }
        else{
            u_psi.push_back( rcp( new Epetra_MultiVector(*map, index_of_HOMO[i_spin])));

//      RCP<Epetra_MultiVector> sigma = rcp( new Epetra_MultiVector(map, number_of_orbitals[i_spin]) );
//      RCP<Epetra_MultiVector> wavefunctions = rcp(new Epetra_MultiVector(map, number_of_orbitals[i_spin]));
        //  TODO: Reduce memory when ind_start > 0
            RCP<Epetra_MultiVector> sigma = rcp( new Epetra_MultiVector(*map, index_of_HOMO[i_spin]) );
            RCP<Epetra_MultiVector> wavefunctions = rcp(new Epetra_MultiVector(*map, index_of_HOMO[i_spin]));
            for(int i = ind_start[i_spin];i<index_of_HOMO[i_spin];i++){
                RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*map));
                tmp->Scale(1.0,*orbital_coeff[i_spin]->operator()(i));
                Value_Coef::Value_Coef(basis, tmp, false, true, tmp);
                wavefunctions->operator()(i)->Scale(1.0, *tmp);
            }
//        Interpolation::change_basis_and_grid(basis,grid_setting,eigenvectors[i_spin],false,basis,grid_setting,wavefunctions, true);

        //생성시 0으로 초기화되있음

//        int vector_index = 0;
        // need to modify! use index_of_HOMO!
        // fixed! 150428 23:19

//      for(int j=0;j<eigenvectors[i_spin]->NumVectors();j++){
            for(int j = ind_start[i_spin];j<index_of_HOMO[i_spin];j++){
                double occupation_j = occupations[i_spin]->operator[](j);
                if (occupations.size() ==1){
                    occupation_j = occupation_j*0.5;
                }
                RCP<Epetra_Vector> Kjj;
                Two_e_integral::compute_Kji(basis,wavefunctions->operator()(j), wavefunctions->operator()(j), poisson_solver, Kjj);
//          sigma->operator()(j) -> Multiply(occupation_j,*(wavefunctions->operator()(j)),*(K[i_spin]->operator()(vector_index)),1.0);
                sigma->operator()(j) -> Multiply(occupation_j,*(wavefunctions->operator()(j)),*Kjj,1.0);
//          vector_index++;
//          for(int i=j+1;i<eigenvectors[i_spin]->NumVectors();i++){
                for(int i=j+1;i<index_of_HOMO[i_spin];i++){
                    double occupation_i = occupations[i_spin]->operator[](i);
                    if (occupations.size()==1){
                        occupation_i = occupation_i*0.5;
                    }
                    RCP<Epetra_Vector> Kji;
                    Two_e_integral::compute_Kji(basis,wavefunctions->operator()(j), wavefunctions->operator()(i), poisson_solver, Kji); //Kji==Kij
                    sigma->operator()(j) -> Multiply(occupation_i,*(wavefunctions->operator()(i)),*(Kji),1.0);
                    sigma->operator()(i) -> Multiply(occupation_j,*(wavefunctions->operator()(j)),*(Kji),1.0);
//              sigma->operator()(j) -> Multiply(occupation_i,*(wavefunctions->operator()(i)),*(K[i_spin]->operator()(vector_index)),1.0);
//              sigma->operator()(i) -> Multiply(occupation_j,*(wavefunctions->operator()(j)),*(K[i_spin]->operator()(vector_index)),1.0);
//              vector_index++;
                }
            }
            for(int i = ind_start[i_spin];i<index_of_HOMO[i_spin];i++){
                u_psi[i_spin]->operator()(i) -> Scale( -1.0, *(sigma->operator()(i)));
            }
        }
    }

    return u_psi;
}

//slater potential_sigma(r) = sum(rho_sigma,i(r) * u_sigma,i(r) / rho_sigma(r), i=1..N_sigma)
//XPotential = -11
void KLI::slater_potential(Array<RCP<const Epetra_Vector> > density, Array< RCP<const Epetra_MultiVector> > orbital_coeff, Array< RCP<const Occupation> > occupations, Array<RCP<Epetra_Vector> > & exchange_potential){
//void KLI::slater_potential(Teuchos::RCP<Epetra_MultiVector> density, Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvectors, Teuchos::Array<Occupation*> occupations, Teuchos::RCP<Epetra_MultiVector> exchange_potential){
//    RCP<Epetra_MultiVector> slater = rcp(new Epetra_MultiVector( basis->Map(), occupations.size()));
//    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > wavefunction_products = obtain_wavefunction_products(eigenvectors);
//    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > u_psi = obtain_u_psi(eigenvectors, wavefunction_products, occupations);

    Array< RCP<Epetra_MultiVector> > u_psi = obtain_u_psi(orbital_coeff, occupations);
    Array<RCP<Epetra_Vector> > slater = slater_potential_internal(density, orbital_coeff, u_psi, occupations);

    for(int i_spin=0;i_spin<occupations.size();i_spin++){
        exchange_potential[i_spin]->Update(1.0,*(slater[i_spin]),1.0);
    }
//    exchange_potential->Update(1.0, *slater, 1.0);
    return;
}


//slater potential_sigma(r) = sum(rho_sigma,i(r) * u_sigma,i(r) / rho_sigma(r), i=1..N_sigma)
//inside KLI
Array<RCP<Epetra_Vector> > KLI::slater_potential_internal(Array<RCP<const Epetra_Vector> > density, Array< RCP<const Epetra_MultiVector> > orbital_coeff, Array<RCP<Epetra_MultiVector> > u_psi, Array<RCP<const Occupation> > occupations){
//Teuchos::RCP<Epetra_MultiVector> KLI::slater_potential(Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvectors, Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > u_psi, Teuchos::RCP<Epetra_MultiVector> density, Teuchos::Array<Occupation*> occupations){
    auto map = basis->get_map();
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();

    Array<RCP<Epetra_Vector> > slater;

    for(int i_spin=0;i_spin<occupations.size();i_spin++){
        slater.push_back(rcp(new Epetra_Vector(*map)));
        if(index_of_HOMO[i_spin] == ind_start[i_spin]){
            RCP<Epetra_MultiVector> wavefunctions = rcp(new Epetra_MultiVector(*map, 1));
            wavefunctions->operator()(0)->PutScalar(0.0);
        }
        else{
            RCP<Epetra_MultiVector> wavefunctions = rcp(new Epetra_MultiVector(*map, index_of_HOMO[i_spin]));
            for(int i = ind_start[i_spin];i<index_of_HOMO[i_spin];i++){
                RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*map));
                tmp->Scale(1.0,*orbital_coeff[i_spin]->operator()(i));
                Value_Coef::Value_Coef(basis, tmp, false, true, tmp);
                wavefunctions->operator()(i)->Scale(1.0, *tmp);
            }

            RCP<Epetra_Vector> sigma = rcp( new Epetra_Vector(*map) );
            for(int i = ind_start[i_spin]; i < index_of_HOMO[i_spin] ;i++){
                double occupation_i = occupations[i_spin]->operator[](i);
                if (occupations.size() ==1){
                    occupation_i = occupation_i*0.5;
                }
                sigma-> Multiply(occupation_i, *(wavefunctions->operator()(i)), *(u_psi[i_spin]->operator()(i)) , 1.0);
                //n_sigmai = f_sigmai |psi_sigmai|^2
            }
            if (occupations.size() == 1){
                slater[0] -> ReciprocalMultiply(2.0, *(density[0]), *sigma, 0.0);
            }
            else{
                slater[i_spin] -> ReciprocalMultiply(1.0, *(density[i_spin]), *sigma, 0.0);
            }
        }
        /******************************/
        //For grid cutting, 0 density exception solved. jaewook 151230
        Array<RCP<Epetra_Vector> > spin_density;
        spin_density.push_back(rcp(new Epetra_Vector(*density[i_spin])));

        RCP<Epetra_Vector> spin_density_coulomb = rcp(new Epetra_Vector(*map));
        double tmp_hartree_energy;
        poisson_solver->compute(spin_density, spin_density_coulomb, tmp_hartree_energy);
        for(int i=0;i<NumMyElements;i++){
            if(spin_density[0]->operator[](i)<1E-10){
                double tmpval = (spin_density_coulomb->operator[](i))/(-1.0*number_of_electrons[i_spin]);
                if (occupations.size() == 1)        tmpval *= 0.5;
                slater[i_spin]->ReplaceGlobalValue(MyGlobalElements[i], 0, tmpval);
            }
        }
        /******************************/
    }
    return slater;
}

//delta_M = delta_ji - M_sigma,ji
//M_sigma_ji = int(rho_sigma,j(r) * rho_sigma,i(r) / rho_sigma(r), dr)
//i, j = 1..N_sigma-1(HOMO-1)
//Teuchos::RCP< Teuchos::SerialDenseMatrix<int, double> > KLI::obtain_delta_M(Teuchos::RCP<Epetra_MultiVector> density,Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > wavefunction_products, Teuchos::Array<Occupation*> occupations, int alpha){
RCP< SerialDenseMatrix<int, double> > KLI::obtain_delta_M(RCP<const Epetra_Vector> density, RCP<Epetra_MultiVector> orbital_density, Array<RCP<const Occupation> > occupations, int alpha){
    //calculating delta_ji-M_sigmaji
    auto map = basis->get_map();
    RCP< SerialDenseMatrix<int, double> > delta_M;
    delta_M = rcp( new SerialDenseMatrix<int, double> (index_of_HOMO[alpha]-1, index_of_HOMO[alpha]-1) );
    RCP<Epetra_Vector> tmp_vector1 = rcp( new Epetra_Vector(*map));
    RCP<Epetra_Vector> tmp_vector2 = rcp( new Epetra_Vector(*map));
    tmp_vector2 -> Reciprocal(*density);
    Value_Coef::Value_Coef(basis, tmp_vector2, true, false, tmp_vector2);

	for(int j = ind_start[alpha];j<index_of_HOMO[alpha]-1;j++){
		double occupation_j = occupations[alpha]->operator[](j);
       	if (occupations.size()==1) occupation_j = occupation_j*0.5;

		for(int i = ind_start[alpha];i<index_of_HOMO[alpha]-1;i++){
			double occupation_i = occupations[alpha]->operator[](i);
       	    if (occupations.size()==1)		occupation_i = occupation_i*0.5;

            //n_sigmai = f_sigmai |psi_sigmai|^2
//            tmp_vector1 -> Multiply(occupation_i*occupation_j, *(wavefunction_products[alpha]->operator()(two_ptcl_index_diagonal(j,number_of_orbitals[alpha]))), *(wavefunction_products[alpha]->operator()(two_ptcl_index_diagonal(i,number_of_orbitals[alpha]))), 0.0);
            tmp_vector1 -> Multiply(occupation_i*occupation_j, *(orbital_density->operator()(j)), *(orbital_density->operator()(i)), 0.0);


            if (occupations.size()==1){
                delta_M->operator()(j,i) = -2.0*Integration::integrate(basis, tmp_vector1, true, tmp_vector2, false);
            }
            else{
                delta_M->operator()(j,i) = -Integration::integrate(basis, tmp_vector1, true, tmp_vector2, false);
            }
            if(i==j){
                delta_M->operator()(j,i) += 1.0;
            }
        }
    }
    return delta_M;
}

//slater_norm_sigma,i = <sigma,i|slater_sigma - u_sigma,i|sigma,i>
//i = 1..N_sigma-1(HOMO-1)
//Teuchos::RCP< Teuchos::SerialDenseMatrix<int, double> > KLI::obtain_slater_norm(Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvectors, Teuchos::RCP<Epetra_MultiVector> slater, Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > u_psi, int alpha){
RCP< SerialDenseMatrix<int, double> > KLI::obtain_slater_norm(Array< RCP<const Epetra_MultiVector> > orbital_coeff, Array<RCP<Epetra_Vector> > slater, Array<RCP<Epetra_MultiVector> > u_psi, int alpha){
    auto map = basis->get_map();
    RCP< SerialDenseMatrix<int, double> > output = rcp( new SerialDenseMatrix<int, double>(index_of_HOMO[alpha]-1, 1) ) ;

    for(int i = ind_start[alpha]; i<index_of_HOMO[alpha]-1;i++){
        RCP<Epetra_Vector> orbcoef = rcp(new Epetra_Vector(*map));
        orbcoef->Scale(1.0,*orbital_coeff[alpha]->operator()(i));
        RCP<Epetra_Vector> potential_difference = rcp(new Epetra_Vector(*map) );
//        potential_difference->Multiply(1.0, *(orbital_coeff[alpha]->operator()(i)), *(slater[alpha]), 0.0);
        potential_difference->Multiply(1.0, *(orbcoef), *(slater[alpha]), 0.0);
//        RCP<Epetra_Vector> u_coeff = rcp(new Epetra_Vector(*map));
        RCP<Epetra_Vector> u_coeff = rcp(new Epetra_Vector(*u_psi[alpha]->operator()(i)));
//        Interpolation::change_basis_and_grid(basis,grid_setting,Teuchos::rcp(new Epetra_Vector(*(u_psi[alpha]->operator()(i)))),true,basis,grid_setting,u_coeff, false);
//        Value_Coef::Value_Coef(basis,rcp(u_psi[alpha]->operator()(i)),true,false,u_coeff);
        Value_Coef::Value_Coef(basis,u_coeff,true,false,u_coeff);
        potential_difference->Update(-1.0, *(u_coeff), 1.0);
        double value = 0.0;
        potential_difference->Dot(*orbcoef, &value);
        //potential_difference->Dot(*orbital_coeff[alpha]->operator()(i), &value);
        output->operator()(i,0) = value;
    }
    return output;
}

//kli_norm_sigma,i = <sigma,i|kli_sigma - u_sigma,i|sigma,i>
//i = 1..N_sigma-1(HOMO-1)
RCP<SerialDenseMatrix<int, double> > KLI::obtain_kli_norm(RCP<SerialDenseMatrix<int, double> > delta_M, RCP<SerialDenseMatrix<int, double> > slater_norm){
    int numrows = slater_norm->numRows();
    int numcols = slater_norm->numCols();

    RCP<SerialDenseMatrix<int, double> >KLI_norm = rcp( new SerialDenseMatrix<int, double>(numrows, numcols) );

    Teuchos::SerialDenseSolver<int, double> linear_solver;
//    linear_solver.setMatrix( Teuchos::rcp( new Teuchos::SerialDenseMatrix<int, double>( *delta_M)) ) ;
//    linear_solver.setVectors(KLI_norm, Teuchos::rcp( new Teuchos::SerialDenseMatrix<int, double>( *slater_norm ) ) ) ;
    linear_solver.setMatrix( delta_M ) ;
    linear_solver.setVectors(KLI_norm, slater_norm ) ;
    if(linear_solver.shouldEquilibrate()){
        linear_solver.equilibrateMatrix();
        linear_solver.equilibrateRHS();
        linear_solver.solve();
        linear_solver.unequilibrateLHS();
    }
    else{

        linear_solver.solve();
    }
    return KLI_norm;
}


//void KLI::kli_potential(Teuchos::RCP<Epetra_MultiVector> density, Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvectors, Teuchos::Array<Occupation*> occupations, Teuchos::RCP<Epetra_MultiVector> exchange_potential){
void KLI::kli_potential(Array<RCP<const Epetra_Vector> > density, Array< RCP<const Epetra_MultiVector> > orbital_coeff, Array< RCP<const Occupation> > occupations, Array<RCP<Epetra_Vector> > & exchange_potential){

    auto map = basis->get_map();
//    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > wavefunction_products = obtain_wavefunction_products(eigenvectors);
//    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > u_psi = obtain_u_psi(eigenvectors, wavefunction_products, occupations);

    //Array<RCP<Epetra_MultiVector> > u_psi = obtain_u_psi(orbital_coeff, occupations);
    Array<RCP<Epetra_MultiVector> > u_psi = obtain_u_psi2(orbital_coeff, occupations);
    //수정해야함
    //std::cout << "check" << std::endl;
    /*
    for(int i=0; i<u_psi[0]->NumVectors(); i++){
        for(int j=0; j<u_psi[0]->operator()(0)->MyLength(); j++){
            if(abs(u_psi[0]->operator[](i)[j]-u_psi2[0]->operator[](i)[j]) >0.00000000000000001)
                Verbose::single(Verbose::Detail) << i << " " << j << " " << u_psi[0]->operator[](i)[j] << " " << u_psi2[0]->operator[](i)[j] << " "
                    << u_psi[0]->operator[](i)[j]/u_psi2[0]->operator[](i)[j] << std::endl;
        }

    }
    */
    //exit(-1);



    Array<RCP<Epetra_Vector> > slater = slater_potential_internal(density, orbital_coeff, u_psi, occupations);
    Array<RCP<Epetra_Vector> > kli;
    for(int i_spin=0;i_spin<occupations.size();i_spin++){
        kli.push_back(rcp(new Epetra_Vector(*slater[i_spin])));
        //exchange_potential.push_back(rcp(new Epetra_Vector(*map)));
    }
    // major term. v_kli = v_slater + ...
    //
//    Teuchos::RCP<Epetra_MultiVector> kli = Teuchos::rcp(new Epetra_MultiVector( *slater) );

//    Teuchos::Array< Teuchos::RCP< Teuchos::SerialDenseMatrix<int, double> > > kli_norm = obtain_kli_norm(obtain_delta_M(density, wavefunction_products, occupations), obtain_slater_norm(eigenvectors, slater, u_psi));

//  for(int alpha=0 ; alpha < occupations.size(); alpha++){
    for(int i_spin=0 ; i_spin < occupations.size(); i_spin++){

        if(index_of_HOMO[i_spin] <= ind_start[i_spin] + 1){
            //V_x = V_slater, already initialized
        }
        else{ //index_of_HOMO[i_spin]>1
            RCP<Epetra_Vector> sigma = rcp( new Epetra_Vector(*map) );

////////////////////////
            RCP<Epetra_MultiVector> orbital_val = rcp(new Epetra_MultiVector(*map, index_of_HOMO[i_spin]-1));
            for(int i = ind_start[i_spin] ;i<index_of_HOMO[i_spin]-1;i++){
                RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*map));
                tmp->Scale(1.0,*orbital_coeff[i_spin]->operator()(i));
                Value_Coef::Value_Coef(basis, tmp, false, true, tmp);
                orbital_val->operator()(i)->Scale(1.0, *tmp);
            }
            RCP<Epetra_MultiVector> orbital_density = rcp( new Epetra_MultiVector(*map, index_of_HOMO[i_spin]-1));
            orbital_density->Multiply(1.0, *orbital_val, *orbital_val, 0.0);
////////////////////////

            RCP< SerialDenseMatrix<int, double> > delta_M = obtain_delta_M(density[i_spin], orbital_density, occupations, i_spin);
            RCP< SerialDenseMatrix<int, double> > slater_norm = obtain_slater_norm(orbital_coeff, slater, u_psi, i_spin);
            RCP< SerialDenseMatrix<int, double> > kli_norm = obtain_kli_norm(delta_M, slater_norm);
            /*
               RCP< SerialDenseMatrix<int, double> > kli_norm = obtain_kli_norm(obtain_delta_M(density[i_spin], orbital_density, occupations, i_spin), obtain_slater_norm(orbital_coeff, slater, u_psi, i_spin));
            Verbose::single(Verbose::Detail) << "kli_norm is obtained\n";
	        */
            for(int i = ind_start[i_spin];i<index_of_HOMO[i_spin]-1;i++){
                double occupation_i = occupations[i_spin]->operator[](i);
                if (occupations.size()==1)	occupation_i = occupation_i*0.5;

                sigma->Update( (occupation_i * kli_norm->operator()(i,0)), *(orbital_density->operator()(i)), 1.0);
//              sigma->Update( (occupation_i), *(orbital_density->operator()(i)), 1.0);
            }
            if (occupations.size()==1){
                kli[i_spin] -> ReciprocalMultiply(2.0, *(density[i_spin]), *sigma, 1.0);
            }
            else{
                kli[i_spin] -> ReciprocalMultiply(1.0, *(density[i_spin]), *sigma, 1.0);
            }
        }
        exchange_potential[i_spin]->Update(1.0,*(kli[i_spin]),0.0);
    }
//    exchange_potential->Update(1.0, *kli, 1.0);
    return;

}

std::string KLI::get_functional_type(){
    return "KLI-PGG";
}

std::string KLI::get_info_string(bool verbose/* = true*/){
    if(!verbose){
        return "KLI";
    }
    std::string info = "KLI\n";
    switch(this->function_id){
        case -10:
            info += "No potential (exchange energy only)\n";
            break;
        case -11:
            info += "Slater potential\n";
            break;
        case -12:
            info += "OEP-EXX potential with KLI approximation\n";
            break;
        case -13:
            info += "Not implemented OEP-EXX potential\n";
        default:
            info += "Unknown functional\n";
    }
    info += this -> poisson_solver -> get_info_string();
    return info;
}

int KLI::get_functional_kind(){
    return 0;
}

Array< RCP<Epetra_MultiVector> > KLI::obtain_u_psi2(Array<RCP<const Epetra_MultiVector> > orbital_coeff, Array<RCP<const Occupation> > occupations){
    Array<RCP<Epetra_MultiVector> > u_psi;
    auto map = basis->get_map();
    for(int i_spin = 0; i_spin < occupations.size(); i_spin++){
    //for(int i_spin = 0; i_spin < 1; i_spin++){
        if(index_of_HOMO[i_spin]==0){
            u_psi.push_back( rcp( new Epetra_MultiVector(*map, 1)));
            u_psi[i_spin]->operator()(0)->PutScalar(0.0);
        }
        else{
            u_psi.push_back( rcp( new Epetra_MultiVector(*map, index_of_HOMO[i_spin])));

//            RCP<Epetra_MultiVector> sigma = rcp( new Epetra_MultiVector(*map, index_of_HOMO[i_spin]) );

            RCP<Epetra_MultiVector> wavefunctions = rcp(new Epetra_MultiVector(*map, index_of_HOMO[i_spin]));
            for(int i=0;i<index_of_HOMO[i_spin];i++){
                RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*map));
                tmp->Scale(1.0,*orbital_coeff[i_spin]->operator()(i));
                Value_Coef::Value_Coef(basis, tmp, false, true, tmp);
                wavefunctions->operator()(i)->Scale(1.0, *tmp);
            }
            //std::cout << std::setprecision(6);
            //std::cout << *wavefunctions << std::endl;
            int vec_dim = index_of_HOMO[i_spin];
            int dim = wavefunctions -> GlobalLength();
            double** combine_wf = new double*[vec_dim];
            double* combine_sigma = new double[vec_dim*dim];
            for(int i=0; i<vec_dim; ++i){
                combine_wf[i] = new double[dim]();
            }
            for(int i=0; i<vec_dim*dim; i++)
                combine_sigma[i] = 0;
            Parallel_Util::group_allgather_multivector(wavefunctions, combine_wf);
            /*
            for(int j=0; j<index_of_HOMO[i_spin];j++){
                double occupation_j = occupations[i_spin]->operator[](j);
                if (occupations.size() ==1){
                    occupation_j = occupation_j*0.5;
                }
                double* Kjj = new double[dim];
                Two_e_integral::compute_Kji(combine_wf[j], combine_wf[j], poisson_solver, Kjj, dim);

                for(int k=0; k<dim; k++)
                    combine_sigma[j*dim+k] += occupation_j*combine_wf[j][k]*Kjj[k];
                delete [] Kjj;
            }
            */
            std::vector< std::vector<int> > index_list;
            check_PD_norm(index_list, wavefunctions, i_spin);


            int mpi_size = Parallel_Manager::info().get_total_mpi_size();
            int mpi_rank = Parallel_Manager::info().get_total_mpi_rank();
            for(int index=0; index<index_list.size(); index++){
                if(index%mpi_size != mpi_rank)
                    continue;
                int j = index_list[index][0];
                int i = index_list[index][1];
                double occupation_i = occupations[i_spin]->operator[](i);
                double occupation_j = occupations[i_spin]->operator[](j);
                if (occupations.size()==1){
                    occupation_i = occupation_i*0.5;
                    occupation_j = occupation_j*0.5;
                }
                double* Kji = new double[dim];
                Two_e_integral::compute_Kji(combine_wf[j], combine_wf[i], poisson_solver, Kji, dim); //Kji==Kij
                if(i!=j){
                    for(int k=0; k<dim; k++){
                        combine_sigma[j*dim+k] += occupation_i*combine_wf[i][k]*Kji[k];
                        combine_sigma[i*dim+k] += occupation_j*combine_wf[j][k]*Kji[k];
                    }
                }
                else{
                    for(int k=0; k<dim; k++)
                        combine_sigma[j*dim+k] += occupation_i*combine_wf[i][k]*Kji[k];
                }
                delete [] Kji;
            }

            for(int i=0; i<vec_dim; ++i){
                delete [] combine_wf[i];
            }
            delete [] combine_wf;

            double* total_sigma = new double [vec_dim*dim];
            Parallel_Util::group_sum(combine_sigma, total_sigma, vec_dim*dim);
            int NumMyElements = basis->get_map()->NumMyElements();
            int* MyGlobalElements = basis->get_map()->MyGlobalElements();
            for(int i=0;i<index_of_HOMO[i_spin];i++){
                for(int j=0; j<NumMyElements; j++){
                    u_psi[i_spin]->operator[](i)[j] = -total_sigma[i*dim+MyGlobalElements[j]];
                    //u_psi[i_spin]->operator[](i)[j] = -combine_sigma[i*dim+MyGlobalElements[j]];
                    //u_psi[i_spin]->operator[](i)[j] = 0.0;
                }
            }
            delete [] combine_sigma;
            delete [] total_sigma;
        }
    }
    //std::cout << *u_psi[0] << std::endl;
    return u_psi;
}

double KLI::obtain_exchange_energy2(Array< RCP<const Epetra_MultiVector> > orbital_coeff, Array< RCP<const Occupation> > occupations){
    double energy = 0.0;
    for(int i_spin=0 ; i_spin < occupations.size() ; i_spin++){
    //for(int i_spin=0 ; i_spin < 1 ; i_spin++){
        if(index_of_HOMO[i_spin] <= this -> ind_start[i_spin]){
            // energy = 0.0
        }
        else{
//        int index=0;
            int vec_dim = index_of_HOMO[i_spin];
            int dim = orbital_coeff[i_spin] -> GlobalLength();
            double** combine_wf = new double*[vec_dim];
            for(int i=0; i<vec_dim; ++i) combine_wf[i] = new double[dim]();
            Teuchos::RCP<Epetra_MultiVector> wavefunctions = rcp(new Epetra_MultiVector(orbital_coeff[i_spin]->Map(), index_of_HOMO[i_spin]));
            for(int n = this -> ind_start[i_spin]; n<index_of_HOMO[i_spin]; n++){
                for(int m=0; m<orbital_coeff[i_spin]->Map().NumMyElements(); m++){
                    wavefunctions->operator[](n)[m] = orbital_coeff[i_spin]->operator[](n)[m];
                }
            }

//            Parallel_Uil::group_allgather_multivector(orbital_coeff[i_spin], combine_wf);
            Parallel_Util::group_allgather_multivector(wavefunctions, combine_wf);
            std::vector< std::vector<int> > index_list;
            check_PD_norm(index_list, wavefunctions, i_spin);
            wavefunctions = Teuchos::null;
            int mpi_size = Parallel_Manager::info().get_total_mpi_size();
            int mpi_rank = Parallel_Manager::info().get_total_mpi_rank();
            double tmp_energy = 0.0;
            double tmp_energy2 = 0.0;
            for(int index=0; index<index_list.size(); index++){
                if(index%mpi_size != mpi_rank)
                    continue;
                int j = index_list[index][0];
                int i = index_list[index][1];
                double integrated_value = 0.0;
                double integrated_value2 = 0.0;
                integrated_value = Two_e_integral::compute_two_center_integral(basis, combine_wf ,i,j,j,i,poisson_solver);
                //integrated_value = Two_e_integral::compute_two_center_integral(basis, orbital_coeff[i_spin],i,j,j,i,poisson_solver);
                integrated_value *= occupations[i_spin]->operator[](i);
                integrated_value *= occupations[i_spin]->operator[](j);
                if(occupations.size() == 1) integrated_value*=0.5;
                if(i!=j) integrated_value *= 2.0;
                tmp_energy += integrated_value;
                //                index++;
            }
            double total_E = 0.0;

            Parallel_Util::group_sum(&tmp_energy, &tmp_energy2, 1);
            //tmp_energy2 = tmp_energy;
            energy += tmp_energy2;
            //            index = index + number_of_orbitals[i_spin] - index_of_HOMO[i_spin];
            for(int i=0; i<vec_dim; ++i) delete [] combine_wf[i];
            delete[] combine_wf;
        }
    }
    return -0.5*energy;
}

void KLI::check_PD_norm(std::vector< std::vector<int> >& index_list, Teuchos::RCP<Epetra_MultiVector> orbitals, int i_spin){
    double st = MPI_Wtime();
    RCP<Epetra_Vector> pair_density = rcp(new Epetra_Vector(*basis->get_map()));
    int original_index=0;
    Verbose::single(Verbose::Normal) << "PD cutoff is " << PD_cutoff << std::endl;
    for(int j=0;j<index_of_HOMO[i_spin];j++){
        for(int i=j;i<index_of_HOMO[i_spin];i++){
            std::vector<int> tmp;
            double norm1 = 0.0;
            if(PD_cutoff>0.0){

                pair_density->Multiply(1.0, *orbitals->operator()(i), *orbitals->operator()(j), 0.0);
                pair_density->Norm1(&norm1);
                if(norm1>PD_cutoff){
                    tmp.push_back(j);
                    tmp.push_back(i);
                    index_list.push_back(tmp);
                }
            }
            else{
                tmp.push_back(j);
                tmp.push_back(i);
                index_list.push_back(tmp);
            }
            original_index++;
        }
    }
    double end = MPI_Wtime();
    Verbose::single(Verbose::Normal) << "Number of pair density is reduced from " << original_index << " " << index_list.size()
                                     << ", time : " << end-st << " s" << std::endl;
}
