#pragma once
#include <vector>
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "Compute_Interface.hpp"
#include "Core_Hamiltonian.hpp"
#include "../Core/Mixing/Mixing.hpp"
#include "../Core/ExternalField/Create_External_Field.hpp"
#include "Poisson_Solver.hpp"
#include "../Core/Convergence/Convergence.hpp"
#include "../Core/Diagonalize/Diagonalize.hpp"
#include "../State/Scf_State.hpp"
#include "Exchange_Correlation.hpp"

#include "../Core/Solvation/Solvation.hpp"

/**
@brief Self Consistant Field calculation
@author Kwangwoo Hong, and Sunghwan Choi, (modified by Jaewook Kim, Sungwoo Kang, and Jaechang Lim)
*/
class Scf: public Compute_Interface{
    public:
        //vector<double> iterate(double ion_ion);
        //vector<double> nonSCF(double ion_ion);
        //Scf(Teuchos::RCP<Core_Hamiltonian> core_hamiltonian,Teuchos::RCP<Poisson_Solver> poisson_solver,Teuchos::RCP<Exchange_Correlation> exchange_correlation, Teuchos::RCP<ParameterList> parameters);
        /**
         * @brief Scf class constructor.
         * @callergraph
         * @callgraph
         **/
        Scf(
            Teuchos::RCP<const Basis> basis,
            Teuchos::RCP<Teuchos::ParameterList> parameters,
            Teuchos::RCP<Core_Hamiltonian> core_hamiltonian = Teuchos::null
        );

        Scf(){};

        /**
         * @brief Destructor. Print time informations.
         * @callergraph
         * @callgraph
         **/
        ~Scf();

        /**
         * @brief Perform computations.
         * @callergraph
         * @callgraph
         **/
        virtual int compute(
            Teuchos::RCP<const Basis> basis,
            Teuchos::Array< Teuchos::RCP<State> >& states
        );

        Teuchos::RCP<Core_Hamiltonian> get_core_hamiltonian();

        //Teuchos::RCP<Epetra_MultiVector> get_density();
        //Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > get_orbitals();
        //Teuchos::RCP<Epetra_Vector> get_hartree_potential();
        //Teuchos::RCP<Epetra_MultiVector> get_x_potential();
        //Teuchos::RCP<Epetra_MultiVector> get_c_potential();
        //Teuchos::Array< vector<double > > get_eigenvalues();
        //Teuchos::Array<Occupation*> get_occupations();

    protected:
        /**
         * @brief Actual initialization routine.
         **/
        virtual void initialize(Teuchos::RCP<const Basis> mesh, Teuchos::RCP<Teuchos::ParameterList> scf_param);
        /**
         * @brief Diagonalization of converged states.
         * @callergraph
         * @callgraph
         **/
        virtual int final_diagonalize(Teuchos::Array<Teuchos::RCP<Epetra_Vector> > external_potential,Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix);

        /**
         * @brief Creates new states.
         * @callergraph
         * @callgraph
         **/
        virtual int append_states(Teuchos::RCP<Scf_State> out_state );
        /**
         * @callergraph
         * @callgraph
         **/
        virtual bool print_energy(int i_scf, Teuchos::RCP<Scf_State> out_state);
        /**
         * @callergraph
         * @callgraph
         **/
        virtual bool convergence_check(int i_scf,bool is_compute_energy_this_step);

        virtual int calculate_energy(Teuchos::RCP<Scf_State>& state,
            double &hartree_energy,
            std::vector<double> &kinetic_energies,
            double &x_energy,
            double &c_energy,
            double &int_n_vxc,
            double &external_energy,
            double &orbital_energy_sum,
            double &numerical_correction,
            double &solvation_energy,
            double &dispersion_energy
        );

        int display_energy(Teuchos::RCP<Scf_State> state,
            double hartree_energy,
            std::vector<double> kinetic_energies,
            double x_energy,
            double c_energy,
            double int_n_vxc,
            double external_energy,
            double orbital_energy_sum,
            double numerical_correction,
            double solvation_energy,
            double dispersion_energy
        );

        /**
         * @callergraph
         * @callgraph
         **/
        Teuchos::RCP<Scf_State> generate_in_state();
        bool is_final_diag = false;
        Teuchos::RCP<const Basis> basis;
        Teuchos::RCP<Core_Hamiltonian> core_hamiltonian;
        Teuchos::RCP<Poisson_Solver> poisson_solver;
        Teuchos::RCP<Exchange_Correlation> exchange_correlation;
        const Teuchos::RCP<Teuchos::ParameterList> parameters;

        Teuchos::RCP<Diagonalize> diagonalize;
        Teuchos::RCP<Diagonalize> final_diagonalizer;
        Teuchos::RCP<Mixing> mixing; ///< Mixing class for Scf iteration. Requires whole scf_states. See the iterate() function for the detail.
        Teuchos::RCP<Convergence> convergence;
        Teuchos::RCP<External_Field> external_field;

        Teuchos::Array< Teuchos::RCP<Scf_State> > scf_states;

/**
 * @brief Actual Itration is done in here.
 * @todo The part before the main for loop should be changed to Create_Compute_Interface
 * @todo StepSizeForReduceDiagonalizeTol modify
 * @detail Definitions of the "in_state" and "out_state" are written in
    http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf
    3rd page

    for linear mixing, we mix "in_state" and "out_state"
    for pulay mixing, we mix "in_state", which are stored in states
    (be aware that pulay mixing class stores "residue" = "out_state" - "in_state"
    in scf_states, ONLY the "in_state" should be stored (to restart the calculation),
    except for the last state(last state is "out_state", which is converged state)

    nth scf_states component contain follwing information:
      -  n-1th out_state orbitals & orbital energies
      -  n-1th mixed_state(or out_state) density = nth in_state density
      -  n-1th mixed_state(or out_state) local_potential = nth in_state local_potential
      -  occupations (occupation information would not be changed)
      -  core_density & core_density_grad (which come from psudopotential)
      -  n-1th mixed_state(or out_state) hartree, x, c_potential, which are components of the local_potential.
        
         * @callergraph
         * @callgraph
         **/
        virtual int iterate(Teuchos::RCP<State> initial_state, Teuchos::RCP<State>& final_state);
//        int generate_local_potential(Teuchos::RCP<Scf_State> state,int i_spin,Teuchos::RCP<Epetra_Vector>& local_potential);
//        int generate_local_potential(Teuchos::RCP<Scf_State> state,int i_spin);
        /**
         * @callergraph
         * @callgraph
         **/
        virtual int generate_local_potential(Teuchos::RCP<Scf_State> state);
        //void initialize_parameters();
/*
        struct Convergence_Checking_Parameters{
            double energy;
            Teuchos::Array< vector<double > > eigenvalues;
            Teuchos::RCP<Epetra_MultiVector> density;
            Teuchos::RCP<Epetra_MultiVector> potential;
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals;
        };

        bool check_convergence(Convergence_Checking_Parameters* current,Convergence_Checking_Parameters* before);
*/
        //vector<double> calculate_energy(double ion_ion, Convergence_Checking_Parameters* convergence_current);
        //vector<double> calculate_energy_1(double ion_ion, Convergence_Checking_Parameters* convergence);
        /**
         * @callergraph
         * @callgraph
         **/
        virtual int calculate_energy(Teuchos::RCP<Scf_State>& state);
        /**
         * @callergraph
         * @callgraph
         **/
        void print_coulomb_exchange_integrals(Teuchos::RCP<State> state);
        /**
         * @callergraph
         * @callgraph
         **/
        void print_spin_contamination(Teuchos::RCP<State> state);

        bool make_hamiltonian_matrix = true;
        int seed = -1;

        Teuchos::RCP<Solvation> solvation = Teuchos::null;
//        void set_occupations(int num_eigenpairs);
};
