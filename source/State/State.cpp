#include "State.hpp"
#include <fstream>
#include "../Io/Io_Atoms.hpp"
#include "../Utility/Value_Coef.hpp"
#include "../Utility/Value_Coef.hpp"
using std::vector;
using std::string;

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

bool State::do_shallow_copy_orbitals=false;

State::State(){
    this->timer= rcp(new Time_Measure() );
}
/*
State::State(RCP<const Epetra_Map> map,int polarize){

    for (int i =0;i<polarize+1;i++){
        this->density.append( rcp(new Epetra_Vector(*map) ) );
    }
    this->atoms=rcp(new Atoms() );
    total_energy=0.0;
    nuclear_nuclear_repulsion=0.0;

};
*/
/*
State::State(Array<RCP<Epetra_MultiVector> > orbitals,RCP<Epetra_MultiVector> density, Array<RCP<Occupation> > occupations, Array< vector<double > > orbital_energies,RCP<Atoms> atoms,RCP<Epetra_Vector> hartree_potential){

    this->orbitals=Array< RCP<Epetra_MultiVector> >();
    for (int i =0; i < orbitals.size(); i++){
        this->orbitals.append( rcp(new Epetra_MultiVector(*orbitals.at(i) )) );
    }
    this->density =rcp(new Epetra_MultiVector(*density) );
    this->orbital_energies=Array< vector<double > > ();
    for (int i=0; i<orbital_energies.size();i++){
        vector<double > destVector;
        vector<double > sourceVector=orbital_energies.at(i);
        destVector.assign(sourceVector.begin(),sourceVector.end());
        this->orbital_energies.append( destVector );
    }
    this->atoms = rcp(new Atoms(*atoms) );
    this->hartree_potential =rcp( new Epetra_Vector(*hartree_potential) );
}*/
State::State(const State& state){
    this->operator=(state);
}

State& State::operator=(const State& state){
//    this->orbitals=Array< RCP<Epetra_MultiVector> >(state.orbitals);
    this->orbitals=Array< RCP<Epetra_MultiVector> >();
    copy_orbitals(state.orbitals);
//    this->density =Array<RCP<Epetra_Vector> > (state.density);
    this->density =Array<RCP<Epetra_Vector> > ();
    for (int i =0; i < state.density.size(); i++){
        this->density.append( rcp(new Epetra_Vector(*state.density.at(i) )) );
    }
//    this->occupations = Array<RCP<Occupation> >(state.occupations);
    this->occupations = Array<RCP<Occupation> >();
    for(int i =0; i <state.occupations.size();i++){
        this->occupations.append( rcp(state.occupations[i]->clone()  ) );
    }
//    this->orbital_energies=Array< vector<double > > (state.orbital_energies);
    this->orbital_energies=Array< vector<double > > ();
    for (int i=0; i<state.orbital_energies.size();i++){
        vector<double > destVector;
        vector<double > sourceVector=state.orbital_energies.at(i);
        destVector.assign(sourceVector.begin(),sourceVector.end());
        this->orbital_energies.append( destVector );
    }
//    this->local_potential = Array<RCP<Epetra_Vector> > (state.local_potential);
    this->local_potential = Array<RCP<Epetra_Vector> > ();
    for (int i =0; i < state.local_potential.size(); i++){
        this->local_potential.append( rcp(new Epetra_Vector(*state.local_potential.at(i) )) );
    }

    if( state.hartree_potential != Teuchos::null ){
        this->hartree_potential = rcp(new Epetra_Vector(*state.hartree_potential ));
    } else {
        this -> hartree_potential = Teuchos::null;
    }

    for (int i=0; i< state.potential_from_field.size(); i++){
        this->potential_from_field.append( rcp(new Epetra_Vector(*state.potential_from_field.at(i) )) );
    }

    if (state.core_density.size() > 0){
    //if (state.core_density!=null){
        for(int i_spin=0; i_spin<occupations.size(); i_spin++){
            core_density.push_back(rcp(new Epetra_Vector(*state.core_density[i_spin])));
        }
    }
    if(state.core_density_grad.size() > 0){
    //if(state.core_density_grad!=null){
        for(int i_spin=0; i_spin<occupations.size(); i_spin++){
            core_density_grad.push_back(rcp(new Epetra_MultiVector(*state.core_density_grad[i_spin])));
        }
    }
    if(state.polarizability.size()>0){
        this->polarizability.resize(state.polarizability.size());
        for(int i=0; i<polarizability.size(); i++){
            polarizability[i].resize(state.polarizability[i].size());
            for(int j=0; j<polarizability[i].size(); j++){
                polarizability[i][j] = state.polarizability[i][j];
            }
        }
    }
    if(state.atoms==Teuchos::null){
        this->atoms = Teuchos::null;
    }
    else{
        this->atoms = rcp(new Atoms(*state.atoms) );
    }
    this->nuclear_nuclear_repulsion = state.nuclear_nuclear_repulsion;
    this->total_energy = state.total_energy;
    this->total_free_energy = state.total_free_energy;
    this->timer = rcp(new Time_Measure() );
    return *this;
}
Array<RCP<const Epetra_MultiVector> > State::get_orbitals() const {
    Array<RCP<const Epetra_MultiVector> > return_val;
    for (int i =0;i < orbitals.size();i++){
        return_val.append(orbitals[i]);
    }
    return return_val;
}
Array<RCP<const Epetra_Vector> > State::get_density() const{
    Array<RCP<const Epetra_Vector> > return_val;
    for (int i =0;i < density.size();i++){
        return_val.append(density[i]);
    }
    return return_val;
}

Array<RCP<const Occupation> > State::get_occupations() const {
    Array<RCP<const Occupation> > return_val;
    for (int i =0;i < occupations.size();i++){
        return_val.append(occupations[i]);
    }
    return return_val;
}

RCP<const Atoms> State::get_atoms() const{
    return atoms;
}

int State::set_atoms(RCP<Atoms> atoms){
    if (this->atoms!=Teuchos::null){
        this->atoms = atoms;
        return 1;
    }
    else{
        this->atoms = atoms;
        return 0;
    }
    return 0;
}

Array< vector<double > > State::get_orbital_energies() const{
    return orbital_energies;
}

Teuchos::Array< Teuchos::RCP<Epetra_Vector> > State::get_local_potential() const{
    return local_potential;
}
Teuchos::RCP<Epetra_Vector> State::get_hartree_potential() const{
    return hartree_potential;
}

Teuchos::Array< Teuchos::RCP<Epetra_Vector> > State::get_core_density() const{
    return core_density;
}
Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > State::get_core_density_grad() const{
    return core_density_grad;
}

double State::get_nuclear_nuclear_repulsion() const{
    return nuclear_nuclear_repulsion;
}

double State::get_total_energy() const {
    return total_energy;
}

int State::write_orbital(string name, RCP<const Basis> basis, string type, int start/* = 0*/, int end/* = -1*/){
    Array< RCP<Epetra_MultiVector> > output;
    for( int s = 0; s < this -> orbitals.size(); ++s){
        if( end < 0 or end > this -> orbitals[s] -> NumVectors()-1 ){
            end = this -> orbitals[s] -> NumVectors()-1;
        }
        output.push_back( rcp( new Epetra_MultiVector( *basis -> get_map(), end-start+1 ) ) );
        for(int i = start; i <= end; ++i){
            output[s] -> operator()(i-start) -> Update( 1.0, *orbitals[s] -> operator()(i), 0.0 );

        }
        Value_Coef::Value_Coef(basis,output[s], false, true, output[s]);
    }

    if( type == "cube" ){
        basis -> write_cube(name, this -> get_atoms(), output, start);
    } else if( type == "x" ){
        basis -> write_along_axis(name, 0, output, vector<double>(), start);
    } else if( type == "y" ){
        basis -> write_along_axis(name, 1, output, vector<double>(), start);
    } else if( type == "z" ){
        basis -> write_along_axis(name, 2, output, vector<double>(), start);
    } else if( type == "None" ){
        return 0;
    } else {
        Verbose::all() << "Unkonwn write_orbital type!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return 0;
}

int State::write_density(string name, RCP<const Basis> basis, string type, bool is_ae){
    Array< RCP<Epetra_Vector> > output;
    for(int s = 0; s < this -> density.size(); ++s){
        output.push_back( rcp( new Epetra_Vector( *this -> get_density()[s] ) ) );
        if( is_ae ){
            output[s] -> Update( 1.0, *this -> core_density[s], 1.0 );
        }
    }
    if( type == "cube" ){
        basis -> write_cube(name, this -> get_atoms(), output);
        if(output.size() > 1){
            RCP<Epetra_Vector> tot_density = rcp(new Epetra_Vector(*output[0]));
            for(int s = 1; s < output.size(); ++s){
                tot_density -> Update(1.0, *output[s], 1.0);
            }
            basis -> write_cube(name+".tot", this -> get_atoms(), tot_density);
        }
    } else if( type == "x" ){
        basis -> write_along_axis(name, 0, output);
    } else if( type == "y" ){
        basis -> write_along_axis(name, 1, output);
    } else if( type == "z" ){
        basis -> write_along_axis(name, 2, output);
    } else if( type == "None" ){
        return 0;
    } else {
        Verbose::all() << "Unkonwn write_density type!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return 0;
}

int State::write_local_potential(string name, RCP<const Basis> basis, string type){
    //jaewook tmp 171128
    Array< RCP<Epetra_Vector> > output;
    for(int s = 0; s < this -> local_potential.size(); ++s){
        output.push_back( rcp( new Epetra_Vector( *this -> get_local_potential()[s] ) ) );
    }
    if( type == "cube" ){
        basis -> write_cube(name, this -> get_atoms(), output);
        if(output.size() > 1){
            RCP<Epetra_Vector> tot_potential = rcp(new Epetra_Vector(*output[0]));
            for(int s = 1; s < output.size(); ++s){
                tot_potential -> Update(1.0, *output[s], 1.0);
            }
            basis -> write_cube(name+".tot", this -> get_atoms(), tot_potential);
        }
    /*
    if( type == "cube" ){
        basis -> write_cube(name, this -> get_atoms(), this -> local_potential);
    } else if( type == "x" ){
        basis -> write_along_axis(name, 0, this -> local_potential);
    } else if( type == "y" ){
        basis -> write_along_axis(name, 1, this -> local_potential);
    } else if( type == "z" ){
        basis -> write_along_axis(name, 2, this -> local_potential);
    }
    */
    } else if( type == "x" ){
        basis -> write_along_axis(name, 0, output);
    } else if( type == "y" ){
        basis -> write_along_axis(name, 1, output);
    } else if( type == "z" ){
        basis -> write_along_axis(name, 2, output);
    } else if( type == "None" ){
        return 0;
    } else {
        Verbose::all() << "Unkonwn write_local_potential type!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return 0;
}
int State::write_total_xc_potential(string name, RCP<const Basis> basis, string type){
    Array< RCP<Epetra_Vector> > output_tmp;
    RCP<Epetra_Vector> output;
    for(int s = 0; s < this -> local_potential.size(); ++s){
        output_tmp.push_back( rcp( new Epetra_Vector( *this -> get_local_potential()[s] ) ) );
    }
    if(output_tmp.size() > 0){
        output = rcp( new Epetra_Vector( *(output_tmp[0]) ) );
        for(int s = 1; s < output_tmp.size(); ++s){
            output -> Update(1.0,*output_tmp[s],1.0);
        }
        output -> Update(-1.0, *(this->hartree_potential),1.0);

    if( type == "cube" ){
        basis -> write_cube(name, this -> get_atoms(), output);
    } else if( type == "x" ){
        basis -> write_along_axis(name, 0, output);
    } else if( type == "y" ){
        basis -> write_along_axis(name, 1, output);
    } else if( type == "z" ){
        basis -> write_along_axis(name, 2, output);
    } else if( type == "None" ){
        return 0;
    } else {
        Verbose::all() << "Unkonwn write_local_potential type!" << std::endl;
        exit(EXIT_FAILURE);
    }
}
    return 0;
}

int State::write_hartree_potential(string name, RCP<const Basis> basis, string type){
    if( type == "cube" ){
        basis -> write_cube(name, this -> get_atoms(), this -> hartree_potential);
    } else if( type == "x" ){
        basis -> write_along_axis(name, 0, this -> hartree_potential);
    } else if( type == "y" ){
        basis -> write_along_axis(name, 1, this -> hartree_potential);
    } else if( type == "z" ){
        basis -> write_along_axis(name, 2, this -> hartree_potential);
    } else if( type == "None" ){
        return 0;
    } else {
        Verbose::all() << "Unkonwn write_hartree_potential type!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return 0;
}
int State::write_potential_from_field(std::string name, Teuchos::RCP<const Basis> basis, std::string type){
    Verbose::single(Verbose::Normal) << "write_potential_from_field\t"<< type << std::endl;

    if( type == "cube" ){
        Verbose::single(Verbose::Normal) << "cube: "<< name << "\t" << this -> potential_from_field <<std::endl;
        basis -> write_cube(name, this -> get_atoms(), this -> potential_from_field);
    } else if( type == "x" ){
        basis -> write_along_axis(name, 0, this -> potential_from_field);
    } else if( type == "y" ){
        basis -> write_along_axis(name, 1, this -> potential_from_field);
    } else if( type == "z" ){
        basis -> write_along_axis(name, 2, this -> potential_from_field);
    } else if( type == "None" ){
        return 0;
    } else {
        Verbose::all() << "Unkonwn write_hartree_potential type!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return 0;
}

int State::write(RCP<const Basis> basis, RCP<Teuchos::ParameterList> parameters){
    string routine = parameters -> name();
    if( parameters -> sublist(routine).isSublist("Output") ){
        RCP<Teuchos::ParameterList> output_param = rcp( new Teuchos::ParameterList(parameters -> sublist(routine).sublist("Output")) );
        string prefix = "";
        if( output_param -> isParameter("Prefix") ){
            prefix = output_param -> get<string>("Prefix")+routine;
        } else {
            //prefix = parameters -> sublist("BasicInformation").get<string>("Label");
            prefix = routine;
        }

        if( output_param -> isParameter("Density") ){
            Verbose::single(Verbose::Normal) << "Density will be written." << std::endl;
            bool is_ae = false;
            if( output_param -> isParameter("AllElectronDensity") ){
                if( output_param -> get<int>("AllElectronDensity") != 0 ){
                    is_ae = true;
                }
            }
            this -> write_density( prefix+".density", basis, output_param -> get<string>("Density"), is_ae );
        }
        if( output_param -> isParameter("Hartree") ){
            this -> write_hartree_potential( prefix+".hartree", basis, output_param -> get<string>("Hartree") );
        }
        if( output_param -> isParameter("XCPotential") ){
            this -> write_total_xc_potential( prefix+".XC", basis, output_param -> get<string>("XCPotential") );
        }
        if( output_param -> isParameter("Potential") ){
            this -> write_local_potential( prefix+".potential", basis, output_param -> get<string>("Potential") );
        }
        if( output_param -> isParameter("PotentialFromField") ){
            Verbose::single(Verbose::Normal) << "PotentialFromField will be written." <<std::endl;
            this -> write_potential_from_field( prefix+".potential.from.field", basis, output_param -> get<string>("PotentialFromField") );
        }
        if( output_param -> isParameter("Orbitals") ){
            int orbital_start = 0;
            int orbital_end = orbitals[0] -> NumVectors()-1;
            if( orbitals[orbitals.size()-1] -> NumVectors()-1 > orbital_end ) orbital_end = orbitals[orbitals.size()-1] -> NumVectors()-1;
            if( output_param -> isParameter("OrbitalStartIndex") ) orbital_start = output_param -> get<int>("OrbitalStartIndex");
            if( output_param -> isParameter("OrbitalEndIndex") ) orbital_end = output_param -> get<int>("OrbitalEndIndex");
            this -> write_orbital( prefix+".orbitals", basis, output_param -> get<string>("Orbitals"), orbital_start, orbital_end);
        }

        if( output_param -> isParameter("Geometry") ){
            Io_Atoms io;
            Atoms out_atoms(*this->atoms);
            io.write_atoms(output_param -> get<string>("Geometry"), out_atoms, "xyz");
        }
        if(output_param -> isParameter("Summary")){
            std::ofstream fp(output_param -> get<string>("Summary"));
            //this -> write_summary(fp);
            fp.close();
        }
    }
    return 0;
}

/*
void State::write_summary(std::ofstream fp){
    fp.write("# ACE summary file - DO NOT CHANGE FORMAT. INTENDED FOR CONSISTENT OUTPUT PARSER.\n");
    fp.write("# IF YOU ADD NEW SECTION, DO NOT USE EXISTING SECTION NAME\n");
    fp.write("# IF YOU ADD NEW ENTRY TO SECTION, YOU SHOULD APPEND & OR / SEPARATED SECTION AT LAST.\n");
    fp.write("# IF YOU WANT TO DELETE ENTRY OF SECTION, JUST REMOVE VALUE. NEVER REMOVE SEPARATORS.\n");
    fp.write("# IF YOU CHANGE SUMMARY FILE FORMAT, WRITE CHANGES HERE\n");
    fp.write("# #: COMMENT\n");
    fp.write("# !NAME: STARTS SECTION 'NAME'. EACH NON-COMMENT LINE SHOULD START WITH !NAME. EACH SECTION SHOULD BE ONE-LINE. EACH SECTION(LINE) SHOULD HAVE UNIQUE NAME.\n");
    fp.write("# &: PRIMARY SEPARATOR, /: SECONDARY SEPARATOR. NO SPACE BETWEEN VALUE AND SEPARATORS.\n");
    fp.write("# VERSION section: [summary format version]&[program version]\n");
    fp.write("# DO NOT CHANGE SUMMARY FORMAT VERSION IF FORMAT IS NOT CHANGED\n");

    fp.write("# ATOM section: [Number of atom]&[Charge]&[Spin Multiplicity]&[Molecule symmetry(TBD)]/[Symbol]&[X]&[Y]&[Z]/...\n");
    fp.write("!ATOM "
    fp.write("# ENERGY section: Etot&[RESERVED]/[Ion-ion]/[eigenvalue sum]/[Hartree E]/[XC energy]/[Kinetic energy]&[Spin alpha]&[Spin beta if exists]/[External energy]\n");
    fp.write("# ORBOCC section FOR RESTRICTED: 1/[occupation separated by &]\n");
    fp.write("# ORBOCC section FOR UNRESTRICTED: 2/[alpha occupation separated by &]/[beta occupation separated by &]\n");
    fp.write("# ORBENE section is similar with ORBOCC but orbital energies instead of occupations\n");
    fp.write("# ORBSYM section is similar with ORBOCC but contains orbital symmetries.\n");
}
*/


void State::copy_orbitals(const Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals){
    for (int i =0; i < orbitals.size(); i++){
        if(do_shallow_copy_orbitals==false){
            this->orbitals.append( rcp(new Epetra_MultiVector(*orbitals.at(i) )) );
        }
        else{
            this->orbitals.append( orbitals.at(i) );
        }
    }
    return;
}

void State::copy_orbitals(Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals){
    for (int i =0; i < orbitals.size(); i++){
        if(do_shallow_copy_orbitals==false){
            this->orbitals.append( rcp(new Epetra_MultiVector(*orbitals.at(i) )) );
        }
        else{
            this->orbitals.append( Teuchos::rcp_const_cast<Epetra_MultiVector>(orbitals.at(i)) );
        }
    }
    return;
}
