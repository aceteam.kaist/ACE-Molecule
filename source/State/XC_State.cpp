#include "../Utility/Value_Coef.hpp"
#include <stdexcept>
#include <cmath>
#include "xc.h"

#include "XC_State.hpp"

using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

XC_State::XC_State(int xc_id): State(){
    std::map<std::string, bool> fields;
    fields["density_gradient"] = false;
    fields["density_laplacian"] = false;
    fields["kinetic_energy_density"] = false;
    this -> xc_id = xc_id;
    if(xc_id > 0){
        fields["all_density"] = true;
        fields["val_density"] = false;
        fields["orbitals"] = false;
        this -> is_necessary = fields;
        this -> initialize_Libxc(xc_id);
    } else {
        fields["all_density"] = false;
        fields["val_density"] = true;
        fields["orbitals"] = true;
        this -> is_necessary = fields;
        this -> initialize_KLI(xc_id);
    }
}

void XC_State::initialize_Libxc(int function_id){
    xc_func_type func;
    xc_func_init(&func, function_id % 10000, XC_UNPOLARIZED);
    this -> EXX_portion = xc_hyb_exx_coef(&func);
    xc_hyb_cam_coef(&func, &this -> CAM_omega, &this -> CAM_alpha, &this -> CAM_beta);

    switch(func.info->family){
        case XC_FAMILY_LDA:{
            this -> type = "LDA";
            break;
        }
        case XC_FAMILY_GGA:{
            this -> type = "GGA";
            this -> is_necessary["density_gradient"] = true;
            break;
        }
        case XC_FAMILY_HYB_GGA:{
            //this -> type = "HYB_GGA";
            this -> type = "GGA";// HYB(EXX) is considered in other XC_State.
            this -> is_necessary["density_gradient"] = true;
            break;
        }
        case XC_FAMILY_MGGA:{
            this -> type = "MGGA";
            std::cout << "MGGA is not implemented!" << std::endl;
            throw std::logic_error("MGGA is not implemented!");
            break;
        }
        case XC_FAMILY_HYB_MGGA:{
            //this -> type = "HYB_MGGA";
            this -> type = "MGGA";// HYB(EXX) is considered in other XC_State.
            std::cout << "HYB MGGA is not implemented!" << std::endl;
            throw std::logic_error("HYB MGGA is not implemented!");
            break;
        }
        case XC_FAMILY_LCA:{
            this -> type = "LCA";
            std::cout << "LCA is not implemented!" << std::endl;
            throw std::logic_error("LCA is not implemented!");
            break;
        }
        default:{
            std::cout << "Unknown XC FAMILY " << func.info->family << std::endl;
            throw std::logic_error("Unknown XC FAMILY detected!");
            break;
        }
    }

    if(CAM_beta != 0.0 and CAM_alpha != 0 and CAM_alpha + CAM_beta != 0){
        if(CAM_beta > 0.0){
            std::cout << "CAM-type RSH with global hybrid is not implemented! Global portion: " << CAM_alpha << ", SR portion: " << CAM_beta << std::endl;
        } else {
            std::cout << "CAM-type RSH with global hybrid is not implemented! Global portion: " << CAM_alpha + CAM_beta << ", LR portion: " << -CAM_beta << std::endl;
        }
        throw std::invalid_argument("CAM-type RSH with global hybrid is not implemented!");
    }

    xc_nlc_coef(&func, &nlc_b, &nlc_c);
    if(nlc_b > 0.0 or nlc_c > 0.0){
        std::cout << "VV10-type NL correction is not implemented!" << std::endl;
        throw std::invalid_argument("VV10-type NL correction is not implemented!");
    }
    xc_func_end(&func);
    return;
}

void XC_State::initialize_KLI(int function_id){
    this -> EXX_portion = 0.0;
    this -> CAM_omega = 0.0, this -> CAM_alpha = 0.0, this -> CAM_beta = 0.0;
    this -> type = "KLI-PGG";

    switch(function_id){
        case -10:
            Verbose::single(Verbose::Simple) << "XC_State::KLI - No xc potential, exchange energy only \n";
            break;
        case -11:
            Verbose::single(Verbose::Simple) << "XC_State::KLI - Slater potential\n";
            break;
        case -12:
            Verbose::single(Verbose::Simple) << "XC_State::KLI - OEP-EXX potential, KLI approximation\n";
            break;
        case -13:
            Verbose::all() << "XC_State::KLI - OEP-EXX potential, not implemented\n";
            throw std::logic_error("-13 KLI - OEP-EXX potential is not implemented!");
            break;
        default:
            Verbose::all() << "XC_State::KLI - Undefined functional " << function_id << "!\n";
            throw std::logic_error("Undefined funcional detected!");
    }
    return;
}

std::string XC_State::get_functional_type()const {
    return this -> type;
}
double XC_State::get_EXX_portion() const{
    return this -> EXX_portion;
}
void XC_State::get_CAM_portion(double &omega, double &beta, double &alpha) const{
    omega = this -> CAM_omega;
    beta = this -> CAM_beta;
    alpha = this -> CAM_alpha;
}

std::map<std::string, bool> XC_State::get_necessary_fields() const{
    return this -> is_necessary;
}
void XC_State::set_lrc_info(double exx_portion, double omega){
    this -> LRC_alpha = exx_portion;
    this -> LRC_omega = omega;
}

double XC_State::get_energy(){
    return this -> energy;
}
Array< RCP<const Epetra_Vector> > XC_State::get_potential() const{
    Array< RCP<const Epetra_Vector> > rv;
    for(int s = 0; s < this -> potential.size(); ++s){
        rv.append(this -> potential[s]);
    }
    return rv;
}
Array< RCP<const Epetra_MultiVector> > XC_State::get_density_gradient() const{
    Array< RCP<const Epetra_MultiVector> > rv;
    for(int s = 0; s < this -> density_grad.size(); ++s){
        rv.append(this -> density_grad[s]);
    }
    return rv;
}
Array< RCP<const Epetra_Vector> > XC_State::get_val_density() const{
    Array< RCP<const Epetra_Vector> > rv;
    for(int s = 0; s < this -> val_density.size(); ++s){
        rv.append(this -> val_density[s]);
    }
    return rv;
}

RCP<const Epetra_Vector> XC_State::get_energy_density() const{
    return this -> energy_density;
}
void XC_State::set_energy(double energy){
    this -> energy = energy;
}

void XC_State::set_potential(Array< RCP<Epetra_Vector> > vxc){
    this -> potential = vxc;
}
void XC_State::set_energy_density(RCP<Epetra_Vector> exc){
    this -> energy_density = exc;
}
int XC_State::get_xc_id() const{
    return this -> xc_id;
}
void XC_State::set_lda_kernel(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > fxc){
    this -> vsigma.clear();
    this -> v2rho2.clear();
    this -> v2rhosigma.clear();
    this -> v2sigma2.clear();
    this -> fxc = fxc;
};
void XC_State::set_gga_kernel(
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > vsigma,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rho2,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2rhosigma,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > v2sigma2
                ){
    this -> vsigma = vsigma;
    this -> v2rho2 = v2rho2;
    this -> v2rhosigma = v2rhosigma;
    this -> v2sigma2 = v2sigma2;
    this -> fxc.clear();
    /*
    for(int s = 0; s < vsigma.size(); ++s){
        double norm = 0.0;
        vsigma[s] -> Norm2(&norm);
        if(!std::isfinite(norm)){
            Verbose::all() << s << " vsigma contains not a number! " << norm << std::endl;
            throw std::runtime_error("vsigma contains not a number!");
        }
    }
    for(int s = 0; s < v2rho2.size(); ++s){
        double norm = 0.0;
        v2rho2[s] -> Norm2(&norm);
        if(!std::isfinite(norm)){
            Verbose::all() << s << " v2rho2 contains not a number! " << norm << std::endl;
            throw std::runtime_error("v2rho2 contains not a number!");
        }
        v2rhosigma[s] -> Norm2(&norm);
        if(!std::isfinite(norm)){
            Verbose::all() << s << " v2rhosigma contains not a number! " << norm << std::endl;
            throw std::runtime_error("v2rhosigma contains not a number!");
        }
        v2sigma2[s] -> Norm2(&norm);
        if(!std::isfinite(norm)){
            Verbose::all() << s << " v2sigma2 contains not a number! " << norm << std::endl;
            throw std::runtime_error("v2sigma2 contains not a number!");
        }
    }
    // */
}

void XC_State::get_lda_kernel(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &fxc){
    fxc.clear();
    for(int s = 0; s < this -> fxc.size(); ++s){
        fxc.append(rcp(new Epetra_Vector(*this -> fxc[s])));
    }
}

void XC_State::get_gga_kernel(
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &vsigma,
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &v2rho2,
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &v2rhosigma,
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &v2sigma2
        ){
    vsigma.clear();
    v2rho2.clear();
    v2rhosigma.clear();
    v2sigma2.clear();
    for(int s = 0; s < this -> vsigma.size(); ++s){
        vsigma.append(rcp(new Epetra_Vector(*this -> vsigma[s])));
        v2rho2.append(rcp(new Epetra_Vector(*this -> v2rho2[s])));
        v2rhosigma.append(rcp(new Epetra_Vector(*this -> v2rhosigma[s])));
        v2sigma2.append(rcp(new Epetra_Vector(*this -> v2sigma2[s])));
    }
    vsigma = this -> vsigma;
    v2rho2 = this -> v2rho2;
    v2rhosigma = this -> v2rhosigma;
    v2sigma2 = this -> v2sigma2;
}

XC_State& XC_State::operator=(const XC_State& state){
    State::operator=(state);
    this -> xc_id = state.get_xc_id();
    this -> is_necessary = state.is_necessary;

    this -> EXX_portion = state.EXX_portion;
    this -> CAM_omega = state.CAM_omega;
    this -> CAM_alpha = state.CAM_alpha;
    this -> CAM_beta = state.CAM_beta;
    this -> type = state.type;

    this -> energy = state.energy;

    if(state.energy_density != Teuchos::null){
        this -> energy_density = rcp(new Epetra_Vector(*state.energy_density));
    }
    for(int s = 0; s < state.potential.size(); ++s){
        if(state.potential[s] != Teuchos::null){
            this -> potential.append(rcp(new Epetra_Vector(*state.potential[s])));
        }
    }
    for(int s = 0; s < state.fxc.size(); ++s){
        if(state.fxc[s] != Teuchos::null){
            this -> fxc.append(rcp(new Epetra_Vector(*state.fxc[s])));
        }
    }

    for(int s = 0; s < state.vsigma.size(); ++s){
        if(state.vsigma[s] != Teuchos::null){
            this -> vsigma.append(rcp(new Epetra_Vector(*state.vsigma[s])));
        }
    }

    for(int s = 0; s < state.v2rho2.size(); ++s){
        if(state.v2rho2[s] != Teuchos::null){
            this -> v2rho2.append(rcp(new Epetra_Vector(*state.v2rho2[s])));
        }
    }
    for(int s = 0; s < state.v2rhosigma.size(); ++s){
        if(state.v2rhosigma[s] != Teuchos::null){
            this -> v2rhosigma.append(rcp(new Epetra_Vector(*state.v2rhosigma[s])));
        }
    }
    for(int s = 0; s < state.v2sigma2.size(); ++s){
        if(state.v2sigma2[s] != Teuchos::null){
            this -> v2sigma2.append(rcp(new Epetra_Vector(*state.v2sigma2[s])));
        }
    }
    return *this;
}

XC_State::XC_State(const XC_State& state){
    this->operator=(state);
}
