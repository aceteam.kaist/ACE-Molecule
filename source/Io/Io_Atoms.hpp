#pragma once
#include "Atoms.hpp"
#include <string>
#include <fstream>

/**
 * @brief Read geometry file name and returns Atoms object.
 **/
class Io_Atoms
{
    public :
        /**
         * @brief Read geometry files now it supports xyz, and pdb formats
         * @param [in] filename filename of geometry file 
         * @param [in] format format of geometry file
         */
        Atoms read_atoms(std::string filename, std::string format = "xyz");
        /**
         * @brief Write geometry files. It only support xyz format. 
         * @param [in] filename filename of output filename
         * @param [in] atoms Atoms object to be written
         * @param [in] extension of output file. it can be any string but contents of output file is always written in xyz format 
         */
        void write_atoms(std::string filename, Atoms atoms, std::string format = "xyz");
        /**
         * @brief Read xyz file 
         * @param [in] input file stream object of input file
         */
        Atoms read_xyz(std::ifstream &input);
        /**
         * @brief Read pdb file 
         * @param [in] input file stream object of input file
         */
        Atoms read_pdb(std::ifstream &input);
};
