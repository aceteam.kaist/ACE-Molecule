#pragma once
#include <string>

/**
 * @brief Simple periodic table.
 **/
class Periodic_atom
{
    public:
        static constexpr double periodic_max = 118;
        static std::string periodic_table[118];
        static double mass_table[118];
        static double covalent_radii[118];
//        static double length[38];
        static double comp_charge_exp[118];
};

