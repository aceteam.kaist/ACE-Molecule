#include "Io_Atoms.hpp"
#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <algorithm>
#include "Atom.hpp"
#include "../Utility/Verbose.hpp"
#include "../Utility/String_Util.hpp"
#include "../Utility/Read/Read_Cube.hpp"
#include "mpi.h"

using std::string;
using std::vector;
using std::ofstream;
using std::ifstream;
using std::istringstream;

Atoms Io_Atoms::read_atoms(string filename, string format)
{
    Atoms retval;

    ifstream input(filename.c_str());
    if(input.fail()){
        input.open((filename + "." + format).c_str());
        if(input.fail()){
            Verbose::all()<< "Io_Atoms::read_atoms - CANNOT find file " << filename << " with format " << format << std::endl;
            throw std::invalid_argument("Io_Atoms::read_atoms - CANNOT find geometry file!");
        }
    }
    if(format == "xyz"){
        retval = read_xyz(input);
    } else if(format == "pdb"){
        retval = read_pdb(input);
    } else if(format == "cube"){
        retval = Read::Cube::read_cube_atoms(input);
    } 
    //else if(format == "coord"){
    //    retval = read_coord(input);
    //} 
    else {
        Verbose::all()<< "Io_Atoms::read_atoms invalid format " << format << std::endl;
        throw std::invalid_argument("Io_Atoms::read_atoms - invalid format!");
    }
    input.close();

    return retval;
}

void Io_Atoms::write_atoms(string filename, Atoms atoms, string format)
{
    ofstream output;
    string file = filename;
    file.append(".");
    file.append(format);
    output.open(file.c_str());

    output << atoms.get_size() << std::endl;
    output << std::endl;

    for(int atom_num = 0; atom_num < atoms.get_size(); ++atom_num){
        output << atoms[atom_num].get_symbol() << "    "
            << atoms.get_positions()[atom_num][0] << "    "
            << atoms.get_positions()[atom_num][1] << "    "
            << atoms.get_positions()[atom_num][2] << std::endl;
    }
    output.close();
}

Atoms Io_Atoms::read_xyz(ifstream &input)
{
    double st = MPI_Wtime();
    string word;
    //input >> word;
    std::getline(input, word);
    int max_size = atoi(word.c_str());
    string symbol;
    double charge = 0;
    Atoms return_val;
    std::getline(input, word);

    /*
    char dumy[256];
    for(int atom_num = 0; atom_num < max_size; ++atom_num){
        input >> symbol;
        std::transform(symbol.begin(), symbol.end(), symbol.begin(), ::toupper );
        std::array<double,3> position;
        position.fill(0.);
        for(int i = 0; i < 3; i++){
            input >> word;
            position[i] = atof(word.c_str());
            position[i] = position[i] * 1.889725989; // Unit conversion from Ang to Bohr
        }
        Atom atom(symbol, position, charge);
        return_val.push(atom, false);
        input.getline(dumy, 256);
    }
    */
    for(int atom_num = 0; atom_num < max_size; ++atom_num){
        if(!std::getline(input, word)){
            string errmsg = "Io_Atoms::read_xyz xyz file ended while reading the geometry specification!";
            Verbose::all() << errmsg << std::endl;
            throw std::invalid_argument(errmsg);
        }
        vector<string> tokens = String_Util::Split_ws(word);
        if(tokens.size() < 4){
            string errmsg = "Io_Atoms::read_xyz xyz file got suprious line while reading the geometry specification!";
            Verbose::all() << errmsg << std::endl;
            throw std::invalid_argument(errmsg);
        }
        symbol = tokens[0];
        std::transform(symbol.begin(), symbol.end(), symbol.begin(), ::toupper );
        std::array<double,3> position;
        position.fill(0.);
        for(int i = 0; i < 3; i++){
            position[i] = atof(tokens[i+1].c_str());
            position[i] = position[i] * 1.889725989; // Unit conversion from Ang to Bohr
        }
        Atom atom(symbol, position, charge);
        return_val.push(atom, false);
    }
// */
    return_val.reset_connection_table();
    return return_val;
}

Atoms Io_Atoms::read_pdb(ifstream &input)
{
    char dumy[100];
    input.getline(dumy,100);
    string word;
    input >> word;
    double charge = 0;
    string symbol;
    std::vector<Atom> atom_list;
    while(word != "END"){
        input >> word;
        input >> word;
        input >> word;
        charge = atof(word.c_str());
        std::array<double,3> position;
        for(int i = 0; i < 3; i++){
            position[i] = 0;
        }
        for(int i = 0; i < 3; i++){
            input >> word;
            position[i] = atof(word.c_str());
        }
        input >> word;
        symbol = word;
        std::transform(symbol.begin(), symbol.end(), symbol.begin(), ::toupper);
        Atom atom(symbol, position, charge);
        atom_list.push_back(atom);
        input >> word;
    }
    Atoms as(atom_list);

    return as;
}

//Atoms Io_Atoms::read_coord(ifstream &input)
//{
//    string word;
//    vector<Atom> atom_list;
//    string symbol;
//    double charge = 0;
//    int atom_num = 0;
//    char dumy[50];
//    Atoms return_val;
//
//    while(true){
//        if( input.eof() ) break;
//        input >> symbol;
//        std::transform(symbol.begin(), symbol.end(), symbol.begin(), ::toupper );
//        std::array<double,3> position;
//        for(int i = 0; i < 3; i++){
//            position[i] = 0;
//        }
//        for(int i = 0; i < 3; i++){
//            input >> word;
//            position[i] = atof(word.c_str());
//            position[i] = position[i] * 1.889725989; // Unit conversion from Ang to Bohr
//        }
//        Atom atom(symbol, position, charge);
//        return_val.push(atom, false);
//        input.getline(dumy, 50);
//    }
//    return_val.reset_connection_table();
//
//    return return_val;
//}
