#pragma once
#include <iostream>
#include <string>
#include <array>
class Atom
{
    friend std::ostream &operator<<(std::ostream &c, Atom &atom);
    friend std::ostream &operator<<(std::ostream &c, Atom* patom);
  public : 
    Atom(); 
    ~Atom();
    //Atom(Atom& atom); 
    Atom(int atomic_number, std::array<double,3> position, double charge);
    Atom(std::string symbol, std::array<double,3> position, double charge);
    Atom(Atom& atom);
    Atom(const Atom& atom); //< @brief Copy constructor.???
    /*
     * @return Atomic number of the atom.
     */
    int get_atomic_number();
    /*
     *  @return Atomic number of the atom.
     */
    int get_atomic_number() const;

    /*
     * @return Valence charge of the atom.
     */
    double get_valence_charge() const;
    /*
     * @brief Set valecne charge to given value.
     * @param [in] valence_charge Valence charge.
     */
    void set_valence_charge(double valence_charge);

    /*
     * @return Array of x,y,z coordinate of the atom.
     */
    std::array<double,3> get_position() const;
//    double * get_position() const;
    /*
     * @return Charge of the atom.
     */
    double get_charge();
    /*
     * @return Charge of the atom.
     */
    double get_charge() const;
    /*
     * @return Radius of the atom.
     */
    double get_radius();

    /*
     * @brief Set x,y,z coordinate.
     * @param [in] position Position coordinate.
     */
    void set_position(std::array<double,3> position);

    /*
     * @return Symbol of the atom.
     */
    std::string get_symbol();
    
    //double get_comp_charge_exp();
    /*
     * @brief Return true if Atom is the same with new_atom.
     * @detail The atomic number and the position should be the same. 
     */
    bool operator==(Atom new_atom);
    /*
     * @brief Return false if Atom is the same with new_atom.
     * @detail The atomic number and the position should be the same. 
     */
    bool operator!=(Atom new_atom);
    /*
     * @brief Substitute original_atom to Atom.
     */
    Atom& operator=(const Atom& original_atom);
    /*
     * @brief Substitute original_atom to Atom.
     */
    Atom& operator=(Atom& original_atom);
    
  private : 
    double charge;
    double valence_charge;
    std::array<double,3> position;
    int atomic_number;
    std::string symbol;

};

