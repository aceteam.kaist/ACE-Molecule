#pragma once
#include <iostream>
#include <vector>
#include "Atom.hpp"
/**
 * @brief Class for handle atoms information.
  */
class Atoms
{
    friend std::ostream &operator <<(std::ostream &c, const Atoms &atoms);
    friend std::ostream &operator <<(std::ostream &c, Atoms* patoms);

  public :
    Atoms();//< @brief Constructor.
    Atoms(std::vector<Atom> atom_list);//< @brief Construct from Atom classes.
    Atoms(const Atoms& atoms);//< @brief Copy constructor.
    ~Atoms();
    /**
     * @return Vector of atomic numbers of molecule. Ordered.
     **/
    std::vector<int> get_atomic_numbers() const ;
    /**
     * @return Vector of x, y, z coordinates of atoms.
     **/
    std::vector<std::array<double ,3> > get_positions() const;
    /**
     * @return Vector of covalent radii of atoms.
     */
    std::vector<double> get_radii() const;
    /**
     * @return Vector of charges of atoms.
     */
    std::vector<double> get_charges() const;
    /**
     * @return array of x, y, z coordinate of the center of mass.
     */
    std::array<double,3> get_center_of_mass() const;
    /**
     * @brief works like std::vector.
     **/
    void pop();
//    void push(Atom atom);
    /**
     * @brief works like std::vector.
     **/
    void push(Atom& atom, bool make_connection_table=true);
    /**
     * @brief works like std::vector.
     **/
    Atom& operator[] (int i);
    /**
     * @brief works like std::vector.
     **/
    Atom operator[] (int i) const;
    /**
     * @return Total number of atoms.
     **/
    int get_size() const;
    /**
     * @return Distance between ith atom and jth atom.
     */
    double distance(int i, int j) const;
    /**
     * @breif Make connection table.
     * @details if the distance between ith and jth atoms is shorter than the sum of each covalent radii, set the value of (i,j) component of the connection table to 1.
     */
    void make_connection();
    /**
     * @return connection table.
     */
    std::vector< std::vector<int> > get_connection() const;
    /**
     * @brief return true if the new_atoms is same to the Atoms.
     * @details The size and the orders should be the same.
     */
    bool operator==( Atoms new_atoms);
    /**
     * @brief return false if the new_atoms is same to the Atoms.
     * @details The size and the orders should be the same.
     */
    bool operator!=( Atoms new_atoms);
    /**
     * @brief Substitute original_atoms to Atoms.
     */
    Atoms& operator=(const Atoms& original_atoms);
    /**
     * @return Atom type index. Atom type index is ordered in ascending way.
     **/
    std::vector<int> get_atom_types() const;
    /**
     * @param [in] Atom index.
     * @return Atom type index. Atom type index is ordered in ascending way.
     **/
    int get_atom_type(int index) const;
    /**
     * @param [in] Atom type index.
     * @return Atomic number.
     **/
    int get_atomic_number_from_type(int i_type) const;
    std::vector<double> get_comp_charge_exps();
    /**
     * @brief Number of elements in molecule. Ex) For C2H4, returns 2 and for C2H2O2, returns 3.
     * @note Useful for pseudopotential file reading, etc.
     * @return Number of elements in molecule. Ex) For C2H4, returns 2 and for C2H2O2, returns 3.
     **/
    int get_num_types() const;
    void move_center_of_mass(double new_center_of_mass_x, double new_center_of_mass_y,double new_center_of_mass_z);
    void move_positions(double x, double y,double z);
//    void move_center_of_mass_to_origin();
    void get_vertex_info(int& index1, double& min_x, int& index2, double& max_x,
                         int& index3, double& min_y, int& index4, double& max_y,
                         int& index5, double& min_z, int& index6, double& max_z, std::vector<double> double_radious, bool is_relative_radius) const;

    void get_vertex_info(int& index1, double& min_x,
            int& index2, double& max_x,
            int& index3, double& min_y,
            int& index4, double& max_y,
            int& index5, double& min_z,
            int& index6, double& max_z,
            std::vector<double> double_radious,
            bool is_relative_radius,
            double* cell ) ;
    void reset_connection_table();
  private :
    std::vector<Atom> atom_list;
    std::vector< std::vector<int> > connection_table;

};
