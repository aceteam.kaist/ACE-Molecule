#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Absorption spectrum from ACE-Molecule TDDFT calculations
# Written by kwangwoo (15.11.13)

import math
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse
import os
#import sys.argv as argv

IS_EDISON = False

nm_per_ev = 1239.8424121
oscillator_cutoff = 0.01

def parse_ace(inputfile, sigma, limit = None):
    """Parse ACE-Molecule output to get TDDFT excitation information.

    Parameters
    ----------
    inputfile : file object
        ACE-Molecule log file object.
    sigma : float
        Gaussian broadning for peak.
    limit : (float or None, float or None) or None
        Plot range.

    Returns
    -------
    x_value_nm : list of floats
        Excitation energy list (x-axis to plot).
    spectrum_value: list of floats
        UV spectrum absorbance value, corresponds to x_value_nm
    excitation_energy_nm: list of floats
        Parsed excitation energy, in nm.
    oscillator_strength : list of floats
        Parsed oscillator strength.

    Notes
    -----
    This function consists of sevral parts.
        1) Read log and store excitation energy (eV) and oscillator strength to excitation_energy_eV and oscillator_strength.
        2) Decides the x-axis for the UV spectrum plot. Including auto-detection of max values. Note that x-axis has 1000 points, and oscillator strength that is 10^6 times smaller than max peak is considered "dark".
        3) Change eV to nm and draw points for UV spectrum.
    Each section is indicated by comments like "Part 1 Start/End"

    TODO
    ----
    Part 2 seems that it can be integrated with parse_gaus.
    """

    """ Part 1 Start """
    input = inputfile

    line = input.readline()

    excitation_energy_eV = []
    oscillator_strength = []

    while line:
        if (line.find("Excitation energy") >= 0):
            tmp = line.split()
            excitation_energy_eV.append(tmp[7])
        elif (line.find("Oscillator strength") >= 0):
            tmp = line.split()
            oscillator_strength.append(tmp[3])

        line = input.readline()

    input.close()
    """ Part 1 End """

    """ Part 2 Start """
    # Find the range of the spectrum
    max_oscillator_strength = 0.0
    for i in range(0,len(oscillator_strength)):
        if(float(oscillator_strength[i]) > max_oscillator_strength):
            max_oscillator_strength = float(oscillator_strength[i])

    filtered_excit_eV = [float(x) for x in excitation_energy_eV if float(x) > 0]
    min_eV = min(np.array(filtered_excit_eV, dtype = float))
    max_eV = max(np.array(filtered_excit_eV, dtype = float))

    number_points = 5000
    if limit is None:
        limit = (None, None)
    if limit[0] is None or limit[1] is None:
        for i in range(len(oscillator_strength)):
            if(float(oscillator_strength[i]) * 1000000.0 > max_oscillator_strength):
                tmp = 0.5*sigma * math.sqrt(math.log(1000000.0*float(oscillator_strength[i])/max_oscillator_strength))
                if float(excitation_energy_eV[i]) <= 0:
                    continue

                min_eV_tmp = float(excitation_energy_eV[i]) - tmp
                max_eV_tmp = float(excitation_energy_eV[i]) + tmp

                if min_eV_tmp < 0:
                    min_eV_tmp += tmp

                if(min_eV_tmp < min_eV and limit[0] is None):
                    min_eV = min_eV_tmp
                if(max_eV_tmp > max_eV and limit[1] is None):
                    max_eV = max_eV_tmp
        if limit[0] is None:
            limit = (int(nm_per_ev/max_eV), limit[1])
        if limit[1] is None:
            limit = (limit[0], int(nm_per_ev/min_eV))
        #number_points = int(limit[1] - limit[0])*10
    else:
        min_eV = nm_per_ev/limit[1]
        max_eV = nm_per_ev/limit[0]
        #number_points = int(limit[1] - limit[0])*10
    #print "minmax eV:", min_eV, max_eV

    # Points at integer nm
    number_points = limit[1] - limit[0]
    scaling = (max_eV - min_eV) / float(number_points-1)
    """ Part 2 End """

    """ Part 3 Start """
    x_value_ev = []
    x_value_nm = []
    #x_value_cm = [] # cm-1
    spectrum_value = []

    for i in range(0, number_points):
        #x = min_eV + scaling * float(i)
        #x_nm = nm_per_ev / x
        x_nm = limit[0] + i
        x = nm_per_ev / x_nm
        x_value_ev.append(float(x))
        x_value_nm.append(float(x_nm))
        #x_value_cm.append(float(x * 8065.54429))

        spectrum_value_tmp = 0.0
        for k in range(0,len(oscillator_strength)):
            spectrum_value_tmp += 1.3062974e8 * float(oscillator_strength[k]) / (0.5*sigma*8065.54445) * math.exp(-((x-float(excitation_energy_eV[k]))/(0.5*sigma))**2)

        spectrum_value.append(float(spectrum_value_tmp))

    excitation_energy_nm = []
    for x in excitation_energy_eV:
        excitation_energy_nm.append(nm_per_ev/float(x))
    """ Part 3 End """

    return x_value_nm, spectrum_value, excitation_energy_nm, oscillator_strength

def get_epsilon(wavelength,oscil,lambda_range, sigma):
    """
    See: http://gaussian.com/uvvisplot equation 5
    Our equation.
    a * f / b * exp(-((v-vi)/d)**2)
    a = sqrt(pi) * e**2 * N / 1000 / ln(10) / c**2 / m_e
      = 1.3062974*10**8
    b = 1/(sigma/2), in cm**-1
    d = sigma/2, in nm**-1

    Parameters
    ----------
    wavelength : list of floats
        Excitation energy as nm.
    oscil : list of floats
        Oscillator strengths.
    lambda_range : list of floats
        Wavelengths to plot in nm.
    sigma : float
        Gaussian broadning, times two.
    """
    a=1.3062974*(10.0**8.0)
    b=(10.0**7.0)/(0.5*sigma*8065.54445)
    d=1.0/(0.5*sigma*8065.54445)
    return np.array(list(map(lambda x: a*(oscil/b)*math.exp(-((1.0/x-1.0/wavelength)/d)**2.0),lambda_range)))

def parse_gaussian(inputfile, sigma, limit):
    """Parse ACE-Molecule output to get TDDFT excitation information.
    See: http://www.gaussian.com/g_whitepap/tn_uvvisplot.htm
    See (New site since 2017): http://gaussian.com/uvvisplot/

    Parameters
    ----------
    inputfile : file object
        ACE-Molecule log file object.
    sigma : float
        Gaussian broadning for peak.
    limit : (float or None, float or None) or None
        Plot range.

    Returns
    -------
    x_value_nm : list of floats
        Excitation energy list (x-axis to plot).
    spectrum_value: list of floats
        UV spectrum absorbance value, corresponds to x_value_nm
    excitation_energy_nm: list of floats
        Parsed excitation energy, in nm.
    oscillator_strength : list of floats
        Parsed oscillator strength.

    Notes
    -----
    This function consists of sevral parts.
        1) Decides the x-axis for the UV spectrum plot. Including auto-detection of max values.
        2) Read log and store excitation energy (converted to nm) and oscillator strength and draw points for UV spectrum. Note that x-axis number of points is same with nm range value.
    Each section is indicated by comments like "Part 1 Start/End"

    TODO
    ----
    Part 1 seems that it can be integrated with parse_ace.
    """
    f=inputfile

    """ Part 1 Start """
    # Find appropriate plot range.
    if limit is None:
        limit = (None, None)
    if limit[0] is None or limit[1] is None:
        min_eV = float("inf")
        max_eV = -float("inf")
        for a_line in f.readlines():
            line=a_line.split()
            if len(line)>=9 and line[0]=='Excited' and line[1]=='State':
                tmp = 25 * 0.5*sigma * 2.449489743# sqrt(log(1000000))

                min_eV_tmp = float(line[6]) - tmp
                max_eV_tmp = float(line[6]) + tmp

                if(min_eV_tmp < min_eV and limit[0] is None):
                    min_eV = min_eV_tmp
                if(max_eV_tmp > max_eV and limit[1] is None):
                    max_eV = max_eV_tmp
        if limit[0] is None:
            limit = (max(1,int(min_eV)), limit[1])
        if limit[1] is None:
            limit = (limit[0], int(max_eV))
        f.seek(0)
    """ Part 1 End """

    """ Part 2 Start """
    x_value_nm = []
    oscillator_strength = []
    lambda_range = range(limit[0],limit[1]+1)
    epsilon = np.zeros((len(lambda_range)))
    for a_line in f:
        line=a_line.split()
        if len(line)>=9 and line[0]=='Excited' and line[1]=='State':
            x_value_nm.append(float(line[6]))
            oscillator_strength.append(float(line[8][2:]))
            tmp = get_epsilon(float(line[6]),float(line[8][2:]),lambda_range, sigma)
            epsilon += get_epsilon(float(line[6]),float(line[8][2:]),lambda_range, sigma)
    f.close()
    """ Part 2 End """
    return lambda_range, epsilon, x_value_nm, oscillator_strength

def plot_oscillator(x_value_nm, epsilon, excit_e_nm, oscillator, label = None, filename = None, x_range = None):
    """Plot the UV spectrum

    Parameters
    ----------
    x_value_nm : list of floats
        x axis values.
    epsilon : list of floats
        UV spectrum values.
    excit_e_nm : list of floats or None
        List of exact excitation energy values.
        Should be supplied with oscillator.
        If not None then peak position will be displayed as a line.
    oscillator : list of floats or None
        List of oscillator strengths corresponds to excit_e_nm.
        Should be supplied with oscillator.
        If not None then peak position will be displayed as a line.
    label : str or None
        If str, plot title becomes "Molar extinction coefficient of "+label.
    filename : file object or None
        If None, draws to "output.png".
    x_range : list of floats or None
        Slices the plot.
    """
    if x_range == None:
        x_range = (min(x_value_nm), max(x_value_nm))
    if x_range[0] is None:
        x_range = (min(x_value_nm), x_range[1])
    if x_range[1] is None:
        x_range = (x_range[0], max(x_value_nm))

    new_x_value_nm = [x for x in x_value_nm if x >= x_range[0] and x <= x_range[1]]
    new_epsilon = epsilon[x_value_nm.index(new_x_value_nm[0]):x_value_nm.index(new_x_value_nm[-1])+1]
    new_excit_e_nm = []
    new_oscill = []

    ymax = max(new_epsilon)

    if excit_e_nm is not None:
        new_excit_e_nm = [x for x in excit_e_nm if x >= x_range[0] and x <= x_range[1]]
        new_oscill = oscillator[np.where(excit_e_nm == new_excit_e_nm[0])[0][0]:np.where(excit_e_nm == new_excit_e_nm[0])[0][0]+len(new_excit_e_nm)]
        max_oscill = max(new_oscill)
        new_oscill *= ymax / max_oscill
        new_oscill = list(map(lambda x: x if x >= ymax*oscillator_cutoff else -oscillator_cutoff*ymax, new_oscill))

    plt.figure(figsize=(10, 4.5))
    plt.plot(new_x_value_nm, new_epsilon)
    plt.ylabel("Molar extinction coefficient [L/(mol*cm)]")
    plt.xlabel("Wavelength (nm)")
    #x_range = (x_range[1], x_range[0])
    plt.xlim(x_range)
    if excit_e_nm is None:
        plt.ylim((0, ymax*1.05))
    else:
        plt.ylim((-1.05*oscillator_cutoff*ymax, ymax*1.05))
    if label is None:
        plt.title("Molar extinction coefficient")
    else:
        plt.title("Molar extinction coefficient of "+label)
    if excit_e_nm is not None:
        plt.bar(new_excit_e_nm, new_oscill, width=0.5, linewidth=0, color='k')
        """
        for x, v in zip(excit_e_nm, new_oscill):
            if v*max_oscill/ymax > 0.01:
                plt.text(x, v, str(v*max_oscill/ymax))
        """

    if filename is None:
        filename = open("output.png", 'w')
    print(filename)
    plt.savefig(filename)

def write_oscillator(x_value_nm, epsilon, excit_e_nm, oscillator, filename = None, x_range = None):
    """Plot the UV spectrum

    Parameters
    ----------
    x_value_nm : list of floats
        x axis values.
    epsilon : list of floats
        UV spectrum values.
    excit_e_nm : list of floats or None
        List of exact excitation energy values.
        Should be supplied with oscillator.
        If not None then peak position will be displayed as a line.
    oscillator : list of floats or None
        List of oscillator strengths corresponds to excit_e_nm.
        Should be supplied with oscillator.
        If not None then peak position will be displayed as a line.
    filename : file object or None
        If None, writes to "output.txt".
    x_range : list of floats or None
        Slices the plot.
    """
    if filename is None:
        filename = open("output.txt", 'w')
    if x_range == None:
        x_range = (min(x_value_nm), max(x_value_nm))
    if x_range[0] == None:
        x_range = (min(x_value_nm), x_range[1])
    if x_range[1] == None:
        x_range = (x_range[0], max(x_value_nm))

    new_x_value_nm = [x for x in x_value_nm if x >= x_range[0] and x <= x_range[1]]
    new_epsilon = epsilon[x_value_nm.index(new_x_value_nm[0]):x_value_nm.index(new_x_value_nm[-1])+1]

    new_excit_e_nm = []
    new_oscill = []
    if excit_e_nm is not None:
        new_excit_e_nm = [x for x in excit_e_nm if x >= x_range[0] and x <= x_range[1]]
        #new_oscill = oscillator[np.where(excit_e_nm == new_excit_e_nm[0])[0][0]:np.where(excit_e_nm == new_excit_e_nm[-1])[0][0]+1]
        new_oscill = oscillator[np.where(excit_e_nm == new_excit_e_nm[0])[0][0]:np.where(excit_e_nm == new_excit_e_nm[0])[0][0]+len(new_excit_e_nm)]
        spec_max = max(new_epsilon)
        max_oscill = max(new_oscill)
        new_oscill *= spec_max / max_oscill
        new_oscill = list(map(lambda x: x if x >= spec_max*oscillator_cutoff else -oscillator_cutoff*spec_max, new_oscill))

    if isinstance(filename, str):
        filename = open(filename, 'w')
    if excit_e_nm is None:
        filename.write("Wavelength (nm) \t Epsilon [L/(mol*cm)]\n")
    else:
        filename.write("Wavelength (nm) \t Epsilon [L/(mol*cm)] \t Wavelength (nm) \t Oscillator Strength\n")
    j = 0
    for i in range(len(new_x_value_nm)):
        if i < len(new_excit_e_nm):
            filename.write("\t".join([str(new_x_value_nm[i]), str(new_epsilon[i]), str(new_excit_e_nm[j]), str(new_oscill[j])])+"\n")
            j += 1
        else:
            filename.write("\t".join([str(new_x_value_nm[i]), str(new_epsilon[i])])+"\n")
    if j < len(new_excit_e_nm):
        for i in range(j, len(new_excit_e_nm)):
            filename.write("\t".join(["?", "?", str(new_excit_e_nm[i]), str(new_oscill[i])])+"\n")

if __name__ == "__main__":
    if IS_EDISON:
        parser1 = argparse.ArgumentParser()
        parser1.add_argument("-i", type=argparse.FileType('r'))
        parser1.add_argument("-o", type=argparse.FileType('r'))
        args1 = parser1.parse_args()

        if not os.path.exists("result"):
            os.makedirs("result")
        fname = "result/output.png"
        out_name = fname
        inp_name = args1.i
        xmin = xmax = "-1"
        for line in args1.o.readlines():
            if "broad" in line:
                args1.broad = line.split()[-1]
            elif "min_nm" in line:
                xmin = line.split()[-1]
            elif "max_nm" in line:
                xmax = line.split()[-1]

        if "broad" in args1:
            new_inp = ["--ace", "--broad", str(args1.broad), "--range", xmin, xmax, inp_name.name, out_name]
        else:
            new_inp = ["--ace", "--range", xmin, xmax, inp_name, out_name]

    parser = argparse.ArgumentParser(description = "Plot UV spectrum from ACE-Molecule or Gaussian output.", formatter_class=argparse.RawTextHelpFormatter)
    runtype = parser.add_mutually_exclusive_group()
    runtype.add_argument("--ace", dest="runtype", action="store_const", const="ace", default="ace", help="Plot from ACE-Molecule output. Required. Mutually exclusive with --gau.")
    runtype.add_argument("--gau", dest="runtype", action="store_const", const="gau", default="ace",help="Plot from Gaussian output. Required. Mutually exclusive with --ace.")
    parser.add_argument("inputname", type=argparse.FileType('r'), help="Input filename.")
    parser.add_argument("outputname", type=str, help="Output filename.")
    #parser.add_argument("--range", metavar=("xmin","xmax"), nargs=2, type=int, default=[200, 450], help="Plot range in nm. Default: 200nm to 450nm")
    parser.add_argument("--range", metavar=("xmin","xmax"), nargs=2, type=int, default=None, help="Plot range in nm. Negative values for automatic behavior. Default: Auto")
    parser.add_argument("--broad", metavar="broadning", type=float, default=0.8, help="Gaussian broadning of peaks. Default = 0.8")
    parser.add_argument("--txt", dest="is_txt", action="store_true", help="Prints output as text instead of directly drawing png file.")
    parser.add_argument("--bar", dest="is_bar", action="store_true", help="Add or print exact excitation position. Zero oscillator strength will be displayed as negative value.")

    args = parser.parse_args(new_inp) if IS_EDISON else parser.parse_args()

    #print args
    wavelength = []
    epsilon = []

    if not args.range is None:
        if args.range[0] < 0:
            args.range = (None, args.range[1])
        if args.range[1] < 0:
            args.range = (args.range[0], None)

    if args.runtype == "ace":
        wavelength, epsilon, excit_nm, oscil_strength = parse_ace(args.inputname, args.broad, args.range)
    elif args.runtype == "gau":
        wavelength, epsilon, excit_nm, oscil_strength = parse_gaussian(args.inputname, args.broad, args.range)
    else:
        print ("Broken: This program does not support outputs other than ACE-Molecule or Gaussian")
    #print wavelength
    #print oscillator_strength
    excit_e = None
    oscillator = None
    if args.is_bar:
        excit_e = np.array(excit_nm, dtype=float)
        oscillator = np.array(oscil_strength, dtype=float)
    if args.is_txt:
        write_oscillator(wavelength, epsilon, excit_e, oscillator, filename = args.outputname, x_range = args.range )
    else:
        plot_oscillator(wavelength, epsilon, excit_e, oscillator, filename = args.outputname, x_range = args.range )
