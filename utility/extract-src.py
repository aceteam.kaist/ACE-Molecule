#! -*- coding: utf-8 -*-
from subprocess import Popen
from glob import glob
import argparse

CMAKE_FILES = ["FileLists.cmake", "Find_Trilinos.cmake", "Version.cmake", "cmake_install.cmake"]
PY_FILES = ["clean.py", "install_sample.py", "install.py"]

FILES = {
    "minimum": ["source", "utility", "CMakeLists.txt"] + CMAKE_FILES + PY_FILES,
}
FILES["default"] = FILES["minimum"] + ["test", "vars", "Doxyfile"]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Create zip file for compilation.")
    parser.add_argument("--output", "-o", dest = "output", default = "ACE.src.tar.gz")
    parser.add_argument("--type", "-t", dest = "type", default = "default", choices = ["default", "minimum"])
    parser.add_argument("--ace-home", "-H", dest = "home", default = "../")
    args = parser.parse_args()

    Popen(["tar", "-czf", args.output]+FILES[args.type], cwd = args.home)
