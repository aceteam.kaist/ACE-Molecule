#/bin/env python

from collections import OrderedDict
from copy import deepcopy
from re import sub
import itertools

#node_procs = initialize_node_procs()

#"""
def parse_template(fpt, preserve_comment = False, sublist_name = "root"):
    param = OrderedDict()

    for line2 in fpt:
        line = line2.strip()
        line = line.replace('%%', '%% ')
        line = sub(r"(#+)", r"\1 ", line)
        if not preserve_comment:
            line = line.split('#')[0].strip()
        if len(line)==0:
            continue
        L_line = line.split()

        if len(L_line) < 2:
            if not L_line[0] in param:
                param[L_line[0]] = ""
        else:
            if L_line[0] == "%%":
                if L_line[1] == "End":
                    return param
                else:
                    param[L_line[1]] = parse_template(fpt, preserve_comment, L_line[1])
            elif L_line[0] in param:
                if not type(param[L_line[0]]) is list:
                    param[L_line[0]] = [param[L_line[0]]]
                param[L_line[0]].append(" ".join(L_line[1:]))
            else:
                param[L_line[0]] = " ".join(L_line[1:])
    if not "root" == sublist_name:
        print(param)
        raise ValueError("Not matching ending block in "+sublist_name)
    return param

def write_input(fpt, param, indent = 0):
    prefix = "\t"*indent
    for key, val in param.items():
        if type(val) is OrderedDict:
            fpt.write(prefix+"%% "+str(key)+"\n")
            write_input(fpt, param[key], indent+1)
            fpt.write(prefix+"%% End\n")
        elif type(val) is list:
            for item in val:
                if '#' in key:
                    fpt.write(prefix+str(key)+" "+str(item)+"\n")
                else:
                    fpt.write(prefix+str(key)+"\t\t"+str(item)+"\n")
        else:
            if '#' in key:
                fpt.write(prefix+str(key)+" "+str(val)+"\n")
            else:
                fpt.write(prefix+str(key)+"\t\t"+str(val)+"\n")
    #fpt.write(prefix+"%% End\n\n")
    return
            
def append_args(old_param, new_args):
    param = deepcopy(old_param)
    for key, val in new_args.items():
        key_list = key.split('.')
        param2 = param
        for key2 in key_list[:-1]:
            param2 = param2.setdefault(key2, OrderedDict())
        if not val is None:
            if type(val) is list:
                param2[key_list[-1]] = val
            else:
                param2[key_list[-1]] = str(val)
    return param

def dict_combination(orig):
    new = []
    val_list = [item for item in itertools.product(*orig.values())]
    for new_vals in val_list:
        new.append(dict(zip(orig.keys(), new_vals)))
    return new

def create_inputs(template, new_args):
    template = OrderedDict()
    inputs = OrderedDict()
    with open("ACE.template", 'r') as fpt:
        template = parse_template(fpt, True)
    for args_nick, new_arg in new_args.items():
        param = append_args(template, new_arg)
        name = "ACE-"+args_nick
        inputs[name] = name+".inp"
        with open(inputs[name], 'w') as fpt:
            write_input(fpt, param)
    return inputs

#"""
"""
def get_input(fpt):
    inp = ""
    for line in fpt:
        match = re.search(\r"\\f[*\\f]", line)
        if match:
            line.replace(line[match.start:match.end], fuck) 
        inp += line
        inp += '\n'
"""

if __name__ == "__main__":
    param = ""
    with open("ACE.template", 'r') as fpt:
        param = parse_template(fpt, True)
    append_args(param, {"TDDFT.Exchange_Correlation.TestVal": 123321, "Basic_Information.Pseudopotential.TestVal2": 321123})
    with open("test.out", 'w') as fpt:
        write_input(fpt, param)
