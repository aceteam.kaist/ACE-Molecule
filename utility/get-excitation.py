#! /bin/env python
import argparse

HA_TO_EV = 27.211396132

def read_excitations(fp, spin, is_new = True):
    line = fp.readline()

    excit_no = []
    excitation_energy_eV = []
    oscillator_strength = []
    ct_character = [[], [], []]
    contribution_tmp = []
    multiplicity = 1

    while line:
        if (line.find("Triplet excitation strength") >= 0):
            multiplicity = 3
        if spin == multiplicity:
            if (line.find("Excitation energy") >= 0):
                tmp = line.strip().split()
                excitation_energy_eV.append(float(tmp[7]))
                excit_no.append(int(tmp[0][1:-1]))
            elif (line.find("Oscillator strength") >= 0):
                tmp = line.strip().split()
                oscillator_strength.append(float(tmp[3]))
            elif (line.find("Orbital overlap") >= 0):
                tmp = float(line.strip().split()[-1])
                ct_character[0].append(tmp)
            elif (line.find("r-index") >= 0):
                tmp = float(line.strip().split()[-1])
                ct_character[1].append(tmp)
            elif (line.find("Gammaa-index") >= 0):
                tmp = float(line.strip().split()[-1])
                ct_character[2].append(tmp)
#           elif (line.find("Eigenvalue contribution") >= 0):
#                tmp = float(line.strip().split()[-1])*HA_TO_EV
#                eps_diff_eV.append(tmp)
            elif line.find(" to  ") >= 0:
                tmp = line.strip().split()
                if is_new:
                    contribution_tmp.append({int(tmp[0][:-1]): float(tmp[1])})
                else:
                    contribution_tmp.append({int(tmp[0][:-1]): float(tmp[1])**2})
        line = fp.readline()

    if not is_new:
        orbital_overlap = [None]*len(excitation_energy_eV)
    contribution = []
    num_contrib = len(contribution_tmp)/len(excitation_energy_eV)
    for i in range(len(excitation_energy_eV)):
        contribution.append(contribution_tmp[i*num_contrib:(i+1)*num_contrib])
    
    retval = {"#": excit_no, "E": excitation_energy_eV, "Oscill": oscillator_strength, "CT": ct_character, "Contrib": contribution}
    return retval


def from_number(info, number):
    n = number-1
    retvals = [{
            "#": info["#"][n],
            "E": info["E"][n], 
            "Oscill": info["Oscill"][n], 
            "CT": list(map(list, map(None, *info["CT"])))[n], 
            "Contrib": info["Contrib"][n]
    }]
    return retvals

def sort_oscill(info, num_max):
    ilist = reversed(sorted(range(len(info["Oscill"])), key=lambda x:info["Oscill"][x])[-num_max:])
    #retvals = {"#": [], "E": [], "Oscill": [], "Overlap": [], "Contrib": []}
    retvals = []
    for i in ilist:
        """
        retvals["#"].append(info["#"][i])
        retvals["E"].append(info["E"][i])
        retvals["Oscill"].append(info["Oscill"][i])
        retvals["Overlap"].append(info["Overlap"][i])
        retvals["Contrib"].append(info["Contrib"][i])
        """
        retvals.append(from_number(info, i+1)[0])
    return retvals

def from_orb(info, number):
    contrib_list = [x[0].keys()[0] for x in info["Contrib"]]
    ilist = [i for i, x in enumerate(contrib_list) if x == number]
    #retvals = {"#": [], "E": [], "Oscill": [], "Overlap": [], "Contrib": []}
    retvals = []
    for i in ilist:
        """
        retvals["#"].append(info["#"][i])
        retvals["E"].append(info["E"][i])
        retvals["Oscill"].append(info["Oscill"][i])
        retvals["Overlap"].append(info["Overlap"][i])
        retvals["Contrib"].append(info["Contrib"][i])
        """
        retvals.append(from_number(info, i+1)[0])
    return retvals

def print_result(val, max_len):
    for i in range(max_len):
        print "Excitation Number:", val[i]["#"]
        print "Excitation Energy:", val[i]["E"]
        print "Oscillator Strength:", val[i]["Oscill"]
        print "Orbital Overlap:", val[i]["CT"][0]
        print "r-index:", val[i]["CT"][1]
        print "Gamma-index:", val[i]["CT"][2]
        print "Contribution:", val[i]["Contrib"]

def print_table(val, max_len):
    for i in range(max_len):
        print "Excit_#      ", val[i]["#"]
        print "Excit_E      ", val[i]["E"]
        print "Oscillator   ", val[i]["Oscill"]
        print "Overlap      ", val[i]["CT"][0]
        print "r-index      ", val[i]["CT"][1]
        print "Gamma-index ", val[i]["CT"][2]
        for contrib in val[i]["Contrib"]:
            print "Contrib      ", contrib

def main():
    parser = argparse.ArgumentParser()

    runtype = parser.add_mutually_exclusive_group(required=True)
    runtype.add_argument("-N", type = int, help="Parse from excitation number.")
    runtype.add_argument("-O", type = int, help="Parse from oscillator strength order.")
    runtype.add_argument("-C", type = int, help="Parse from largest contribution.")

    parser.add_argument("--table", action="store_true", help="Tabular form")
    parser.add_argument("log", type = argparse.FileType('r'))
    args = parser.parse_args()

    info = read_excitations(args.log, 1)
    max_len = 1
    if not args.N is None:
        val = from_number(info, args.N)
    if not args.O is None:
        val = sort_oscill(info, args.O)
        max_len = args.O
    if not args.C is None:
        val = from_orb(info, args.C)
        max_len = len(val["#"])
    if args.table:
        print_table(val, max_len)
    else:
        print_result(val, max_len)

if __name__ == "__main__":
    main()
