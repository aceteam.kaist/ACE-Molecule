from setuptools import setup, Extension, find_packages
from distutils.command.build import build
from distutils.command.install import install
import os

file_list = [
        "PyACE/Util/Util.i",
        "../source/Util/Verbose.cpp",
        "../source/Util/Parallel_Manager.cpp",
        "../source/Util/String_Util.cpp",
]

header_dirs = [
        "/appl/trilinos/libs/12.12.1_openmp_release_intel_openmpi-1.8.3/include",
#        "/PATH/TO/TRILINOS/include",
]

os.environ["CC"] = "mpicc"
#os.environ["CC"] = "mpiicc"

class build_ext_first(build):

    def __init__(self, *args):
        super().__init__(*args)
        self.sub_commands = [('build_ext', build.has_ext_modules),
                             ('build_py', build.has_pure_modules)]


class install_ext_first(install):

    def run(self):
        self.run_command("build_ext")
        return install.run(self)


extension_mod = Extension("PyACE.Util._Util", sources = file_list, 
        include_dirs = header_dirs,
        language = "C++",
        extra_compile_args = ["-std=c++11"],
        swig_opts = ["-c++", "-py3"],
        )

setup(
        name = "PyACE", 
        ext_modules = [extension_mod], 
        py_modules = [
            "PyACE.Util.Util"
        ],
        cmdclass = {'build': build_ext_first, 'install': install_ext_first}, 
     )
