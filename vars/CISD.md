## CISD

This section controls parameters related to CISD

(J. Chem. Phys. 2016, 145, 224309. Jaechang Lim, Sunghwan Choi, Jaewook Kim, and Woo Youn Kim*)

#### **Subsection List**
  +  Diagonalize 

### **NumberOfEigenvalues**
> #### **Description**
> Number of eigenvalues which will be obtained from diagonalization of CISD matrix. 
> #### **Type**
> int
> #### **Possible Options**
> + key: 10 # if you want to calculate the 10 lowest states.

### **ConvergenceTolerance**
> #### **Description**
> Convergence criteria of Davidson digonalization. Relevant only if Davidson diagonalizer is used.<br>
> Defaults to 1.0E-6.
> #### **Type**
> float

### **Method**
> #### **Possible Options**
> + 0: Use Davidson diagonalization.
> + 1: Make full matrix and diagonalize it.
