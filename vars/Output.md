## **Output**

 This section controls output density, potential, orbitals and geometry.

### **Prefix**
> #### **Description**
> Decides the prefix of output density/potential/orbitals. \ 
> Output file name is: \
 [Prefix][Compute Interface].[density/potential/orbitals].s[Spin].ind[index].[txt/cube]
> #### **Type**
> string 

### **Density**
> ### **Description**
> Decides the format for the final density for corresponding Compute Interface
> #### **Type**
> string
> #### **Possible Options**
> + None: outputs for density are not generated. [default]
> + x: Print density value along x axis to file
> + y: Print density value along y axis to file
> + z: Print density value along z axis to file
> + cube: Print density as cube file

### **AllElectronDensity**
> ### **Description**
>  If density is not printed, this parameter does not affect output.
>  Decides that whether all-electron density will be printed, if available
>  This option is currently available only for the PAW pseudopotential method.
> #### **Type**
> int
> #### **Possible Options**
> + 0: Pseudo electron density will be printed. [default]
> + Otherwise: All electron density will be printed

### **Hartree**
> #### **Description**
> Decides the format for the final hartree potential output for corresponding Compute Interface
> #### **Type**
> string 
> #### **Possible Options**
> + None: outputs for Hartree potential are not generated. [default]
> + x: Print Hartree potential value along x axis to file
> + y: Print Hartree potential value along y axis to file
> + z: Print Hartree potential value along z axis to file
> + cube: Print local potential as cube file

### **Potential**
> #### **Description**
> Decides the format for the final mean field potential caused by electrons.
> #### **Type**
> string 
> #### **Possible Options**
> + None: outputs for potential are not generated. [default]
> + x: Print local potential value along x axis to file
> + y: Print local potential value along y axis to file
> + z: Print local potential value along z axis to file
> + cube: Print local potential as cube file

### **Orbitals**
>#### **Description**
> Decides the format for the final orbitals output for corresponding Compute Interface. 
>#### **Type**
> string
> #### **Possible Options**
> + None: outputs for orbital are not generated. [default]
> + x: Print orbital along x axis to file.
> + y: Print orbital along y axis to file.
> + z: Print orbital along z axis to file.
> + cube: Print orbital as cube file.

### **OrbitalStartIndex**
>#### **Description**
> Decides the lower boundary of range of orbitals to be printed. \
> Default value: 0 (Start from lowest orbital).
> #### **Type**
> int

### **OrbitalEndIndex**
> #### **Description**
> Decides the upper boundary of range of orbitals to be printed. \
> Default value: [Number of Orbitals]-1 (Print up to highest orbital calculated).
> #### **Type**
> int

### **Geometry**
> #### **Description**
> This parameter decide the name of geometry file. The format for output is fixed as "xyz" and full name of output file will be [Geometry].xyz
> #### **Type**
> string


