## **SolvationModel**

Adds solvation model in SCF routine. Will be implemented to TDDFT routines, probably.

### **SolvationLibrary**
> #### **Description**
> Controls the solvation model library/routine to be used. 

> #### **Type**
> string
> #### **Possible Options**
> + PCMSolver: Currently only option. Requires compilation with PCMSolver. [default]

### **SolverType**
> #### **Description**
> Controls PCM theory.
> #### **Type**
> string
> #### **Possible Options**
> + CPCM: typical  conductor-like polarizable continuum model  
> + IEFPCM: Polarizable Continuum Model with the integral equation formalism 
> + None: Turns off the PCM routine. Default value.

### **ChargeWidth**
> #### **Description**
> Controls gaussian broadning of surface charge. See https://doi.org/10.1063/1.4932593 for further details. \
 This corresponds to alpha in equation A1 of aformentioned reference.
> #### **Type**
> float

### **Solvent**
> #### **Description**
> Defines the solvent. Currently explicit solvent definition is not supported.
 See http://pcmsolver.readthedocs.io/en/stable/users/input.html#available-solvents
> #### **Type**
> string

### **RadiiSet**
> #### **Description**
> See http://pcmsolver.readthedocs.io/en/stable/users/input.html
> ### **Type**
> string
> #### **Possible Options**
> + Bondi [Default]
> + UFF
> + Allinger

### **Area**
> #### **Description**
> See http://pcmsolver.readthedocs.io/en/stable/users/input.html<br>
 Default value is 0.3.
> #### **Type**
> float


### **Scaling**
>#### **Description**
> Determine whether radii is scaled or not. See http://pcmsolver.readthedocs.io/en/stable/users/input.html for details
>  #### **Possible Options**
> + 1: scales all radius by 1.2
> + 0: radii are not scaled [Default]

### **MinRadius**
>#### **Description**
> See http://pcmsolver.readthedocs.io/en/stable/users/input.html \
> Default value is 100, which is equivalent to turning this off.
> #### **Type**
> float


### **Correction**
> #### **Description**
> See http://pcmsolver.readthedocs.io/en/stable/users/input.html
> #### **Type**
> string

### **ProbeRadius**
> #### **Description**
> See http://pcmsolver.readthedocs.io/en/stable/users/input.html \
> Default value is 1.0.
> #### **Type**
> float

