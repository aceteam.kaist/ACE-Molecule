## **ExternalField**

 Adds an additional field during the SCF run.

### **InputType**
> #### **Description**
> Specifies format. Required.
> #### **Type**
> string
> #### **Possible Options**
> + Analytic: Apply potential in analytic form (Ex, Ey, or Ez where E is constant).
> + Read: Specifies potential by numerical grid.

### **ExternalField**
> #### **Description**
> Relevant only if InputType is Analytic. Only one default option is currently available.
> Type string
> + Electric: Only and default option.

### **ExternalFieldType**
> #### **Description** 
> Relevant only if InputType is Analytic and ExternalField is Electric. Only one default option is currently available.
> #### **Type**
> string
> #### **Possible Option**
> + Static: Only and default option.

### **ExternalFieldDirection**
> #### **Description**
> Relevant only if InputType is Analytic and ExternalField is Electric.\
> Specifies the direction of electric field. \
> Potential will be Ex, Ey or Ez where E is ExternalFieldStrength.
> #### **Possible Option** 
> + x: x direction
> + y: y direction
> + z: z direction

### **ExternalFieldStrength**
> #### **Description**
> Relevant only if InputType is Analytic and ExternalField is Electric. \
> Specifies the strength of electric field. \
> Potential will be Ed where d=x,y,z is ExternalFieldDirefction.
> #### **Type**
> float

### **PotentialFilename**
> #### **Description**
> Relevant only if InputType is Read. \
> Specifies external potential on parallelpiped numerical grid. \
> Potentials are whitespace or newline-separated, and outer loop is z-index, middle loop is y-index, and inner loop is x-index.\
> Devel Note: This behavior is currently index order dependent.
> #### **Type**
> string

### **Variable Interpolation**
> #### **Description** 
> Potential interpolation scheme.
> #### **Type** 
> string
> #### **Possible Options**
> + linear: Use trilinear interpolation.
> + cubic: Use tricubic interpolation. Default value.

### **PointX**
> #### **Description**
> Number of grid points in x-direction.
> #### **Type**
> (positive) int 

### **PointY**
> #### **Description**
> Number of grid points in y-direction.
> #### **Type**
> (positive) int

### **PointZ**
> #### **Description**
> Number of grid points in z-direction.
> #### **Type**
> (positive) int


### **PotentialOffSetX**
> #### **Description**
> Position of potential file center (x) on calculation mesh. Default value: 0.0.
> #### **Type**
> float

### **PotentialOffSetY**
> #### **Description**
> Position of potential file center (y) on calculation mesh. Default value: 0.0.
> #### **Type**
> float

### **PotentialOffSetZ**
> #### **Description**
> Position of potential file center (z) on calculation mesh. Default value: 0.0.
> #### **Type**
> float


### **LatticeVecX**
> #### **Description**
> Specifies x-direction vector of the box containing the potential.
> #### **Type**
> string (ex. "1.0 0.0 0.0")

### **LatticeVecY**
> #### **Description**
> Specifies y-direction vector of the box containing the potential.
> #### **Type**
> string (ex. "0.0 1.0 0.0")

### **LatticeVecZ**
> #### **Description**
> Specifies z-direction vector of the box containing the potential.
> #### **Type**
> string (ex. "0.0 0.0 1.0")


