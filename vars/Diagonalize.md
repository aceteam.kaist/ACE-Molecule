## **Diagonalize**

This section contains parameters related to the diagonalization for sparse matrix.

### **Tolerance**
> #### **Description**
>  Specify diagonalization tolerance. \
> Relative eigenvalue tolerance of solver. \
> If norm2(residual)/eigenvalue is lower than this variable for all eigenstates, diagonalization is considered converged.
> #### **Type**
> string

### **Solver**
> #### **Description**
> Specify Diagonalization Solver.
> #### **Type**
> string
> #### **Possible Options**
> + Pure: Use Anasazi [Default]
> + Direct: Use LAPACK default diagonalization scheme.

### **EigenSolver**
> #### **Description**
> Specify the method to diagonalize symmetric matrix
> #### **Type**
> string
> #### **Possible Options**
> + LOBPCG: Use LOBPCG (Locally Optimal Block Preconditioned Conjugated Gradient) method [Default]
> + BlockDavidson: Use BlockDavision method. It required to compile the program with USE_EX_DIAG set to true.
> + BlockKrylovSchur: Use BlockKrylovSchur method. It required to compile the program with USE_EX_DIAG set to true.

### **RandomizeInitial**
> #### **Description**
> This parameter determine how to generate initial vector for DeflationLanczos method
> #### **Type**
> int
> #### **Possible Options**
> + 0: initial vector is not randomized.
> + Otherwise: Randomly initialize initial vector [default]

### **DiagonalizeVerbosity**
> #### **Description**
> Specify verbosity of diagonalization
> It only works with Solver = Pure
> Simple, Normal, Debug
> #### **Type**
> string
> #### **Possible Options**
> + Simple
> + Normal
> + Debug

### **Locking**
> #### **Descirption**
> This parameters determine whether Locking is employed or not. It only works with *Solver* = Pure
> #### **Type**
> int
> #### **Possible Options**
> + 0: Do not use Locking.
> + Otherwise: Use Locking.

### **LockingTolerance**
> #### **Description**
> Set Locking Tolerance. \
> It only works with Solver = Pure.
> #### **Type**
> float
Description
END

### **MaxLocked**
> #### **Description**
>Set the maximum number of locking vector
> BlockSize + MaxLocked is greater than or equal to number of eigenvalues that you want to solve
> It only works with Solver = Pure and Locking != 0
> #### **Type**
> int

### **DiagonalizeSolverPrec**
> #### **Description**
> Specify types of preconditioner for diagonalization. It only works with Solver = Pure and Locking != 0
> #### **Type**
> string
> #### **Possible Options**
> + 'IC stand-alone': [experimental]
> + 'ILU stand-alone': [experimental]
> + 'ILUT stand-alone': [experimental]
> + 'Amesos stand-alone': [experimental]
> + 'ICT stand-alone': [experimental]
> +  ml: multi-level preconditioner please refer ml homepage for details
> +  None: no preconditioner is used



### **Redistribution**
> #### **Description**
> Determine whether redistribution is used or not
> #### **Type**
> int
> #### **Possible Options**
> + 0: not used [Default]
> + Otherwise:   used


### **BalancingTolerance**
> #### **Description**
> Determine how much imbalance is allowed. More information can be found in Isorropia homepage.
>#### **Type**
>float

### **PartitioningMethod**
> #### **Description**
> Criteria to determine whether eigenvectors from two different MPIGroup are the same or not. This parameter works only when Deflation Lanczos is used.
> #### **Type**
> string
> #### **Possible Options**
> + HYPERGRAPH [Default]


